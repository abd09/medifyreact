import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import 'bootstrap/dist/css/bootstrap.min.css';
import 'react-notifications/lib/notifications.css';
import 'font-awesome/css/font-awesome.min.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import './index.css';

import { Router, Route, Switch } from 'react-router-dom';
import { createHashHistory as createHistory } from "history";
import { configureStore } from './store';
const history = createHistory()

ReactDOM.render(
  <Provider store={configureStore()}>
    <Router history={history}>
      <Switch>
        <Route path="/" component={App} />
      </Switch>
    </Router>
  </Provider>
  ,
  document.getElementById('root')
);

serviceWorker.unregister();
