import {
    GET_INVENTORIES,
    GET_INVENTORIES_SUCCESS,
    GET_INVENTORIES_FAILURE,
    SEARCH_INVENTORIES,
    SEARCH_INVENTORIES_SUCCESS,
    SEARCH_INVENTORIES_FAILURE,
    GET_PHARMACY_INVENTORY,
    GET_PHARMACY_INVENTORY_SUCCESS,
    GET_PHARMACY_INVENTORY_FAILURE,
    FILTER_INVENTORY,
    FILTER_INVENTORY_SUCCESS,
    FILTER_INVENTORY_FAILURE
} from './types'
const LOAD = 'redux-form-examples/account/LOAD'


export const getInventoriesData = (data) => ({
    type: GET_INVENTORIES,
    payload: data
})

export const getInventoriesDataSuccess = (data) => ({
    type: GET_INVENTORIES_SUCCESS,
    payload: data
})

export const getInventoriesDataFailure = (error) => ({
    type: GET_INVENTORIES_FAILURE,
    payload: error
})

export const searchInventories = (data) => ({
    type: SEARCH_INVENTORIES,
    payload: data
})

export const searchInventoriesSuccess = (data) => ({
    type: SEARCH_INVENTORIES_SUCCESS,
    payload: data
})

export const searchInventoriesFailure = (error) => ({
    type: SEARCH_INVENTORIES_FAILURE,
    payload: error
})

export const filterInventory = (data) => ({
    type: FILTER_INVENTORY,
    payload: data
})

export const filterInventorySuccess = (data) => ({
    type: FILTER_INVENTORY_SUCCESS,
    payload: data
})

export const filterInventoryFailure = (error) => ({
    type: FILTER_INVENTORY_FAILURE,
    payload: error
})

export const getPharmacyInventoryData = (data) => ({
    type: GET_PHARMACY_INVENTORY,
    payload: data
})

export const getPharmacyInventoryDataSuccess = (data) => ({
    type: GET_PHARMACY_INVENTORY_SUCCESS,
    payload: data
})

export const getPharmacyInventoryDataFailure = (data) => ({
    type: GET_PHARMACY_INVENTORY_FAILURE,
    payload: data
})