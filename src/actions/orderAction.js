import {
    ADD_ORDER,
    ADD_ORDER_SUCCESS,
    ADD_ORDER_FAILURE,
    GET_ORDERS,
    GET_ORDERS_SUCCESS,
    GET_ORDERS_FAILURE,
    GET_PHARMACY_ORDERS,
    GET_PHARMACY_ORDERS_SUCCESS,
    GET_PHARMACY_ORDERS_FAILURE,
    UPDATE_ORDER,
    UPDATE_ORDER_SUCCESS,
    UPDATE_ORDER_FAILURE,
} from './types';

const LOAD = 'redux-form-examples/account/LOAD'

export const getOrders = (data) => ({
    type: GET_ORDERS,
    payload: data
})

export const getOrdersSuccess = (data) => ({
    type: GET_ORDERS_SUCCESS,
    payload: data
})

export const getOrdersFailure = (error) => ({
    type: GET_ORDERS_FAILURE,
    payload: error
})

export const addOrder = (data) => ({
    type: ADD_ORDER,
    payload: data
});

export const addOrderSuccess = (data) => ({
    type: ADD_ORDER_SUCCESS,
    payload: data
})

export const addOrderFailure = (error) => ({
    type: ADD_ORDER_FAILURE,
    payload: error
})

export const updateOrder = (data) => ({
    type: UPDATE_ORDER,
    payload: data
});

export const updateOrderSuccess = (data) => ({
    type: UPDATE_ORDER_SUCCESS,
    payload: data
})

export const updateOrderFailure = (error) => ({
    type: UPDATE_ORDER_FAILURE,
    payload: error
})

export const getPharmacyOrders = (data) => ({
    type: GET_PHARMACY_ORDERS,
    payload: data
})

export const getPharmacyOrdersSuccess = (data) => ({
    type: GET_PHARMACY_ORDERS_SUCCESS,
    payload: data
})

export const getPharmacyOrdersFailure = (error) => ({
    type: GET_PHARMACY_ORDERS_FAILURE,
    payload: error
})


export const load = data => ({ type: LOAD, data })