import {
    GET_SCHEMES,
    GET_SCHEMES_FAILURE,
    GET_SCHEMES_SUCCESS,
    ADD_SCHEME,
    ADD_SCHEME_SUCCESS,
    ADD_SCHEME_FAILURE
} from './types'
const LOAD = 'redux-form-examples/account/LOAD'

export const getSchemes = (data) => ({
    type: GET_SCHEMES,
    payload: data
});

export const getSchemesSuccess = (data) => ({
    type: GET_SCHEMES_SUCCESS,
    payload: data
})

export const getSchemesFailure = (error) => ({
    type: GET_SCHEMES_FAILURE,
    payload: error
})

export const addScheme = (data) => ({
    type: ADD_SCHEME,
    payload: data
});

export const addSchemeSuccess = (data) => ({
    type: ADD_SCHEME_SUCCESS,
    payload: data
})

export const addSchemeFailure = (error) => ({
    type: ADD_SCHEME_FAILURE,
    payload: error
})

export const load = data => ({ type: LOAD, data })