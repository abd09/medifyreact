import {
  GET_PATIENT_DOCTORS,
  GET_PATIENT_DOCTORS_FAILURE,
  GET_PATIENT_DOCTORS_SUCCESS,
  CONNECTED_DOCTORS,
  CONNECTED_DOCTORS_SUCCESS,
  CONNECTED_DOCTORS_FAILURE,
  GET_ALL_DOCTORS,
  GET_ALL_DOCTORS_SUCCESS,
  GET_ALL_DOCTORS_FAILURE,
  SEARCH_DOCTORS,
  SEARCH_DOCTORS_SUCCESS,
  SEARCH_DOCTORS_FAILURE,
  GET_DOCTOR_PATIENTS,
  GET_DOCTOR_PATIENTS_SUCCESS,
  GET_DOCTOR_PATIENTS_FAILURE,
  SEARCH_DOCTOR_PATIENTS,
  SEARCH_DOCTOR_PATIENTS_SUCCESS,
  SEARCH_DOCTOR_PATIENTS_FAILURE,
  FILTER_DOCTOR,
  FILTER_DOCTOR_SUCCESS,
  FILTER_DOCTOR_FAILURE,
  GET_DOCTOR_DETAIL,
  GET_DOCTOR_DETAIL_SUCCESS,
  GET_DOCTOR_DETAIL_FAILURE
} from './types'
const LOAD = 'redux-form-examples/account/LOAD'

export const getAllDoctorsData = (data) => ({
  type: GET_ALL_DOCTORS,
  payload: data
})

export const getAllDoctorsDataSuccess = (data) => ({
  type: GET_ALL_DOCTORS_SUCCESS,
  payload: data
})

export const getAllDoctorsDataFailure = (error) => ({
  type: GET_ALL_DOCTORS_FAILURE,
  payload: error
})

export const getDoctorDetail = (data) => ({
  type: GET_DOCTOR_DETAIL,
  payload: data
})

export const getDoctorDetailSuccess = (data) => ({
  type: GET_DOCTOR_DETAIL_SUCCESS,
  payload: data
})

export const getDoctorDetailFailure = (error) => ({
  type: GET_DOCTOR_DETAIL_FAILURE,
  payload: error
})

export const getPatientDoctorsData = (data) => ({
  type: GET_PATIENT_DOCTORS,
  payload: data
})

export const getPatientDoctorsDataSuccess = (data) => ({
  type: GET_PATIENT_DOCTORS_SUCCESS,
  payload: data
})

export const getPatientDoctorsDataFailure = (error) => ({
  type: GET_PATIENT_DOCTORS_FAILURE,
  payload: error
})

export const getDoctorPatientsData = (data) => ({
  type: GET_DOCTOR_PATIENTS,
  payload: data
})

export const getDoctorPatientsDataSuccess = (data) => ({
  type: GET_DOCTOR_PATIENTS_SUCCESS,
  payload: data
})

export const getDoctorPatientsDataFailure = (error) => ({
  type: GET_DOCTOR_PATIENTS_FAILURE,
  payload: error
})

export const connectedDoctorsData = (data) => ({
  type: CONNECTED_DOCTORS,
  payload: data
})

export const connectedDoctorsDataSuccess = (data) => ({
  type: CONNECTED_DOCTORS_SUCCESS,
  payload: data
})

export const connectedDoctorsDataFailure = (error) => ({
  type: CONNECTED_DOCTORS_FAILURE,
  payload: error
})

export const searchDoctors = (data) => ({
  type: SEARCH_DOCTORS,
  payload: data
})

export const searchDoctorsSuccess = (data) => ({
  type: SEARCH_DOCTORS_SUCCESS,
  payload: data
})

export const searchDoctorsFailure = (error) => ({
  type: SEARCH_DOCTORS_FAILURE,
  payload: error
})

export const searchDoctorPatients = (data) => ({
  type: SEARCH_DOCTOR_PATIENTS,
  payload: data
})

export const searchDoctorPatientsSuccess = (data) => ({
  type: SEARCH_DOCTOR_PATIENTS_SUCCESS,
  payload: data
})

export const searchDoctorPatientsFailure = (error) => ({
  type: SEARCH_DOCTOR_PATIENTS_FAILURE,
  payload: error
})

export const filterDoctors = (data) => ({
  type: FILTER_DOCTOR,
  payload: data
});

export const filterDoctorsSuccess = (data) => ({
  type: FILTER_DOCTOR_SUCCESS,
  payload: data
})

export const filterDoctorsFailure = (error) => ({
  type: FILTER_DOCTOR_FAILURE,
  payload: error
})

export const load = data => ({ type: LOAD, data })