import {

    GET_PRESCRIPTIONS,
    GET_PRESCRIPTIONS_FAILURE,
    GET_PRESCRIPTIONS_SUCCESS,
    ADD_PRESCRIPTION,
    ADD_PRESCRIPTION_SUCCESS,
    ADD_PRESCRIPTION_FAILURE,
    EDIT_PRESCRIPTION,
    EDIT_PRESCRIPTION_SUCCESS,
    EDIT_PRESCRIPTION_FAILURE,
    GET_PATIENT_PRESCRIPTIONS,
    GET_PATIENT_PRESCRIPTIONS_SUCCESS,
    GET_PATIENT_PRESCRIPTIONS_FAILURE,
    GET_DOCTOR_PRESCRIPTIONS,
    GET_DOCTOR_PRESCRIPTIONS_SUCCESS,
    GET_DOCTOR_PRESCRIPTIONS_FAILURE,
    FILTER_PRESCRIPTION,
    FILTER_PRESCRIPTION_SUCCESS,
    FILTER_PRESCRIPTION_FAILURE
} from './types'
const LOAD = 'redux-form-examples/account/LOAD'

export const getPrescriptionData = (data) => ({
    type: GET_PRESCRIPTIONS,
    payload: data
})

export const getPrescriptionDataSuccess = (data) => ({
    type: GET_PRESCRIPTIONS_SUCCESS,
    payload: data
})

export const getPrescriptionDataFailure = (error) => ({
    type: GET_PRESCRIPTIONS_FAILURE,
    payload: error
})

export const getPatientPrescriptionData = (data) => ({
    type: GET_PATIENT_PRESCRIPTIONS,
    payload: data
})

export const getPatientPrescriptionSuccess = (data) => ({
    type: GET_PATIENT_PRESCRIPTIONS_SUCCESS,
    payload: data
})

export const getPatientPrescriptionFailure = (error) => ({
    type: GET_PATIENT_PRESCRIPTIONS_FAILURE,
    payload: error
})

export const getDoctorPrescriptionData = (data) => ({
    type: GET_DOCTOR_PRESCRIPTIONS,
    payload: data
})

export const getDoctorPrescriptionSuccess = (data) => ({
    type: GET_DOCTOR_PRESCRIPTIONS_SUCCESS,
    payload: data
})

export const getDoctorPrescriptionFailure = (error) => ({
    type: GET_DOCTOR_PRESCRIPTIONS_FAILURE,
    payload: error
})

export const addPrescription = (data) => ({
    type: ADD_PRESCRIPTION,
    payload: data
});

export const addPrescriptionSuccess = (data) => ({
    type: ADD_PRESCRIPTION_SUCCESS,
    payload: data
})

export const addPrescriptionFailure = (error) => ({
    type: ADD_PRESCRIPTION_FAILURE,
    payload: error
})

export const editPrescription = (data) => ({
    type: EDIT_PRESCRIPTION,
    payload: data
});

export const editPrescriptionSuccess = (data) => ({
    type: EDIT_PRESCRIPTION_SUCCESS,
    payload: data
})

export const editPrescriptionFailure = (error) => ({
    type: EDIT_PRESCRIPTION_FAILURE,
    payload: error
})

export const filterPrescription = (data) => ({
    type: FILTER_PRESCRIPTION,
    payload: data
});

export const filterPrescriptionSuccess = (data) => ({
    type: FILTER_PRESCRIPTION_SUCCESS,
    payload: data
})

export const filterPrescriptionFailure = (error) => ({
    type: FILTER_PRESCRIPTION_FAILURE,
    payload: error
})

export const load = data => ({ type: LOAD, data })