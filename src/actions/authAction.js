import {
  SIGNUP,
  SIGNUP_SUCCESS,
  SIGNUP_FAILURE,
  SIGNIN,
  SIGNIN_SUCCESS,
  SIGNIN_FAILURE,
  SIGNOUT,
  FORGOT_PASSWORD,
  FORGOT_PASSWORD_SUCCESS,
  FORGOT_PASSWORD_FAILURE,
  RESET_PASSWORD,
  RESET_PASSWORD_SUCCESS,
  RESET_PASSWORD_FAILURE,
  GETUSERPROFILEDATA,
  GETUSERPROFILEDATA_SUCCESS,
  GETUSERPROFILEDATA_FAILURE,
  GET_PRESCRIPTIONS,
  GET_PRESCRIPTIONS_FAILURE,
  GET_PRESCRIPTIONS_SUCCESS,
  GET_PHARMACY,
  GET_PHARMACY_SUCCESS,
  GET_PHARMACY_FAILURE,
  GET_PATIENTS,
  GET_PATIENTS_SUCCESS,
  GET_PATIENTS_FAILURE,
  GET_PATIENT_PRESCRIPTIONS,
  GET_PATIENT_PRESCRIPTIONS_SUCCESS,
  GET_PATIENT_PRESCRIPTIONS_FAILURE,
  CONNECT_PHARMACY,
  CONNECT_PHARMACY_SUCCESS,
  CONNECT_PHARMACY_FAILURE,
  GET_CONNECTS,
  GET_CONNECTS_SUCCESS,
  GET_CONNECTS_FAILURE,
  GET_PATIENT_PHARMACIES,
  GET_PATIENT_PHARMACIES_SUCCESS,
  GET_PATIENT_PHARMACIES_FAILURE,
  CROPIMAGE,
  CROPIMAGE_SUCCESS,
  CROPIMAGE_FAILURE,
  SEARCH_PHARMACY,
  SEARCH_PHARMACY_SUCCESS,
  SEARCH_PHARMACY_FAILURE,
  CONNECT_DOCTOR,
  CONNECT_DOCTOR_SUCCESS,
  CONNECT_DOCTOR_FAILURE,
  GET_PHARMACY_DETAIL,
  GET_PHARMACY_DETAIL_SUCCESS,
  GET_PHARMACY_DETAIL_FAILURE,
  VERIFY_ACCOUNT,
  VERIFY_ACCOUNT_SUCCESS,
  VERIFY_ACCOUNT_FAILURE,
 
} from './types'

const LOAD = 'redux-form-examples/account/LOAD'

export const signup = (data) => ({
  type: SIGNUP,
  payload: data
})

export const signupSuccess = (data) => ({
  type: SIGNUP_SUCCESS,
  payload: data
})
export const signupFailure = (error) => ({
  type: SIGNUP_FAILURE,
  payload: error
})

export const signin = (data) => ({
  type: SIGNIN,
  payload: data
})

export const signinSuccess = (data) => ({
  type: SIGNIN_SUCCESS,
  payload: data
})

export const signinFailure = (error) => ({
  type: SIGNIN_FAILURE,
  payload: error
})

export const getuserProfileData = (data) => ({
  type: GETUSERPROFILEDATA,
  payload: data
})

export const getuserProfileDataSuccess = (data) => ({
  type: GETUSERPROFILEDATA_SUCCESS,
  payload: data
})

export const getuserProfileDataFailure = (error) => ({
  type: GETUSERPROFILEDATA_FAILURE,
  payload: error
})

export const signout = (error) => ({
  type: SIGNOUT,
  payload: error
})

export const forgotPassword = (data) => ({
  type: FORGOT_PASSWORD,
  payload: data
});

export const forgotPasswordSuccess = (data) => ({
  type: FORGOT_PASSWORD_SUCCESS,
  payload: data
})

export const forgotPasswordFailure = (error) => ({
  type: FORGOT_PASSWORD_FAILURE,
  payload: error
})

export const resetPassword = (data) => ({
  type: RESET_PASSWORD,
  payload: data
});

export const resetPasswordSuccess = (data) => ({
  type: RESET_PASSWORD_SUCCESS,
  payload: data
})

export const resetPasswordFailure = (error) => ({
  type: RESET_PASSWORD_FAILURE,
  payload: error
})

export const verifyAccount = (data) => ({
  type: VERIFY_ACCOUNT,
  payload: data
});

export const verifyAccountSuccess = (data) => ({
  type: VERIFY_ACCOUNT_SUCCESS,
  payload: data
})

export const verifyAccountFailure = (error) => ({
  type: VERIFY_ACCOUNT_FAILURE,
  payload: error
})

export const cropImage = (data) => ({
  type: CROPIMAGE,
  payload: data
})

export const cropImageSuccess = (data) => ({
  type: CROPIMAGE_SUCCESS,
  payload: data
})

export const cropImageFailure = (error) => ({
  type: CROPIMAGE_FAILURE,
  payload: error
})

export const getPrescriptionData = (data) => ({
  type: GET_PRESCRIPTIONS,
  payload: data
})

export const getPrescriptionDataSuccess = (data) => ({
  type: GET_PRESCRIPTIONS_SUCCESS,
  payload: data
})

export const getPrescriptionDataFailure = (error) => ({
  type: GET_PRESCRIPTIONS_FAILURE,
  payload: error
})

export const getPatientPrescriptionData = (data) => ({
  type: GET_PATIENT_PRESCRIPTIONS,
  payload: data
})

export const getPatientPrescriptionSuccess = (data) => ({
  type: GET_PATIENT_PRESCRIPTIONS_SUCCESS,
  payload: data
})

export const getPatientPrescriptionFailure = (error) => ({
  type: GET_PATIENT_PRESCRIPTIONS_FAILURE,
  payload: error
})

export const getPatientData = (data) => ({
  type: GET_PATIENTS,
  payload: data
})

export const getPatientDataSuccess = (data) => ({
  type: GET_PATIENTS_SUCCESS,
  payload: data
})

export const getPatientDataFailure = (error) => ({
  type: GET_PATIENTS_FAILURE,
  payload: error
})

export const getPharmacyData = (data) => ({
  type: GET_PHARMACY,
  payload: data
})

export const getPharmacyDataSuccess = (data) => ({
  type: GET_PHARMACY_SUCCESS,
  payload: data
})

export const getPharmacyDataFailure = (error) => ({
  type: GET_PHARMACY_FAILURE,
  payload: error
})

export const getPharmacyDetail = (data) => ({
  type: GET_PHARMACY_DETAIL,
  payload: data
})

export const getPharmacyDetailSuccess = (data) => ({
  type: GET_PHARMACY_DETAIL_SUCCESS,
  payload: data
})

export const getPharmacyDetailFailure = (error) => ({
  type: GET_PHARMACY_DETAIL_FAILURE,
  payload: error
})

export const searchPharmacyData = (data) => ({
  type: SEARCH_PHARMACY,
  payload: data
})

export const searchPharmacyDataSuccess = (data) => ({
  type: SEARCH_PHARMACY_SUCCESS,
  payload: data
})

export const searchPharmacyDataFailure = (error) => ({
  type: SEARCH_PHARMACY_FAILURE,
  payload: error
})

export const connectPharmacy = (data) => ({
  type: CONNECT_PHARMACY,
  payload: data
});

export const connectPharmacySuccess = (data) => ({
  type: CONNECT_PHARMACY_SUCCESS,
  payload: data
})

export const connectPharmacyFailure = (error) => ({
  type: CONNECT_PHARMACY_FAILURE,
  payload: error
})

export const connectDoctor = (data) => ({
  type: CONNECT_DOCTOR,
  payload: data
});

export const connectDoctorSuccess = (data) => ({
  type: CONNECT_DOCTOR_SUCCESS,
  payload: data
})

export const connectDoctorFailure = (error) => ({
  type: CONNECT_DOCTOR_FAILURE,
  payload: error
})

export const getConnectData = (data) => ({
  type: GET_CONNECTS,
  payload: data
})

export const getConnectDataSuccess = (data) => ({
  type: GET_CONNECTS_SUCCESS,
  payload: data
})

export const getConnectDataFailure = (error) => ({
  type: GET_CONNECTS_FAILURE,
  payload: error
})

export const getPatientPharmacyData = (data) => ({
  type: GET_PATIENT_PHARMACIES,
  payload: data
})

export const getPatientPharmacyDataSuccess = (data) => ({
  type: GET_PATIENT_PHARMACIES_SUCCESS,
  payload: data
})

export const getPatientPharmacyDataFailure = (error) => ({
  type: GET_PATIENT_PHARMACIES_FAILURE,
  payload: error
})

export const load = data => ({ type: LOAD, data })
