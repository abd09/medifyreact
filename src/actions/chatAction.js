import {

  GET_CHAT,
  GET_CHAT_SUCCESS,
  GET_CHAT_FAILURE,
  GET_CHAT_CONNETION,
  GET_CHAT_CONNETION_SUCCESS,
  GET_CHAT_CONNETION_FAILURE,
} from './types'
const LOAD = 'redux-form-examples/account/LOAD'


export const getChat = (data) => {
console.log('getChat',data)
return {type: GET_CHAT,
payload: data}
};

export const getChatSuccess = (data) => ({
  type: GET_CHAT_SUCCESS,
  payload: data
})

export const getChatFailure = (error) => ({
  type: GET_CHAT_FAILURE,
  payload: error
})

export const getChatConnection = (data) => {
  console.log('getChatConnection',data)
  return {type:GET_CHAT_CONNETION,
  payload: data}
  };
  
  export const getChatConnectionSuccess = (data) => ({
    type:GET_CHAT_CONNETION_SUCCESS,
    payload: data
  })
  
  export const getChatConnectionFailure = (error) => ({
    type:GET_CHAT_CONNETION_FAILURE,
    payload: error
  })
  

export const load = data => ({ type: LOAD, data })