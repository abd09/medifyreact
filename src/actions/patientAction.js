import {
    EDIT_VITALS,
    EDIT_VITALS_SUCCESS,
    EDIT_VITALS_FAILURE,

} from './types'
const LOAD = 'redux-form-examples/account/LOAD'

export const editVitals = (data) => ({
    type: EDIT_VITALS,
    payload: data
})

export const editVitalsSuccess = (data) => ({
    type: EDIT_VITALS_SUCCESS,
    payload: data
})

export const editVitalsFailure = (error) => ({
    type: EDIT_VITALS_FAILURE,
    payload: error
})

export const load = data => ({ type: LOAD, data })