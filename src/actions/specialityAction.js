import {
    GET_SPECIALITIES,
    GET_SPECIALITIES_FAILURE,
    GET_SPECIALITIES_SUCCESS,
} from './types'
const LOAD = 'redux-form-examples/account/LOAD'

export const getSpecialities = (data) => ({
    type: GET_SPECIALITIES,
    payload: data
});

export const getSpecialitiesSuccess = (data) => ({
    type: GET_SPECIALITIES_SUCCESS,
    payload: data
})

export const getSpecialitiesFailure = (error) => ({
    type: GET_SPECIALITIES_FAILURE,
    payload: error
})

export const load = data => ({ type: LOAD, data })