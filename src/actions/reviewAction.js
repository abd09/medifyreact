import {

    GET_REVIEW,
    GET_REVIEW_FAILURE,
    GET_REVIEW_SUCCESS,
    ADD_REVIEW,
    ADD_REVIEW_SUCCESS,
    ADD_REVIEW_FAILURE,
    GET_REVIEW_DETAIL,
    GET_REVIEW_DETAIL_SUCCESS,
    GET_REVIEW_DETAIL_FAILURE,
} from './types'
const LOAD = 'redux-form-examples/account/LOAD'


export const getReview = (data) => ({
  type: GET_REVIEW,
  payload: data
});

export const getReviewSuccess = (data) => ({
  type: GET_REVIEW_SUCCESS,
  payload: data
})

export const getReviewFailure = (error) => ({
  type: GET_REVIEW_FAILURE,
  payload: error
})

export const addReview = (data) => ({
  type: ADD_REVIEW,
  payload: data
});

export const addReviewSuccess = (data) => ({
  type: ADD_REVIEW_SUCCESS,
  payload: data
})

export const addReviewFailure = (error) => ({
  type: ADD_REVIEW_FAILURE,
  payload: error
})

export const getReviewDetail = (data) => ({
  type: GET_REVIEW_DETAIL,
  payload: data
});

export const getReviewDetailSuccess = (data) => ({
  type: GET_REVIEW_DETAIL_SUCCESS,
  payload: data
})

export const getReviewDetailFailure = (error) => ({
  type: GET_REVIEW_DETAIL_FAILURE,
  payload: error
})

export const load = data => ({ type: LOAD, data })