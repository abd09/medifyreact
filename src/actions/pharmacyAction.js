import {
    CONNECTED_PHARMACIES,
    CONNECTED_PHARMACIES_SUCCESS,
    CONNECTED_PHARMACIES_FAILURE,

} from './types'
const LOAD = 'redux-form-examples/account/LOAD'

export const connectedPharmaciesData = (data) => ({
    type: CONNECTED_PHARMACIES,
    payload: data
})

export const connectedPharmaciesDataSuccess = (data) => ({
    type: CONNECTED_PHARMACIES_SUCCESS,
    payload: data
})

export const connectedPharmaciesDataFailure = (error) => ({
    type: CONNECTED_PHARMACIES_FAILURE,
    payload: error
})

export const load = data => ({ type: LOAD, data })