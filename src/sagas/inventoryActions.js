import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import baseUrl from '../constant/config';
import { NotificationManager } from 'react-notifications';
import {
    GET_INVENTORIES,
    SEARCH_INVENTORIES,
    GET_PHARMACY_INVENTORY,
    FILTER_INVENTORY
} from '../actions/types';
import {
    signout,
    getInventoriesDataSuccess,
    getInventoriesDataFailure,
    searchInventoriesSuccess,
    searchInventoriesFailure,
    getPharmacyInventoryDataSuccess,
    getPharmacyInventoryDataFailure,
    filterInventorySuccess,
    filterInventoryFailure
} from '../actions';

import axios from '../constant/axios';

const getInventoriesRequest = (data) => {
    console.log('getInventoriesRequest', data)
    return axios.get(`/users/inventory`, {
        headers: {
            'Content-Type': 'application/json',
            'token': localStorage.getItem('token')
        }
    }
    ).then(result => result)
        .catch(error => error)
}

function* getInventoriesFunction({ payload }) {
    const { history } = payload
    try {
        const getInventoriesResponse = yield call(getInventoriesRequest, payload.data)
        console.log('getInventoriesResponse', getInventoriesResponse, history)
        if (getInventoriesResponse.data.error) {
            NotificationManager.error(getInventoriesResponse.data.title)
            if (getInventoriesResponse.data.title === 'Authorization Required') {
                history.push('/categorySignin')
                yield put(signout())
            }
            yield put(getInventoriesDataFailure(getInventoriesResponse.data.detail))
        } else {
            yield put(getInventoriesDataSuccess(getInventoriesResponse.data))
        }
    } catch (error) {
        yield put(getInventoriesDataFailure(error))
    }
}

export function* getInventories() {
    yield takeEvery(GET_INVENTORIES, getInventoriesFunction)
}


const searchInventoriesRequest = (data) => {
    console.log('searchInventoriesRequest', data)
    return axios.post(`/users/searchInventory/${data.id}`, { searchText: data.searchText }, {
        headers: {
            'Content-Type': 'application/json',
            'token': localStorage.getItem('token')
        }
    }
    ).then(result => result)
        .catch(error => error)
        .catch(error => {
            console.log('error', error);
        });
}

function* searchInventoriesFunction({ payload }) {
    const { history } = payload
    try {
        const searchInventoriesResponse = yield call(searchInventoriesRequest, payload.data)
        console.log('searchInventoriesResponse', searchInventoriesResponse, history)
        if (searchInventoriesResponse.data.error) {
            NotificationManager.error(searchInventoriesResponse.data.title)
            if (searchInventoriesResponse.data.title === 'Authorization Required') {
                history.push('/categorySignin')
                yield put(signout())
            }
            yield put(searchInventoriesFailure(searchInventoriesResponse.data.detail))
        } else {
            yield put(searchInventoriesSuccess(searchInventoriesResponse.data))
        }
    } catch (error) {
        yield put(searchInventoriesFailure(error))
    }
}

export function* searchInventories() {
    yield takeEvery(SEARCH_INVENTORIES, searchInventoriesFunction)
}

const getPharmacyInventoryRequest = (data) => {
    console.log('getPharmacyInventoryRequest', data)
    return axios.get(`/users/pharmacyInventory/${data.id}`, {
        headers: {
            'Content-Type': 'application/json',
            'token': localStorage.getItem('token')
        }
    }
    ).then(result => result)
        .catch(error => error)
}

function* getPharmacyInventoryFunction({ payload }) {
    const { history } = payload
    try {
        const getPharmacyInventoryResponse = yield call(getPharmacyInventoryRequest, payload.data)
        console.log('getPharmacyInventoryResponse', getPharmacyInventoryResponse, history)
        if (getPharmacyInventoryResponse.data.error) {
            NotificationManager.error(getPharmacyInventoryResponse.data.title)
            if (getPharmacyInventoryResponse.data.title === 'Authorization Required') {
                history.push('/categorySignin')
                yield put(signout())
            }
            yield put(getPharmacyInventoryDataFailure(getPharmacyInventoryResponse.data.detail))
        } else {
            yield put(getPharmacyInventoryDataSuccess(getPharmacyInventoryResponse.data))
        }
    } catch (error) {
        yield put(getPharmacyInventoryDataFailure(error))
    }
}

export function* getPharmacyInventory() {
    yield takeEvery(GET_PHARMACY_INVENTORY, getPharmacyInventoryFunction)
}

const filterInventoryRequest = (data) => {
    console.log('filterInventoryRequest', data)
    return axios.get(`/users/sortByInventoryCategory/${data.value}/${data.id}`, {
        headers: {
            'Content-Type': 'application/json',
            'token': localStorage.getItem('token')
        }
    }
    ).then(result => result)
        .catch(error => error)
}

function* filterInventoryFunction({ payload }) {
    const { history } = payload
    try {
        const filterInventoryResponse = yield call(filterInventoryRequest, payload.data)
        console.log('filterInventoryResponse', filterInventoryResponse, history)
        if (filterInventoryResponse.data.error) {
            NotificationManager.error(filterInventoryResponse.data.title)
            if (filterInventoryResponse.data.title === 'Authorization Required') {
                history.push('/categorySignin')
                yield put(signout())
            }
            yield put(filterInventoryFailure(filterInventoryResponse.data.detail))
        } else {
            yield put(filterInventorySuccess(filterInventoryResponse.data))
        }
    } catch (error) {
        yield put(filterInventoryFailure(error))
    }
}

export function* filterInventory() {
    yield takeEvery(FILTER_INVENTORY, filterInventoryFunction)
}


export default function* rootSaga() {
    yield all([
        fork(getInventories),
        fork(searchInventories),
        fork(getPharmacyInventory),
        fork(filterInventory)
    ])
}