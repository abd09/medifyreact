import { all } from 'redux-saga/effects';

// sagas
import userAuthSagas from './authActions';
import prescriptionActions from './prescriptionActions';
import orderActions from './orderActions';
import doctorActions from './doctorActions';
import pharmacyActions from './pharmacyActions';
import patientActions from './patientActions';
import inventoryActions from './inventoryActions';
import reviewAction from './reviewAction';
import chatAction from './chatAction';
import specialityActions from './specialityActions';
import schemeActions from './schemeActions';

export default function* rootSaga(getState) {
  yield all([
    userAuthSagas(),
    prescriptionActions(),
    orderActions(),
    doctorActions(),
    pharmacyActions(),
    patientActions(),
    inventoryActions(),
    reviewAction(),
    chatAction(),
    specialityActions(),
    schemeActions()
  ]);
}