import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import baseUrl from '../constant/config';
import { NotificationManager } from 'react-notifications';
import {
    GET_PATIENT_DOCTORS,
    CONNECTED_DOCTORS,
    GET_ALL_DOCTORS,
    SEARCH_DOCTORS,
    GET_DOCTOR_PATIENTS,
    SEARCH_DOCTOR_PATIENTS,
    FILTER_DOCTOR,
    GET_DOCTOR_DETAIL
} from '../actions/types';
import {
    signout,
    getPatientDoctorsDataSuccess,
    getPatientDoctorsDataFailure,
    connectedDoctorsDataSuccess,
    connectedDoctorsDataFailure,
    getAllDoctorsDataSuccess,
    getAllDoctorsDataFailure,
    searchDoctorsSuccess,
    searchDoctorsFailure,
    getDoctorPatientsDataSuccess,
    getDoctorPatientsDataFailure,
    searchDoctorPatientsSuccess,
    searchDoctorPatientsFailure,
    filterDoctorsSuccess,
    filterDoctorsFailure,
    getDoctorDetailSuccess,
    getDoctorDetailFailure
} from '../actions';

import axios from '../constant/axios';

const getAllDoctorsRequest = (data) => {
    console.log('getAllDoctorsRequest', data)
    return axios.get(`/users/doctors`, {
        headers: {
            'Content-Type': 'application/json',
            'token': localStorage.getItem('token')
        }
    }
    ).then(result => result)
        .catch(error => error)
}

function* getAllDoctorsFunction({ payload }) {
    const { history } = payload
    try {
        const getAllDoctorsResponse = yield call(getAllDoctorsRequest, payload.data)
        console.log('getAllDoctorsResponse', getAllDoctorsResponse, history)
        if (getAllDoctorsResponse.data.error) {
            NotificationManager.error(getAllDoctorsResponse.data.title)
            if (getAllDoctorsResponse.data.title === 'Authorization Required') {
                history.push('/categorySignin')
                yield put(signout())
            }
            yield put(getAllDoctorsDataFailure(getAllDoctorsResponse.data.detail))
        } else {
            yield put(getAllDoctorsDataSuccess(getAllDoctorsResponse.data))
        }
    } catch (error) {
        yield put(getAllDoctorsDataFailure(error))
    }
}

export function* getAllDoctors() {
    yield takeEvery(GET_ALL_DOCTORS, getAllDoctorsFunction)
}

const getDoctorDetailsRequest = (data) => {
    console.log('getDoctorDetailsRequest', data)
    return axios.get(`/users/doctorDetails/${data.id}`, {
        headers: {
            'Content-Type': 'application/json',
            'token': localStorage.getItem('token')
        }
    }
    ).then(result => result)
        .catch(error => error)
}

function* getDoctorDetailsFunction({ payload }) {
    const { history } = payload
    try {
        const getDoctorDetailsResponse = yield call(getDoctorDetailsRequest, payload.data)
        console.log('getDoctorDetailsResponse', getDoctorDetailsResponse, history)
        if (getDoctorDetailsResponse.data.error) {
            NotificationManager.error(getDoctorDetailsResponse.data.title)
            if (getDoctorDetailsResponse.data.title === 'Authorization Required') {
                history.push('/categorySignin')
                yield put(signout())
            }
            yield put(getDoctorDetailFailure(getDoctorDetailsResponse.data.detail))
        } else {
            yield put(getDoctorDetailSuccess(getDoctorDetailsResponse.data))
        }
    } catch (error) {
        yield put(getDoctorDetailFailure(error))
    }
}

export function* getDoctorDetails() {
    yield takeEvery(GET_DOCTOR_DETAIL, getDoctorDetailsFunction)
}

const getPatientDoctorsRequest = (data) => {
    console.log('getPatientDoctorsRequest', data)
    return axios.get(`/users/getPatientsDoctors`, {
        headers: {
            'Content-Type': 'application/json',
            'token': localStorage.getItem('token')
        }
    }
    ).then(result => result)
        .catch(error => error)
}

function* getPatientDoctorsFunction({ payload }) {
    const { history } = payload
    try {
        const getPatientDoctorsResponse = yield call(getPatientDoctorsRequest, payload.data)
        console.log('getPatientDoctorsResponse', getPatientDoctorsResponse, history)
        if (getPatientDoctorsResponse.data.error) {
            NotificationManager.error(getPatientDoctorsResponse.data.title)
            if (getPatientDoctorsResponse.data.title === 'Authorization Required') {
                history.push('/categorySignin')
                yield put(signout())
            }
            yield put(getPatientDoctorsDataFailure(getPatientDoctorsResponse.data.detail))
        } else {
            yield put(getPatientDoctorsDataSuccess(getPatientDoctorsResponse.data))
        }
    } catch (error) {
        yield put(getPatientDoctorsDataFailure(error))
    }
}

export function* getPatientDoctors() {
    yield takeEvery(GET_PATIENT_DOCTORS, getPatientDoctorsFunction)
}

const getDoctorPatientsRequest = (data) => {
    console.log('getDoctorPatientsRequest', data)
    return axios.get(`/users/doctorPatients`, {
        headers: {
            'Content-Type': 'application/json',
            'token': localStorage.getItem('token')
        }
    }
    ).then(result => result)
        .catch(error => error)
}

function* getDoctorPatientsFunction({ payload }) {
    const { history } = payload
    try {
        const getDoctorPatientsResponse = yield call(getDoctorPatientsRequest, payload.data)
        console.log('getDoctorPatientsResponse', getDoctorPatientsResponse, history)
        if (getDoctorPatientsResponse.data.error) {
            NotificationManager.error(getDoctorPatientsResponse.data.title)
            if (getDoctorPatientsResponse.data.title === 'Authorization Required') {
                history.push('/categorySignin')
                yield put(signout())
            }
            yield put(getDoctorPatientsDataFailure(getDoctorPatientsResponse.data.detail))
        } else {
            yield put(getDoctorPatientsDataSuccess(getDoctorPatientsResponse.data))
        }
    } catch (error) {
        yield put(getDoctorPatientsDataFailure(error))
    }
}

export function* getDoctorPatients() {
    yield takeEvery(GET_DOCTOR_PATIENTS, getDoctorPatientsFunction)
}

const connectedDoctorsRequest = (data) => {
    console.log('connectedDoctorsRequest', data)
    return axios.get(`/users/connectedDoctors`, {
        headers: {
            'Content-Type': 'application/json',
            'token': localStorage.getItem('token')
        }
    }
    ).then(result => result)
        .catch(error => error)
}

function* connectedDoctorsFunction({ payload }) {
    const { history } = payload
    try {
        const connectedDoctorsResponse = yield call(connectedDoctorsRequest, payload.data)
        console.log('connectedDoctorsResponse', connectedDoctorsResponse, history)
        if (connectedDoctorsResponse.data.error) {
            NotificationManager.error(connectedDoctorsResponse.data.title)
            if (connectedDoctorsResponse.data.title === 'Authorization Required') {
                history.push('/categorySignin')
                yield put(signout())
            }
            yield put(connectedDoctorsDataFailure(connectedDoctorsResponse.data.detail))
        } else {
            yield put(connectedDoctorsDataSuccess(connectedDoctorsResponse.data))
        }
    } catch (error) {
        yield put(connectedDoctorsDataFailure(error))
    }
}

export function* connectedDoctors() {
    yield takeEvery(CONNECTED_DOCTORS, connectedDoctorsFunction)
}

const searchDoctorsRequest = (data) => {
    console.log('searchDoctorsRequest', data)
    return axios.get(`/users/searchDoctors/${data.searchText}`, {
        headers: {
            'Content-Type': 'application/json',
            'token': localStorage.getItem('token')
        }
    }
    ).then(result => result)
        .catch(error => error)
}

function* searchDoctorsFunction({ payload }) {
    const { history } = payload
    try {
        const searchDoctorsResponse = yield call(searchDoctorsRequest, payload.data)
        console.log('searchDoctorsResponse', searchDoctorsResponse, history)
        if (searchDoctorsResponse.data.error) {
            NotificationManager.error(searchDoctorsResponse.data.title)
            if (searchDoctorsResponse.data.title === 'Authorization Required') {
                history.push('/categorySignin')
                yield put(signout())
            }
            yield put(searchDoctorsFailure(searchDoctorsResponse.data.detail))
        } else {
            yield put(searchDoctorsSuccess(searchDoctorsResponse.data))
        }
    } catch (error) {
        yield put(searchDoctorsFailure(error))
    }
}

export function* searchDoctors() {
    yield takeEvery(SEARCH_DOCTORS, searchDoctorsFunction)
}

const searchDoctorPatientsRequest = (data) => {
    console.log('searchDoctorPatientsRequest', data)
    return axios.get(`/users/searchDoctorPatients/${data.searchText}`, {
        headers: {
            'Content-Type': 'application/json',
            'token': localStorage.getItem('token')
        }
    }
    ).then(result => result)
        .catch(error => error)
}

function* searchDoctorPatientsFunction({ payload }) {
    const { history } = payload
    try {
        const searchDoctorPatientsResponse = yield call(searchDoctorPatientsRequest, payload.data)
        console.log('searchDoctorPatientsResponse', searchDoctorPatientsResponse, history)
        if (searchDoctorPatientsResponse.data.error) {
            NotificationManager.error(searchDoctorPatientsResponse.data.title)
            if (searchDoctorPatientsResponse.data.title === 'Authorization Required') {
                history.push('/categorySignin')
                yield put(signout())
            }
            yield put(searchDoctorPatientsFailure(searchDoctorPatientsResponse.data.detail))
        } else {
            yield put(searchDoctorPatientsSuccess(searchDoctorPatientsResponse.data))
        }
    } catch (error) {
        yield put(searchDoctorPatientsFailure(error))
    }
}

export function* searchDoctorPatients() {
    yield takeEvery(SEARCH_DOCTOR_PATIENTS, searchDoctorPatientsFunction)
}


const filterDoctorsRequest = (data) => {
    console.log('filterDoctorsRequest', data.value)
    return axios.get(`/users/sortBySpeciality/${data.value}`, {
        headers: {
            'Content-Type': 'application/json',
            'token': localStorage.getItem('token')
        }
    }
    ).then(result => result)
        .catch(error => error)
}

function* filterDoctorsFunction({ payload }) {
    const { history } = payload
    try {
        const filterDoctorsResponse = yield call(filterDoctorsRequest, payload.data)
        console.log('filterDoctorsResponse', filterDoctorsResponse, history)
        if (filterDoctorsResponse.data.error) {
            NotificationManager.error(filterDoctorsResponse.data.title)
            if (filterDoctorsResponse.data.title === 'Authorization Required') {
                history.push('/categorySignin')
                yield put(signout())
            }
            yield put(filterDoctorsFailure(filterDoctorsResponse.data.detail))
        } else {
            yield put(filterDoctorsSuccess(filterDoctorsResponse.data))
            console.log('filterDoctorsSuccess', filterDoctorsResponse.data)
        }
    } catch (error) {
        yield put(filterDoctorsFailure(error))
    }
}

export function* filterDoctors() {
    yield takeEvery(FILTER_DOCTOR, filterDoctorsFunction)
}

export default function* rootSaga() {
    yield all([
        fork(getAllDoctors),
        fork(getPatientDoctors),
        fork(getDoctorPatients),
        fork(connectedDoctors),
        fork(searchDoctors),
        fork(searchDoctorPatients),
        fork(filterDoctors),
        fork(getDoctorDetails)
    ])
}