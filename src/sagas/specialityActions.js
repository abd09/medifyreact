import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import baseUrl from '../constant/config';
import { NotificationManager } from 'react-notifications';
import {
    GET_SPECIALITIES,
} from '../actions/types';
import {
    getSpecialitiesSuccess,
    getSpecialitiesFailure,
    signout,
} from '../actions';

import axios from '../constant/axios';


const getSpecialitiesRequest = (data) => {
    console.log('getSpecialitiesRequest', data)
    return axios.get('/users/getSpecialities', {
        headers: {
            'Content-Type': 'application/json',
            'token': localStorage.getItem('token')
        }
    }
    ).then(result => result)
        .catch(error => error)
}

function* getSpecialitiesFunction({ payload }) {
    const { history } = payload
    try {
        const getSpecialitiesResponse = yield call(getSpecialitiesRequest, payload.data)
        console.log('getSpecialitiesResponse', getSpecialitiesResponse, history)
        if (getSpecialitiesResponse.data.error) {
            NotificationManager.error(getSpecialitiesResponse.data.title)
            if (getSpecialitiesResponse.data.title === 'Authorization Required') {
                yield put(signout())
            }
            yield put(getSpecialitiesFailure(getSpecialitiesResponse.data.detail))
        } else {
            yield put(getSpecialitiesSuccess(getSpecialitiesResponse.data))
        }
    } catch (error) {
        yield put(getSpecialitiesFailure(error))
    }
}

export function* getSpecialities() {
    yield takeEvery(GET_SPECIALITIES, getSpecialitiesFunction)
}

export default function* rootSaga() {
    yield all([
        fork(getSpecialities),
    ])
}