import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import { NotificationManager } from 'react-notifications';
import {
    EDIT_VITALS,
} from '../actions/types';
import {
    editVitalsSuccess,
    editVitalsFailure,
    signout,
} from '../actions';

import axios from '../constant/axios';

const editVitalsRequest = (data) => {
    console.log('editVitalsRequest', data)
    let postData = {
        temperature: data.temperature,
        pulseRate: data.pulseRate,
        bloodPressure: data.bloodPressure,
        allergies: data.allergies,
        diseases: data.diseases,
        surgeries: data.surgeries,
        others: data.others,
        userId: data.userId
    }
    return axios.post('/users/editVitals', postData, {
        headers: {
            'Content-Type': 'application/json',
            'token': localStorage.getItem('token')
        }
    }
    ).then(result => result)
        .catch(error => error)
}


function* editVitalsFunction({ payload }) {
    const { history } = payload
    console.log('history.,,,,,,,,,,,', history)
    try {
        const editVitalsResponse = yield call(editVitalsRequest, payload.data)
        console.log('editVitalsResponse', editVitalsResponse)
        if (editVitalsResponse.data.error) {
            NotificationManager.error(editVitalsResponse.data.title)
            if (editVitalsResponse.data.title === 'Authorization Required') {
                history.push(`/categorySignin`)
                yield put(signout())
            }
            yield put(editVitalsFailure(editVitalsResponse.data))
        } else {
            NotificationManager.success(editVitalsResponse.data.title)
            yield put(editVitalsSuccess(editVitalsResponse.data))
            payload.history.push(`/${localStorage.getItem('role')}/vitals`)
         
        }
    } catch (error) {
        yield put(editVitalsFailure(error))
    }
}

export function* editVitals() {
    yield takeEvery(EDIT_VITALS, editVitalsFunction)
}

export default function* rootSaga() {
    yield all([
        fork(editVitals),
    ])
}