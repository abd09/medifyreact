import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import { NotificationManager } from 'react-notifications';
import {
  GET_CHAT,
  GET_CHAT_CONNETION,
} from '../actions/types';
import {
  getChatSuccess,
  getChatFailure,
  getChatConnectionFailure,
  getChatConnectionSuccess,
  signout,
} from '../actions';

import axios from '../constant/axios';

const getChatRequest = (data) => {
  console.log('getChatRequest', data)
  return axios.post('/users/fetchChat', data, {
    headers: {
      'Content-Type': 'application/json',
      'token': localStorage.getItem('token')
    }
  }
  ).then(result => result)
    .catch(error => error)
}

function* getChatFunction({ payload }) {
  const { history } = payload
  console.log('history', history)
  try {
    const getChatResponse = yield call(getChatRequest, payload.data)
    console.log('getChatResponse', getChatResponse)
    if (getChatResponse.data.error) {
      NotificationManager.error(getChatResponse.data.title)
      if (getChatResponse.data.title === 'Authorization Required') {
        yield put(signout())
      }
      yield put(getChatFailure(getChatResponse.data))
    } else {
      // NotificationManager.success(getChatResponse.data.title)
      yield put(getChatSuccess(getChatResponse.data))
    }
  } catch (error) {
    yield put(getChatFailure(error))
  }
}

export function* getChat() {
  yield takeEvery(GET_CHAT, getChatFunction)
}

const getChatConnectionRequest = (data) => {
  return axios.get('/users/getChatConnection', {
    headers: {
      'Content-Type': 'application/json',
      'token': localStorage.getItem('token')
    }
  }
  ).then(result => result)
    .catch(error => error)
}

function* getChatConnectionFunction({ payload }) {
  const { history } = payload
  console.log('history', history)
  try {
    const getChatConnectionResponse = yield call(getChatConnectionRequest, payload.data)
    console.log('getChatConnectionResponse', getChatConnectionResponse)
    if (getChatConnectionResponse.data.error) {
      NotificationManager.error(getChatConnectionResponse.data.title)
      if (getChatConnectionResponse.data.title === 'Authorization Required') {
        yield put(signout())
      }
      yield put(getChatConnectionFailure(getChatConnectionResponse.data))
    } else {
      yield put(getChatConnectionSuccess(getChatConnectionResponse.data))
    }
  } catch (error) {
    yield put(getChatConnectionFailure(error))
  }
}

export function* getChatConnection() {
  yield takeEvery(GET_CHAT_CONNETION, getChatConnectionFunction)
}
export default function* rootSaga() {
  yield all([
    fork(getChat),
    fork(getChatConnection),
  ])
}