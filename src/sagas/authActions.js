import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import { NotificationManager } from 'react-notifications';
import Cookies from 'universal-cookie';
import {
    SIGNUP,
    SIGNIN,
    FORGOT_PASSWORD,
    RESET_PASSWORD,
    GETUSERPROFILEDATA,
    CROPIMAGE,
    GET_PHARMACY,
    GET_PHARMACY_DETAIL,
    GET_PATIENTS,
    CONNECT_PHARMACY,
    CONNECT_DOCTOR,
    GET_CONNECTS,
    GET_PATIENT_PHARMACIES,
    SEARCH_PHARMACY,
    VERIFY_ACCOUNT,
    VERIFY_ACCOUNT_FAILURE
} from '../actions/types';

import {
    signupSuccess,
    signupFailure,
    signinSuccess,
    signinFailure,
    signout,
    forgotPasswordSuccess,
    forgotPasswordFailure,
    resetPasswordSuccess,
    resetPasswordFailure,
    verifyAccountSuccess,
    verifyAccountFailure,
    cropImageSuccess,
    cropImageFailure,
    getuserProfileDataSuccess,
    getuserProfileDataFailure,
    getPharmacyDataSuccess,
    getPharmacyDetailSuccess,
    getPharmacyDetailFailure,
    getPharmacyDataFailure,
    getPatientDataFailure,
    getPatientDataSuccess,
    connectPharmacySuccess,
    connectPharmacyFailure,
    connectDoctorSuccess,
    connectDoctorFailure,
    getConnectDataSuccess,
    getConnectDataFailure,
    getPatientPharmacyDataSuccess,
    getPatientPharmacyDataFailure,
    searchPharmacyDataSuccess,
    searchPharmacyDataFailure,
} from '../actions';

import axios from '../constant/axios';

/************************Sign up******************************************* */
const signUpRequest = (data) => {
    console.log('signup', data);

    return axios.post(`/users/signup`,
        {
            email: data.email,
            password: data.password,
            firstname: data.firstname,
            lastname: data.lastname,
            mobile: data.mobile,
            aadharNo: data.aadharNo,
            role: data.role
        }
    ).then(result => result)
        .catch(error => error)
}

function* signupFunction({ payload }) {
    const { history } = payload
    try {

        const signupResponse = yield call(signUpRequest, payload.data)
        console.log('signupResponse', signupResponse, history)
        if (signupResponse.data.error) {
            NotificationManager.error(signupResponse.data.title)
            yield put(signupFailure(signupResponse.data))
        } else {
            localStorage.setItem('loggedIn', true)
            localStorage.setItem('token', signupResponse.data.token)
            localStorage.setItem('role', signupResponse.data.userdata.role)
            NotificationManager.success(signupResponse.data.title)
            yield put(signupSuccess(signupResponse.data))
           /*  history.push(`/${localStorage.getItem('role')}`) */
        }
    } catch (error) {
        console.log('signupResponse.data.userData.role error', error)
        yield put(signupFailure(error))
    }
}
export function* signUpAccount() {
    yield takeEvery(SIGNUP, signupFunction)
}
/*****************************Sign in************************************* */
const signInRequest = (data) => {
    console.log('signInRequest', data)
    if (data.rememberMe) {
        localStorage.setItem('ref', data.password)
    }

    return axios.post(`/users/login`,
        {
            email: data.email,
            password: data.password,
            role: data.role,
            rememberMe: data.rememberMe
        }
    ).then(result => result)
        .catch(error => error)
}

function* signInFunction({ payload }) {
    const { history } = payload
    console.log('payload', payload);

    try {
        const signInResponse = yield call(signInRequest, payload.data)
        console.log("signInResponse", signInResponse)
        if (signInResponse.data.error) {
            NotificationManager.error(signInResponse.data.title)
            yield put(signinFailure(signInResponse.data))
        } else {
            localStorage.setItem('loggedIn', true)

            localStorage.setItem('rememberMe', signInResponse.data.rememberMe)
            localStorage.setItem('token', signInResponse.data.token)
            localStorage.setItem('role', signInResponse.data.userData.role)
            localStorage.setItem('isActive', signInResponse.data.userData.isActive)
            console.log("signInResponse.data.userData", signInResponse.data.userData)
            if (signInResponse.data.userData.isActive === false) {
                NotificationManager.error('User is blocked, Please contact admin.')
            }
            else {
                NotificationManager.success(signInResponse.data.title)
                yield put(signinSuccess(signInResponse.data))
                if (signInResponse.data.userData.role == 'doctor') {
                    payload.history.push(`/doctor`)
                } else if (signInResponse.data.userData.role == 'pharmacy') {
                    payload.history.push(`/pharmacy`)
                }
                else {
                    payload.history.push(`/user`)
                }
            }
        }
    } catch (error) {
        yield put(signinFailure(error))
    }
}

export function* signinAccount() {
    yield takeEvery(SIGNIN, signInFunction)
}

const getuserProfileRequest = (data) => {
    console.log('getuserProfileRequest', data)
    return axios.get('/users/profile', {
        headers: {
            'Content-Type': 'application/json',
            'token': localStorage.getItem('token')
        }
    }
    ).then(result => result)
        .catch(error => error)
}

function* getuserProfileFunction({ payload }) {
    const { history } = payload
    try {
        const getuserProfileResponse = yield call(getuserProfileRequest, payload.data)
        console.log('getuserProfileResponse', getuserProfileResponse, history)
        if (getuserProfileResponse.data.error) {

            NotificationManager.error(getuserProfileResponse.data.title)
            if (getuserProfileResponse.data.title === 'Authorization Required') {
                history.push('/categorySignin')
                yield put(signout())
            }
            yield put(getuserProfileDataFailure(getuserProfileResponse.data.detail))
        } else {
            if (getuserProfileResponse.data.detail.isActive === false) {
                NotificationManager.error('User is blocked, Please contact admin.')
                history.push('/categorySignin')
                yield put(signout())
            }
            else {
                yield put(getuserProfileDataSuccess(getuserProfileResponse.data))
            }
        }
    } catch (error) {
        yield put(getuserProfileDataFailure(error))
    }
}

export function* userProfile() {
    yield takeEvery(GETUSERPROFILEDATA, getuserProfileFunction)
}

const forgotPasswordRequest = (data) => {
    console.log('forgotPasswordRequest', data)
    return axios.post(`/users/forgotPassword`,
        {
            email: data.email,
            role: data.role,
        }
    ).then(result => result)
        .catch(error => error)
}

function* forgotPasswordFunction({ payload }) {
    const { history } = payload
    try {
        const forgotPasswordResponse = yield call(forgotPasswordRequest, payload.data)
        console.log("forgotPasswordResponse", forgotPasswordResponse.data)
        if (forgotPasswordResponse.data.error) {
            NotificationManager.error(forgotPasswordResponse.data.title)
            yield put(forgotPasswordFailure(forgotPasswordResponse.data))
        } else {
            NotificationManager.success(forgotPasswordResponse.data.title)
            yield put(forgotPasswordSuccess(forgotPasswordResponse.data))
            history.push(`/${localStorage.getItem('role')}`)
        }
    } catch (error) {
        yield put(forgotPasswordFailure(error))
    }
}

export function* forgotPassword() {
    yield takeEvery(FORGOT_PASSWORD, forgotPasswordFunction)
}

const resetPasswordRequest = (data) => {
    console.log('resetPasswordRequest', data)
    return axios.post(`/users/resetToken`,
        {
            password: data.password,
            token: data.token
        }
    ).then(result => result)
        .catch(error => error)
}

function* resetPasswordFunction({ payload }) {
    const { history } = payload
    try {
        const resetPasswordResponse = yield call(resetPasswordRequest, payload.data)
        console.log("resetPasswordResponse", resetPasswordResponse)
        if (resetPasswordResponse.data.error) {
            NotificationManager.error(resetPasswordResponse.data.title)
            yield put(resetPasswordFailure(resetPasswordResponse.data))
        } else {
            NotificationManager.success(resetPasswordResponse.data.title)
            yield put(resetPasswordSuccess(resetPasswordResponse.data))
            history.push(`/categorySignin`)
        }
    } catch (error) {
        yield put(resetPasswordFailure(error))
    }
}

export function* resetPassword() {
    yield takeEvery(RESET_PASSWORD, resetPasswordFunction)
}

const verifyAccountRequest = (data) => {
    console.log('verifyAccountRequest', data)
    return axios.post(`/users/verify`,
        {
            password: data.password,
            token: data.token
        }
    ).then(result => result)
        .catch(error => error)
}

function* verifyAccountFunction({ payload }) {
    const { history } = payload
    try {
        const verifyAccountResponse = yield call(verifyAccountRequest, payload.data)
        console.log("verifyAccountResponse", verifyAccountResponse)
        if (verifyAccountResponse.data.error) {
            NotificationManager.error(verifyAccountResponse.data.title)
            yield put(verifyAccountFailure(verifyAccountResponse.data))
        } else {
            NotificationManager.success(verifyAccountResponse.data.title)
            yield put(verifyAccountSuccess(verifyAccountResponse.data))
            history.push(`/categorySignin`)
        }
    } catch (error) {
        yield put(verifyAccountFailure(error))
    }
}

export function* verifyAccount() {
    yield takeEvery(VERIFY_ACCOUNT, verifyAccountFunction)
}

const cropImageRequest = (data) => {
    console.log('cropImageRequest', data)

    return axios.post('/users/uploadProfile', data, {
        headers: {
            'Content-Type': 'application/json',
            'token': localStorage.getItem('token')
        }
    }
    ).then(result => result)
        .catch(error => error)
}

function* cropImageFunction({ payload }) {
    const { history } = payload
    try {
        const cropImageResponse = yield call(cropImageRequest, payload)
        console.log('cropImageResponse', cropImageResponse);

        if (cropImageResponse.data.error) {
            NotificationManager.error(cropImageResponse.data.title)
            if (cropImageResponse.data.title === 'Authorization Required') {
                history.push('/signin')
                yield put(signout())
            }
            yield put(cropImageFailure(cropImageResponse.data.detail))
        } else {
            NotificationManager.success(cropImageResponse.data.title);
            yield put(cropImageSuccess(cropImageResponse.data))
        }
    } catch (error) {
        yield put(cropImageFailure(error))
    }
}

export function* cropImage() {
    yield takeEvery(CROPIMAGE, cropImageFunction)
}


const getPharmaciesRequest = (data) => {
    console.log('getPharmaciesRequest', data)
    return axios.get('/users/pharmacies', {
        headers: {
            'Content-Type': 'application/json',
            'token': localStorage.getItem('token')
        }
    }
    ).then(result => result)
        .catch(error => error)
}

function* getPharmaciesFunction({ payload }) {
    const { history } = payload
    try {
        const getPharmaciesResponse = yield call(getPharmaciesRequest, payload.data)
        console.log('getPharmaciesResponse', getPharmaciesResponse, history)
        if (getPharmaciesResponse.data.error) {
            NotificationManager.error(getPharmaciesResponse.data.title)
            if (getPharmaciesResponse.data.title === 'Authorization Required') {
                history.push('/categorySignin')
                yield put(signout())
            }
            yield put(getPharmacyDataFailure(getPharmaciesResponse.data.detail))
        } else {
            yield put(getPharmacyDataSuccess(getPharmaciesResponse.data))
        }
    } catch (error) {
        yield put(getPharmacyDataFailure(error))
    }
}

export function* getPharmacies() {
    yield takeEvery(GET_PHARMACY, getPharmaciesFunction)
}


const getPharmacyDetailRequest = (data) => {
    console.log('getPharmacyDetailRequest', data)

    /*  axios.get(`/users/pharmacyDetails/${this.props.match.params.id}`, {
         headers: {
             'Content-Type': 'application/json',
             'token': localStorage.getItem('token')
         }
     }) */
    return axios.get(`/users/pharmacyDetails/${data.id}`, {
        headers: {
            'Content-Type': 'application/json',
            'token': localStorage.getItem('token')
        }
    }
    ).then(result => result)
        .catch(error => error)
}

function* getPharmacyDetailFunction({ payload }) {
    const { history } = payload
    try {
        const getPharmacyDetailResponse = yield call(getPharmacyDetailRequest, payload.data)
        console.log('getPharmacyDetailResponse', getPharmacyDetailResponse, history)
        if (getPharmacyDetailResponse.data.error) {
            NotificationManager.error(getPharmacyDetailResponse.data.title)
            if (getPharmacyDetailResponse.data.title === 'Authorization Required') {
                history.push('/categorySignin')
                yield put(signout())
            }
            yield put(getPharmacyDetailFailure(getPharmacyDetailResponse.data.detail))
        } else {
            yield put(getPharmacyDetailSuccess(getPharmacyDetailResponse.data))
        }
    } catch (error) {
        yield put(getPharmacyDetailFailure(error))
    }
}

export function* getPharmacyDetail() {
    yield takeEvery(GET_PHARMACY_DETAIL, getPharmacyDetailFunction)
}


const searchPharmacyRequest = (data) => {
    console.log('searchPharmacyRequest', data)
    return axios.get(`/users/searchPharmacies/${data.searchText}`, {
        headers: {
            'Content-Type': 'application/json',
            'token': localStorage.getItem('token')
        }
    }
    ).then(result => result)
        .catch(error => error)
}

function* searchPharmacyFunction({ payload }) {
    const { history } = payload
    try {
        const searchPharmacyResponse = yield call(searchPharmacyRequest, payload.data)
        console.log('searchPharmacyResponse', searchPharmacyResponse, history)
        if (searchPharmacyResponse.data.error) {
            NotificationManager.error(searchPharmacyResponse.data.title)
            if (searchPharmacyResponse.data.title === 'Authorization Required') {
                history.push('/categorySignin')
                yield put(signout())
            }
            yield put(searchPharmacyDataFailure(searchPharmacyResponse.data.detail))
        } else {
            yield put(searchPharmacyDataSuccess(searchPharmacyResponse.data))
        }
    } catch (error) {
        yield put(searchPharmacyDataFailure(error))
    }
}

export function* searchPharmacy() {
    yield takeEvery(SEARCH_PHARMACY, searchPharmacyFunction)
}

const getPatientsRequest = (data) => {
    console.log('getPatientsRequest', data)
    return axios.get('/users/patients', {
        headers: {
            'Content-Type': 'application/json',
            'token': localStorage.getItem('token')
        }
    }
    ).then(result => result)
        .catch(error => error)
}

function* getPatientsFunction({ payload }) {
    const { history } = payload
    try {
        const getPatientsResponse = yield call(getPatientsRequest, payload.data)
        console.log('getPatientsResponse', getPatientsResponse, history)
        if (getPatientsResponse.data.error) {
            NotificationManager.error(getPatientsResponse.data.title)
            if (getPatientsResponse.data.title === 'Authorization Required') {
                history.push('/categorySignin')
                yield put(signout())
            }
            yield put(getPatientDataFailure(getPatientsResponse.data.detail))
        } else {
            yield put(getPatientDataSuccess(getPatientsResponse.data))
        }
    } catch (error) {
        yield put(getPatientDataFailure(error))
    }
}

export function* getPatients() {
    yield takeEvery(GET_PATIENTS, getPatientsFunction)
}

const connectPharmacyRequest = (data) => {
    console.log('connectPharmacyRequest', data)
    let postData = {
        user: data.user,
        pharmacy: data.pharmacy,
        connect: data.connect,
        doctor: data.doctor
    }
    return axios.post('/users/connectPharmacy', postData, {
        headers: {
            'Content-Type': 'application/json',
            'token': localStorage.getItem('token')
        }
    }
    ).then(result => result)
        .catch(error => error)
}

function* connectPharmacyFunction({ payload }) {
    const { history } = payload
    console.log('history', history)
    try {
        const connectPharmacyResponse = yield call(connectPharmacyRequest, payload.data)
        console.log('connectPharmacyResponse', connectPharmacyResponse)
        if (connectPharmacyResponse.data.error) {
            NotificationManager.error(connectPharmacyResponse.data.title)
            if (connectPharmacyResponse.data.title === 'Authorization Required') {
                history.push(`/categorySignin`)
                yield put(signout())
            }
            yield put(connectPharmacyFailure(connectPharmacyResponse.data))
        } else {
            NotificationManager.success(connectPharmacyResponse.data.title)
            yield put(connectPharmacySuccess(connectPharmacyResponse.data))
            payload.history.push(`/${localStorage.getItem('role')}/pharmacyDetails/${connectPharmacyResponse.data.connectData.pharmacy}`)
        }
    } catch (error) {
        yield put(connectPharmacyFailure(error))
    }
}

export function* connectPharmacy() {
    yield takeEvery(CONNECT_PHARMACY, connectPharmacyFunction)
}

const connectDoctorRequest = (data) => {
    console.log('connectDoctorRequest', data)
    let postData = {
        user: data.user,
        doctor: data.doctor,
        connect: data.connect
    }
    return axios.post('/users/connectDoctor', postData, {
        headers: {
            'Content-Type': 'application/json',
            'token': localStorage.getItem('token')
        }
    }
    ).then(result => result)
        .catch(error => error)
}

function* connectDoctorFunction({ payload }) {
    const { history } = payload
    console.log('history', history)
    try {
        const connectDoctorResponse = yield call(connectDoctorRequest, payload.data)
        console.log('connectDoctorResponse', connectDoctorResponse)
        if (connectDoctorResponse.data.error) {
            NotificationManager.error(connectDoctorResponse.data.title)
            if (connectDoctorResponse.data.title === 'Authorization Required') {
                history.push(`/categorySignin`)
                yield put(signout())
            }
            yield put(connectDoctorFailure(connectDoctorResponse.data))
        } else {
            NotificationManager.success(connectDoctorResponse.data.title)
            yield put(connectDoctorSuccess(connectDoctorResponse.data))
            payload.history.push(`/${localStorage.getItem('role')}/doctorDetails/${connectDoctorResponse.data.connectData.doctor}`)
        }
    } catch (error) {
        yield put(connectDoctorFailure(error))
    }
}

export function* connectDoctor() {
    yield takeEvery(CONNECT_DOCTOR, connectDoctorFunction)
}

const getConnectsRequest = (data) => {
    console.log('getConnectsRequest', data)
    return axios.get('/users/connects', {
        headers: {
            'Content-Type': 'application/json',
            'token': localStorage.getItem('token')
        }
    }
    ).then(result => result)
        .catch(error => error)
}

function* getConnectsFunction({ payload }) {
    const { history } = payload
    try {
        const getConnectsResponse = yield call(getConnectsRequest, payload.data)
        console.log('getConnectsResponse', getConnectsResponse, history)
        if (getConnectsResponse.data.error) {
            NotificationManager.error(getConnectsResponse.data.title)
            if (getConnectsResponse.data.title === 'Authorization Required') {
                history.push('/categorySignin')
                yield put(signout())
            }
            yield put(getConnectDataFailure(getConnectsResponse.data.detail))
        } else {
            yield put(getConnectDataSuccess(getConnectsResponse.data))
        }
    } catch (error) {
        yield put(getConnectDataFailure(error))
    }
}

export function* getConnects() {
    yield takeEvery(GET_CONNECTS, getConnectsFunction)
}

const getPatientPharmaciesRequest = (data) => {
    console.log('getPatientPharmaciesRequest', data)
    return axios.get('/users/patientPharmacies', {
        headers: {
            'Content-Type': 'application/json',
            'token': localStorage.getItem('token')
        }
    }
    ).then(result => result)
        .catch(error => error)
}

function* getPatientPharmaciesFunction({ payload }) {
    const { history } = payload
    try {
        const getPatientPharmaciesResponse = yield call(getPatientPharmaciesRequest, payload.data)
        console.log('getPatientPharmaciesResponse', getPatientPharmaciesResponse, history)
        if (getPatientPharmaciesResponse.data.error) {
            NotificationManager.error(getPatientPharmaciesResponse.data.title)
            if (getPatientPharmaciesResponse.data.title === 'Authorization Required') {
                history.push('/categorySignin')
                yield put(signout())
            }
            yield put(getPatientPharmacyDataFailure(getPatientPharmaciesResponse.data.detail))
        } else {
            yield put(getPatientPharmacyDataSuccess(getPatientPharmaciesResponse.data))
        }
    } catch (error) {
        yield put(getPatientPharmacyDataFailure(error))
    }
}

export function* getPatientPharmacies() {
    yield takeEvery(GET_PATIENT_PHARMACIES, getPatientPharmaciesFunction)
}

/******************************************************************* */
export default function* rootSaga() {
    yield all([
        fork(signUpAccount),
        fork(signinAccount),
        fork(cropImage),
        fork(forgotPassword),
        fork(resetPassword),
        fork(userProfile),
        fork(getPharmacies),
        fork(getPatients),
        fork(connectPharmacy),
        fork(connectDoctor),
        fork(getConnects),
        fork(getPatientPharmacies),
        fork(searchPharmacy),
        fork(getPharmacyDetail)
    ]);
}
