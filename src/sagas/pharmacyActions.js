import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import baseUrl from '../constant/config';
import { NotificationManager } from 'react-notifications';
import {
    CONNECTED_PHARMACIES
} from '../actions/types';
import {
    signout,
    connectedPharmaciesDataSuccess,
    connectedPharmaciesDataFailure
} from '../actions';

import axios from '../constant/axios';

const connectedPharmaciesRequest = (data) => {
    console.log('connectedPharmaciesRequest', data)
    return axios.get(`/users/connectedPharmacies`, {
        headers: {
            'Content-Type': 'application/json',
            'token': localStorage.getItem('token')
        }
    }
    ).then(result => result)
        .catch(error => error)
}

function* connectedPharmaciesFunction({ payload }) {
    const { history } = payload
    try {
        const connectedPharmaciesResponse = yield call(connectedPharmaciesRequest, payload.data)
        console.log('connectedPharmaciesResponse', connectedPharmaciesResponse, history)
        if (connectedPharmaciesResponse.data.error) {
            NotificationManager.error(connectedPharmaciesResponse.data.title)
            if (connectedPharmaciesResponse.data.title === 'Authorization Required') {
                history.push('/categorySignin')
                yield put(signout())
            }
            yield put(connectedPharmaciesDataFailure(connectedPharmaciesResponse.data.detail))
        } else {
            yield put(connectedPharmaciesDataSuccess(connectedPharmaciesResponse.data))
        }
    } catch (error) {
        yield put(connectedPharmaciesDataFailure(error))
    }
}

export function* connectedPharmacies() {
    yield takeEvery(CONNECTED_PHARMACIES, connectedPharmaciesFunction)
}

export default function* rootSaga() {
    yield all([
        fork(connectedPharmacies),
    ])
}