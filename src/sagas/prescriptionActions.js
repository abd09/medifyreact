import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import baseUrl from '../constant/config';
import { NotificationManager } from 'react-notifications';
import {
    ADD_PRESCRIPTION,
    GET_PRESCRIPTIONS,
    GET_PATIENT_PRESCRIPTIONS,
    GET_DOCTOR_PRESCRIPTIONS,
    EDIT_PRESCRIPTION,
    FILTER_PRESCRIPTION
} from '../actions/types';
import {
    addPrescriptionSuccess,
    addPrescriptionFailure,
    getPrescriptionDataSuccess,
    getPrescriptionDataFailure,
    getPatientPrescriptionSuccess,
    getPatientPrescriptionFailure,
    getDoctorPrescriptionSuccess,
    getDoctorPrescriptionFailure,
    editPrescriptionSuccess,
    editPrescriptionFailure,
    filterPrescriptionSuccess,
    filterPrescriptionFailure,
    signout,
} from '../actions';

import axios from '../constant/axios';

const addPrescriptionRequest = (data) => {
    console.log('addPrescriptionRequest', data)
    let postData = {
        title: data.title,
        note: data.note,
        items: data.items,
        images: data.images,
        patient: data.patient,
        pharmacy: data.pharmacy,
        category: data.category,
    }
    return axios.post('/users/addPrescription', postData, {
        headers: {
            'Content-Type': 'application/json',
            'token': localStorage.getItem('token')
        }
    }
    ).then(result => result)
        .catch(error => error)
}

function* addPrescriptionFunction({ payload }) {
    const { history } = payload
    console.log('history', history)
    try {
        const addPrescriptionResponse = yield call(addPrescriptionRequest, payload.data)
        console.log('addPrescriptionResponse', addPrescriptionResponse)
        if (addPrescriptionResponse.data.error) {
            NotificationManager.error(addPrescriptionResponse.data.title)
            if (addPrescriptionResponse.data.title === 'Authorization Required') {
                history.push(`/categorySignin`)
                yield put(signout())
            }
            yield put(addPrescriptionFailure(addPrescriptionResponse.data))
        } else {
            NotificationManager.success(addPrescriptionResponse.data.title)
            yield put(addPrescriptionSuccess(addPrescriptionResponse.data))
            payload.history.push(`/${localStorage.getItem('role')}/prescriptions`)
        }
    } catch (error) {
        yield put(addPrescriptionFailure(error))
    }
}

export function* addPrescription() {
    yield takeEvery(ADD_PRESCRIPTION, addPrescriptionFunction)
}

const editPrescriptionRequest = (data) => {
    console.log('editPrescriptionRequest', data)
    let postData = {
        title: data.title,
        note: data.note,
        otherDetails: data.otherDetails,
        orderQty: data.orderQty,
        id: data.id,
        category: data.category
    }
    return axios.post('/users/editPrescription', postData, {
        headers: {
            'Content-Type': 'application/json',
            'token': localStorage.getItem('token')
        }
    }
    ).then(result => result)
        .catch(error => error)
}

function* editPrescriptionFunction({ payload }) {
    const { history } = payload
    console.log('history', history)
    try {
        const editPrescriptionResponse = yield call(editPrescriptionRequest, payload.data)
        console.log('editPrescriptionResponse', editPrescriptionResponse)
        if (editPrescriptionResponse.data.error) {
            NotificationManager.error(editPrescriptionResponse.data.title)
            if (editPrescriptionResponse.data.title === 'Authorization Required') {
                history.push(`/categorySignin`)
                yield put(signout())
            }
            yield put(editPrescriptionFailure(editPrescriptionResponse.data))
        } else {
            NotificationManager.success(editPrescriptionResponse.data.title)
            yield put(editPrescriptionSuccess(editPrescriptionResponse.data))
            payload.history.push(`/${localStorage.getItem('role')}/prescriptions`)
        }
    } catch (error) {
        yield put(editPrescriptionFailure(error))
    }
}

export function* editPrescription() {
    yield takeEvery(EDIT_PRESCRIPTION, editPrescriptionFunction)
}

const getPrescriptionsRequest = (data) => {
    console.log('getPrescriptionsRequest', data)
    return axios.get('/users/prescriptions', {
        headers: {
            'Content-Type': 'application/json',
            'token': localStorage.getItem('token')
        }
    }
    ).then(result => result)
        .catch(error => error)
}

function* getPrescriptionsFunction({ payload }) {
    const { history } = payload
    try {
        const getPrescriptionsResponse = yield call(getPrescriptionsRequest, payload.data)
        console.log('getPrescriptionsResponse', getPrescriptionsResponse, history)
        if (getPrescriptionsResponse.data.error) {
            NotificationManager.error(getPrescriptionsResponse.data.title)
            if (getPrescriptionsResponse.data.title === 'Authorization Required') {
                history.push('/categorySignin')
                yield put(signout())
            }
            yield put(getPrescriptionDataFailure(getPrescriptionsResponse.data.detail))
        } else {
            yield put(getPrescriptionDataSuccess(getPrescriptionsResponse.data))
        }
    } catch (error) {
        yield put(getPrescriptionDataFailure(error))
    }
}

export function* getPrescriptions() {
    yield takeEvery(GET_PRESCRIPTIONS, getPrescriptionsFunction)
}

const getPatientPrescriptionsRequest = (data) => {
    console.log('getPatientPrescriptionsRequest', data)
    return axios.get(`/users/patientPrescriptions/${data.patientId}`, {
        headers: {
            'Content-Type': 'application/json',
            'token': localStorage.getItem('token')
        }
    }
    ).then(result => result)
        .catch(error => error)
}

function* getPatientPrescriptionsFunction({ payload }) {
    const { history } = payload
    try {
        const getPatientPrescriptionsResponse = yield call(getPatientPrescriptionsRequest, payload.data)
        console.log('getPatientPrescriptionsResponse', getPatientPrescriptionsResponse, history)
        if (getPatientPrescriptionsResponse.data.error) {
            NotificationManager.error(getPatientPrescriptionsResponse.data.title)
            if (getPatientPrescriptionsResponse.data.title === 'Authorization Required') {
                history.push('/categorySignin')
                yield put(signout())
            }
            yield put(getPatientPrescriptionFailure(getPatientPrescriptionsResponse.data.detail))
        } else {
            yield put(getPatientPrescriptionSuccess(getPatientPrescriptionsResponse.data))
        }
    } catch (error) {
        yield put(getPatientPrescriptionFailure(error))
    }
}

export function* getPatientPrescriptions() {
    yield takeEvery(GET_PATIENT_PRESCRIPTIONS, getPatientPrescriptionsFunction)
}


const getDoctorPrescriptionsRequest = (data) => {
    console.log('getDoctorPrescriptionsRequest', data)
    return axios.get(`/users/doctorPrescriptions`, {
        headers: {
            'Content-Type': 'application/json',
            'token': localStorage.getItem('token')
        }
    }
    ).then(result => result)
        .catch(error => error)
}

function* getDoctorPrescriptionsFunction({ payload }) {
    const { history } = payload
    try {
        const getDoctorPrescriptionsResponse = yield call(getDoctorPrescriptionsRequest, payload.data)
        console.log('getDoctorPrescriptionsResponse', getDoctorPrescriptionsResponse, history)
        if (getDoctorPrescriptionsResponse.data.error) {
            NotificationManager.error(getDoctorPrescriptionsResponse.data.title)
            if (getDoctorPrescriptionsResponse.data.title === 'Authorization Required') {
                history.push('/categorySignin')
                yield put(signout())
            }
            yield put(getDoctorPrescriptionFailure(getDoctorPrescriptionsResponse.data.detail))
        } else {
            yield put(getDoctorPrescriptionSuccess(getDoctorPrescriptionsResponse.data))
        }
    } catch (error) {
        yield put(getDoctorPrescriptionFailure(error))
    }
}

export function* getDoctorPrescriptions() {
    yield takeEvery(GET_DOCTOR_PRESCRIPTIONS, getDoctorPrescriptionsFunction)
}

const filterPrescriptionRequest = (data) => {
    console.log('filterPrescriptionRequest', data.value)
    return axios.get(`/users/sortByCategory/${data.value}`, {
        headers: {
            'Content-Type': 'application/json',
            'token': localStorage.getItem('token')
        }
    }
    ).then(result => result)
        .catch(error => error)
}

function* filterPrescriptionFunction({ payload }) {
    const { history } = payload
    try {
        const filterPrescriptionResponse = yield call(filterPrescriptionRequest, payload.data)
        console.log('filterPrescriptionResponse', filterPrescriptionResponse, history)
        if (filterPrescriptionResponse.data.error) {
            NotificationManager.error(filterPrescriptionResponse.data.title)
            if (filterPrescriptionResponse.data.title === 'Authorization Required') {
                history.push('/categorySignin')
                yield put(signout())
            }
            yield put(filterPrescriptionFailure(filterPrescriptionResponse.data.detail))
        } else {
            yield put(filterPrescriptionSuccess(filterPrescriptionResponse.data))
            console.log('filterPrescriptionSuccess', filterPrescriptionResponse.data)
        }
    } catch (error) {
        yield put(filterPrescriptionFailure(error))
    }
}

export function* filterPrescription() {
    yield takeEvery(FILTER_PRESCRIPTION, filterPrescriptionFunction)
}

export default function* rootSaga() {
    yield all([
        fork(addPrescription),
        fork(editPrescription),
        fork(getPrescriptions),
        fork(getPatientPrescriptions),
        fork(filterPrescription),
        fork(getDoctorPrescriptions),
    ])
}