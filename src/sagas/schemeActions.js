import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import baseUrl from '../constant/config';
import { NotificationManager } from 'react-notifications';
import {
    GET_SCHEMES,
    ADD_SCHEME
} from '../actions/types';
import {
    getSchemesSuccess,
    getSchemesFailure,
    addSchemeSuccess,
    addSchemeFailure,
    signout,
} from '../actions';

import axios from '../constant/axios';


const getSchemesRequest = (data) => {
    console.log('getSchemesRequest', data)
    return axios.get(`/users/getSchemes/${data.id}`, {
        headers: {
            'Content-Type': 'application/json',
            'token': localStorage.getItem('token')
        }
    }
    ).then(result => result)
        .catch(error => error)
}

function* getSchemesFunction({ payload }) {
    const { history } = payload
    try {
        const getSchemesResponse = yield call(getSchemesRequest, payload.data)
        console.log('getSchemesResponse', getSchemesResponse, history)
        if (getSchemesResponse.data.error) {
            NotificationManager.error(getSchemesResponse.data.title)
            if (getSchemesResponse.data.title === 'Authorization Required') {
                yield put(signout())
            }
            yield put(getSchemesFailure(getSchemesResponse.data.detail))
        } else {
            yield put(getSchemesSuccess(getSchemesResponse.data))
        }
    } catch (error) {
        yield put(getSchemesFailure(error))
    }
}

export function* getSchemes() {
    yield takeEvery(GET_SCHEMES, getSchemesFunction)
}


const addSchemeRequest = (data) => {
    console.log('addSchemeRequest', data)
    let postData = {
        title: data.title,
        pharmacy: data.pharmacy
    }
    return axios.post('/users/addScheme', postData, {
        headers: {
            'Content-Type': 'application/json',
            'token': localStorage.getItem('token')
        }
    }
    ).then(result => result)
        .catch(error => error)
}

function* addSchemeFunction({ payload }) {
    const { history } = payload
    console.log('history', history)
    try {
        const addSchemeResponse = yield call(addSchemeRequest, payload.data)
        console.log('addSchemeResponse', addSchemeResponse)
        if (addSchemeResponse.data.error) {
            NotificationManager.error(addSchemeResponse.data.title)
            if (addSchemeResponse.data.title === 'Authorization Required') {
                history.push(`/categorySignin`)
                yield put(signout())
            }
            yield put(addSchemeFailure(addSchemeResponse.data))
        } else {
            NotificationManager.success(addSchemeResponse.data.title)
            yield put(addSchemeSuccess(addSchemeResponse.data))
            payload.history.push(`/${localStorage.getItem('role')}/schemes`)
        }
    } catch (error) {
        yield put(addSchemeFailure(error))
    }
}

export function* addScheme() {
    yield takeEvery(ADD_SCHEME, addSchemeFunction)
}

export default function* rootSaga() {
    yield all([
        fork(getSchemes),
        fork(addScheme),
    ])
}