import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import baseUrl from '../constant/config';
import { NotificationManager } from 'react-notifications';
import {
  GET_REVIEW,
  ADD_REVIEW,
  GET_REVIEW_DETAIL
} from '../actions/types';
import {
    getReviewSuccess,
    getReviewFailure,
    addReviewSuccess,
    addReviewFailure,
    getReviewDetailSuccess,
    getReviewDetailFailure,
    signout,
} from '../actions';

import axios from '../constant/axios';

const addReviewRequest = (data) => {
    console.log('addReviewRequest', data)
    return axios.post('/review/addReview', data, {
        headers: {
            'Content-Type': 'application/json',
            'token': localStorage.getItem('token')
        }
    }
    ).then(result => result)
        .catch(error => error)
}

function* addReviewFunction({ payload }) {
    const { history } = payload
    console.log('history', history)
    try {
        const addReviewResponse = yield call(addReviewRequest, payload.data)
        console.log('addReviewResponse', addReviewResponse)
        if (addReviewResponse.data.error) {
            NotificationManager.error(addReviewResponse.data.title)
            if (addReviewResponse.data.title === 'Authorization Required') {
                // history.push(`/categorySignin`)
                yield put(signout())
            }
            yield put(addReviewFailure(addReviewResponse.data))
        } else {
            NotificationManager.success(addReviewResponse.data.title)
            yield put(addReviewSuccess(addReviewResponse.data))
            // payload.history.push(`/${localStorage.getItem('role')}/prescriptions`)
        }
    } catch (error) {
        yield put(addReviewFailure(error))
    }
}

export function* addReview() {
    yield takeEvery(ADD_REVIEW, addReviewFunction)
}

const getReviewRequest = (data) => {
    console.log('getReviewRequest', data)
    return axios.get('/review/getReview', {
        headers: {
            'Content-Type': 'application/json',
            'token': localStorage.getItem('token')
        }
    }
    ).then(result => result)
        .catch(error => error)
}

function* getReviewFunction({ payload }) {
    const { history } = payload
    try {
        const getReviewResponse = yield call(getReviewRequest, payload.data)
        console.log('getReviewResponse', getReviewResponse, history)
        if (getReviewResponse.data.error) {
            NotificationManager.error(getReviewResponse.data.title)
            if (getReviewResponse.data.title === 'Authorization Required') {
                yield put(signout())
            }
            yield put(getReviewFailure(getReviewResponse.data.detail))
        } else {
            yield put(getReviewSuccess(getReviewResponse.data))
        }
    } catch (error) {
        yield put(getReviewFailure(error))
    }
}

export function* getReview() {
    yield takeEvery(GET_REVIEW, getReviewFunction)
}

const getReviewDetailRequest = (data) => {
    console.log('getReviewDetailRequest', data)
    return axios.post('/review/getReviewDetail', {to : data}, {
        headers: {
            'Content-Type': 'application/json',
            'token': localStorage.getItem('token')
        }
    }
    ).then(result => result)
        .catch(error => error)
}

function* getReviewDetailFunction({ payload }) {
    const { history } = payload
    try {
        const getReviewDetailResponse = yield call(getReviewDetailRequest, payload.data)
        console.log('getReviewDetailResponse', getReviewDetailResponse, history)
        if (getReviewDetailResponse.data.error) {
            NotificationManager.error(getReviewDetailResponse.data.title)
            if (getReviewDetailResponse.data.title === 'Authorization Required') {
                yield put(signout())
            }
            yield put(getReviewDetailFailure(getReviewDetailResponse.data.detail))
        } else {
            yield put(getReviewDetailSuccess(getReviewDetailResponse.data));
        }
    } catch (error) {
        yield put(getReviewDetailFailure(error))
    }
}

export function* getReviewDetail() {
    yield takeEvery(GET_REVIEW_DETAIL, getReviewDetailFunction)
}

export default function* rootSaga() {
    yield all([
        fork(addReview),
        fork(getReview),
        fork(getReviewDetail),
    ])
}