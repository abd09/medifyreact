import { all, call, fork, put, takeEvery } from 'redux-saga/effects';
import baseUrl from '../constant/config';
import { NotificationManager } from 'react-notifications';
import {
    ADD_ORDER,
    GET_ORDERS,
    GET_PHARMACY_ORDERS,
    UPDATE_ORDER,
} from '../actions/types';
import {
    addOrderSuccess,
    addOrderFailure,
    getOrdersSuccess,
    getOrdersFailure,
    getPharmacyOrdersSuccess,
    getPharmacyOrdersFailure,
    updateOrderSuccess,
    updateOrderFailure,
    signout,
} from '../actions';

import axios from '../constant/axios';


const getOrdersRequest = (data) => {
    console.log('getOrdersRequest', data)
    return axios.get('/users/orders', {
        headers: {
            'Content-Type': 'application/json',
            'token': localStorage.getItem('token')
        }
    }
    ).then(result => result)
        .catch(error => error)
}

function* getOrdersFunction({ payload }) {
    const { history } = payload
    try {
        const getOrdersResponse = yield call(getOrdersRequest, payload.data)
        console.log('getOrdersResponse', getOrdersResponse, history)
        if (getOrdersResponse.data.error) {
            NotificationManager.error(getOrdersResponse.data.title)
            if (getOrdersResponse.data.title === 'Authorization Required') {
                history.push('/categorySignin')
                yield put(signout())
            }
            yield put(getOrdersFailure(getOrdersResponse.data.detail))
        } else {
            yield put(getOrdersSuccess(getOrdersResponse.data))
        }
    } catch (error) {
        yield put(getOrdersFailure(error))
    }
}

export function* getOrders() {
    yield takeEvery(GET_ORDERS, getOrdersFunction)
}

const getPharmacyOrdersRequest = (data) => {
    console.log('getPharmacyOrdersRequest', data)
    return axios.get('/users/pharmacyOrders', {
        headers: {
            'Content-Type': 'application/json',
            'token': localStorage.getItem('token')
        }
    }
    ).then(result => result)
        .catch(error => error)
}

function* getPharmacyOrdersFunction({ payload }) {
    const { history } = payload
    try {
        const getPharmacyOrdersResponse = yield call(getPharmacyOrdersRequest, payload.data)
        console.log('getPharmacyOrdersResponse', getPharmacyOrdersResponse, history)
        if (getPharmacyOrdersResponse.data.error) {
            NotificationManager.error(getPharmacyOrdersResponse.data.title)
            if (getPharmacyOrdersResponse.data.title === 'Authorization Required') {
                history.push('/categorySignin')
                yield put(signout())
            }
            yield put(getPharmacyOrdersFailure(getPharmacyOrdersResponse.data.detail))
        } else {
            yield put(getPharmacyOrdersSuccess(getPharmacyOrdersResponse.data))
        }
    } catch (error) {
        yield put(getPharmacyOrdersFailure(error))
    }
}

export function* getPharmacyOrders() {
    yield takeEvery(GET_PHARMACY_ORDERS, getPharmacyOrdersFunction)
}

const addOrderRequest = (data) => {
    console.log('addOrderRequest', data)
    let postData = {
        prescription: data.prescription,
        pharmacy: data.pharmacy,
        note: data.note,
        items: data.items
    }
    return axios.post('/users/addOrder', postData, {
        headers: {
            'Content-Type': 'application/json',
            'token': localStorage.getItem('token')
        }
    }
    ).then(result => result)
        .catch(error => error)
}

function* addOrderFunction({ payload }) {
    const { history } = payload
    console.log('history', history)
    try {
        const addOrderResponse = yield call(addOrderRequest, payload.data)
        console.log('addOrderResponse', addOrderResponse)
        if (addOrderResponse.data.error) {
            NotificationManager.error(addOrderResponse.data.title)
            if (addOrderResponse.data.title === 'Authorization Required') {
                history.push(`/categorySignin`)
                yield put(signout())
            }
            yield put(addOrderFailure(addOrderResponse.data))
        } else {
            NotificationManager.success(addOrderResponse.data.title)
            yield put(addOrderSuccess(addOrderResponse.data))
            payload.history.push(`/${localStorage.getItem('role')}/orders`)
        }
    } catch (error) {
        yield put(addOrderFailure(error))
    }
}

export function* addOrder() {
    yield takeEvery(ADD_ORDER, addOrderFunction)
}

const updateOrderRequest = (data) => {
    console.log('updateOrderRequest', data)
    let postData = {
        bills: data.bills ? data.bills : [],
        amount: data.amount ? data.amount : '',
        time: data.time ? data.time : '',
        orderId: data.orderId,
        status: data.status,
        reason: data.reason,
    }
    return axios.post('/users/updateOrder', postData, {
        headers: {
            'Content-Type': 'application/json',
            'token': localStorage.getItem('token')
        }
    }
    ).then(result => result)
        .catch(error => error)
}

function* updateOrderFunction({ payload }) {
    const { history } = payload
    console.log('history', history)
    try {
        const updateOrderResponse = yield call(updateOrderRequest, payload.data)
        console.log('updateOrderResponse', updateOrderResponse)
        if (updateOrderResponse.data.error) {
            NotificationManager.error(updateOrderResponse.data.title)
            if (updateOrderResponse.data.title === 'Authorization Required') {
                history.push(`/categorySignin`)
                yield put(signout())
            }
            yield put(updateOrderFailure(updateOrderResponse.data))
        } else {
            NotificationManager.success(updateOrderResponse.data.title)
            yield put(updateOrderSuccess(updateOrderResponse.data))
            payload.history.push(`/${localStorage.getItem('role')}/orders`)
        }
    } catch (error) {
        yield put(updateOrderFailure(error))
    }
}

export function* updateOrder() {
    yield takeEvery(UPDATE_ORDER, updateOrderFunction)
}

export default function* rootSaga() {
    yield all([
        fork(getOrders),
        fork(addOrder),
        fork(updateOrder),
        fork(getPharmacyOrders),
    ])
}