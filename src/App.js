import React, { Component } from 'react';
import { Route, Redirect } from 'react-router-dom';
import { NotificationContainer } from 'react-notifications';
import './App.css';

import Signup from './routes/signup'
import Signin from './routes/signin';
import ForgotPassword from './routes/forgotPassword';
import ResetPassword from './routes/resetPassword';
import SignupCategory from './routes/categorySignup'
import SigninCategory from './routes/categorySignin'
import ForgotCategory from './routes/categoryForgot';
import UserComponent from './components/UserComponent';
import ChatComponent from './components/ChatComponent';
import PharmacyComponent from './components/PharmacyComponent';
import DoctorComponent from './components/DoctorComponent';

const AdminInitialPath = ({ component: Comp, loggedIn, path, ...rest }) => {
  console.log('AdminInitialPath called', loggedIn)
  return (
    <Route
      path={path}
      {...rest}
      render={props => {
        return loggedIn ? <Comp {...props} /> : <Redirect to="/categorySignin" />;
      }}
    />
  );
}

class App extends Component {
  state = {
    loggedIn: localStorage.getItem('loggedIn'),
    role: localStorage.getItem('role')
  };
  render() {
    let isLoggedIn = true;
    const { state = {} } = this.props.location;
    console.log('isLoggedIn', this.state.loggedIn)
    console.log('this.props', this.props)
    console.log('this.state.role', this.state.role);
    console.log('this.props.location.pathname', this.props.location.pathname);

    if (this.props.location.pathname === '/' || this.props.location.pathname === '/signin') {
      console.log("iffffffffffffffff")
      if (this.state.loggedIn === null) {
        return (<Redirect to={'/categorySignin'} />);
      } else {
        if (this.state.role === 'doctor') {
          return (<Redirect to={'/doctor'} />);
        } else if (this.state.role === 'pharmacy') {
          return (<Redirect to={'/pharmacy'} />);
        } else {
          return (<Redirect to={'/user'} />);
        }
      }
    }
    return (
      <div className="App">
        <NotificationContainer />
        <AdminInitialPath path={`/user`} loggedIn={localStorage.getItem("loggedIn")} component={UserComponent} />
        <AdminInitialPath path={`/pharmacy`} loggedIn={localStorage.getItem("loggedIn")} component={PharmacyComponent} />
        <AdminInitialPath path={`/doctor`} loggedIn={localStorage.getItem("loggedIn")} component={DoctorComponent} />
        <AdminInitialPath path={`/chat`} loggedIn={localStorage.getItem("loggedIn")} component={ChatComponent} />
        <Route path={`/signup/:category`} component={Signup} />
        <Route path={`/signin/:category`} component={Signin} />
        <Route path="/forgotPassword/:category" component={ForgotPassword} />
        <Route path="/category" exact component={SignupCategory} />
        <Route path="/forgotPassword" exact component={ForgotCategory} />
        <Route path="/categorySignin" component={SigninCategory} />
        <Route path={`/reset/:token`} component={ResetPassword} />
      </div>
    );
  }
}

export default App;
