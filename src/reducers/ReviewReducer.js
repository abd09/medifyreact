import {

    GET_REVIEW,
    GET_REVIEW_FAILURE,
    GET_REVIEW_SUCCESS,
    ADD_REVIEW,
    ADD_REVIEW_SUCCESS,
    ADD_REVIEW_FAILURE,
    GET_REVIEW_DETAIL,
    GET_REVIEW_DETAIL_SUCCESS,
    GET_REVIEW_DETAIL_FAILURE,
} from '../actions/types'

const LOAD = 'redux-form-examples/account/LOAD'

const INIT_STATE = {
    loggedIn: localStorage.getItem('loggedIn') ? true : false,
    loading: false,
    reviewList: [],
    reviewDetail: {}
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_REVIEW:
            return { ...state, loading: true }

        case GET_REVIEW_SUCCESS:
            return { ...state, reviewList: action.payload.detail, loading: false }

        case GET_REVIEW_FAILURE:
            return { ...state, loading: false }

        case ADD_REVIEW:
            return { ...state, loading: true }

        case ADD_REVIEW_SUCCESS:
            return { ...state, reviewList: action.payload.detail, loading: false }

        case ADD_REVIEW_FAILURE:
            return { ...state, loading: false }

        case GET_REVIEW_DETAIL:
            return { ...state, loading: true }

        case GET_REVIEW_DETAIL_SUCCESS:
            return { ...state, reviewDetail: action.payload.detail, loading: false }

        case GET_REVIEW_DETAIL_FAILURE:
            return { ...state, loading: false }

        case LOAD:
            return {
                data: action.data
            }

        default:
            return { ...state }
    }
}
