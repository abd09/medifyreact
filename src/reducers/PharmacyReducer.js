import {
    CONNECTED_PHARMACIES,
    CONNECTED_PHARMACIES_SUCCESS,
    CONNECTED_PHARMACIES_FAILURE,
} from '../actions/types';
const LOAD = 'redux-form-examples/account/LOAD'

const INIT_STATE = {
    userDetails: '',
    loggedIn: localStorage.getItem('loggedIn') ? true : false,
    loading: false,
    connectedPharmacies: '',
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {

        case CONNECTED_PHARMACIES:
            return { ...state, loading: true }

        case CONNECTED_PHARMACIES_SUCCESS:
            return { ...state, connectedPharmacies: action.payload.detail, loading: false }

        case CONNECTED_PHARMACIES_FAILURE:
            return { ...state, loading: false }

        case LOAD:
            return {
                data: action.data
            }

        default:
            return { ...state }
    }
}

