import {

    ADD_ORDER,
    ADD_ORDER_SUCCESS,
    ADD_ORDER_FAILURE,
    GET_ORDERS,
    GET_ORDERS_SUCCESS,
    GET_ORDERS_FAILURE,
    GET_PHARMACY_ORDERS,
    GET_PHARMACY_ORDERS_SUCCESS,
    GET_PHARMACY_ORDERS_FAILURE,
    UPDATE_ORDER,
    UPDATE_ORDER_SUCCESS,
    UPDATE_ORDER_FAILURE,
} from '../actions/types';
const LOAD = 'redux-form-examples/account/LOAD'

const INIT_STATE = {
    userDetails: '',
    loggedIn: localStorage.getItem('loggedIn') ? true : false,
    loading: false,
    orderData: '',
    pharmacyOrderData: '',
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_ORDERS:
            return { ...state, loading: true }

        case GET_ORDERS_SUCCESS:
            return { ...state, orderData: action.payload.detail, loading: false }

        case GET_ORDERS_FAILURE:
            return { ...state, loading: false }

        case GET_PHARMACY_ORDERS:
            return { ...state, loading: true }

        case GET_PHARMACY_ORDERS_SUCCESS:
            return { ...state, pharmacyOrderData: action.payload.detail, loading: false }

        case GET_PHARMACY_ORDERS_FAILURE:
            return { ...state, loading: false }

        case ADD_ORDER:
            return { ...state, loading: true }

        case ADD_ORDER_SUCCESS:
            return { ...state, loading: false }

        case ADD_ORDER_FAILURE:
            return { ...state, loading: false }

        case UPDATE_ORDER:
            return { ...state, loading: true }

        case UPDATE_ORDER_SUCCESS:
            return { ...state, loading: false }

        case UPDATE_ORDER_FAILURE:
            return { ...state, loading: false }

        case LOAD:
            return {
                data: action.data
            }

        default:
            return { ...state }
    }
}

