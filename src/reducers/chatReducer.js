
import {

  GET_CHAT,
  GET_CHAT_SUCCESS,
  GET_CHAT_FAILURE,
  GET_CHAT_CONNETION,
  GET_CHAT_CONNETION_SUCCESS,
  GET_CHAT_CONNETION_FAILURE,
} from '../actions/types'

const LOAD = 'redux-form-examples/account/LOAD'

const INIT_STATE = {
  loggedIn: localStorage.getItem('loggedIn') ? true : false,
  loading: false,
  chatList: [],
  chatConnectionList: [],
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case GET_CHAT:
      return { ...state, loading: true }

    case GET_CHAT_SUCCESS:
      return { ...state, chatList: action.payload.detail, loading: false }

    case GET_CHAT_FAILURE:
      return { ...state, loading: false }

    case GET_CHAT_CONNETION:
      return { ...state, loading: true }

    case GET_CHAT_CONNETION_SUCCESS:
      return { ...state, chatConnectionList: action.payload.detail, loading: false }

    case GET_CHAT_CONNETION_FAILURE:
      return { ...state, loading: false }

    case LOAD:
      return {
        data: action.data
      }

    default:
      return { ...state }
  }
}
