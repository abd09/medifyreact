import {
    GET_INVENTORIES,
    GET_INVENTORIES_SUCCESS,
    GET_INVENTORIES_FAILURE,
    SEARCH_INVENTORIES,
    SEARCH_INVENTORIES_SUCCESS,
    SEARCH_INVENTORIES_FAILURE,
    GET_PHARMACY_INVENTORY,
    GET_PHARMACY_INVENTORY_FAILURE,
    GET_PHARMACY_INVENTORY_SUCCESS,
    FILTER_INVENTORY,
    FILTER_INVENTORY_SUCCESS,
    FILTER_INVENTORY_FAILURE
} from '../actions/types';
const LOAD = 'redux-form-examples/account/LOAD'

const INIT_STATE = {
    userDetails: '',
    loggedIn: localStorage.getItem('loggedIn') ? true : false,
    loading: false,
    inventoryData: '',
    pharmacyInventoryData: ''
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {

        case GET_INVENTORIES:
            return { ...state, loading: true }

        case GET_INVENTORIES_SUCCESS:
            return { ...state, inventoryData: action.payload.detail, loading: false }

        case GET_INVENTORIES_FAILURE:
            return { ...state, loading: false }

        case GET_PHARMACY_INVENTORY:
            return { ...state, loading: true }

        case GET_PHARMACY_INVENTORY_SUCCESS:
            return { ...state, pharmacyInventoryData: action.payload.detail, loading: false }

        case GET_PHARMACY_INVENTORY_FAILURE:
            return { ...state, loading: false }

        case SEARCH_INVENTORIES:
            return { ...state, loading: true }

        case SEARCH_INVENTORIES_SUCCESS:
            return { ...state, pharmacyInventoryData: action.payload.detail, loading: false }

        case SEARCH_INVENTORIES_FAILURE:
            return { ...state, loading: false }

        case FILTER_INVENTORY:
            return { ...state, loading: true }

        case FILTER_INVENTORY_SUCCESS:
            return { ...state, pharmacyInventoryData: action.payload.detail, loading: false }

        case FILTER_INVENTORY_FAILURE:
            return { ...state, loading: false }

        case LOAD:
            return {
                data: action.data
            }

        default:
            return { ...state }
    }
}

