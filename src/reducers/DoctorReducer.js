import {

    GET_PATIENT_DOCTORS,
    GET_PATIENT_DOCTORS_SUCCESS,
    GET_PATIENT_DOCTORS_FAILURE,
    CONNECTED_DOCTORS,
    CONNECTED_DOCTORS_SUCCESS,
    CONNECTED_DOCTORS_FAILURE,
    GET_ALL_DOCTORS,
    GET_ALL_DOCTORS_SUCCESS,
    GET_ALL_DOCTORS_FAILURE,
    SEARCH_DOCTORS,
    SEARCH_DOCTORS_SUCCESS,
    SEARCH_DOCTORS_FAILURE,
    GET_DOCTOR_PATIENTS,
    GET_DOCTOR_PATIENTS_SUCCESS,
    GET_DOCTOR_PATIENTS_FAILURE,
    SEARCH_DOCTOR_PATIENTS,
    SEARCH_DOCTOR_PATIENTS_SUCCESS,
    SEARCH_DOCTOR_PATIENTS_FAILURE,
    FILTER_DOCTOR,
    FILTER_DOCTOR_SUCCESS,
    FILTER_DOCTOR_FAILURE,
    GET_DOCTOR_DETAIL,
    GET_DOCTOR_DETAIL_SUCCESS,
    GET_DOCTOR_DETAIL_FAILURE
} from '../actions/types';
const LOAD = 'redux-form-examples/account/LOAD'

const INIT_STATE = {
    userDetails: '',
    loggedIn: localStorage.getItem('loggedIn') ? true : false,
    loading: false,
    doctorsData: '',
    patientDoctorData: '',
    connectedDoctors: '',
    doctorPatientData: '',
    filterDoctorData: [],
    doctorDetails: ''
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {

        case GET_ALL_DOCTORS:
            return { ...state, loading: true }

        case GET_ALL_DOCTORS_SUCCESS:
            return { ...state, doctorsData: action.payload.detail, loading: false }

        case GET_ALL_DOCTORS_FAILURE:
            return { ...state, loading: false }

        case GET_DOCTOR_DETAIL:
            return { ...state, loading: true }

        case GET_DOCTOR_DETAIL_SUCCESS:
            return { ...state, doctorDetails: action.payload.detail, loading: false }

        case GET_DOCTOR_DETAIL_FAILURE:
            return { ...state, loading: false }

        case SEARCH_DOCTORS:
            return { ...state, loading: true }

        case SEARCH_DOCTORS_SUCCESS:
            return { ...state, doctorsData: action.payload.detail, loading: false }

        case SEARCH_DOCTORS_FAILURE:
            return { ...state, loading: false }

        case GET_PATIENT_DOCTORS:
            return { ...state, loading: true }

        case GET_PATIENT_DOCTORS_SUCCESS:
            return { ...state, patientDoctorData: action.payload.detail, loading: false }

        case GET_PATIENT_DOCTORS_FAILURE:
            return { ...state, loading: false }

        case GET_DOCTOR_PATIENTS:
            return { ...state, loading: true }

        case GET_DOCTOR_PATIENTS_SUCCESS:
            return { ...state, doctorPatientData: action.payload.detail, loading: false }

        case GET_DOCTOR_PATIENTS_FAILURE:
            return { ...state, loading: false }

        case SEARCH_DOCTOR_PATIENTS:
            return { ...state, loading: true }

        case SEARCH_DOCTOR_PATIENTS_SUCCESS:
            return { ...state, doctorPatientData: action.payload.detail, loading: false }

        case SEARCH_DOCTOR_PATIENTS_FAILURE:
            return { ...state, loading: false }

        case CONNECTED_DOCTORS:
            return { ...state, loading: true }

        case CONNECTED_DOCTORS_SUCCESS:
            return { ...state, connectedDoctors: action.payload.detail, loading: false }

        case CONNECTED_DOCTORS_FAILURE:
            return { ...state, loading: false }

        case FILTER_DOCTOR:
            return { ...state, loading: true }

        case FILTER_DOCTOR_SUCCESS:
            console.log('filterDoctorData', action.payload)
            return { ...state, doctorsData: action.payload.detail, loading: false }

        case FILTER_DOCTOR_FAILURE:
            return { ...state, loading: false }

        case LOAD:
            return {
                data: action.data
            }

        default:
            return { ...state }
    }
}

