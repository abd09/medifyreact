import {

    ADD_PRESCRIPTION,
    ADD_PRESCRIPTION_SUCCESS,
    ADD_PRESCRIPTION_FAILURE,
    GET_PRESCRIPTIONS,
    GET_PRESCRIPTIONS_SUCCESS,
    GET_PRESCRIPTIONS_FAILURE,
    GET_PATIENT_PRESCRIPTIONS,
    GET_PATIENT_PRESCRIPTIONS_SUCCESS,
    GET_PATIENT_PRESCRIPTIONS_FAILURE,
    GET_DOCTOR_PRESCRIPTIONS,
    GET_DOCTOR_PRESCRIPTIONS_SUCCESS,
    GET_DOCTOR_PRESCRIPTIONS_FAILURE,
    EDIT_PRESCRIPTION,
    EDIT_PRESCRIPTION_SUCCESS,
    EDIT_PRESCRIPTION_FAILURE,
    FILTER_PRESCRIPTION,
    FILTER_PRESCRIPTION_SUCCESS,
    FILTER_PRESCRIPTION_FAILURE
} from '../actions/types';
const LOAD = 'redux-form-examples/account/LOAD'

const INIT_STATE = {
    userDetails: '',
    loggedIn: localStorage.getItem('loggedIn') ? true : false,
    loading: false,
    prescriptionData: '',
    patientPrescriptionData: '',
    doctorPrescriptionData: '',
    filterPrescriptionData : []
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_PRESCRIPTIONS:
            return { ...state, loading: true }

        case GET_PRESCRIPTIONS_SUCCESS:
            return { ...state, prescriptionData: action.payload.detail, loading: false }

        case GET_PRESCRIPTIONS_FAILURE:
            return { ...state, loading: false }

        case GET_PATIENT_PRESCRIPTIONS:
            return { ...state, loading: true }

        case GET_PATIENT_PRESCRIPTIONS_SUCCESS:
            return { ...state, patientPrescriptionData: action.payload.detail, loading: false }

        case GET_PATIENT_PRESCRIPTIONS_FAILURE:
            return { ...state, loading: false }

        case GET_DOCTOR_PRESCRIPTIONS:
            return { ...state, loading: true }

        case GET_DOCTOR_PRESCRIPTIONS_SUCCESS:
            return { ...state, doctorPrescriptionData: action.payload.detail, loading: false }

        case GET_DOCTOR_PRESCRIPTIONS_FAILURE:
            return { ...state, loading: false }

        case ADD_PRESCRIPTION:
            return { ...state, loading: true }

        case ADD_PRESCRIPTION_SUCCESS:
            return { ...state, loading: false }

        case ADD_PRESCRIPTION_FAILURE:
            return { ...state, loading: false }

        case EDIT_PRESCRIPTION:
            return { ...state, loading: true }

        case EDIT_PRESCRIPTION_SUCCESS:
            return { ...state, loading: false }

        case EDIT_PRESCRIPTION_FAILURE:
            return { ...state, loading: false }

        case FILTER_PRESCRIPTION:
            return { ...state, loading: true }

        case FILTER_PRESCRIPTION_SUCCESS:
            console.log('filterPrescriptionData',action.payload)
            return { ...state, filterPrescriptionData: action.payload.detail, loading: false }

        case FILTER_PRESCRIPTION_FAILURE:
            return { ...state, loading: false }

        case LOAD:
            return {
                data: action.data
            }

        default:
            return { ...state }
    }
}

