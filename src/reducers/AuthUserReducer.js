/**
 * Auth User Reducers
 */

import {
    SIGNUP,
    SIGNUP_SUCCESS,
    SIGNUP_FAILURE,
    SIGNIN,
    SIGNIN_SUCCESS,
    SIGNIN_FAILURE,
    FORGOT_PASSWORD,
    FORGOT_PASSWORD_SUCCESS,
    FORGOT_PASSWORD_FAILURE,
    VERIFY_ACCOUNT,
    VERIFY_ACCOUNT_SUCCESS,
    VERIFY_ACCOUNT_FAILURE,
    RESET_PASSWORD,
    RESET_PASSWORD_SUCCESS,
    RESET_PASSWORD_FAILURE,
    SIGNOUT,
    GETUSERPROFILEDATA,
    GETUSERPROFILEDATA_SUCCESS,
    GETUSERPROFILEDATA_FAILURE,
    GET_PHARMACY,
    GET_PHARMACY_SUCCESS,
    GET_PHARMACY_FAILURE,
    GET_PATIENTS,
    GET_PATIENTS_SUCCESS,
    GET_PATIENTS_FAILURE,
    CONNECT_PHARMACY,
    CONNECT_PHARMACY_SUCCESS,
    CONNECT_PHARMACY_FAILURE,
    CROPIMAGE,
    CROPIMAGE_SUCCESS,
    CROPIMAGE_FAILURE,
    GET_CONNECTS,
    GET_CONNECTS_SUCCESS,
    GET_CONNECTS_FAILURE,
    GET_PATIENT_PHARMACIES,
    GET_PATIENT_PHARMACIES_SUCCESS,
    GET_PATIENT_PHARMACIES_FAILURE,
    SEARCH_PHARMACY,
    SEARCH_PHARMACY_SUCCESS,
    SEARCH_PHARMACY_FAILURE,
    CONNECT_DOCTOR,
    CONNECT_DOCTOR_SUCCESS,
    CONNECT_DOCTOR_FAILURE,
    GET_PHARMACY_DETAIL,
    GET_PHARMACY_DETAIL_SUCCESS,
    GET_PHARMACY_DETAIL_FAILURE
} from '../actions/types';

const LOAD = 'redux-form-examples/account/LOAD'

/**
 * initial auth user
 */
const INIT_STATE = {
    userDetails: '',
    loggedIn: localStorage.getItem('loggedIn') ? true : false,
    loading: false,
    userData: [],
    userProfileData: '',
    pharmacyData: '',
    patientData: '',
    connectData: '',
    searchpharmacyData: '',
    count: 0,
    profileImage: '',
    url: '',
    patientPharmacies: '',
    pharmacyDetail: {}
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case SIGNUP:
            return { ...state, loading: true }

        case SIGNUP_SUCCESS:
            return { ...state, loading: false, token: true, userDetails: action.payload.data }

        case SIGNUP_FAILURE:
            return { ...state, loading: false }

        case SIGNIN:
            return { ...state, loading: true }

        case SIGNIN_SUCCESS:
            return { ...state, loading: false }

        case SIGNIN_FAILURE:
            return { ...state, loading: false }

        case SIGNOUT:
            localStorage.removeItem('loggedIn');
            localStorage.removeItem('token');
            localStorage.removeItem('role');
            localStorage.removeItem('email');
            localStorage.removeItem('ref');
            localStorage.removeItem('rememberMe');
            return { ...state, loading: false }

        case GETUSERPROFILEDATA:
            return { ...state, loading: true }

        case GETUSERPROFILEDATA_SUCCESS:
            return { ...state, userProfileData: action.payload.detail, profileImage: action.payload.profileImage, loading: false }

        case GETUSERPROFILEDATA_FAILURE:
            return { ...state, loading: false }

        case FORGOT_PASSWORD:
            return { ...state, loading: true }

        case FORGOT_PASSWORD_SUCCESS:
            return { ...state, loading: false }

        case FORGOT_PASSWORD_FAILURE:
            return { ...state, loading: false }

        case VERIFY_ACCOUNT:
            return { ...state, loading: true }

        case VERIFY_ACCOUNT_SUCCESS:
            return { ...state, loading: false }

        case VERIFY_ACCOUNT_FAILURE:
            return { ...state, loading: false }

        case RESET_PASSWORD:
            return { ...state, loading: true }

        case RESET_PASSWORD_SUCCESS:
            return { ...state, loading: false }

        case RESET_PASSWORD_FAILURE:
            return { ...state, loading: false }

        case CROPIMAGE:
            return { ...state, loading: false }

        case CROPIMAGE_SUCCESS:
            return { ...state, userProfileData: action.payload.detail, profileImage: action.payload.profileImage, loading: false }

        case CROPIMAGE_FAILURE:
            return { ...state, loading: false }

        case GET_PHARMACY:
            return { ...state, loading: true }

        case GET_PHARMACY_SUCCESS:
            return { ...state, pharmacyData: action.payload.detail, loading: false }

        case GET_PHARMACY_FAILURE:
            return { ...state, loading: false }

        case GET_PHARMACY_DETAIL:
            return { ...state, loading: true }

        case GET_PHARMACY_DETAIL_SUCCESS:
            return { ...state, pharmacyDetail: action.payload.detail, loading: false }

        case GET_PHARMACY_DETAIL_FAILURE:
            return { ...state, loading: false }

        case SEARCH_PHARMACY:
            return { ...state, loading: true }

        case SEARCH_PHARMACY_SUCCESS:
            return { ...state, pharmacyData: action.payload.detail, searchpharmacyData: action.payload.detail, loading: false }

        case SEARCH_PHARMACY_FAILURE:
            return { ...state, loading: false }

        case GET_PATIENTS:
            return { ...state, loading: true }

        case GET_PATIENTS_SUCCESS:
            return { ...state, patientData: action.payload.detail, loading: false }

        case GET_PATIENTS_FAILURE:
            return { ...state, loading: false }

        case CONNECT_PHARMACY:
            return { ...state, loading: true }

        case CONNECT_PHARMACY_SUCCESS:
            return { ...state, loading: false }

        case CONNECT_PHARMACY_FAILURE:
            return { ...state, loading: false }

        case CONNECT_DOCTOR:
            return { ...state, loading: true }

        case CONNECT_DOCTOR_SUCCESS:
            return { ...state, loading: false }

        case CONNECT_DOCTOR_FAILURE:
            return { ...state, loading: false }

        case GET_CONNECTS:
            return { ...state, loading: true }

        case GET_CONNECTS_SUCCESS:
            return { ...state, connectData: action.payload.detail, loading: false }

        case GET_CONNECTS_FAILURE:
            return { ...state, loading: false }

        case GET_PATIENT_PHARMACIES:
            return { ...state, loading: true }

        case GET_PATIENT_PHARMACIES_SUCCESS:
            return { ...state, patientPharmacies: action.payload.detail, loading: false }

        case GET_PATIENT_PHARMACIES_FAILURE:
            return { ...state, loading: false }

        case LOAD:
            return {
                data: action.data
            }

        default:
            return { ...state }
    }
}