import { combineReducers } from 'redux';
import authUserReducer from './AuthUserReducer';
import PrescriptionReducer from './PrescriptionReducer';
import OrderReducer from './OrderReducer';
import DoctorReducer from './DoctorReducer';
import PharmacyReducer from './PharmacyReducer';
import PatientReducer from './PatientReducer';
import InventoryReducer from './InventoryReducer';
import ReviewReducer from './ReviewReducer';
import ChatReducer from './chatReducer'
import SpecialityReducer from './SpecialityReducer'
import SchemeReducer from './SchemeReducer';

import { reducer as reduxFormReducer } from 'redux-form';

const reducers = combineReducers({
    form: reduxFormReducer,
    authUser: authUserReducer,
    prescriptionReducer: PrescriptionReducer,
    orderReducer: OrderReducer,
    doctorReducer: DoctorReducer,
    pharmacyReducer: PharmacyReducer,
    patientReducer: PatientReducer,
    inventoryReducer: InventoryReducer,
    ReviewReducer: ReviewReducer,
    ChatReducer: ChatReducer,
    specialityReducer: SpecialityReducer,
    schemeReducer: SchemeReducer
})

export default reducers;