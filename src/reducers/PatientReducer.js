import {
    EDIT_VITALS,
    EDIT_VITALS_SUCCESS,
    EDIT_VITALS_FAILURE,
} from '../actions/types';
const LOAD = 'redux-form-examples/account/LOAD'

const INIT_STATE = {
    loggedIn: localStorage.getItem('loggedIn') ? true : false,
    loading: false,
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {

        case EDIT_VITALS:
            return { ...state, loading: true }

        case EDIT_VITALS_SUCCESS:
            return { ...state, loading: false }

        case EDIT_VITALS_FAILURE:
            return { ...state, loading: false }

        case LOAD:
            return {
                data: action.data
            }

        default:
            return { ...state }
    }
}

