import {
    GET_SCHEMES,
    GET_SCHEMES_FAILURE,
    GET_SCHEMES_SUCCESS,
    ADD_SCHEME,
    ADD_SCHEME_SUCCESS,
    ADD_SCHEME_FAILURE
} from '../actions/types'

const LOAD = 'redux-form-examples/account/LOAD'

const INIT_STATE = {
    loggedIn: localStorage.getItem('loggedIn') ? true : false,
    loading: false,
    schemeList: [],
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_SCHEMES:
            return { ...state, loading: true }

        case GET_SCHEMES_SUCCESS:
            return { ...state, schemeList: action.payload.detail, loading: false }

        case GET_SCHEMES_FAILURE:
            return { ...state, loading: false }

        case ADD_SCHEME:
            return { ...state, loading: true }

        case ADD_SCHEME_SUCCESS:
            return { ...state, loading: false }

        case ADD_SCHEME_FAILURE:
            return { ...state, loading: false }
            
        case LOAD:
            return {
                data: action.data
            }

        default:
            return { ...state }
    }
}
