import {
    GET_SPECIALITIES,
    GET_SPECIALITIES_FAILURE,
    GET_SPECIALITIES_SUCCESS,
} from '../actions/types'

const LOAD = 'redux-form-examples/account/LOAD'

const INIT_STATE = {
    loggedIn: localStorage.getItem('loggedIn') ? true : false,
    loading: false,
    specialityList: [],
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_SPECIALITIES:
            return { ...state, loading: true }

        case GET_SPECIALITIES_SUCCESS:
            return { ...state, specialityList: action.payload.detail, loading: false }

        case GET_SPECIALITIES_FAILURE:
            return { ...state, loading: false }

        case LOAD:
            return {
                data: action.data
            }

        default:
            return { ...state }
    }
}
