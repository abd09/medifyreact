const AppConfig = {
  // baseUrl: 'https://medify-api.infiny.dev/',
  baseUrl: 'http://localhost:3000/',
  // baseUrl: 'http://medify.net:3000/',
  colors: {
    'primary': '#43415e',
    'secondary': '#42425E',
    'success': '#429945',
    'danger': '#FF3739',
    'warning': '#FFB70F',
    /*   'info': '#00D0BD', */
    'info': 'darkcyan',
    'dark': '#464D69',
    'default': '#FAFAFA',
    'greyLighten': '#A5A7B2',
    'grey': '#677080',
    'darkGrey': '#454545',
    'white': '#FFFFFF',
    'purple': '#896BD6',
    'green': '#4caf50',
    'yellow': '#D46B08',
    'darkGreen': 'darkgreen'
  }
  // 8001
}

export default AppConfig;