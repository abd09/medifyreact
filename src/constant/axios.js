import axios from "axios";
import appConfig from './config'
export default axios.create({
  baseURL: appConfig.baseUrl,
  timeout: 10000,
  headers: {}
});
