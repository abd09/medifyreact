import React, { Component } from 'react';
import { FormGroup, Input } from 'reactstrap';
import PubNubReact from 'pubnub-react';
import { withStyles } from '@material-ui/core/styles';
import { Col, Row } from 'reactstrap';
import { getuserProfileData, getChat } from '../../actions';
import { connect } from 'react-redux';
import axios from '../../constant/axios';
import Typography from '@material-ui/core/Typography';
import SideBar from '../../components/userSideBar'
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import AppBar from '@material-ui/core/AppBar';
import { NotificationManager } from 'react-notifications';
import Send from '@material-ui/icons/Send';

const drawerWidth = 240;
const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    appBar: {
        marginLeft: drawerWidth,
        [theme.breakpoints.up('sm')]: {
            width: `calc(100% - ${drawerWidth}px)`,
        },
    },
    menuButton: {
        marginRight: 20,
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
    pageContent: {
        marginTop: -125,
        fontWeight: 'bold',
        backgroundColor: '#fff',
        borderRight: '1px solid #655368'
    },
    menu: {
        width: 200,
    }
});

class ChatComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            messages: [],
            messageText: '',
            networkIssue: false
        };
        this.pubnub = new PubNubReact({
            publishKey: 'pub-c-d2f7b1f2-bc28-4145-8253-2c887cc3b9ba',
            subscribeKey: 'sub-c-54f1cba2-93ef-11e9-8277-da7aa9a31542'
        });
        this.pubnub.init(this);
    }

    componentDidUpdate() {
        this.scrollToBottom()
    }

    componentWillMount() {
        let data = {
            chatConnectId: this.props.match.params.id
        }
        this.pubnub.subscribe({ channels: [this.props.match.params.id], withPresence: true });
        this.props.getuserProfileData({ history: this.props.history });
        this.props.getChat({ data: data, history: this.props.history })
        this.scrollToBottom();
    }

    componentWillReceiveProps = (nextProps) => {
        if (this.props.chatList !== nextProps.chatList) {
            this.setState({ messages: nextProps.chatList })
        }
    }

    componentWillUnmount() {
        console.log('addchat componentWillUnmount called');
        this.pubnub.unsubscribe({ channels: [this.props.match.params.id] });
    }

    sendMessage = (e) => {
        let keyCode =  e.keyCode;
        let messageText = this.state.messageText;
        let params = this.props.match.params;
        let userProfileData = this.props.userProfileData;
        let temp = this;
        this.pubnub.hereNow(
            {
                channels: [params.id],
                channelGroups: [],
                includeUUIDs: true,
                includeState: true
            },
            function (status, response) {
                console.log('componentDidUpdate',status,keyCode)
                if (status.error === true && status.category === 'PNNetworkIssuesCategory' &&  keyCode === 13) {
                    NotificationManager.error('You are offline, Please connect to internet.')
                } else if (keyCode === 13 && messageText !== '') {
                    e.preventDefault();
                    let msg = messageText;
        
                    let data = {
                        chatConnectId: params.id,
                        to: params.to,
                        from: userProfileData._id,
                        name: userProfileData.firstname,
                        message: msg
                    }
        
                    axios.post(`/users/addChat/`, data, {
                        headers: {
                            'Content-Type': 'application/json',
                            'token': localStorage.getItem('token')
                        }
                    }).then(result => {
                        console.log('result', result);
                        if (this.state.networkIssue === true) {
                            NotificationManager.error('You are offline, Please connect to internet.')
                        }
                    }).catch(error => {
                        console.log('addchat error', error)
                    });
                    temp.setState({ messageText: '' })
                }
            });
            
    }

    sendMessageButton = (e) => {
        let tempRef = this;
        this.pubnub.hereNow(
            {
                channels: [tempRef.props.match.params.id],
                channelGroups: [],
                includeUUIDs: true,
                includeState: true
            },
            function (status, response) {
                if (status.category === 'PNNetworkIssuesCategory') {
                    NotificationManager.error('You are offline, Please connect to internet.')
                } else if (tempRef.state.messageText !== '') {
                    e.preventDefault();
                    let msg = tempRef.state.messageText;
        
                    let data = {
                        chatConnectId: tempRef.props.match.params.id,
                        to: tempRef.props.match.params.to,
                        from: tempRef.props.userProfileData._id,
                        name: tempRef.props.userProfileData.firstname,
                        message: msg
                    }
        
                    axios.post(`/users/addChat/`, data, {
                        headers: {
                            'Content-Type': 'application/json',
                            'token': localStorage.getItem('token')
                        }
                    }).then(result => {
                        console.log('result', result);
                        if (tempRef.state.networkIssue === true) {
                            NotificationManager.error('You are offline, Please connect to internet.')
                        }
                    }).catch(error => {
                        console.log('addchat error', error)
                    });
        
                    tempRef.setState({ messageText: '' })
                }
            })
    }

    scrollToBottom = () => {
        const chat = document.getElementById("chatList");
        if (chat !== null) {
            chat.scrollTop = chat.scrollHeight;
        }
    };

    handleChange = (event) => {
        this.setState({ messageText: event.target.value });
    };

    handleDrawerToggle = () => {
        this.setState(state => ({ mobileOpen: !state.mobileOpen }));
    };


    render() {
        let { chatList, classes, userProfileData, children } = this.props;
        let { mobileOpen } = this.state;
        let data = {
            chatConnectId: this.props.match.params.id
        }

        this.pubnub.getMessage(data.chatConnectId, (msg) => {
            this.props.getChat({ data: data, history: this.props.history })
        });
        return (
            <React.Fragment>
                <AppBar position="fixed" className={classes.appBar}>
                    <Toolbar>
                        <IconButton
                            color="inherit"
                            aria-label="Open drawer"
                            onClick={this.handleDrawerToggle}
                            className={classes.menuButton}>
                            <MenuIcon />
                        </IconButton>
                        <Typography variant="h6" color="inherit" style={{ fontWeight: 'bold', color: '#fff' }} noWrap>
                            {/* {"Chat"} */}
                            {
                                (chatList && chatList !== undefined && chatList.length > 0) ? (userProfileData._id === chatList[0].from._id) ? chatList[0].to.firstname : chatList[0].from.firstname : 'Chat'
                            }
                        </Typography>
                    </Toolbar>
                </AppBar>
                <div className={classes.pageContent}>
                    <SideBar mobileOpen={mobileOpen} handleDrawerToggle={this.handleDrawerToggle} userProfileData={userProfileData} >
                        {children}
                    </SideBar>
                </div>
                <div className={classes.root}>
                    <div className="chatWindow">
                        <ul className="chat" id="chatList">
                            {userProfileData !== undefined ? chatList.map((data, key) => {
                                return <div key={key}>
                                    {userProfileData._id === data.from._id ? (
                                        <li className="self">
                                            <div className="msg">
                                                <p>You</p>
                                                <div className="message-chat"> {data.message}</div>
                                            </div>
                                        </li>
                                    ) : (
                                            <li className="other">
                                                <div className="msg">
                                                    <p>{data.from.firstname}</p>
                                                    <div className="message-chat"> {data.message} </div>
                                                </div>
                                            </li>
                                        )}
                                </div>
                            }) : ''}
                        </ul>
                        <div className="chatInputWrapper">
                            <form onSubmit={this.handleSubmit}>
                                <FormGroup style={{ marginBottom: '1.5rem' }}>
                                    <Row style={{ margin: 0, padding: 8 }}>
                                        <Col sm={10} xs={10} md={10} lg={11}>
                                            <Input
                                                className="textarea input"
                                                type="text"
                                                placeholder="Enter your message..."
                                                onChange={(e) => this.handleChange(e)}
                                                onKeyDown={(e) => this.sendMessage(e)}
                                                value={this.state.messageText}
                                                autoFocus
                                            />
                                        </Col>
                                        <Col sm={2} xs={2} md={2} lg={1}>
                                            <React.Fragment>
                                                <Send
                                                    style={{
                                                        color: '#43425d',
                                                        fontSize: 50
                                                    }}
                                                    className="sendButton"
                                                    onClick={(e) => this.sendMessageButton(e)}
                                                />
                                            </React.Fragment>
                                        </Col>
                                    </Row>
                                </FormGroup>
                            </form>
                        </div>
                    </div>
                </div>
            </React.Fragment>
        );
    }
}

const mapStateToProps = ({ authUser, ChatReducer }) => {
    console.log('authUser', authUser)
    const { userProfileData, profileImage } = authUser;
    console.log('chatAction', ChatReducer)
    const { chatList } = ChatReducer;
    return { userProfileData, profileImage, chatList }
}

export default connect(mapStateToProps, { getuserProfileData, getChat })(withStyles(styles)(ChatComponent));
