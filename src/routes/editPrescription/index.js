import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { Col, Row } from 'reactstrap';
import SideBar from '../../components/userSideBar'
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import { Button, FormGroup, Label, Input } from 'reactstrap';
import AppBar from '@material-ui/core/AppBar';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import {
    isMobile
} from "react-device-detect";
import { red } from '@material-ui/core/colors';

import axios from '../../constant/axios';
import { getuserProfileData, cropImage, editPrescription } from '../../actions';
import { connect } from 'react-redux';
import appConfig from '../../constant/config';
const drawerWidth = 240;
const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120
    },
    lable: {
        textAlign: 'left'
    },
    selectEmpty: {
        marginTop: theme.spacing(2)
    },
    appBar: {
        marginLeft: drawerWidth,
        [theme.breakpoints.up('sm')]: {
            width: `calc(100% - ${drawerWidth}px)`,
        },
    },
    menuButton: {
        marginRight: 20,
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
    dialogDone: {
        backgroundColor: appConfig.colors.darkGrey,
        color: "#3ddb20",
        '&:hover': {
            background: 'none',
            backgroundColor: appConfig.colors.darkGrey,
            color: '#3ddb20'
        },
        borderColor: appConfig.colors.darkGrey
    },
    dialogTitle: {
        backgroundColor: '#5a5a5a',
        '& h2': {
            color: 'white',
            textAlign: 'center'
        },
        borderBottom: '1px solid #5a5a5a'
    },
    dialogContent: {
        backgroundColor: '#5a5a5a',
    },
    root2: {
        flexGrow: 1,
        backgroundColor: '#fff'
    },
    tabsRoot: {
        // padding: '0 24px 0 24px',
        color: '#42425E',
        borderBottom: '1px solid #7a7a7a',
    },
    tabsIndicator: {
        backgroundColor: '#7a7a7a',
        height: '3px'
    },
    rightDiv: {
        // right: '30px',
        color: "#CCCACD",
        marginBottom: 0,
        fontWeight: 'bold',
        fontSize: '16px',
        /*   float: 'right', */
        textAlign: '-webkit-center',

    },
    listitems: {
        paddingLeft: 40,
        paddingRight: 10,
        paddingTop: 5,
        paddingBottom: 5
    },
    listitems1: {
        paddingLeft: 40,
        paddingRight: 10,
        paddingTop: 14,
        paddingBottom: 5
    },
    tabRoot: {
        textTransform: 'initial',
        minWidth: '50%',
        fontWeight: theme.typography.fontWeightRegular,
        // marginRight: theme.spacing.unit * 4,
        fontFamily: [
            '-apple-system',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ].join(','),
    },
    typography: {
        padding: '15px 0px',
        color: '#CCCACD',
        ['@media (max-width:787px)']: {
            padding: '15px 15px',
        },
    },
    input: {
        '::-webkit-input-placeholder': {
            color: 'red'
        }
    },
    imageDiv: {

        height: '70px',
        width: '70px',

    },
    avatar: {
        margin: theme.spacing(2),
        marginLeft: theme.spacing(4),
        width: 70,
        height: 70
    },
    profile: {
        textAlign: '-webkit-center',
        margin: theme.spacing.unit * 3,
        lineHeight: 'normal'
    },
    tableDiv: {
        padding: '0px',
    },
    tabsFlex: {
        borderBottom: '0px solid #454545',
    },
    tableThTd: {
        color: '#ffffff'
    },
    infoDiv: {
        padding: 18,
        backgroundColor: "#fff"
    },
    disableClick: {
        pointerEvents: 'none'
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: '#ffffff',
        '&:hover': {
            backgroundColor: '#ffffff',
        },
        marginRight: theme.spacing(2),
        marginLeft: 0,
        width: '100%',
        borderRadius: '40px',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(3),
            width: 'auto',
        },
    },
    searchIcon: {
        width: theme.spacing(7),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        fontWeight: 'bold',
        opacity: 1
    },
    inputRoot: {
        color: '#c6c6c6',
    },
    inputInput: {
        color: '#c6c6c6',
        padding: theme.spacing(1, 1, 1, 7),
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: 200,
        },
    },
    icon: {
        position: 'fixed',
        right: '10px',
        color: '#fff'

    },
    iconHover: {
        margin: theme.spacing(2),
        '&:hover': {
            color: red[800],
        },
    },
    avatar: {
        margin: 0,
        padding: 0,
        // height: 90,
        // float: 'right'
    },
    avatarlist: {
        margin: 0,
        padding: 0,
        width: 35,
        height: 35,
    }/* ,
    profile: {
        margin: 0,
        height: 100,
       
    } */,
    signup: {
        backgroundColor: appConfig.colors.primary,
        borderRadius: '0px',
        color: '#fff',
        marginTop: '85px',
        padding: '5px 22px',
        textTransform: 'none',
        marginRight: '8px',
        ['@media (max-width:707px)']: {
            marginRight: 'unset',
        },
        ['@media (max-width:599px)']: {
            marginRight: '8px',
        },
        ['@media (max-width:436px)']: {
            marginRight: 'unset',
        },
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        borderRadius: '8px',
        color: appConfig.colors.primary
    },
    dense: {
        marginTop: theme.spacing(2),
    },
    menu: {
        width: 200,
    },
    submit: {
        backgroundColor: appConfig.colors.primary,
        color: '#fff',
        marginTop: theme.spacing(1),
        padding: '5px 10px',
        textTransform: 'none',
        borderRadius: '6px'
    },
    submit2: {
        backgroundColor: '#fff',
        color: appConfig.colors.primary,
        border: 'none',
        marginTop: theme.spacing(2),
        textTransform: 'none',
        fontWeight: 'bolder',
        borderRadius: '0',
        '&:hover': {
            backgroundColor: '#ffffff',
            color: appConfig.colors.primary,
        },
    },
    form: {
        width: '100%',
        paddingLeft: '20px',
        paddingRight: '35px',
        marginTop: theme.spacing.unit,
    },
});

class EditDoctorDetail extends Component {
    constructor(props) {
        super(props)
        this.state = {
            uploaded_image_path: '',
            value: 0,
            openModal: false,
            isLoading: true,
            prescription: '',
            pharmacy: '',
            note: '',
            addmoreText: [{ description: '', duration: '', qty: '', other: { M: false, A: false, N: false }, orderQty: '' }],
            tool: false,
            croppedImage: '',
            profileimage: '',
            imagePreview: '',
            profileimageMain: '',
            image: '',
            imageLoaded: false,
            alert: false,
            mobileOpen: false,
            age: '',
            firstname: '',
            lastname: '',
            mobile: '',
            about: '',
            title: '',
            category: ''
        };
    }

    componentWillMount() {
        this.props.getuserProfileData({ history: this.props.history });
        axios.get(`/users/prescriptionDetails/${this.props.match.params.id}`, {
            headers: {
                'Content-Type': 'application/json',
                'token': localStorage.getItem('token')
            }
        }).then(result => {
            console.log("prescriptionDetails result###", result.data.detail);
            this.setState({ title: result.data.detail ? result.data.detail.title : '', note: result.data.detail ? result.data.detail.note : '', addmoreText: result.data.detail ? result.data.detail.otherDetails : {}, url: result.data.url, category: result.data.detail ? result.data.detail.category : '' })
        })
            .catch(error => {
                console.log("error....", error);
            });

    }

    componentWillReceiveProps = (nextProps) => {
        console.log("this.props.userProfileData", nextProps)

        if (this.props.profileImage !== nextProps.profileImage) {
            this.setState({ croppedImage: nextProps.profileImage })
        }
    }
    handleDrawerToggle = () => {
        this.setState(state => ({ mobileOpen: !state.mobileOpen }));
    };



    updatePrescription = (e) => {
        e.preventDefault();
        let data = {
            otherDetails: this.state.addmoreText,
            title: this.state.title,
            note: this.state.note,
            id: this.props.match.params.id,
            category: this.state.category
        }
        console.log("editPrescription", data)
        this.props.editPrescription({
            data,
            history: this.props.history
        })
    }

    handleItemChange = (e, name, ind) => {
        console.log('nameeee', name, e.target.value, ind);

        e.preventDefault()
        var oldState = [...this.state.addmoreText];
        oldState.map((data, i) => {
            if (ind == i) {
                console.log('e.target.value', e.target.value, e.target.checked);
                if (name == 'other') {
                    oldState[i]['other'][e.target.value] = e.target.checked;
                } else {
                    oldState[i][name] = e.target.value;
                }
            }
        })
        this.setState({ addmoreText: oldState });
    }


    handleChange = (event, key) => {
        console.log('key', key)
        this.setState({ [key]: event.target.value });
    };



    render() {
        const { classes, userProfileData, children } = this.props;
        const { tool, alert, title, note, category } = this.state;
        console.log('this.state.addmoreText', this.state.addmoreText)
        return (
            <React.Fragment>
                <AppBar position="fixed" className={classes.appBar}>
                    <Toolbar>
                        <IconButton
                            color="inherit"
                            aria-label="Open drawer"
                            onClick={this.handleDrawerToggle}
                            className={classes.menuButton}
                        >
                            <MenuIcon />
                        </IconButton>
                        <Typography variant="h6" color="inherit" style={{ fontWeight: 'bold', color: '#fff' }} noWrap>
                            {"Edit Prescription"}
                        </Typography>
                    </Toolbar>
                </AppBar>
                <div className='page-content' style={{ fontWeight: 'bold', backgroundColor: '#fff', marginTop: '-56px', borderRight: '1px solid #655368' }}>
                    <SideBar mobileOpen={this.state.mobileOpen} handleDrawerToggle={this.handleDrawerToggle} userProfileData={userProfileData} >
                        {children}
                    </SideBar>
                </div>
                <div className={classes.root}>
                    <div className={classes.container}>
                        <Col xs="12" md="12" sm="12" >
                            <div className={classes.profile}>
                                <div className={classes.tableDiv}>
                                    <form className={classes.form} onSubmit={(e) => this.updatePrescription(e)}>
                                        <FormGroup className={classes.lable}>
                                            <Label for="title" style={{ color: appConfig.colors.primary, fontSize: '16px' }}>Title:</Label>
                                            <Input type="text" style={{ borderColor: appConfig.colors.primary, fontSize: '18px' }} value={title} name="title" id="title" placeholder="Title" onChange={(e) => this.handleChange(e, 'title')} readOnly disabled />
                                        </FormGroup>
                                        <FormGroup className={classes.lable}>
                                            <Label for="category" style={{ color: appConfig.colors.primary, fontSize: '16px' }}>Prescription for:</Label>
                                            <Input type="text" style={{ borderColor: appConfig.colors.primary, fontSize: '18px' }} value={category} name="category" id="category" placeholder="Prescription for" onChange={(e) => this.handleChange(e, 'category')} />
                                        </FormGroup>
                                        {/*   <span className={classes.error} >{this.state.errors["category"]}</span> */}
                                        <FormGroup className={classes.lable}>
                                            <Label for="note" style={{ color: appConfig.colors.primary, fontSize: '16px' }}>Note:</Label>
                                            <Input type="textarea" style={{ borderColor: appConfig.colors.primary, fontSize: '18px' }} onChange={(e) => this.handleChange(e, 'note')} value={note} className={classes.input} placeholder="Note" name="note" id="note" readOnly disabled />
                                        </FormGroup>
                                        {this.state.addmoreText.length > 0 && (this.state.addmoreText.description !== '' || this.state.addmoreText.qty !== '' || this.state.addmoreText.orderQty !== '') ?
                                            <div>
                                                {isMobile ? '' :
                                                    <div style={{ textAlign: 'left' }}>
                                                        <Col md={2} sm={2} xs={12} style={{ display: 'inline-block', paddingLeft: '0px' }} >
                                                            <Label for="description" style={{ color: appConfig.colors.primary, fontSize: '16px' }}>Description:</Label>
                                                        </Col>
                                                        <Col md={2} sm={2} xs={12} style={{ display: 'inline-block' }} >
                                                            <Label for="day" style={{ color: appConfig.colors.primary, fontSize: '16px' }}>Duration:</Label>
                                                        </Col>
                                                        <Col md={2} sm={2} xs={12} style={{ display: 'inline-block' }}>
                                                            <Label for="qty" style={{ color: appConfig.colors.primary, fontSize: '16px' }}>Quantity:</Label>
                                                        </Col>
                                                        <Col md={2} sm={2} xs={12} style={{ display: 'inline-block' }}>
                                                            <Label for="times" style={{ color: appConfig.colors.primary, fontSize: '16px' }}>Order Qty:</Label>
                                                        </Col>
                                                        <Col md={2} sm={2} xs={12} style={{ display: 'inline-block', paddingRight: '0px', paddingLeft: '0px' }}>
                                                            <Label for="times" style={{ color: appConfig.colors.primary, fontSize: '16px' }}>Times:</Label>
                                                        </Col>
                                                    </div>}
                                                {this.state.addmoreText.map((data, index) => {
                                                    console.log('data!!!!', data);
                                                    if (data.description !== '' || data.duration !== '' || data.qty !== '' || data.orderQty !== '')
                                                        return <div key={index} style={{ textAlign: 'left' }}>
                                                            <Col md={2} sm={2} xs={12} style={{ display: 'inline-block', paddingLeft: '0px'}} >
                                                                <FormGroup>
                                                                    {isMobile ? <Label for="description" style={{ color: appConfig.colors.primary, fontSize: '16px' }}>Description:</Label> : ''}
                                                                    <Input type="text" style={{ borderColor: appConfig.colors.primary, fontSize: '18px' }} className={classes.input} value={data.description} placeholder="Description" name="description" id={`description${index}`} readOnly disabled />
                                                                </FormGroup>
                                                            </Col>
                                                            <Col md={2} sm={2} xs={12} style={{ display: 'inline-block' }} >
                                                                <FormGroup>
                                                                    {isMobile ? <Label for="day" style={{ color: appConfig.colors.primary, fontSize: '16px' }}>Duration:</Label> : ''}
                                                                    <Input type="text" style={{ borderColor: appConfig.colors.primary, fontSize: '18px' }} className={classes.input} value={data.duration} placeholder="Duration" name="duration" id={`duration${index}`} readOnly disabled />
                                                                </FormGroup>
                                                            </Col>
                                                            <Col md={2} sm={2} xs={12} style={{ display: 'inline-block' }}>
                                                                <FormGroup>
                                                                    {isMobile ? <Label for="qty" style={{ color: appConfig.colors.primary, fontSize: '16px' }}>Quantity:</Label> : ''}
                                                                    <Input type="number" style={{ borderColor: appConfig.colors.primary, fontSize: '18px' }} className={classes.input} value={data.qty} placeholder="Quantity" readOnly disabled name="qty" id={`qty${index}`} />
                                                                </FormGroup>
                                                            </Col>

                                                            <Col md={2} sm={2} xs={12} style={{ display: 'inline-block' }}>
                                                                <FormGroup>
                                                                    {isMobile ? <Label for="qty" style={{ color: appConfig.colors.primary, fontSize: '16px' }}>Order Qty:</Label> : ''}

                                                                    <Input type="text" style={{ borderColor: appConfig.colors.primary, fontSize: '18px' }} className={classes.input} value={data.orderQty} onChange={(e) => this.handleItemChange(e, 'orderQty', index)} placeholder="Order Qty" name="orderQty" id={`orderQty${index}`} />
                                                                </FormGroup>
                                                            </Col>
                                                            <Col md={2} sm={2} xs={12} style={{ display: 'inline-block', paddingRight: '0px', textAlign: 'center', paddingLeft: '5px' }}>
                                                                {isMobile ? <FormGroup style={{ marginBottom: '0px' }}>  <Label for="times" style={{ color: appConfig.colors.primary, fontSize: '16px' }}>Times:</Label></FormGroup> : ''}
                                                                <FormGroup aria-label="position" name="position" style={{ display: 'unset' }} row>
                                                                    <FormControlLabel
                                                                        value="M"
                                                                        control={<Checkbox style={{ borderColor: appConfig.colors.primary, fontSize: '18px', color: "#43415e" }} checked={data.other && data.other.M ? data.other.M : false} disabled />}
                                                                        label="M"
                                                                        labelPlacement="end"
                                                                    />
                                                                    <FormControlLabel
                                                                        value="A"
                                                                        control={<Checkbox style={{ borderColor: appConfig.colors.primary, fontSize: '18px', color: "#43415e" }} checked={data.other && data.other.A ? data.other.A : false} disabled />}
                                                                        label="A"
                                                                        labelPlacement="end"
                                                                    />
                                                                    <FormControlLabel
                                                                        value="N"
                                                                        control={<Checkbox style={{ borderColor: appConfig.colors.primary, fontSize: '18px', color: "#43415e" }} checked={data.other && data.other.N ? data.other.N : false} disabled />}
                                                                        label="N"
                                                                        labelPlacement="end"
                                                                    />
                                                                </FormGroup>
                                                            </Col>
                                                        </div>
                                                })}
                                            </div> : ''
                                        }




                                        <Col md={12} sm={12} xs={12} style={{ textAlign: 'right', display: 'inline-block' }}>
                                            <Button variant="outlined" className={classes.submit} type="submit" >
                                                Submit </Button>
                                        </Col>
                                    </form>
                                </div>
                            </div>
                        </Col>
                    </div>
                </div>
            </React.Fragment>);
    }
}

EditDoctorDetail.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = ({ authUser }) => {
    const { userProfileData, profileImage } = authUser
    return { userProfileData, profileImage }
}

export default connect(mapStateToProps, { getuserProfileData, cropImage, editPrescription })(withStyles(styles)(EditDoctorDetail));