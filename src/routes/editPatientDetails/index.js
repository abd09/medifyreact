import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import { Col } from 'reactstrap';
import SideBar from '../../components/userSideBar'
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import { Button, FormGroup, Label, Input } from 'reactstrap';
import AppBar from '@material-ui/core/AppBar';
import { Cropper } from 'react-image-cropper'
import SweetAlert from 'react-bootstrap-sweetalert'
import MatButton from '@material-ui/core/Button';
import { red } from '@material-ui/core/colors';
import Autocomplete from 'react-google-autocomplete';

import axios from '../../constant/axios';
import { getuserProfileData, getPharmacyData, addOrder, cropImage } from '../../actions';
import { connect } from 'react-redux';
import AppConfig from '../../constant/config';

const drawerWidth = 240;
const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120
    },
    lable: {
        textAlign: 'left'
    },
    selectEmpty: {
        marginTop: theme.spacing(2)
    },
    appBar: {
        marginLeft: drawerWidth,
        [theme.breakpoints.up('sm')]: {
            width: `calc(100% - ${drawerWidth}px)`,
        },
    },
    menuButton: {
        marginRight: 20,
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
    dialogDone: {
        backgroundColor: AppConfig.colors.darkGrey,
        color: "#3ddb20",
        '&:hover': {
            background: 'none',
            backgroundColor: AppConfig.colors.darkGrey,
            color: '#3ddb20'
        },
        borderColor: AppConfig.colors.darkGrey
    },
    dialogTitle: {
        backgroundColor: '#5a5a5a',
        '& h2': {
            color: 'white',
            textAlign: 'center'
        },
        borderBottom: '1px solid #5a5a5a'
    },
    dialogContent: {
        backgroundColor: '#5a5a5a',
    },
    root2: {
        flexGrow: 1,
        backgroundColor: '#fff'
    },
    tabsRoot: {
        color: '#42425E',
        borderBottom: '1px solid #7a7a7a',
    },
    tabsIndicator: {
        backgroundColor: '#7a7a7a',
        height: '3px'
    },
    rightDiv: {
        color: "#CCCACD",
        marginBottom: 0,
        fontWeight: 'bold',
        fontSize: '16px',
        textAlign: '-webkit-center',
    },
    listitems: {
        paddingLeft: 40,
        paddingRight: 10,
        paddingTop: 5,
        paddingBottom: 5
    },
    listitems1: {
        paddingLeft: 40,
        paddingRight: 10,
        paddingTop: 14,
        paddingBottom: 5
    },
    tabRoot: {
        textTransform: 'initial',
        minWidth: '50%',
        fontWeight: theme.typography.fontWeightRegular,
        fontFamily: [
            '-apple-system',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ].join(','),
    },
    typography: {
        padding: '15px 0px',
        color: '#CCCACD',
        ['@media (max-width:787px)']: {
            padding: '15px 15px',
        },
    },
    input: {
        '::-webkit-input-placeholder': {
            color: 'red'
        }
    },
    imageDiv: {
        height: '70px',
        width: '70px',
    },
    avatar: {
        margin: theme.spacing(2),
        marginLeft: theme.spacing(4),
        width: 70,
        height: 70
    },
    profile: {
        textAlign: '-webkit-center',
        margin: theme.spacing.unit * 3,
        lineHeight: 'normal'
    },
    tableDiv: {
        padding: '0px',
    },
    tabsFlex: {
        borderBottom: '0px solid #454545',
    },
    tableThTd: {
        color: '#ffffff',
    },
    infoDiv: {
        padding: 18,
        backgroundColor: "#fff"
    },
    disableClick: {
        pointerEvents: 'none'
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: '#ffffff',
        '&:hover': {
            backgroundColor: '#ffffff',
        },
        marginRight: theme.spacing(2),
        marginLeft: 0,
        width: '100%',
        borderRadius: '40px',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(3),
            width: 'auto',
        },
    },
    searchIcon: {
        width: theme.spacing(7),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        fontWeight: 'bold',
        opacity: 1
    },
    inputRoot: {
        color: '#c6c6c6',
    },
    inputInput: {
        color: '#c6c6c6',
        padding: theme.spacing(1, 1, 1, 7),
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: 200,
        },
    },
    icon: {
        position: 'fixed',
        right: '10px',
        color: '#fff'
    },
    iconHover: {
        margin: theme.spacing(2),
        '&:hover': {
            color: red[800],
        },
    },
    avatar: {
        margin: 0,
        padding: 0,
    },
    avatarlist: {
        margin: 0,
        padding: 0,
        width: 35,
        height: 35,
    },
    signup: {
        backgroundColor: AppConfig.colors.primary,
        color: '#fff',
        marginTop: '5px',
        padding: '5px 22px',
        textTransform: 'none',
        borderRadius: '5px',
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        borderRadius: '8px',
        color: AppConfig.colors.primary
    },
    dense: {
        marginTop: theme.spacing(2),
    },
    menu: {
        width: 200,
    },
    submit: {
        backgroundColor: AppConfig.colors.primary,
        color: '#fff',
        marginTop: theme.spacing(1),
        padding: '5px 10px',
        textTransform: 'none',
        borderRadius: '8px'
    },
    submit2: {
        backgroundColor: '#fff',
        color: AppConfig.colors.primary,
        border: 'none',
        marginTop: theme.spacing(2),
        textTransform: 'none',
        fontWeight: 'bolder',
        borderRadius: '0',
        '&:hover': {
            backgroundColor: '#ffffff',
            color: AppConfig.colors.primary,
        },
    },
    form: {
        width: '100%',
        paddingLeft: '20px',
        paddingRight: '35px',
        marginTop: theme.spacing.unit,
    },
});

class EditPatientDetail extends Component {
    constructor(props) {
        super(props)
        this.state = {
            uploaded_image_path: '',
            value: 0,
            openModal: false,
            isLoading: true,
            prescription: '',
            pharmacy: '',
            prescriptions: [],
            note: '',
            addmoreText: [{ location: '', day: '', from: '', to: '' }],
            tool: false,
            croppedImage: '',
            profileimage: '',
            imagePreview: '',
            profileimageMain: '',
            image: '',
            imageLoaded: false,
            alert: false,
            mobileOpen: false,
            firstname: '',
            lastname: '',
            mobile: '',
            location: '',
            age: '',
            dob: '',
            gender: '',
            address: '',
            city: '',
            area: '',
            state: '',
            lat: '',
            lng: '',
            familyHistory: '',
            isPhoneEnabled: false,
            isChatEnabled: false,
            occupation: '',
            physicalActivity: '',
            sleepingHours: '',
            smokingHabits: '',
            alcoholConsumption: '',
            foodPreferences: '',
            height: '',
            weight: ''
        };
    }

    componentWillMount() {
        this.props.getuserProfileData({ history: this.props.history });
        this.props.getPharmacyData({ history: this.props.history });
        console.log('k,dsfkdsfkd', localStorage.getItem('token'));
        axios.get('/users/prescriptionsById', {
            headers: {
                'Content-Type': 'application/json',
                'token': localStorage.getItem('token')
            }
        }).then(result => {
            console.log("getPrescriptionsById result###", result.data.detail);
            this.setState({ prescriptions: result.data.detail && result.data.detail.length > 0 ? result.data.detail : [] })
        })
            .catch(error => {
                console.log("error....", error);
            });
    }

    componentWillReceiveProps = (nextProps) => {
        console.log("this.props.userProfileData", nextProps.profileImage)
        if (this.props.userProfileData !== nextProps.userProfileData) {
            console.log(' nextProps.availability', nextProps.userProfileData);
            this.setState({
                croppedImage: nextProps.profileImage, firstname: nextProps.userProfileData.firstname, lastname: nextProps.userProfileData.lastname, familyHistory: nextProps.userProfileData.familyHistory, mobile: nextProps.userProfileData.mobile, diseases: nextProps.userProfileData.diseases, surgeries: nextProps.userProfileData.surgeries, others: nextProps.userProfileData.others, addmoreText: nextProps.userProfileData.availability, address: nextProps.userProfileData.address, age: nextProps.userProfileData.age, gender: nextProps.userProfileData.gender, isPhoneEnabled: nextProps.userProfileData.isPhoneEnabled,
                isChatEnabled: nextProps.userProfileData.isChatEnabled, lat: nextProps.userProfileData.geoLocation && nextProps.userProfileData.geoLocation.coordinates && nextProps.userProfileData.geoLocation.coordinates.length > 0 ? nextProps.userProfileData.geoLocation.coordinates[1] : '', lng: nextProps.userProfileData.geoLocation && nextProps.userProfileData.geoLocation.coordinates && nextProps.userProfileData.geoLocation.coordinates.length > 0 ? nextProps.userProfileData.geoLocation.coordinates[0] : '',
                occupation: nextProps.userProfileData.occupation,
                physicalActivity: nextProps.userProfileData.physicalActivity,
                sleepingHours: nextProps.userProfileData.sleepingHours,
                smokingHabits: nextProps.userProfileData.smokingHabits,
                alcoholConsumption: nextProps.userProfileData.alcoholConsumption,
                foodPreferences: nextProps.userProfileData.foodPreferences,
                dob: nextProps.userProfileData.dob,
                height: nextProps.userProfileData.height,
                weight: nextProps.userProfileData.weight,
            })
        }

        if (this.props.profileImage !== nextProps.profileImage) {
            this.setState({ croppedImage: nextProps.profileImage })
        }

    }

    addMoreItems = (e) => {
        e.preventDefault()
        let addmoreText = [...this.state.addmoreText];
        addmoreText.push({ location: '', day: '', from: '', to: '' });
        this.setState({ addmoreText: addmoreText })
    }

    openUrl = (e, url) => {
        e.preventDefault();
        window.open(url);
    }

    handleItemChange = (e, name, ind) => {
        e.preventDefault()
        var oldState = [...this.state.addmoreText];
        // eslint-disable-next-line array-callback-return
        oldState.map((data, i) => {
            if (ind === i) {
                oldState[i][name] = e.target.value;
            }
        })
        this.setState({ addmoreText: oldState });
    }

    removeHandler = (e, ind) => {
        e.preventDefault()
        var oldState = [...this.state.addmoreText];
        var newState = []
        // eslint-disable-next-line array-callback-return
        oldState.map((data, i) => {
            if (ind !== i) {
                newState.push(data)
            }
        })
        this.setState({ addmoreText: newState });
    }

    getCity = (addressArray) => {
        console.log('addressArray', addressArray);

        let city = '';
        if (addressArray && addressArray.length > 0) {
            for (let i = 0; i < addressArray.length; i++) {
                if (addressArray[i].types[0] && 'administrative_area_level_2' === addressArray[i].types[0]) {
                    city = addressArray[i].long_name;
                    return city;
                }
            }
        }

    };
    /**
      * Get the area and set the area input value to the one selected
      *
      * @param addressArray
      * @return {string}
      */
    getArea = (addressArray) => {
        let area = '';
        if (addressArray && addressArray.length > 0) {
            for (let i = 0; i < addressArray.length; i++) {
                if (addressArray[i].types[0]) {
                    for (let j = 0; j < addressArray[i].types.length; j++) {
                        if ('sublocality_level_1' === addressArray[i].types[j] || 'locality' === addressArray[i].types[j]) {
                            area = addressArray[i].long_name;
                            return area;
                        }
                    }
                }
            }
        }
    };
    /**
      * Get the address and set the address input value to the one selected
      *
      * @param addressArray
      * @return {string}
      */
    getState = (addressArray) => {
        let state = '';
        if (addressArray && addressArray !== undefined && addressArray.length > 0) {
            for (let i = 0; i < addressArray.length; i++) {
                for (let i = 0; i < addressArray.length; i++) {
                    if (addressArray[i].types[0] && 'administrative_area_level_1' === addressArray[i].types[0]) {
                        state = addressArray[i].long_name;
                        return state;
                    }
                }
            }
        }
    };

    SelectedPlace = (place) => {
        console.log('place', place);
        const address = place.formatted_address ? place.formatted_address : place.name,
            addressArray = place.address_components ? place.address_components : '',
            city = addressArray !== '' ? this.getCity(addressArray) : '',
            area = addressArray !== '' ? this.getArea(addressArray) : '',
            state = addressArray !== '' ? this.getState(addressArray) : '',
            latValue = place.geometry && place.geometry.location ? place.geometry.location.lat() : 0,
            lngValue = place.geometry && place.geometry.location ? place.geometry.location.lng() : 0;
        this.setState({
            address: (address) ? address : '',
            area: (area) ? area : '',
            city: (city) ? city : '',
            state: (state) ? state : '',
            lat: latValue !== 0 ? latValue : '',
            lng: lngValue !== 0 ? lngValue : '',
        })
    }

    handleChange = (event, key) => {
        console.log('key', key, event.target.value)
        if (key === 'dob') {
            var today = new Date();
            var birthDate = new Date(event.target.value);  // create a date object directly from `dob1` argument
            var age_now = today.getFullYear() - birthDate.getFullYear();
            var m = today.getMonth() - birthDate.getMonth();
            if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                age_now--;
            }
            console.log('age_now', age_now)
            if (age_now === NaN) {
                this.setState({ age: 0 });
            } else {
                this.setState({ age: age_now });
            }
        } else {
            this.setState({ [key]: event.target.value });
        }

    };

    handleImageChange = name => event => {
        console.log('name=', name, event.target.files[0].name)
        if (name === 'uploaded_image_path') {
            this.setState({
                [name]: event.target.files,
            });
        }
        this.setState({
            [name]: event.target.files[0].name,
        });
    };

    handleDrawerToggle = () => {
        this.setState(state => ({ mobileOpen: !state.mobileOpen }));
    };

    handleModalClose = () => {
        this.setState({ openModal: false, errors: {} });
    }

    saveProfile = (e) => {
        e.preventDefault();
        const { profileimageMain, firstname, lastname, address, mobile, age, gender, lat, lng, surgeries, isPhoneEnabled, familyHistory, isChatEnabled, occupation, physicalActivity, sleepingHours, alcoholConsumption, smokingHabits, foodPreferences, dob, height, weight } = this.state;
        console.log('this.state', this.state);

        let data = {
            profileimageMain: profileimageMain,
            firstname: firstname,
            lastname: lastname,
            address: address,
            mobile: mobile,
            age: age,
            gender: gender,
            lat: lat,
            lng: lng,
            dob: dob,
            surgeries: surgeries,
            isPhoneEnabled: isPhoneEnabled,
            familyHistory: familyHistory,
            isChatEnabled: isChatEnabled,
            occupation: occupation,
            height: height,
            weight: weight,
            physicalActivity: physicalActivity,
            sleepingHours: sleepingHours,
            smokingHabits: smokingHabits,
            foodPreferences: foodPreferences,
            alcoholConsumption: alcoholConsumption
        }
        console.log("this.state.profileimageMain", data)
        this.props.cropImage(data)
    }

    onConfirm(key, image) {
        let node = this[image]
        console.log('nodes=', node.crop())
        let img = node.crop()
        this.setState({ [key]: false, croppedImage: img, profileimageMain: img })
        console.log('image=', this.state)
    }

    onCancel(key) {
        console.log("key", key)
        this.setState({ [key]: false })
    }

    handleImageLoaded(state) {
        this.setState({
            [state + 'Loaded']: true
        })
    }

    openAlert(e, key) {
        this.setState({ images: e.target.files, image: '' })
        console.log('files=', e.target.files[0], e.target.files)
        var file = e.target.files[0];
        var reader = new FileReader();
        var url = reader.readAsDataURL(file);
        console.log('url=,', url)
        reader.onloadend = function (e) {
            this.setState({
                imagePreview: [reader.result]
            })
        }.bind(this);
        console.log('url=,', url)
        this.setState({ [key]: true });
        console.log('files=', e.target.files[0])
    }

    uploadFile(e) {
        e.preventDefault();
        this.setState({ tool: true })
    }

    openImageUploader() {
        this.setState({ tool: false })
        this.refs.imageUploader.click();
    }

    removeProfilePhoto = () => {
        this.setState({ profileimageMain: null, croppedImage: null, tool: false });
    }

    render() {
        const { classes, userProfileData, children } = this.props;

        const { tool, alert, firstname, lastname, address, mobile, age, gender, familyHistory, lat, lng, isPhoneEnabled, surgeries, isChatEnabled, occupation, physicalActivity, alcoholConsumption, sleepingHours, smokingHabits, foodPreferences, croppedImage, dob, height, weight } = this.state;
        const { addmoreText } = this.state;
        console.log('prescriptions,addmoreTextcroppedImage', this.props.google, addmoreText, croppedImage)
        return (
            <React.Fragment>
                <AppBar position="fixed" className={classes.appBar}>
                    <Toolbar>
                        <IconButton
                            color="inherit"
                            aria-label="Open drawer"
                            onClick={this.handleDrawerToggle}
                            className={classes.menuButton}>
                            <MenuIcon />
                        </IconButton>
                        <Typography variant="h6" color="inherit" style={{ fontWeight: 'bold', color: '#fff' }} noWrap>
                            {"Edit Profile"}
                        </Typography>
                    </Toolbar>
                </AppBar>
                <div className='page-content' style={{ fontWeight: 'bold', backgroundColor: '#fff', marginTop: '-56px', borderRight: '1px solid #655368' }}>
                    <SideBar mobileOpen={this.state.mobileOpen} handleDrawerToggle={this.handleDrawerToggle} userProfileData={userProfileData} >
                        {children}
                    </SideBar>
                </div>
                <div className={classes.root}>
                    <div className={classes.container}>

                        <Col xs="12" md="12" sm="12" >
                            <div className={classes.rightDiv}>
                                {
                                    croppedImage &&
                                    <Avatar alt='Profile' src={croppedImage ? croppedImage : require('../../assets/img/noImage.png')} className={classes.imageDiv} onClick={(e) => this.uploadFile(e)}>
                                    </Avatar>
                                }
                                {(croppedImage === undefined || croppedImage === "" || croppedImage === null) &&
                                    <Avatar alt='Profile' src={require('../../assets/img/noImage.png')} className={classes.imageDiv} onClick={(e) => this.uploadFile(e)}>
                                    </Avatar>
                                }
                                <input type="file" id="file" ref="imageUploader" accept="image/*" style={{ display: "none" }} onChange={(e) => this.openAlert(e, 'alert')} />
                                <SweetAlert
                                    btnSize="sm"
                                    show={tool}
                                    showCancel
                                    cancelBtnBsStyle="danger"
                                    title="Choose an option"
                                    confirmBtnStyle={{ display: 'none' }}
                                    onConfirm={() => this.onConfirm()}
                                    onCancel={() => this.onCancel('tool')}>
                                    <MatButton variant="contained" color="primary" onClick={this.removeProfilePhoto}>Remove Image</MatButton>
                                    <MatButton variant="contained" style={{ backgroundColor: '#58de45', marginLeft: 20 }} onClick={() => this.openImageUploader()}>Select Image</MatButton>
                                </SweetAlert>
                            </div>
                            <div className={classes.profile}>
                                <SweetAlert
                                    style={{ position: 'absolute' }}
                                    btnSize="sm"
                                    show={alert}
                                    showCancel
                                    cancelBtnBsStyle="danger"
                                    title="Image Cropper"
                                    onConfirm={() => this.onConfirm('alert', 'image')}
                                    onCancel={() => this.onCancel('alert')}>
                                    {
                                        this.state.imagePreview &&
                                        <Cropper src={this.state.imagePreview[0]} newItem={300} width={300} height={300}
                                            ref={ref => { this.image = ref }}
                                            fixedRatio={true}
                                            onImgLoad={() => this.handleImageLoaded('image')}
                                        />
                                    }
                                </SweetAlert>
                            </div>
                            <div className={classes.profile}>
                                <div className={classes.tableDiv}>
                                    <form className={classes.form} onSubmit={(e) => this.saveProfile(e)}>

                                        <FormGroup className={classes.lable}>
                                            <Label for="firstname" style={{ color: AppConfig.colors.primary, fontSize: '16px' }}>First name:</Label>
                                            <Input type="text" style={{ borderColor: AppConfig.colors.primary, fontSize: '18px' }} value={firstname} name="firstname" id="firstname" onChange={(e) => this.handleChange(e, 'firstname')} />
                                        </FormGroup>
                                        <FormGroup className={classes.lable}>
                                            <Label for="name" style={{ color: AppConfig.colors.primary, fontSize: '16px' }}>Last name:</Label>
                                            <Input type="text" style={{ borderColor: AppConfig.colors.primary, fontSize: '18px' }} value={lastname} name="name" id="name" onChange={(e) => this.handleChange(e, 'lastname')} />
                                        </FormGroup>
                                        <FormGroup className={classes.lable}>
                                            <Label for="email" style={{ color: AppConfig.colors.primary, fontSize: '16px' }}>Email:</Label>
                                            <Input type="email" style={{ borderColor: AppConfig.colors.primary, fontSize: '18px' }} value={userProfileData ? userProfileData.email : ''} name="email" id="email" disabled />
                                        </FormGroup>
                                        <FormGroup className={classes.lable}>
                                            <Label for="name" style={{ color: '#43415e', fontSize: '16px' }}>Date of Birth:</Label>
                                            <Input type="date" style={{ borderColor: '#43415e', fontSize: '18px' }} value={dob} name="dob" id="dob" maxDate={new Date()} onChange={(e) => this.handleChange(e, 'dob')} />
                                        </FormGroup>
                                        <FormGroup className={classes.lable}>
                                            <Label for="name" style={{ color: AppConfig.colors.primary, fontSize: '16px' }}>Age:</Label>
                                            <Input type="text" style={{ borderColor: AppConfig.colors.primary, fontSize: '18px' }} value={age} name="age" id="age" onChange={(e) => this.handleChange(e, 'age')} />
                                        </FormGroup>
                                        <FormGroup className={classes.lable}>
                                            <Label for="name" style={{ color: AppConfig.colors.primary, fontSize: '16px' }}>Occupation:</Label>
                                            <Input type="text" style={{ borderColor: AppConfig.colors.primary, fontSize: '18px' }} value={occupation} name="occupation" id="occupation" onChange={(e) => this.handleChange(e, 'occupation')} />
                                        </FormGroup>
                                        <FormGroup className={classes.lable}>
                                            <Label for="name" style={{ color: AppConfig.colors.primary, fontSize: '16px' }}>Height (cms):</Label>
                                            <Input type="number" style={{ borderColor: AppConfig.colors.primary, fontSize: '18px' }} value={height} name="height" id="height" onChange={(e) => this.handleChange(e, 'height')} />
                                        </FormGroup>
                                        <FormGroup className={classes.lable}>
                                            <Label for="name" style={{ color: AppConfig.colors.primary, fontSize: '16px' }}>Weight (kgs):</Label>
                                            <Input type="number" style={{ borderColor: AppConfig.colors.primary, fontSize: '18px' }} value={weight} name="weight" id="weight" onChange={(e) => this.handleChange(e, 'weight')} />
                                        </FormGroup>
                                        <FormGroup className={classes.lable}>
                                            <Label for="physicalActivity" style={{ color: AppConfig.colors.primary, fontSize: '16px' }}>Physical Activity:</Label>
                                            <Input type="select" value={physicalActivity} name="physicalActivity" id="physicalActivity" onChange={(e) => this.handleChange(e, 'physicalActivity')} style={{ borderColor: AppConfig.colors.primary, fontSize: '18px' }}>
                                                <option value="" >Select physical activity</option>
                                                <option value={'rare'}>Rare (0-1 days)</option>
                                                <option value={'periodic'}>Periodic (1-3 days)</option>
                                                <option value={'regular'}>Regular (3-5 days)</option>
                                                <option value={'athletic'}>Athletic (5-7 days))</option>
                                            </Input>
                                        </FormGroup>
                                        <FormGroup className={classes.lable}>
                                            <Label for="smokingHabits" style={{ color: AppConfig.colors.primary, fontSize: '16px' }}>Smoking Habits:</Label>
                                            <Input type="select" value={smokingHabits} name="smokingHabits" id="smokingHabits" onChange={(e) => this.handleChange(e, 'smokingHabits')} style={{ borderColor: AppConfig.colors.primary, fontSize: '18px' }}>
                                                <option value="" >Select smoking habits</option>
                                                <option value={'none'}>None</option>
                                                <option value={'occasional'}>Occasional</option>
                                                <option value={'social'}>Social</option>
                                                <option value={'regular'}>Regular</option>
                                                <option value={'heavy'}>Heavy</option>
                                            </Input>
                                        </FormGroup>
                                        <FormGroup className={classes.lable}>
                                            <Label for="alcoholConsumption" style={{ color: AppConfig.colors.primary, fontSize: '16px' }}>Alcohol Consumption:</Label>
                                            <Input type="select" value={alcoholConsumption} name="alcoholConsumption" id="alcoholConsumption" onChange={(e) => this.handleChange(e, 'alcoholConsumption')} style={{ borderColor: AppConfig.colors.primary, fontSize: '18px' }}>
                                                <option value="" >Select alcohol consumption</option>
                                                <option value={'none'}>None</option>
                                                <option value={'occasional'}>Occasional</option>
                                                <option value={'social'}>Social</option>
                                                <option value={'regular'}>Regular</option>
                                                <option value={'heavy'}>Heavy</option>
                                            </Input>
                                        </FormGroup>
                                        <FormGroup className={classes.lable}>
                                            <Label for="sleepingHours" style={{ color: AppConfig.colors.primary, fontSize: '16px' }}>Sleeping Hours:</Label>
                                            <Input type="select" value={sleepingHours} name="sleepingHours" id="sleepingHours" onChange={(e) => this.handleChange(e, 'sleepingHours')} style={{ borderColor: AppConfig.colors.primary, fontSize: '18px' }}>
                                                <option value="" >Select sleeping hours</option>
                                                <option value={'4 hours'}>4 hours</option>
                                                <option value={'5 hours'}>5 hours</option>
                                                <option value={'6 hours'}>6 hours</option>
                                                <option value={'7 hours'}>7 hours</option>
                                                <option value={'8 hours'}>8 hours</option>
                                                <option value={'9 hours'}>9 hours</option>
                                                <option value={'10 hours'}>10 hours</option>
                                                <option value={'11 hours'}>11 hours</option>
                                                <option value={'12 hours'}>12 hours</option>
                                            </Input>
                                        </FormGroup>
                                        <FormGroup className={classes.lable}>
                                            <Label for="foodPreferences" style={{ color: AppConfig.colors.primary, fontSize: '16px' }}>Food Preferences:</Label>
                                            <Input type="select" value={foodPreferences} name="foodPreferences" id="foodPreferences" onChange={(e) => this.handleChange(e, 'foodPreferences')} style={{ borderColor: AppConfig.colors.primary, fontSize: '18px' }}>
                                                <option value="" >Select food preferences</option>
                                                <option value={'vegetarian'}>Vegetarian</option>
                                                <option value={'non-vegetarian'}>Non-Vegetarian</option>
                                                <option value={'eggeterian'}>Eggeterian</option>
                                                <option value={'vegan'}>Vegan</option>
                                                <option value={'gluten-free'}>Gluten- free</option>
                                                <option value={'ketosis'}>Ketosis</option>
                                                <option value={'pescatarian'}>Pescatarian</option>
                                            </Input>
                                        </FormGroup>
                                        <FormGroup className={classes.lable}>
                                            <Label for="name" style={{ color: AppConfig.colors.primary, fontSize: '16px' }}>Gender:</Label>
                                            <div>
                                                <FormGroup check style={{ display: 'inline-block' }}>
                                                    <Label check style={{ color: AppConfig.colors.primary, fontSize: '16px' }}>
                                                        <Input type="radio" name="gender" value="female" style={{ marginTop: '.1rem' }} onChange={(e) => this.handleChange(e, 'gender')} checked={gender === 'female'} />{' '}Female </Label>
                                                </FormGroup>
                                                <FormGroup check style={{ display: 'inline-block', marginLeft: '15px' }}>
                                                    <Label check style={{ color: AppConfig.colors.primary, fontSize: '16px' }}>
                                                        <Input style={{ marginTop: '.1rem' }} onChange={(e) => this.handleChange(e, 'gender')} type="radio" name="gender" value="male" checked={gender === 'male'} />{' '}Male </Label>
                                                </FormGroup>
                                                <FormGroup check style={{ display: 'inline-block', marginLeft: '15px' }}>
                                                    <Label check style={{ color: AppConfig.colors.primary, fontSize: '16px' }}>
                                                        <Input style={{ marginTop: '.1rem' }} onChange={(e) => this.handleChange(e, 'gender')} type="radio" name="gender" value="other" checked={this.state.gender == 'other'} />{' '}Other </Label>
                                                </FormGroup>
                                            </div>
                                        </FormGroup>
                                        <FormGroup className={classes.lable}>
                                            <Label for="location" style={{ color: AppConfig.colors.primary, fontSize: '16px' }}>Location:</Label>
                                            <Autocomplete
                                                className="form-control"
                                                style={{ borderColor: AppConfig.colors.primary, fontSize: '18px' }} name="address"
                                                onChange={(e) => this.handleChange(e, 'address')}
                                                onPlaceSelected={(place) => this.SelectedPlace(place)}
                                                types={['(regions)']}
                                                value={address}
                                                componentRestrictions={{ country: "in" }}
                                            />
                                            {lat !== '' && lng !== '' ?
                                                <Button
                                                    variant="contained"
                                                    className={classes.signup} onClick={(e) => this.openUrl(e, `http://www.google.com/maps/place/${lat},${lng}`)}
                                                > View location</Button>
                                                : ''}
                                        </FormGroup>
                                        <FormGroup className={classes.lable}>
                                            <Label for="mobile" style={{ color: AppConfig.colors.primary, fontSize: '16px' }}>Phone Number:</Label>
                                            <Input type="text" style={{ borderColor: AppConfig.colors.primary, fontSize: '18px' }} value={mobile} name="mobile" id="mobile" onChange={(e) => this.handleChange(e, 'mobile')} />
                                        </FormGroup>
                                        <FormGroup className={classes.lable}>
                                            <Label for="surgeries" style={{ color: AppConfig.colors.primary, fontSize: '16px' }}>Surgeries:</Label>
                                            <Input type="text" style={{ borderColor: AppConfig.colors.primary, fontSize: '18px' }} onChange={(e) => this.handleChange(e, 'surgeries')} value={surgeries} className={classes.input} name="surgeries" id="surgeries" />
                                        </FormGroup>

                                        <FormGroup className={classes.lable}>
                                            <Label for="familyHistory" style={{ color: AppConfig.colors.primary, fontSize: '16px' }}>Family History:</Label>
                                            <Input type="familyHistory" style={{ borderColor: AppConfig.colors.primary, fontSize: '18px' }} value={familyHistory} name="familyHistory" id="familyHistory" onChange={(e) => this.handleChange(e, 'familyHistory')} />
                                        </FormGroup>
                                        <FormGroup className={classes.lable}>
                                            <Label for="isPhoneEnabled" style={{ color: AppConfig.colors.primary, fontSize: '16px' }}>Display phone number:</Label>
                                            <Input type="select" value={isPhoneEnabled} name="isPhoneEnabled" id="isPhoneEnabled" onChange={(e) => this.handleChange(e, 'isPhoneEnabled')} style={{ borderColor: AppConfig.colors.primary, fontSize: '18px' }}>
                                                <option value="" >Select Status</option>
                                                <option value={true}>Enable</option>
                                                <option value={false}>Disable</option>
                                            </Input>
                                        </FormGroup>
                                        <FormGroup className={classes.lable}>
                                            <Label for="isChatEnabled" style={{ color: AppConfig.colors.primary, fontSize: '16px' }}>Chat Status:</Label>
                                            <Input type="select" value={isChatEnabled} name="isChatEnabled" id="isChatEnabled" onChange={(e) => this.handleChange(e, 'isChatEnabled')} style={{ borderColor: AppConfig.colors.primary, fontSize: '18px' }}>
                                                <option value={true}>Enabled</option>
                                                <option value={false}>Disabled</option>
                                            </Input>
                                        </FormGroup>


                                        <Button variant="outlined" className={classes.submit} type="submit" >Submit</Button>
                                    </form>
                                </div>
                            </div>
                        </Col>
                    </div>
                </div>
            </React.Fragment>);
    }
}

EditPatientDetail.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = ({ authUser }) => {
    const { userProfileData, pharmacyData, profileImage } = authUser
    return { userProfileData, pharmacyData, profileImage }
}

export default connect(mapStateToProps, { getuserProfileData, getPharmacyData, addOrder, cropImage })(withStyles(styles)(EditPatientDetail));