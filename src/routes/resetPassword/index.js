import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import { createMuiTheme } from "@material-ui/core/styles";
import { resetPassword } from '../../actions';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form'
import { ThemeProvider } from "@material-ui/styles";
import Box from '@material-ui/core/Box';
import { Col, Row } from 'reactstrap';

const styles = theme => ({
    main: {
        marginTop: theme.spacing.unit * 8,
        width: 'auto',
        display: 'block',
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3,
        [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
            width: 400,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    heading: {
        color: '#43415e',
    },
    paper: {
        marginTop: theme.spacing.unit * 8,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
    },
    heading2: {
        marginTop: '25px',
        fontSize: '16px',
        color: '#A6A7AD'
    },
    avatar: {
        height: '80px',
    },
    form: {
        width: '100%',
        marginTop: theme.spacing.unit,
    },
    label: {
        color: '#43415e',
        "&$focused": {
            color: '#43415e'
        }
    },
    signup: {
        backgroundColor: '#fff',
        border: '1px solid #43415e',
        color: '#43415e',
        marginTop: theme.spacing.unit * 3,
        padding: '10px 50px'
    },
    submit: {
        backgroundColor: '#43415e',
        color: '#fff',
        marginTop: theme.spacing.unit * 3,
        padding: '10px 50px',
        textTransform: 'none',
        fontSize: '16px'
    },
    error: {
        color: 'red',
        textAlign: 'left'
    },
    font: {
        fontFamily: [
            '"Tangerine"',
            'cursive'
        ].join(','),
    }
});

class ResetPassword extends Component {
    constructor(props) {
        super(props)
        this.state = {
            password: '',
            confirm: '',
            checkedB: false,
            errors: {},
        }
        this.onSubmit = this.onSubmit.bind(this);
        this.handleValidation = this.handleValidation.bind(this);
    }

    handleValidation = () => {
        let errors = {};
        let formIsValid = true;
        console.log("this.state handleValidation", this.state)

        //password
        if (this.state.password === '') {
            formIsValid = false;
            errors["password"] = "This field is required";
        }

        //confirm
        if (this.state.confirm === '') {
            formIsValid = false;
            errors["confirm"] = "This field is required";
        }

        if (this.state.password !== '' && this.state.confirm !== '' && this.state.password != this.state.confirm) {
            formIsValid = false;
            errors["confirm"] = "Passwords do not match";
        }

        this.setState({ errors: errors });
        return formIsValid;
    }

    onSubmit = async (e) => {
        e.preventDefault()
        let postData = {
            password: this.state.password,
            confirm: this.state.confirm,
            token: this.props.match.params.token 
        }
        console.log("postData", postData)
        let isValid = await this.handleValidation();
        if (isValid === true) {
            this.props.resetPassword({ data: postData, history: this.props.history });
        } else {
            return;
        }
    }

    handleChange = (e, key) => {
        console.log("e.....", key)
        if (key == 'checkedB') {
            this.setState({ [key]: e.target.checked })
        } else {
            this.setState({ [key]: e.target.value })
        }
    }

    render() {
        const { classes } = this.props;
        const theme = createMuiTheme({
            palette: {
                primary: { main: '#43415e' },
                secondary: { main: '#43415e' },

            },
            overrides: {
                MuiInputLabel: { // Name of the component ⚛️ / style sheet
                    root: {
                        color: "orange",
                        "&$focused": {
                            color: "#43415e",
                            borderColor: '#fff'
                        }
                    }
                },
                MuiInput: {
                    /*  focused: {
                         color: '#43415e',
                         borderColor: '#fff'
                     }, */
                    root: {
                        color: '#43415e',
                        borderColor: '#fff'
                    },

                    underline: {
                        color: '#43415e',
                        "&$focused": {
                            color: "#43415e",
                        },
                        "&:before": {
                            color: "#43415e",
                            borderBottom: '1px solid rgba(231, 231, 231, 1)'
                        },
                        borderBottom: '1px solid rgba(231, 231, 231, 1)'
                    }
                },
                MuiFormLabel: {

                },
                MuiInputBase: {
                    root: {
                        padding: '8px 0 7px'
                    }
                },
            },

        });
        console.log(' this.props', this.props);
    
        return (
            <main className={classes.main}>
                <CssBaseline />
                <Typography className={classes.heading} component="h1" variant="h5">
                    <img className={classes.avatar} src={require('../../assets/img/medifylogo2.png')} />
                </Typography>
                <Typography className={classes.heading2} component="h1" variant="h5">
                    Enter new password.</Typography>
                <Row>
                    <Col md={12} sm={12} xs={12} lg={12} xl={12}>
                        <form className={classes.form} onSubmit={(e) => this.onSubmit(e)}>
                            <ThemeProvider theme={theme}>
                                <Col md={12} sm={12} xs={12} lg={12} xl={12}>
                                    <FormControl margin="normal" fullWidth>
                                        <InputLabel htmlFor="password" className={classes.label}>Password</InputLabel>
                                        <Input value={this.state.password} id="password" type="password" name="password" onChange={(e) => this.handleChange(e, 'password')} />
                                        <span className={classes.error} >{this.state.errors["password"]}</span>
                                    </FormControl>
                                </Col>
                                <Col md={12} sm={12} xs={12} lg={12} xl={12}>
                                    <FormControl margin="normal" fullWidth>
                                        <InputLabel htmlFor="confirm" className={classes.label}>Confirm</InputLabel>
                                        <Input value={this.state.confirm} id="confirm" type="password" name="confirm" onChange={(e) => this.handleChange(e, 'confirm')} />
                                        <span className={classes.error} >{this.state.errors["confirm"]}</span>
                                    </FormControl>
                                </Col>
                                <br />
                                <Button
                                    type="submit"
                                    variant="contained"
                                    className={classes.submit}
                                >Reset</Button>
                            </ThemeProvider>
                        </form>
                    </Col>
                </Row>
                <Box mt={5} style={{ marginTop: '150px' }}>
                    <Typography variant="body2" align="center" className={classes.heading} style={{ fontSize: '11px' }}>
                        {'Terms of use. Privacy policy'}
                    </Typography>
                </Box>
            </main>
        );
    }
}

ResetPassword.propTypes = {
    classes: PropTypes.object.isRequired,
};

ResetPassword = reduxForm({
    form: 'ResetPasswordValidation'  // a unique identifier for this form
})(ResetPassword)

ResetPassword = connect(
    state => ({
        initialValues: state // pull initial values from account reducer
    }),
    { resetPassword }               // bind account loading action creator
)(ResetPassword)

export default withStyles(styles)(ResetPassword);