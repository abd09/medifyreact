import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import Button from '@material-ui/core/Button';
import SideBar from '../../components/userSideBar'
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import AppBar from '@material-ui/core/AppBar';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { getuserProfileData, getOrders, updateOrder } from '../../actions';
import { connect } from 'react-redux';
import Icon from '@material-ui/core/Icon';
import { red } from '@material-ui/core/colors';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import appconfig from '../../constant/config';
import {
    Col,
} from 'reactstrap';
import moment from 'moment';

const drawerWidth = 240;

const styles = theme => ({
    root: {
        flexGrow: 1,
        backgroundColor: '#f5f5f5 !important',
    },
    submit2: {
        backgroundColor: '#43415e',
        color: '#fff',
        marginTop: theme.spacing.unit * 2,
        padding: '5px 12px',
        textTransform: 'none',
        borderRadius: '0',
        '&:hover': {
            border: '1px solid #2c2a4b',
            backgroundColor: '#2c2a4b'
        }
    },
    appBar: {
        marginLeft: drawerWidth,
        [theme.breakpoints.up('sm')]: {
            width: `calc(100% - ${drawerWidth}px)`,
        },
        boxShadow: 'none'
    },
    menuButton: {
        marginRight: 20,
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
    dialogDone: {
        backgroundColor: '#454545',
        color: "#3ddb20",
        '&:hover': {
            background: 'none',
            backgroundColor: '#454545',
            color: '#3ddb20'
        },
        borderColor: '#454545'
    },
    dialogTitle: {
        backgroundColor: '#5a5a5a',
        '& h2': {
            color: 'white',
            textAlign: 'center'
        },
        borderBottom: '1px solid #5a5a5a'
    },
    dialogContent: {
        backgroundColor: '#5a5a5a',
    },
    root2: {
        flexGrow: 1,
        backgroundColor: '#fff'
    },
    tabsRoot: {
        borderBottom: '1px solid #454545',
    },
    tabsIndicator: {
        backgroundColor: '#58de45',
        height: '4px'
    },
    listitems: {
        paddingLeft: 10,
        paddingRight: 10,
    },
    tabRoot: {
        textTransform: 'initial',
        minWidth: '50%',
        fontWeight: theme.typography.fontWeightRegular,
        // marginRight: theme.spacing.unit * 4,
        fontFamily: [
            '-apple-system',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ].join(','),
    },
    tabSelected: {},
    typography: {
        padding: '15px 0px',
        color: '#CCCACD',
        ['@media (max-width:787px)']: {
            padding: '15px 15px',
        },
    },
    avatar: {
        margin: theme.spacing.unit * 2,
        marginLeft: theme.spacing.unit * 4,
        width: 70,
        height: 70
    },
    profile: {
        marginTop: theme.spacing.unit * 4,
        lineHeight: 1
    },
    tableDiv: {
        padding: '0px',
    },
    tabsFlex: {
        borderBottom: '1px solid #454545',
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: '#ffffff',
        '&:hover': {
            backgroundColor: '#ffffff',
        },
        marginRight: theme.spacing(2),
        marginLeft: 0,
        width: '100%',
        borderRadius: '40px',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(3),
            width: 'auto',
        },
    },
    searchIcon: {
        width: theme.spacing(7),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        fontWeight: 'bold',
        opacity: 1
    },
    inputRoot: {
        color: '#c6c6c6',
    },
    icon: {
        // margin: theme.spacing(2),
        position: 'fixed',
        right: '10px',
        color: '#fff'

    },
    iconHover: {
        margin: theme.spacing(2),
        '&:hover': {
            color: red[800],
        },
    },
    docName: {
        color: '#FFFFFF',
        font: 'bolder'
    },
    inputInput: {
        color: '#c6c6c6',
        padding: theme.spacing(1, 1, 1, 7),
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: 200,
        },
    },
    docdivide: {
        marginLeft: '12px',
        marginRight: '12px',
        borderBottom: '1px solid #eeeeee'
    },
    form: {
        width: '100%',
        paddingLeft: '20px',
        paddingRight: '35px',
        marginTop: theme.spacing.unit,
    }
});

class Order extends Component {
    constructor(props) {
        super(props)
        console.log('hey  i m here', this.props.location)
        this.state = {
            docs: [],
            value: '',
            isLoading: true,
            mobileOpen: false,
            anchorEl: null,
            fullWidth: false,
            maxWidth: 'md',
            orderId: '',
            status: '',
            openModal: false,
            openCancelModal: false,
        };
    }

    // *snip*
    componentWillMount() {
        this.props.getuserProfileData({ history: this.props.history });
        this.props.getOrders({ history: this.props.history });
    }

    handleChange = (event, value) => {
        console.log('value', value)
        this.setState({ value });
    };

    handleStepChange = (value) => {
    }

    handleCancelModal = (e, orderId) => {
        e.preventDefault();
        this.setState({ openCancelModal: true, orderId: orderId, status: 'cancelled' })
    }

    handleCancelModalClose = () => {
        this.setState({ openCancelModal: false, errors: {} });
    }


    handleModal = (e, orderId) => {
        e.preventDefault();
        this.setState({ openModal: true, orderId: orderId, status: 'completed' })
    }

    handleModalClose = () => {
        this.setState({ openModal: false, errors: {} });
    }

    handleOnClick = (e) => {
        e.preventDefault();
        this.props.history.push(`/${localStorage.getItem('role')}/addOrder`)
    }

    handleOnClickDetail = (e, orderId) => {
        e.preventDefault();
        this.props.history.push(`/${localStorage.getItem('role')}/orderDetails/${orderId}`)
    }

    handleDrawerToggle = () => {
        this.setState(state => ({ mobileOpen: !state.mobileOpen }));
    };

    handleSubmit = async (e) => {
        e.preventDefault();
        let data = {
            status: this.state.status,
            orderId: this.state.orderId && this.state.orderId != null && this.state.orderId ? this.state.orderId : null
        }

        this.props.updateOrder({
            data,
            history: this.props.history
        })
        this.setState({ openModal: false, openCancelModal: false, errors: {} });
        this.props.getOrders({ history: this.props.history });
    }

    render() {
        const { classes, userProfileData, orderData, children } = this.props;
        const { openModal, openCancelModal } = this.state;
        console.log('userProfileData,orderData', userProfileData, orderData)

        return (
            <React.Fragment>
                <AppBar position="fixed" className={classes.appBar}>
                    <Toolbar>
                        <IconButton
                            color="inherit"
                            aria-label="Open drawer"
                            onClick={this.handleDrawerToggle}
                            className={classes.menuButton}
                        >
                            <MenuIcon />
                        </IconButton>
                        <Typography variant="h6" color="inherit" style={{ fontWeight: 'bold', color: '#fff' }} noWrap>
                            Orders
                        </Typography>
                        <Icon className={classes.icon} color="disabled" onClick={(e) => { this.handleOnClick(e) }} fontSize="large">
                            add</Icon>
                    </Toolbar>
                </AppBar>
                <div className='page-content' style={{ marginTop: '-60px' }}>
                    <SideBar mobileOpen={this.state.mobileOpen} handleDrawerToggle={this.handleDrawerToggle} userProfileData={userProfileData}>
                        {children}
                    </SideBar>

                </div>
                <div className={classes.root}>
                    <div key={'orderList'} className={classes.infoDiv}>

                        <List className={classes.root2} >
                            {
                                orderData && orderData.length > 0 ?
                                    orderData.map((data, index) => {
                                        console.log('dataaaa', data);
                                        return <div key={index}>
                                            <ListItem className={classes.listitems}>
                                                <Col sm={7} xs={7} md={7} lg={7} xl={7} onClick={(e) => { data && data != null && data._id ? this.handleOnClickDetail(e, data._id) : this.handleOnClickDetail(e, '') }}>
                                                    <ListItemText primary={<Typography style={{ color: '#42425E', fontWeight: 'bold', fontSize: '11px' }}>Order ID: {data.orderNo}</Typography>} secondary={<React.Fragment>
                                                        <Typography variant="h5" style={{ color: '#42425E', fontWeight: 'bold', fontSize: '16px', padding: '2px 0 0px 0px', letterSpacing: '0.04em' }} className={classes.docName}>{data && data != null && data.prescription && data.prescription != null && data.prescription.title && data.prescription.title != null ? data.prescription.title : ''}</Typography>
                                                        <Typography variant="body1" style={{ color: '#6b6b78', fontSize: '17px', fontWeight: 'lighter' }} className={classes.docName}>Assigned to : {data && data != null && data.pharmacy && data.pharmacy != null && data.pharmacy.firstname && data.pharmacy.firstname != null ? data.pharmacy.firstname + ' ' + data.pharmacy.lastname : ''}</Typography>
                                                    </React.Fragment>} />
                                                </Col>
                                                <Col sm={5} xs={5} md={5} lg={5} xl={5}>
                                                    <ListItemText style={{
                                                        textAlign: 'right'
                                                    }} primary={
                                                        <div>
                                                            <Typography style={{ color: appconfig.colors.primary, fontWeight: 'bold', fontSize: '13px', marginTop: `${data && data != null && data.status && data.status != null && data.status == 'cancelled' ? '-28px' : ''}`, display: 'inline-block' }}>Status:  </Typography>
                                                            <Typography style={{ color: ((data && data != null && data.status && data.status != null && data.status == 'accepted') ? appconfig.colors.info : data.status == 'cancelled' ? appconfig.colors.grey : data.status == 'completed' ? appconfig.colors.success : data.status == 'rejected' ? appconfig.colors.danger : appconfig.colors.warning), fontWeight: 'bold', fontSize: '13px', marginTop: `${data && data != null && data.status && data.status != null && data.status == 'cancelled' ? '-28px' : ''}`, display: 'inline-block',marginLeft:'2px' }}>{data && data != null && data.status && data.status != null ? `${data.status.charAt(0).toUpperCase()}${data.status.slice(1)}` : ''}</Typography>
                                                        </div>
                                                    } />
                                                    {data && data != null && data.status && data.status != null && data.status == 'accepted' ? <ListItemText style={{
                                                        textAlign: 'right',
                                                        marginRight: '-6px'
                                                    }} primary={<Button onClick={(e) => this.handleModal(e, data._id)} style={{
                                                        backgroundColor: '#42425E', color: '#fff', fontWeight: 'bold', fontSize: '15px', padding: '1px 16px',
                                                        borderRadius: '25px',
                                                        textTransform: 'none'
                                                    }}>Received</Button>} /> : ''}
                                                    {data && data != null && data.status && data.status != null && (data.status == 'accepted' || data.status == 'completed') ?
                                                        <div>
                                                            <ListItemText style={{
                                                                textAlign: 'right',
                                                                right: '15px', marginTop: '0', marginBottom: '0'
                                                            }} primary={<Typography style={{
                                                                backgroundColor: '#fff', color: '#42425E', fontWeight: 'bold', fontSize: '15px',
                                                            }}><div style={{ display: 'inline-block' }}>Bill Amount: </div> <div style={{ display: 'inline-block' }}>{data && data != null && data.amount && data.amount != null ? 'Rs. ' + data.amount : ''}</div></Typography>} />
                                                            <ListItemText style={{
                                                                textAlign: 'right',
                                                                right: '15px', marginTop: '0', marginBottom: '0'
                                                            }} primary={<Typography style={{
                                                                backgroundColor: '#fff', color: '#42425E', fontWeight: 'bold', fontSize: '13px',
                                                            }}>{data && data != null && data.createdAt && data.createdAt != null ? moment(data.createdAt).format("D MMMM YYYY") : ''}</Typography>} />
                                                        </div> : data && data != null && data.status && data.status != null && data.status == 'requested' ? <ListItemText style={{
                                                            textAlign: 'right',
                                                            marginRight: '-6px'
                                                        }} primary={<Button onClick={(e) => this.handleCancelModal(e, data._id)} style={{
                                                            backgroundColor: '#fff', color: '#42425E', fontWeight: 'bold', fontSize: '15px', padding: '0px 22px',
                                                            border: '1px solid #42425E',
                                                            borderRadius: '25px',
                                                            textTransform: 'none'
                                                        }}>Cancel</Button>} /> : ''}
                                                </Col>
                                            </ListItem>
                                            <li className={classes.docdivide}></li>
                                        </div>
                                    }) : <div><Typography style={{
                                        backgroundColor: '#fff', color: 'gray', fontWeight: 'bold', fontSize: '25px',
                                    }}>No Orders</Typography></div>
                            }

                        </List>


                    </div>
                    <Dialog
                        fullWidth={this.state.fullWidth}
                        maxWidth={this.state.maxWidth}
                        open={openModal}
                        onClose={this.handleModalClose}
                        aria-labelledby="max-width-dialog-title"
                    >
                        <DialogTitle id="max-width-dialog-title">Did you receive the order?</DialogTitle>
                        <DialogContent>
                            <form className={classes.form} onSubmit={(e) => this.handleSubmit(e)}>
                                <div>
                                    <Button variant="outlined" style={{ padding: '1px 10px', float: 'left', borderRadius: '10px' }} className={classes.submit2} type="submit" >
                                        Received</Button>
                                    <Button className={classes.submit2} onClick={this.handleModalClose} style={{ padding: '1px 10px', float: 'right', borderRadius: '10px' }} variant="outlined" >
                                        Close</Button>
                                </div>
                            </form>
                        </DialogContent>
                    </Dialog>
                    <Dialog
                        fullWidth={this.state.fullWidth}
                        maxWidth={this.state.maxWidth}
                        open={openCancelModal}
                        onClose={this.handleCancelModalClose}
                        aria-labelledby="max-width-dialog-title"
                    >
                        <DialogTitle id="max-width-dialog-title">Are you sure you want to cancel the order?</DialogTitle>
                        <DialogContent>
                            <form className={classes.form} onSubmit={(e) => this.handleSubmit(e)}>
                                <div >
                                    <Button variant="outlined" style={{ padding: '1px 10px', borderRadius: '10px' }} className={classes.submit2} type="submit" >
                                        Yes</Button>
                                    <Button className={`${classes.submit2} ml-2`} onClick={this.handleCancelModalClose} style={{ padding: '1px 10px', borderRadius: '10px' }} variant="outlined" >
                                        Cancel</Button>

                                </div>

                            </form>
                        </DialogContent>
                    </Dialog>

                </div>

            </React.Fragment>);
    }
}

Order.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = ({ authUser, orderReducer }) => {
    const { userProfileData } = authUser
    const { orderData } = orderReducer
    return { userProfileData, orderData }
}

export default connect(mapStateToProps, { getuserProfileData, getOrders, updateOrder })(withStyles(styles)(Order));