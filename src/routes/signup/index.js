import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import Box from '@material-ui/core/Box';
import { createMuiTheme } from "@material-ui/core/styles";
import { ThemeProvider } from "@material-ui/styles";
import { Col, Row } from 'reactstrap';
import Link from '@material-ui/core/Link';
import { signup } from '../../actions';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form'
import Checkbox from '@material-ui/core/Checkbox';
import axios from '../../constant/axios';

const styles = theme => ({
    main: {
        marginTop: theme.spacing.unit * 8,
        width: 'auto',
        display: 'block', // Fix IE 11 issue.
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3,
        [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
            width: 400,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    heading: {
        color: '#43415e',
    },
    paper: {
        marginTop: theme.spacing.unit * 8,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
    },
    avatar: {
        height: "25px",
        width: "114px"
    },
    form: {
        width: '100%',
        marginTop: theme.spacing.unit,
    },
    label: {
        color: '#43415e',
        "&$focused": {
            color: '#43415e'
        }
    },
    submit: {
        backgroundColor: '#43415e',
        color: '#fff',
        padding: '10px 50px',
        textTransform: 'none'
    },
    error: {
        color: 'red',
        textAlign: 'left'
    }
});

class SignUp extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email: '',
            password: '',
            confirm: '',
            mobile: '',
            firstname: '',
            lastname: '',
            aadharNo: '',
            mobile: '',
            checkedB: false,
            errors: {},
            redirect: false,
        }
        this.onSubmit = this.onSubmit.bind(this);
        this.handleEmailValidation = this.handleEmailValidation.bind(this);
        this.handlePhoneValidation = this.handlePhoneValidation.bind(this);
        this.handleValidation = this.handleValidation.bind(this);
    }

    handleValidation = () => {
        let errors = {};
        let formIsValid = true;
        console.log("this.state handleValidation", this.state)
        //email

        if (typeof this.state.email !== "") {
            let lastAtPos = this.state.email.lastIndexOf('@');
            let lastDotPos = this.state.email.lastIndexOf('.');

            if (!(lastAtPos < lastDotPos && lastAtPos > 0 && this.state.email.indexOf('@@') == -1 && lastDotPos > 2 && (this.state.email.length - lastDotPos) > 2)) {
                formIsValid = false;
                errors["email"] = "Please enter valid email address.";
            }
        }

        //email
        if (this.state.email === '') {
            formIsValid = false;
            errors["email"] = "This field is required";
        }
        //password
        if (this.state.password === '') {
            formIsValid = false;
            errors["password"] = "This field is required";
        }

        //confirm
        if (this.state.confirm === '') {
            formIsValid = false;
            errors["confirm"] = "This field is required";
        }

        // phoneNo
        if (this.state.mobile === '') {
            formIsValid = false;
            errors["mobile"] = "This field is required";
        }
        //aadharNo
        if (this.state.aadharNo === '') {
            formIsValid = false;
            errors["aadharNo"] = "This field is required";
        }

        //firstname
        if (this.state.firstname === '') {
            formIsValid = false;
            errors["firstname"] = "This field is required";
        }

        if (this.state.lastname === '') {
            formIsValid = false;
            errors["lastname"] = "This field is required";
        }

        if (this.state.password !== '' && this.state.confirm !== '' && this.state.password != this.state.confirm) {
            formIsValid = false;
            errors["confirm"] = "Passwords do not match";
        }

        if (this.state.checkedB === false) {
            formIsValid = false;
            errors["checkedB"] = "Please accept terms and conditions";
        }

        this.setState({ errors: errors });
        return formIsValid;
    }

    handlePhoneValidation = async () => {
        console.log('this.state....', this.state);
        let errors = {};
        let formIsValid = true;
        //email
        if (this.state.aadharNo === '') {
            formIsValid = false;
            errors["aadharNo"] = "This field is required";
        }

        if (this.state.aadharNo !== '' || this.state.aadharNo !== undefined) {
            let data = {
                aadharNo: this.state.aadharNo
            }
            await axios.post('/users/phoneNoExist', data, {
                headers: {
                    'Content-Type': 'application/json',
                    'token': localStorage.getItem('token')
                }
            }
            ).then(result => {
                console.log("result,,,,,,,,,,", result)
                if (result.data.error === true) {
                    formIsValid = false;
                    errors["aadharNo"] = "Invalid registration code";
                } else {
                    formIsValid = true;
                    errors["aadharNo"] = "";
                }
            })
                .catch(error => {
                    console.log("error", error)
                    formIsValid = false;
                    errors["aadharNo"] = "Invalid registration code";
                });
        }
        this.setState({ errors: errors });
        console.log('formIsValid', formIsValid,errors);

        return formIsValid;
    }

    handleEmailValidation = async () => {
        console.log('this.state....', this.state);
        let errors = {};
        let formIsValid = true;
        //email
        if (this.state.email === '') {
            formIsValid = false;
            errors["email"] = "This field is required";
        }

        if (this.state.email !== '' || this.state.email !== undefined) {
            let data = {
                email: this.state.email,
                role: this.props.match.params.category
            }
            await axios.post('/users/emailExist', data, {
                headers: {
                    'Content-Type': 'application/json',
                    'token': localStorage.getItem('token')
                }
            }
            ).then(result => {
                console.log("result", result)
                if (result.data.error === true) {
                    formIsValid = false;
                    errors["email"] = "Email Already Exists";
                } else {
                    formIsValid = true;
                }
            })
                .catch(error => {
                    console.log("error", error)
                    formIsValid = false;
                    errors["email"] = "Email Already Exists";
                });
        }
        this.setState({ errors: errors });
        return formIsValid;
    }

    onSubmit = async (e) => {
        e.preventDefault()
        let postData = {
            email: this.state.email,
            password: this.state.password,
            mobile: this.state.mobile,
            aadharNo: this.state.aadharNo,
            lastname: this.state.lastname,
            firstname: this.state.firstname,
            role: this.props.match.params.category,
        }
        console.log("postData", postData)
        let isValid = await this.handleValidation();
        let isCodeValid = await this.handlePhoneValidation();
        let isEmailValid = await this.handleEmailValidation();
        if (isValid === true && isCodeValid == true && isEmailValid == true) {
            this.props.signup({ data: postData, history: this.props.history });
        } else {
            return;
        }
    }

    handleChange = (e, key) => {
        console.log("e.....", key)
        if (key == 'checkedB') {
            this.setState({ [key]: e.target.checked })
        } else {
            this.setState({ [key]: e.target.value })
        }
    }

    render() {
        const { classes } = this.props;
        console.log('this.propsss', this.props, this.params, this.props.history);
        const theme = createMuiTheme({
            palette: {
                primary: { main: '#43415e' },
                secondary: { main: '#43415e' },
            },
            overrides: {
                MuiInputLabel: { // Name of the component ⚛️ / style sheet
                    root: {
                        color: "orange",
                        "&$focused": {
                            color: "#43415e",
                            borderColor: '#fff'
                        }
                    }
                },
                MuiInput: {
                    root: {
                        color: '#43415e',
                        borderColor: '#fff'
                    },
                    underline: {
                        color: '#43415e',
                        "&$focused": {
                            color: "#43415e",
                        },
                        "&:before": {
                            color: "#43415e",
                            borderBottom: '1px solid rgba(231, 231, 231, 1)'
                        },
                        borderBottom: '1px solid rgba(231, 231, 231, 1)'
                    }
                },
                MuiInputBase: {
                    root: {
                        padding: '8px 0 7px'
                    }
                },
            },
        });
        return (
            <main className={classes.main}>
                <CssBaseline />
                <div className="container">
                    <Typography className={classes.heading} component="h1" variant="h5">
                        <img className={classes.avatar} src={require('../../assets/img/logo.png')} />
                    </Typography>
                    <Row>
                        <Col md={12} sm={12} xs={12} lg={12} xl={12}>
                            <form className={classes.form} onSubmit={(e) => this.onSubmit(e)}>
                                <ThemeProvider theme={theme}>
                                    <Col md={12} sm={12} xs={12} lg={12} xl={12}>
                                        <FormControl margin="normal" fullWidth>
                                            <InputLabel htmlFor="firstname" className={classes.label}>First name</InputLabel>
                                            <Input value={this.state.firstname} id="firstname" name="firstname" onChange={(e) => this.handleChange(e, 'firstname')} autoFocus />
                                            <span className={classes.error} >{this.state.errors["firstname"]}</span>
                                        </FormControl>
                                    </Col>
                                    <Col md={12} sm={12} xs={12} lg={12} xl={12}>
                                        <FormControl margin="normal" fullWidth>
                                            <InputLabel htmlFor="lastname" className={classes.label}>Last name</InputLabel>
                                            <Input value={this.state.lastname} id="lastname" name="lastname" onChange={(e) => this.handleChange(e, 'lastname')} />
                                            <span className={classes.error} >{this.state.errors["lastname"]}</span>
                                        </FormControl>
                                    </Col>
                                    <Col md={12} sm={12} xs={12} lg={12} xl={12}>
                                        <FormControl margin="normal" fullWidth>
                                            <InputLabel htmlFor="aadharNo" className={classes.label}>Registration/Aadhar No.</InputLabel>
                                            <Input value={this.state.aadharNo} onBlur={this.handlePhoneValidation} id="aadharNo" name="aadharNo" onChange={(e) => this.handleChange(e, 'aadharNo')} />
                                            <span className={classes.error} >{this.state.errors["aadharNo"]}</span>
                                        </FormControl>
                                    </Col>
                                    <Col md={12} sm={12} xs={12} lg={12} xl={12}>
                                        <FormControl margin="normal" fullWidth>
                                            <InputLabel htmlFor="mobile" className={classes.label}>Phone No.</InputLabel>
                                            <Input value={this.state.mobile} id="mobile" name="mobile" onChange={(e) => this.handleChange(e, 'mobile')} />
                                            <span className={classes.error} >{this.state.errors["mobile"]}</span>
                                        </FormControl>
                                    </Col>
                                    <Col md={12} sm={12} xs={12} lg={12} xl={12}>
                                        <FormControl margin="normal" fullWidth>
                                            <InputLabel htmlFor="email" className={classes.label}>Email</InputLabel>
                                            <Input value={this.state.email} id="email" type="email" name="email" onBlur={this.handleEmailValidation} onChange={(e) => this.handleChange(e, 'email')} />
                                            <span className={classes.error} >{this.state.errors["email"]}</span>
                                        </FormControl>
                                    </Col>
                                    <Col md={12} sm={12} xs={12} lg={12} xl={12}>
                                        <FormControl margin="normal" fullWidth>
                                            <InputLabel htmlFor="password" className={classes.label}>Password</InputLabel>
                                            <Input value={this.state.password} name="password" onChange={(e) => this.handleChange(e, 'password')} type="password" id="password" />
                                            <span className={classes.error} >{this.state.errors["password"]}</span>
                                        </FormControl>
                                    </Col>
                                    <Col md={12} sm={12} xs={12} lg={12} xl={12}>
                                        <FormControl margin="normal" fullWidth>
                                            <InputLabel htmlFor="confirm" className={classes.label}>Confirm Password</InputLabel>
                                            <Input value={this.state.confirm} name="confirm" onChange={(e) => this.handleChange(e, 'confirm')} type="password" id="confirm" />
                                            <span className={classes.error} >{this.state.errors["confirm"]}</span>
                                        </FormControl>
                                    </Col>
                                    <Col md={12} sm={12} xs={12} lg={12} xl={12}>
                                        <FormControlLabel className={classes.heading}
                                            control={
                                                <Checkbox
                                                    checked={this.state.checkedB}
                                                    onChange={(e) => this.handleChange(e, 'checkedB')}
                                                    name="checkedB"
                                                    value="checkedB"
                                                    color="primary" />
                                            }
                                            label="I agree with terms and conditions"
                                        /><br />
                                        <span className={classes.error} >{this.state.errors["checkedB"]}</span>
                                    </Col>
                                    <br />
                                    <Button
                                        type="submit"
                                        variant="contained"
                                        className={classes.submit}
                                    > Sign Up</Button>
                                    <InputLabel style={{ paddingTop: '15px' }} className={classes.heading}>
                                        <Link color="inherit" href="/medify/medifyweb/#/categorySignin" style={{ textDecoration: 'underline' }}>
                                            Already have an account? Sign in. </Link>
                                    </InputLabel>
                                </ThemeProvider>
                                <Box mt={5}>
                                    <Link color="inherit" className={classes.heading} style={{ fontSize: '12px' }} target={'_blank'} href="http://medify.net/page-privacy.html">Terms of use. Privacy policy</Link>
                                    {/*   <Typography variant="body2" align="center" className={classes.heading} style={{ fontSize: '11px' }} >
                                        {'Terms of use. Privacy policy'}
                                    </Typography> */}
                                </Box>
                            </form>
                        </Col>
                    </Row>
                </div>
            </main>
        );
    }
}

SignUp.propTypes = {
    classes: PropTypes.object.isRequired,
};

SignUp = reduxForm({
    form: 'SignUpValidation'  // a unique identifier for this form
})(SignUp)

SignUp = connect(
    state => ({
        initialValues: state // pull initial values from account reducer
    }),
    { signup }               // bind account loading action creator
)(SignUp)

export default withStyles(styles)(SignUp);