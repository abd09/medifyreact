import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import { Col, Row } from 'reactstrap';
import SideBar from '../../components/userSideBar'
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import { Button, FormGroup, Label, Input } from 'reactstrap';
import AppBar from '@material-ui/core/AppBar';
import { Cropper } from 'react-image-cropper'
import SweetAlert from 'react-bootstrap-sweetalert'
import MatButton from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Autocomplete from 'react-google-autocomplete';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import appConfig from '../../constant/config';
import {
    isMobile
} from "react-device-detect";

import { red } from '@material-ui/core/colors';

import { getuserProfileData, cropImage } from '../../actions';
import { connect } from 'react-redux';

const drawerWidth = 240;
const styles = theme => ({
    root: {
        flexGrow: 1
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120
    },
    lable: {
        textAlign: 'left'
    },
    selectEmpty: {
        marginTop: theme.spacing(2)
    },
    appBar: {
        marginLeft: drawerWidth,
        [theme.breakpoints.up('sm')]: {
            width: `calc(100% - ${drawerWidth}px)`,
        },
    },
    menuButton: {
        marginRight: 20,
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
    dialogDone: {
        backgroundColor: appConfig.colors.darkGrey,
        color: "#3ddb20",
        '&:hover': {
            background: 'none',
            backgroundColor: appConfig.colors.darkGrey,
            color: '#3ddb20'
        },
        borderColor: appConfig.colors.darkGrey
    },
    dialogTitle: {
        backgroundColor: '#5a5a5a',
        '& h2': {
            color: 'white',
            textAlign: 'center'
        },
        borderBottom: '1px solid #5a5a5a'
    },
    dialogContent: {
        backgroundColor: '#5a5a5a',
    },
    root2: {
        flexGrow: 1,
        backgroundColor: appConfig.colors.white
    },
    tabsRoot: {
        color: '#42425E',
        borderBottom: '1px solid #7a7a7a',
    },
    tabsIndicator: {
        backgroundColor: '#7a7a7a',
        height: '3px'
    },
    rightDiv: {
        color: "#CCCACD",
        marginBottom: 0,
        fontWeight: 'bold',
        fontSize: '16px',
        textAlign: '-webkit-center',
    },
    listitems: {
        paddingLeft: 40,
        paddingRight: 10,
        paddingTop: 5,
        paddingBottom: 5
    },
    listitems1: {
        paddingLeft: 40,
        paddingRight: 10,
        paddingTop: 14,
        paddingBottom: 5
    },
    signup: {
        backgroundColor: appConfig.colors.primary,
        color: appConfig.colors.white,
        marginTop: '5px',
        padding: '5px 22px',
        textTransform: 'none',
        borderRadius: '5px',
    },
    tabRoot: {
        textTransform: 'initial',
        minWidth: '50%',
        fontWeight: theme.typography.fontWeightRegular,
        fontFamily: [
            '-apple-system',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ].join(','),
    },
    typography: {
        padding: '15px 0px',
        color: '#CCCACD',
        ['@media (max-width:787px)']: {
            padding: '15px 15px',
        },
    },
    input: {
        '::-webkit-input-placeholder': {
            color: 'red'
        }
    },
    imageDiv: {
        height: '70px',
        width: '70px',
    },
    avatar: {
        margin: theme.spacing(2),
        marginLeft: theme.spacing(4),
        width: 70,
        height: 70
    },
    profile: {
        textAlign: '-webkit-center',
        margin: theme.spacing.unit * 3,
        lineHeight: 'normal'
    },
    tableDiv: {
        padding: '0px',
    },
    tabsFlex: {
        borderBottom: '0px solid #454545',
    },
    tableThTd: {
        color: appConfig.colors.white,
    },
    infoDiv: {
        padding: 18,
        backgroundColor: appConfig.colors.white,
    },
    disableClick: {
        pointerEvents: 'none'
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: appConfig.colors.white,
        '&:hover': {
            backgroundColor: appConfig.colors.white,
        },
        marginRight: theme.spacing(2),
        marginLeft: 0,
        width: '100%',
        borderRadius: '40px',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(3),
            width: 'auto',
        },
    },
    searchIcon: {
        width: theme.spacing(7),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        fontWeight: 'bold',
        opacity: 1
    },
    inputRoot: {
        color: '#c6c6c6',
    },
    inputInput: {
        color: '#c6c6c6',
        padding: theme.spacing(1, 1, 1, 7),
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: 200,
        },
    },
    icon: {
        position: 'fixed',
        right: '10px',
        color: appConfig.colors.white
    },
    iconHover: {
        margin: theme.spacing(2),
        '&:hover': {
            color: red[800],
        },
    },
    avatar: {
        margin: 0,
        padding: 0,
    },
    avatarlist: {
        margin: 0,
        padding: 0,
        width: 35,
        height: 35,
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        borderRadius: '8px',
        color: appConfig.colors.primary
    },
    dense: {
        marginTop: theme.spacing(2),
    },
    menu: {
        width: 200,
    },
    submit: {
        backgroundColor: appConfig.colors.primary,
        color: appConfig.colors.white,
        marginTop: theme.spacing(1),
        padding: '5px 10px',
        textTransform: 'none',
        borderRadius: '8px',
        float:'left'
    },
    submit2: {
        backgroundColor: appConfig.colors.white,
        color: appConfig.colors.primary,
        border: 'none',
        marginTop: theme.spacing(2),
        textTransform: 'none',
        fontWeight: 'bolder',
        borderRadius: '0',
        '&:hover': {
            backgroundColor: appConfig.colors.white,
            color: appConfig.colors.primary,
        },
    },
    form: {
        width: '100%',
        paddingLeft: '20px',
        paddingRight: '35px',
        marginTop: theme.spacing.unit,
    },
});

class EditPharmacyDetail extends Component {
    constructor(props) {
        super(props)
        console.log('hey  i m here', this.props.location)
        this.state = {
            uploaded_image_path: '',
            certificate_image: '',
            value: 0,
            openModal: false,
            isLoading: true,
            prescription: '',
            pharmacy: '',
            prescriptions: [],
            note: '',
            addmoreText: [{
                location: '', days: [
                    { name: 'Sunday', value: 'Sunday', isSelected: false },
                    { name: 'Monday', value: 'Monday', isSelected: false },
                    { name: 'Tuesday', value: 'Tuesday', isSelected: false },
                    { name: 'Wednesday', value: 'Wednesday', isSelected: false },
                    { name: 'Thursday', value: 'Thursday', isSelected: false },
                    { name: 'Friday', value: 'Friday', isSelected: false },
                    { name: 'Saturday', value: 'Saturday', isSelected: false }
                ], from: '', to: ''
            }],
            tool: false,
            croppedImage: '',
            profileimage: '',
            imagePreview: '',
            profileimageMain: '',
            image: '',
            imageLoaded: false,
            alert: false,
            mobileOpen: false,
            firstname: '',
            lastname: '',
            mobile: '',
            address: '',
            age: '',
            gender: '',
            deliveryTime: '',
            deliveryDistance: '',
            isDeliveryEnabled: false,
            isChatEnabled: false,
            isPhoneEnabled: false,
            imagesArray: [],
            imageArray: [],
            errors: {},
            schemes: '',
            terms_policy: '',
            licenseNo: '',
            GSTNo: '',
            options: []
        };

    }

    componentWillMount() {
        
        this.props.getuserProfileData({ history: this.props.history });
       
    }

    componentWillReceiveProps = (nextProps) => {
        console.log("this.props.userProfileData", nextProps.profileImage)
        if (this.props.userProfileData !== nextProps.userProfileData) {
            console.log(' nextProps.availability', nextProps.userProfileData.availability);
            this.setState({
                croppedImage: nextProps.profileImage, firstname: nextProps.userProfileData.firstname, lastname: nextProps.userProfileData.lastname, mobile: nextProps.userProfileData.mobile, diseases: nextProps.userProfileData.diseases, surgeries: nextProps.userProfileData.surgeries, others: nextProps.userProfileData.others, addmoreText: (nextProps.userProfileData.availability.length === 0 ? [{
                    location: '', days: [
                        { name: 'Sunday', value: 'Sunday', isSelected: false },
                        { name: 'Monday', value: 'Monday', isSelected: false },
                        { name: 'Tuesday', value: 'Tuesday', isSelected: false },
                        { name: 'Wednesday', value: 'Wednesday', isSelected: false },
                        { name: 'Thursday', value: 'Thursday', isSelected: false },
                        { name: 'Friday', value: 'Friday', isSelected: false },
                        { name: 'Saturday', value: 'Saturday', isSelected: false }
                    ], from: '', to: ''
                }] : nextProps.userProfileData.availability), address: nextProps.userProfileData.address, age: nextProps.userProfileData.age, gender: nextProps.userProfileData.gender, deliveryTime: nextProps.userProfileData.deliveryTime, deliveryDistance: nextProps.userProfileData.deliveryDistance, isDeliveryEnabled: nextProps.userProfileData.isDeliveryEnabled,
                imageArray: nextProps.userProfileData.certificates,
                isPhoneEnabled: nextProps.userProfileData.isPhoneEnabled,
                schemes: nextProps.userProfileData.schemes,
                terms_policy: nextProps.userProfileData.terms_policy,
                isChatEnabled: nextProps.userProfileData.isChatEnabled,
                lat: nextProps.userProfileData.geoLocation && nextProps.userProfileData.geoLocation.coordinates && nextProps.userProfileData.geoLocation.coordinates.length > 0 ? nextProps.userProfileData.geoLocation.coordinates[1] : '', lng: nextProps.userProfileData.geoLocation && nextProps.userProfileData.geoLocation.coordinates && nextProps.userProfileData.geoLocation.coordinates.length > 0 ? nextProps.userProfileData.geoLocation.coordinates[0] : '',
                licenseNo: nextProps.userProfileData.licenseNo,
                GSTNo: nextProps.userProfileData.GSTNo
            })
        }
        if (this.props.profileImage !== nextProps.profileImage) {
            this.setState({ croppedImage: nextProps.profileImage })
        }
    }

    addMoreItems = (e) => {
        e.preventDefault()
        let addmoreText = this.state.addmoreText;
        addmoreText.push({
            location: '', days: [
                { name: 'Sunday', value: 'Sunday', isSelected: false },
                { name: 'Monday', value: 'Monday', isSelected: false },
                { name: 'Tuesday', value: 'Tuesday', isSelected: false },
                { name: 'Wednesday', value: 'Wednesday', isSelected: false },
                { name: 'Thursday', value: 'Thursday', isSelected: false },
                { name: 'Friday', value: 'Friday', isSelected: false },
                { name: 'Saturday', value: 'Saturday', isSelected: false }
            ], from: '', to: ''
        });
        this.setState({ addmoreText: addmoreText })
    }


    handleItemChange = (e, name, ind) => {
        e.preventDefault()
        var oldState = [...this.state.addmoreText];
        oldState.map((data, i) => {
            if (ind == i) {
                oldState[i][name] = e.target.value;
            }
        })
        this.setState({ addmoreText: oldState });
    }

    removeHandler = (e, ind) => {
        e.preventDefault()
        var oldState = [...this.state.addmoreText];
        var newState = []
        oldState.map((data, i) => {
            if (ind !== i) {
                newState.push(data)
            }
        })
        this.setState({ addmoreText: newState });
    }


    handleChange = (event, key) => {
        console.log('key', key)
        this.setState({ [key]: event.target.value });
    };

    openImageUploader() {
        this.refs.imageUploader.click();
    }

    handleImageChange = name => event => {
        console.log('name=', name, event.target.files[0].name)
        if (name === 'uploaded_image_path') {
            this.setState({
                [name]: event.target.files,
            });
        }
        this.setState({
            [name]: event.target.files[0].name,
        });
    };



    handleDrawerToggle = () => {
        this.setState(state => ({ mobileOpen: !state.mobileOpen }));
    };

    handleModalClose = () => {
        this.setState({ openModal: false, errors: {} });
    }

    openUrl = (e, url) => {
        e.preventDefault();
        window.open(url);
    }

    getCity = (addressArray) => {
        let city = '';
        if (addressArray && addressArray.length > 0) {
            for (let i = 0; i < addressArray.length; i++) {
                if (addressArray[i].types[0] && 'administrative_area_level_2' === addressArray[i].types[0]) {
                    city = addressArray[i].long_name;
                    return city;
                }
            }
        }
    };

    getArea = (addressArray) => {
        let area = '';
        if (addressArray && addressArray.length > 0) {
            for (let i = 0; i < addressArray.length; i++) {
                if (addressArray[i].types[0]) {
                    for (let j = 0; j < addressArray[i].types.length; j++) {
                        if ('sublocality_level_1' === addressArray[i].types[j] || 'locality' === addressArray[i].types[j]) {
                            area = addressArray[i].long_name;
                            return area;
                        }
                    }
                }
            }
        }
    };

    getState = (addressArray) => {
        let state = '';
        if (addressArray && addressArray !== undefined && addressArray.length > 0) {
            for (let i = 0; i < addressArray.length; i++) {
                for (let i = 0; i < addressArray.length; i++) {
                    if (addressArray[i].types[0] && 'administrative_area_level_1' === addressArray[i].types[0]) {
                        state = addressArray[i].long_name;
                        return state;
                    }
                }
            }
        }
    };

    handleCheck = (e, index, locationI) => {
        e.preventDefault();
        let tempPermission = this.state.addmoreText[locationI].days;
        console.log('tempPermission', tempPermission);
        console.log('index', index);
        tempPermission[index].isSelected = !tempPermission[index].isSelected;
        this.setState({ options: tempPermission })
        var oldState = [...this.state.addmoreText];


        oldState.map((data, i) => {
            console.log('locationI', locationI, i);

            if (locationI == i) {
                oldState[i]['days'] = tempPermission;
            }
        })
        console.log('oldState', oldState);
        this.setState({ addmoreText: oldState });
    }
    SelectedPlace = (place, index) => {
        console.log('place', place, index);
        const address = place.formatted_address ? place.formatted_address : place.name;
        console.log('address', address);

        const addressArray = place.address_components ? place.address_components : '',
            city = addressArray !== '' ? this.getCity(addressArray) : '',
            area = addressArray !== '' ? this.getArea(addressArray) : '',
            state = addressArray !== '' ? this.getState(addressArray) : '',
            latValue = place.geometry && place.geometry.location ? place.geometry.location.lat() : 0,
            lngValue = place.geometry && place.geometry.location ? place.geometry.location.lng() : 0;
        var oldState = [...this.state.addmoreText];
        oldState.map((data, i) => {
            if (index == i) {
                oldState[i]['location'] = address;
            }
        })
        this.setState({ addmoreText: oldState });
        this.setState({
            address: (address) ? address : '',
            area: (area) ? area : '',
            city: (city) ? city : '',
            state: (state) ? state : '',
            lat: latValue !== 0 ? latValue : '',
            lng: lngValue !== 0 ? lngValue : '',
        })
    }

    saveProfile = (e) => {
        e.preventDefault();
        let data = {
            profileimageMain: this.state.profileimageMain,
            firstname: this.state.firstname,
            lastname: this.state.lastname,
            address: this.state.address,
            mobile: this.state.mobile,
            age: this.state.age,
            gender: this.state.gender,
            deliveryDistance: this.state.deliveryDistance,
            deliveryTime: this.state.deliveryTime,
            availability: this.state.addmoreText,
            isDeliveryEnabled: this.state.isDeliveryEnabled,
            certificates: this.state.imagesArray,
            isPhoneEnabled: this.state.isPhoneEnabled,
            schemes: this.state.schemes,
            terms_policy: this.state.terms_policy,
            lat: this.state.lat,
            lng: this.state.lng,
            isChatEnabled: this.state.isChatEnabled,
            GSTNo: this.state.GSTNo,
            licenseNo: this.state.licenseNo
        }
        console.log("this.state.profileimageMain", data)
        this.props.cropImage(data)
    }

    handleUploadCertificateChange = name => event => {
        console.log('name=', name, event.target.files[0])
        let arr = [...this.state.imageArray];
        let imagearr = [...this.state.imagesArray];
        console.log("this.state imagearr", imagearr)
        if (name === 'certificate_image') {
            let errors = {};
            let formIsValid = true;
            let validExtension = false;
            if (event.target.files[0]) {
                var file = event.target.files[0];
                var reader = new FileReader();
                var url = reader.readAsDataURL(file);
                let name = event.target.files[0].name
                reader.onloadend = function (e) {
                    if (reader.result.substring("data:image/".length, reader.result.indexOf(";base64")) === 'jpeg' || reader.result.substring("data:image/".length, reader.result.indexOf(";base64")) === 'png') {
                        arr.push(name);
                        imagearr.push(reader.result);
                        this.setState({
                            imageArray: arr,
                            imagesArray: imagearr,
                        });
                    } else {
                        formIsValid = false;
                        errors['certificates'] = "Only jpeg and png file supported."
                        this.setState({ errors: errors });
                    }
                    return formIsValid;
                }.bind(this);
            }
        }
    };

    openCertificateImageUploader() {
        this.setState({ tool: false, errors: {} })
        this.refs.CertificateImageUploader.click();
    }

    onConfirm(key, image) {
        let node = this[image]
        console.log('nodes=', node.crop())
        let img = node.crop()
        this.setState({ [key]: false, croppedImage: img, profileimageMain: img })
        console.log('image=', this.state)
    }
    /**
    * Open Alert
    * @param {key} key
    */
    onCancel(key) {
        console.log("key", key)
        this.setState({ [key]: false })
    }

    handleImageLoaded(state) {
        this.setState({
            [state + 'Loaded']: true
        })
    }
    /**
    * Open Alert
    * @param {key} key
    */
    openAlert(e, key) {
        this.setState({ images: e.target.files, image: '' })
        console.log('files=', e.target.files[0], e.target.files)
        var file = e.target.files[0];
        var reader = new FileReader();
        var url = reader.readAsDataURL(file);
        console.log('url=,', url)
        reader.onloadend = function (e) {
            this.setState({
                imagePreview: [reader.result]
            })
        }.bind(this);
        console.log('url=,', url)
        this.setState({ [key]: true });
        console.log('files=', e.target.files[0])
    }

    uploadFile(e) {
        e.preventDefault();
        this.setState({ tool: true })
    }

    openImageUploader() {
        this.setState({ tool: false })
        this.refs.imageUploader.click();
    }

    removeProfilePhoto = () => {
        this.setState({ profileimageMain: null, croppedImage: null, tool: false });
    }

    render() {
        const { classes, userProfileData, children } = this.props;
        const { prescriptions, tool, alert, firstname, lastname, mobile, isChatEnabled, address, age, deliveryDistance, deliveryTime, isDeliveryEnabled, addmoreText, isPhoneEnabled, schemes, terms_policy, licenseNo, GSTNo, editorState, options } = this.state;


        /* const options = [
            { name : 'Order' , value:'order', isSelected : false},
            { name : 'Dashboard' , value:'dashboard', isSelected : false},
            { name : 'Inventory' , value:'inventory', isSelected : false},
            { name : 'Settlements' , value:'settlement', isSelected : false},
            { name : 'Transactions' , value:'transaction', isSelected : false},
            { name : 'Product Requests' , value:'productRequests', isSelected : false},
          ] */

        console.log('prescriptions,addmoreText', typeof schemes)
        return (
            <React.Fragment>
                <AppBar position="fixed" className={classes.appBar}>
                    <Toolbar>
                        <IconButton
                            color="inherit"
                            aria-label="Open drawer"
                            onClick={this.handleDrawerToggle}
                            className={classes.menuButton}>
                            <MenuIcon />
                        </IconButton>
                        <Typography variant="h6" color="inherit" style={{ fontWeight: 'bold', color: appConfig.colors.white }} noWrap>
                            {"Edit Profile"}
                        </Typography>
                    </Toolbar>
                </AppBar>
                <div className='page-content' style={{ fontWeight: 'bold', backgroundColor: appConfig.colors.white, marginTop: '-56px', borderRight: '1px solid #655368' }}>
                    <SideBar mobileOpen={this.state.mobileOpen} handleDrawerToggle={this.handleDrawerToggle} userProfileData={userProfileData} >
                        {children}
                    </SideBar>
                </div>
                <div className={classes.root}>
                    <div className={classes.container}>
                        <Col xs="12" md="12" sm="12" >
                            <div className={classes.rightDiv}>
                                {
                                    this.state.croppedImage &&
                                    <Avatar alt='Profile' src={this.state.croppedImage ? this.state.croppedImage : require('../../assets/img/noImage.png')} className={classes.imageDiv} onClick={(e) => this.uploadFile(e)}>
                                    </Avatar>
                                }
                                {(this.state.croppedImage == undefined || this.state.croppedImage == "") &&
                                    <Avatar alt='Profile' src={require('../../assets/img/noImage.png')} className={classes.imageDiv} onClick={(e) => this.uploadFile(e)}>
                                    </Avatar>
                                }
                                <input type="file" id="file" ref="imageUploader" accept="image/*" style={{ display: "none" }} onChange={(e) => this.openAlert(e, 'alert')} />
                                <SweetAlert
                                    btnSize="sm"
                                    show={tool}
                                    showCancel
                                    cancelBtnBsStyle="danger"
                                    title="Choose an option"
                                    confirmBtnStyle={{ display: 'none' }}
                                    onConfirm={() => this.onConfirm()}
                                    onCancel={() => this.onCancel('tool')}>
                                    <MatButton variant="contained" color="primary" onClick={this.removeProfilePhoto}>Remove Image</MatButton>
                                    <MatButton variant="contained" style={{ backgroundColor: '#58de45', marginLeft: 20 }} onClick={() => this.openImageUploader()}>Select Image</MatButton>
                                </SweetAlert>
                            </div>
                            <div className={classes.profile}>
                                <SweetAlert
                                    style={{ position: 'absolute' }}
                                    btnSize="sm"
                                    show={alert}
                                    showCancel
                                    cancelBtnBsStyle="danger"
                                    title="Image Cropper"
                                    onConfirm={() => this.onConfirm('alert', 'image')}
                                    onCancel={() => this.onCancel('alert')}>
                                    {
                                        this.state.imagePreview &&
                                        <Cropper src={this.state.imagePreview[0]} newItem={300} width={300} height={300}
                                            ref={ref => { this.image = ref }}
                                            fixedRatio={true}
                                            onImgLoad={() => this.handleImageLoaded('image')}
                                        />
                                    }
                                </SweetAlert>
                            </div>
                            <div className={classes.profile}>
                                <div className={classes.tableDiv}>
                                    <form className={classes.form} onSubmit={(e) => this.saveProfile(e)}>
                                        <FormGroup className={classes.lable}>
                                            <Label for="firstname" style={{ color: appConfig.colors.primary, fontSize: '16px' }}>First name:</Label>
                                            <Input type="text" style={{ borderColor: appConfig.colors.primary, fontSize: '18px' }} value={firstname} name="firstname" id="firstname" onChange={(e) => this.handleChange(e, 'firstname')} />
                                        </FormGroup>
                                        <FormGroup className={classes.lable}>
                                            <Label for="name" style={{ color: appConfig.colors.primary, fontSize: '16px' }}>Last name:</Label>
                                            <Input type="text" style={{ borderColor: appConfig.colors.primary, fontSize: '18px' }} value={lastname} name="name" id="name" onChange={(e) => this.handleChange(e, 'lastname')} />
                                        </FormGroup>
                                        {/* <FormGroup className={classes.lable}>
                                            <Label for="location" style={{ color: appConfig.colors.primary, fontSize: '16px' }}>Location:</Label>
                                            <Autocomplete
                                                className="form-control"
                                                style={{ borderColor: appConfig.colors.primary, fontSize: '18px' }} onChange={(e) => this.handleChange(e, 'address')}
                                                onPlaceSelected={(place) => this.SelectedPlace(place)}
                                                types={['(regions)']}
                                                
                                                value={address}
                                                componentRestrictions={{ country: "in" }}
                                            />
                                            {this.state.lat !== '' && this.state.lng !== '' ?
                                                <Button
                                                    variant="contained"
                                                    className={classes.signup} onClick={(e) => this.openUrl(e, `http://www.google.com/maps/place/${this.state.lat},${this.state.lng}`)}
                                                > View location</Button>
                                                : ''}
                                        </FormGroup> */}
                                        <FormGroup className={classes.lable}>
                                            <Label for="mobile" style={{ color: appConfig.colors.primary, fontSize: '16px' }}>Phone Number:</Label>
                                            <Input type="text" style={{ borderColor: appConfig.colors.primary, fontSize: '18px' }} value={mobile} name="mobile" id="mobile" onChange={(e) => this.handleChange(e, 'mobile')} />
                                        </FormGroup>
                                        <FormGroup className={classes.lable}>
                                            <Label for="email" style={{ color: appConfig.colors.primary, fontSize: '16px' }}>Email:</Label>
                                            <Input type="email" style={{ borderColor: appConfig.colors.primary, fontSize: '18px' }} value={userProfileData ? userProfileData.email : ''} name="email" id="email" disabled />
                                        </FormGroup>

                                        <FormGroup className={classes.lable}>
                                            <Label for="licenseNo" style={{ color: appConfig.colors.primary, fontSize: '16px' }}>License No.:</Label>
                                            <Input type="text" style={{ borderColor: appConfig.colors.primary, fontSize: '18px' }} onChange={(e) => this.handleChange(e, 'licenseNo')} value={licenseNo} className={classes.input} name="licenseNo" id="licenseNo" />
                                        </FormGroup>
                                        <FormGroup className={classes.lable}>
                                            <Label for="GSTNo" style={{ color: appConfig.colors.primary, fontSize: '16px' }}>GST No.:</Label>
                                            <Input type="text" style={{ borderColor: appConfig.colors.primary, fontSize: '18px' }} onChange={(e) => this.handleChange(e, 'GSTNo')} value={GSTNo} className={classes.input} name="GSTNo" id="GSTNo" />
                                        </FormGroup>
                                        <FormGroup className={classes.lable}>
                                            <Label for="deliveryTime" style={{ color: appConfig.colors.primary, fontSize: '16px' }}>Delivery Time:</Label>
                                            <Input type="text" style={{ borderColor: appConfig.colors.primary, fontSize: '18px' }} onChange={(e) => this.handleChange(e, 'deliveryTime')} value={deliveryTime} className={classes.input} name="deliveryTime" id="deliveryTime" />
                                        </FormGroup>
                                        <FormGroup className={classes.lable}>
                                            <Label for="deliveryDistance" style={{ color: appConfig.colors.primary, fontSize: '16px' }}>Radius / Distance:</Label>
                                            <Input type="text" style={{ borderColor: appConfig.colors.primary, fontSize: '18px' }} onChange={(e) => this.handleChange(e, 'deliveryDistance')} value={deliveryDistance} className={classes.input} name="deliveryDistance" id="deliveryDistance" />
                                        </FormGroup>
                                        <FormGroup className={classes.lable}>
                                            <Label for="isDeliveryEnabled" style={{ color: appConfig.colors.primary, fontSize: '16px' }}>Delivery Status:</Label>
                                            <Input type="select" value={isDeliveryEnabled} name="isDeliveryEnabled" id="isDeliveryEnabled" onChange={(e) => this.handleChange(e, 'isDeliveryEnabled')} style={{ borderColor: appConfig.colors.primary, fontSize: '18px' }}>
                                                <option value="" >Select Delivery Status</option>
                                                <option value={true}>Enabled</option>
                                                <option value={false}>Disabled</option>
                                            </Input>
                                        </FormGroup>
                                        <FormGroup className={classes.lable}>
                                            <Label for="isChatEnabled" style={{ color: appConfig.colors.primary, fontSize: '16px' }}>Chat Status:</Label>
                                            <Input type="select" value={isChatEnabled} name="isChatEnabled" id="isChatEnabled" onChange={(e) => this.handleChange(e, 'isChatEnabled')} style={{ borderColor: appConfig.colors.primary, fontSize: '18px' }}>
                                                <option value={true}>Enabled</option>
                                                <option value={false}>Disabled</option>
                                            </Input>
                                        </FormGroup>

                                        <FormGroup className={classes.lable}>
                                            <Label for="isPhoneEnabled" style={{ color: appConfig.colors.primary, fontSize: '16px' }}>Display phone number:</Label>
                                            <Input type="select" value={isPhoneEnabled} name="isPhoneEnabled" id="isPhoneEnabled" onChange={(e) => this.handleChange(e, 'isPhoneEnabled')} style={{ borderColor: appConfig.colors.primary, fontSize: '18px' }}>
                                                <option value="" >Select Status</option>
                                                <option value={true}>Enable</option>
                                                <option value={false}>Disable</option>
                                            </Input>
                                        </FormGroup>
                                        <FormGroup className={classes.lable}>
                                            <Label for="schemes" style={{ color: appConfig.colors.primary, fontSize: '16px' }}>Schemes / Discounts:</Label>
                                            <Input type="textarea" style={{ borderColor: appConfig.colors.primary, fontSize: '18px' }} onChange={(e) => this.handleChange(e, 'schemes')} value={schemes} className={classes.input} name="schemes" id="schemes" />
                                        </FormGroup>

                                        {
                                            (this.state.imageArray.length > 0) ?
                                                this.state.imageArray.map((image, key) =>
                                                    <React.Fragment key={key + 'certificateImage'}>
                                                        <Row>
                                                            <Col sm={12}>
                                                                <TextField
                                                                    id="certificate_image"
                                                                    fullWidth
                                                                    style={{ color: appConfig.colors.primary, marginTop: '8px', padding: '10px 14px !important' }}
                                                                    value=
                                                                    {
                                                                        image ? image : ''} disabled
                                                                    margin="normal"
                                                                    variant="outlined" />
                                                                {
                                                                    this.state.imageArray.length === (key + 1) ? <span style={{ color: 'red', float: 'left' }} >{this.state.errors["certificates"]}</span> : ''
                                                                }
                                                            </Col>
                                                            {

                                                                this.state.imageArray.length === (key + 1) ? <Col sm={2}>
                                                                    <input type="file" id="certificate_image" ref="CertificateImageUploader" name="certificate_image" onChange={this.handleUploadCertificateChange('certificate_image')} style={{ display: 'none' }} />

                                                                    <Button className={classes.submit} style={{ borderRadius: '11px', margin: 10 }} onClick={() => this.openCertificateImageUploader()} >Upload More</Button>

                                                                </Col> : ''
                                                            }
                                                        </Row>
                                                    </React.Fragment>) :
                                                <React.Fragment>
                                                    <Row>
                                                        <Col sm={10}>
                                                            <TextField
                                                                id="certificate_image"
                                                                fullWidth
                                                                style={{ color: appConfig.colors.primary, marginTop: '8px', padding: '10px 14px !important' }}
                                                                value=
                                                                {''} disabled
                                                                margin="normal"
                                                                variant="outlined"
                                                            />
                                                            <span style={{ color: 'red', float: 'left' }} >{this.state.errors["certificates"]}</span>
                                                        </Col>
                                                        <Col sm={2}>
                                                            <input type="file" id="certificate_image" ref="CertificateImageUploader" name="certificate_image" onChange={this.handleUploadCertificateChange('certificate_image')} style={{ display: 'none' }} />

                                                            <Button className={classes.submit} style={{ borderRadius: '11px' }} onClick={() => this.openCertificateImageUploader()} >Upload Certificates</Button>

                                                        </Col>
                                                    </Row>
                                                </React.Fragment>
                                        }
                                        {isMobile ? '' :
                                            <div>
                                                <Col md={3} sm={3} xs={12} style={{ display: 'inline-block', paddingLeft: '0px', paddingRight: '0px', }} >
                                                    <Label for="location" style={{ color: appConfig.colors.primary, fontSize: '16px' }}>Location:</Label>
                                                </Col>
                                                <Col md={3} sm={3} xs={12} style={{ display: 'inline-block' }} >
                                                    <Label for="days" style={{ color: appConfig.colors.primary, fontSize: '16px', paddingLeft: '0px', paddingRight: '0px' }}>Days:</Label>
                                                </Col>
                                                <Col md={3} sm={3} xs={12} style={{ display: 'inline-block' }}>
                                                    <Label for="email" style={{ color: appConfig.colors.primary, fontSize: '16px', paddingLeft: '0px', paddingRight: '0px' }}>From:</Label>
                                                </Col>
                                                <Col md={3} sm={3} xs={12} style={{ display: 'inline-block', paddingRight: '0px' }}>
                                                    <Label for="to" style={{ color: appConfig.colors.primary, fontSize: '16px', paddingLeft: '0px', paddingRight: '0px' }}>To:</Label>
                                                </Col>
                                            </div>}
                                        {this.state.addmoreText.map((data, index) => {
                                            console.log('data!!!!', data);
                                            return <div key={index}>
                                                <Col md={3} sm={3} xs={12} style={{ display: 'inline-block', paddingLeft: '0px', paddingRight: isMobile ? '0px' : '5px' }} >
                                                    <FormGroup>
                                                        {isMobile ? <Label for="location" style={{ color: appConfig.colors.primary, fontSize: '16px' }}>Location:</Label> : ''}
                                                        <Autocomplete
                                                            className="form-control"
                                                            style={{ borderColor: appConfig.colors.primary, fontSize: '18px' }} onChange={(e) => this.handleItemChange(e, 'location', index)}
                                                            onPlaceSelected={(place) => this.SelectedPlace(place, index)}
                                                            types={['(regions)']}

                                                            value={data.location}
                                                            componentRestrictions={{ country: "in" }}
                                                        />
                                                        {/* <Input type="text" style={{ borderColor: appConfig.colors.primary, fontSize: '18px' }} className={classes.input} value={data.location}  onChange={(e) => this.handleItemChange(e, 'location', index)} name="location" id={`location${index}`} /> */}
                                                    </FormGroup>
                                                </Col>
                                                <Col md={3} sm={3} xs={12} style={{ display: 'inline-block', paddingLeft: isMobile ? '0px' : '5px', paddingRight: isMobile ? '0px' : '5px' }} >
                                                    <FormGroup style={{marginBottom:'0'}}>
                                                        {isMobile ? <Label for="days" style={{ color: appConfig.colors.primary, fontSize: '16px' }}>Days:</Label> : ''}
                                                        <Row>
                                                            {
                                                                (data.days !== undefined && data.days.length > 0) ? data.days.map((value, key) => {
                                                                    console.log('value', value)
                                                                    return <Col xs={6} md={6} sm={6} xl={6} lg={6}>
                                                                        <FormControlLabel
                                                                            control={
                                                                                <Checkbox
                                                                                    checked={value.isSelected}
                                                                                    onChange={(e) => this.handleCheck(e, key, index)}
                                                                                    value={value.value}
                                                                                    color="primary"
                                                                                />
                                                                            }
                                                                            label={value.name}
                                                                        />
                                                                    </Col>
                                                                }) : ''
                                                            }
                                                        </Row>
                                                      
                                                        {/*   <ReactMultiSelectCheckboxes options={options} /> */}
                                                        {/*   <Input type="select" value={data.day} name="day" id="day" onChange={(e) => this.handleItemChange(e, 'day', index)} style={{ borderColor: appConfig.colors.primary, fontSize: '18px' }} >
                                                            <option value="" >Select day</option>
                                                            <option value={'Sunday'}>{'Sunday'}</option>
                                                            <option value={'Monday'}>{'Monday'}</option>
                                                            <option value={'Tuesday'}>{'Tuesday'}</option>
                                                            <option value={'Wednesday'}>{'Wednesday'}</option>
                                                            <option value={'Thursday'}>{'Thursday'}</option>
                                                            <option value={'Friday'}>{'Friday'}</option>
                                                            <option value={'Saturday'}>{'Saturday'}</option>
                                                        </Input>
                                                    */}
                                                    </FormGroup>
                                                </Col>
                                                <Col md={3} sm={3} xs={12} style={{ display: 'inline-block', paddingLeft: isMobile ? '0px' : '5px', paddingRight: isMobile ? '0px' : '5px' }}>
                                                    <FormGroup>
                                                        {isMobile ? <Label for="email" style={{ color: appConfig.colors.primary, fontSize: '16px' }}>From:</Label> : ''}
                                                        <Input type="select" name="from" id="from" onChange={(e) => this.handleItemChange(e, 'from', index)} style={{ borderColor: appConfig.colors.primary, fontSize: '18px' }} value={data.from} >
                                                            <option value="" >From Time</option>
                                                            <option value={'09:00 AM'}>{'09:00 AM'}</option>
                                                            <option value={'10:00 AM'}>{'10:00 AM'}</option>
                                                            <option value={'11:00 AM'}>{'11:00 AM'}</option>
                                                            <option value={'12:00 PM'}>{'12:00 PM'}</option>
                                                            <option value={'01:00 PM'}>{'01:00 PM'}</option>
                                                            <option value={'02:00 PM'}>{'02:00 PM'}</option>
                                                            <option value={'03:00 PM'}>{'03:00 PM'}</option>
                                                            <option value={'04:00 PM'}>{'04:00 PM'}</option>
                                                            <option value={'05:00 PM'}>{'05:00 PM'}</option>
                                                            <option value={'06:00 PM'}>{'06:00 PM'}</option>
                                                            <option value={'07:00 PM'}>{'07:00 PM'}</option>
                                                            <option value={'08:00 PM'}>{'08:00 PM'}</option>
                                                            <option value={'09:00 PM'}>{'09:00 PM'}</option>
                                                            <option value={'10:00 PM'}>{'10:00 PM'}</option>
                                                            <option value={'11:00 PM'}>{'11:00 PM'}</option>
                                                            <option value={'12:00 PM'}>{'12:00 PM'}</option>
                                                        </Input>
                                                    </FormGroup>
                                                </Col>
                                                <Col md={3} sm={3} xs={12} style={{ display: 'inline-block', paddingLeft: isMobile ? '0px' : '5px', paddingRight: '0px' }}>
                                                    <FormGroup>
                                                        {isMobile ? <Label for="to" style={{ color: appConfig.colors.primary, fontSize: '16px' }}>To:</Label> : ''}
                                                        <Input type="select" name="to" id="to" onChange={(e) => this.handleItemChange(e, 'to', index)} style={{ borderColor: appConfig.colors.primary, fontSize: '18px' }} value={data.to} >
                                                            <option value="" >To Time</option>
                                                            <option value={'10:00 AM'}>{'10:00 AM'}</option>
                                                            <option value={'11:00 AM'}>{'11:00 AM'}</option>
                                                            <option value={'12:00 PM'}>{'12:00 PM'}</option>
                                                            <option value={'01:00 PM'}>{'01:00 PM'}</option>
                                                            <option value={'02:00 PM'}>{'02:00 PM'}</option>
                                                            <option value={'03:00 PM'}>{'03:00 PM'}</option>
                                                            <option value={'04:00 PM'}>{'04:00 PM'}</option>
                                                            <option value={'05:00 PM'}>{'05:00 PM'}</option>
                                                            <option value={'06:00 PM'}>{'06:00 PM'}</option>
                                                            <option value={'07:00 PM'}>{'07:00 PM'}</option>
                                                            <option value={'08:00 PM'}>{'08:00 PM'}</option>
                                                            <option value={'09:00 PM'}>{'09:00 PM'}</option>
                                                            <option value={'10:00 PM'}>{'10:00 PM'}</option>
                                                            <option value={'11:00 PM'}>{'11:00 PM'}</option>
                                                            <option value={'12:00 PM'}>{'12:00 PM'}</option>
                                                        </Input>
                                                    </FormGroup>
                                                </Col>
                                               {/*  {this.state.addmoreText.length > 1 &&
                                                    <FormGroup>
                                                        <span>
                                                            <Button className={classes.submit} style={{}} onClick={(e) => this.removeHandler(e, index)}>Remove </Button>
                                                        </span>
                                                    </FormGroup>
                                                } */}
                                            </div>
                                        })}
                                        {/* <Col md={6} sm={6} xs={6} style={{ textAlign: 'left', display: 'inline-block' }}>
                                            <Button
                                                type="button"
                                                variant="contained" style={{ textAlign: 'left', display: 'inline-block' }}
                                                className={classes.submit} onClick={(e) => this.addMoreItems(e)}
                                            >Add More</Button>
                                        </Col> */}
                                        <Col md={12} lg={12} sm={12} xs={12} style={{  display: 'inline-block' }}>
                                            <Button variant="outlined" className={classes.submit} type="submit" >
                                                Submit </Button>
                                        </Col>
                                    </form>
                                </div>
                            </div>
                        </Col>
                    </div>
                </div>
            </React.Fragment >);
    }
}

EditPharmacyDetail.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = ({ authUser }) => {
    const { userProfileData, profileImage } = authUser
    return { userProfileData, profileImage }
}

export default connect(mapStateToProps, { getuserProfileData, cropImage })(withStyles(styles)(EditPharmacyDetail));