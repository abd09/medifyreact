import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import { Container, Col, Row } from 'reactstrap';
import SideBar from '../../components/userSideBar'
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import Create from '@material-ui/icons/Create';
import Button from '@material-ui/core/Button';
import AppBar from '@material-ui/core/AppBar';
import { createMuiTheme } from "@material-ui/core/styles";
import Icon from '@material-ui/core/Icon';
import { red } from '@material-ui/core/colors';
import { getuserProfileData } from '../../actions';
import { connect } from 'react-redux';
import axios from '../../constant/axios';
import AppConfig from '../../constant/config';

const drawerWidth = 240;
const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    appBar: {
        marginLeft: drawerWidth,
        [theme.breakpoints.up('sm')]: {
            width: `calc(100% - ${drawerWidth}px)`,
        },
    },
    menuButton: {
        marginRight: 20,
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
    dialogDone: {
        backgroundColor: '#454545',
        color: "#3ddb20",
        '&:hover': {
            background: 'none',
            backgroundColor: '#454545',
            color: '#3ddb20'
        },
        borderColor: '#454545'
    },
    dialogTitle: {
        backgroundColor: '#5a5a5a',
        '& h2': {
            color: 'white',
            textAlign: 'center'
        },
        borderBottom: '1px solid #5a5a5a'
    },
    dialogContent: {
        backgroundColor: '#5a5a5a',
    },
    root2: {
        flexGrow: 1,
        backgroundColor: '#fff'
    },
    tabsRoot: {
        color: AppConfig.colors.secondary,
        borderBottom: '1px solid #7a7a7a',
    },
    tabsIndicator: {
        backgroundColor: '#7a7a7a',
        height: '3px'
    },
    rightDiv: {
        color: "#CCCACD",
        marginBottom: 0,
        fontWeight: 'bold',
        fontSize: '16px',
        textAlign: 'right',
        float: 'right'
    },
    listitems: {
        paddingLeft: 40,
        paddingRight: 10,
        paddingTop: 5,
        paddingBottom: 5
    },
    listitems1: {
        paddingLeft: 40,
        paddingRight: 10,
        paddingTop: 14,
        paddingBottom: 5
    },
    tabRoot: {
        textTransform: 'initial',
        minWidth: '50%',
        fontWeight: theme.typography.fontWeightRegular,
        fontFamily: [
            '-apple-system',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ].join(','),
    },
    typography: {
        padding: '15px 0px',
        color: '#CCCACD',
        ['@media (max-width:787px)']: {
            padding: '15px 15px',
        },
    },
    imageDiv: {
        textAlign: 'right',
        height: '70px',
        width: '70px',
    },
    avatar: {
        margin: theme.spacing.unit * 2,
        marginLeft: theme.spacing.unit * 4,
        width: 70,
        height: 70
    },
    profile: {
        marginTop: theme.spacing.unit * 4,
        lineHeight: 1
    },
    tableDiv: {
        padding: '0px',
    },
    tabsFlex: {
        borderBottom: '0px solid #454545',
    },
    tableThTd: {
        color: '#ffffff',
    },
    infoDiv: {
        padding: 18,
        backgroundColor: "#fff"
    },
    disableClick: {
        pointerEvents: 'none'
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: '#ffffff',
        '&:hover': {
            backgroundColor: '#ffffff',
        },
        marginRight: theme.spacing(2),
        marginLeft: 0,
        width: '100%',
        borderRadius: '40px',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(3),
            width: 'auto',
        },
    },
    searchIcon: {
        width: theme.spacing(7),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        fontWeight: 'bold',
        opacity: 1
    },
    inputRoot: {
        color: '#c6c6c6',
    },
    inputInput: {
        color: '#c6c6c6',
        padding: theme.spacing(1, 1, 1, 7),
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: 200,
        },
    },
    icon: {
        position: 'fixed',
        right: '10px',
        color: '#fff'
    },
    iconHover: {
        margin: theme.spacing(2),
        '&:hover': {
            color: red[800],
        },
    },
    avatar: {
        margin: 0,
        padding: 0,
    },
    avatarlist: {
        margin: 0,
        padding: 0,
        width: 35,
        height: 35,
    },
    profile: {
        margin: 0,
        height: 100,
        lineHeight: 'normal'
    },
    signup: {
        backgroundColor: '#43415e',
        borderRadius: '0px',
        color: '#fff',
        marginTop: '5px',
        padding: '5px 22px',
        textTransform: 'none',

    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        borderRadius: '8px',
        color: '#43415e'
    },
    dense: {
        marginTop: theme.spacing(2),
    },
    menu: {
        width: 200,
    },
    submit: {
        backgroundColor: '#43415e',
        color: '#fff',
        marginTop: theme.spacing.unit * 2,
        padding: '8px 30px',
        textTransform: 'none',
        borderRadius: '0'
    },
    form: {
        width: '100%',
        paddingLeft: '20px',
        paddingRight: '35px',
        marginTop: theme.spacing.unit,
    },
});

class PrescriptionDetails extends Component {
    constructor(props) {
        super(props)
        console.log('hey  i m here', this.props.location)
        this.state = {
            uploaded_image_path: '',
            value: 0,
            openModal: false,
            isLoading: true,
            prescription: '',
            quantity: 0,
            url: '',
            croppedImage: ''
        };
    }
    componentWillReceiveProps(nextProps) {
        if (this.props.userProfileData !== nextProps.userProfileData) {
            this.setState({ croppedImage: nextProps.profileImage })
        }
        if (this.props.profileImage !== nextProps.profileImage) {
            this.setState({ croppedImage: nextProps.profileImage })
        }
    }
    componentWillMount() {

        this.props.getuserProfileData({ history: this.props.history });
        axios.get(`/users/prescriptionDetails/${this.props.match.params.id}`, {
            headers: {
                'Content-Type': 'application/json',
                'token': localStorage.getItem('token')
            }
        }).then(result => {
            console.log("prescriptionDetails result###", result.data.detail);
            this.setState({ prescription: result.data.detail ? result.data.detail : '', url: result.data.url })
        })
            .catch(error => {
                console.log("error....", error);
            });
    }

    handleChange = (event, value) => {
        console.log('value', value)
        this.setState({ value });
    };

    openImageUploader() {
        this.refs.imageUploader.click();
    }

    handleClick = (e) => {
        localStorage.getItem('role') == 'user'
            ?
            this.props.history.push(`/${localStorage.getItem('role')}/editPrescription/${this.props.match.params.id}`)
            :
            this.props.history.push(`/${localStorage.getItem('role')}/addPrescription`)
    }

    handleImageChange = name => event => {
        console.log('name=', name, event.target.files[0].name)
        if (name === 'uploaded_image_path') {
            this.setState({
                [name]: event.target.files,
            });
        }
        this.setState({
            [name]: event.target.files[0].name,
        });
    };

    handleDrawerToggle = () => {
        this.setState(state => ({ mobileOpen: !state.mobileOpen }));
    };

    handleModalClose = () => {
        this.setState({ openModal: false, errors: {} });
    }

    goBack = (e) => {
        console.log('e', e);
        e.preventDefault();
        console.log('this.props.history', this.props.history);
        this.props.history.goBack();
    }

    handleInputChange = (e, key) => {
        console.log('key,e,', key, e.target.value);
        this.setState({ [key]: e.target.value });
    }

    render() {
        const { classes, userProfileData, children } = this.props;
        const { prescription, url, quantity, croppedImage } = this.state;
        console.log('prescription', prescription.patient && prescription.patient != null && prescription.patient._id && userProfileData && userProfileData._id && userProfileData._id === prescription.patient._id);
        return (
            <React.Fragment>
                <AppBar position="fixed" className={classes.appBar}>
                    <Toolbar>
                        <IconButton
                            color="inherit"
                            aria-label="Open drawer"
                            onClick={this.handleDrawerToggle}
                            className={classes.menuButton}>
                            <MenuIcon />
                        </IconButton>
                        <Typography variant="h6" color="inherit" style={{ fontWeight: 'bold', color: '#fff' }} noWrap>
                            {localStorage.getItem('role') == 'doctor' ? "Consultations" : "Medical History"}
                        </Typography>
                        {localStorage.getItem('role') == 'user' ?
                            <Create onClick={(e) => { this.handleClick(e) }} className={classes.icon} />
                            :
                            <Icon className={classes.icon} color="disabled" onClick={(e) => { this.handleClick(e) }} fontSize="large">
                                add </Icon>
                        }
                    </Toolbar>
                </AppBar>
                <div className='page-content' style={{ fontWeight: 'bold', backgroundColor: '#fff', marginTop: '-56px' }}>
                    <SideBar mobileOpen={this.state.mobileOpen} handleDrawerToggle={this.handleDrawerToggle} userProfileData={userProfileData} >
                        {children}
                    </SideBar>
                </div>
                <div className={classes.root}>
                    <div className={classes.infoDiv}>
                        <Row>
                            <Col className={classes.profile} xs="8" md="8" sm="8">
                                {
                                    localStorage.getItem('role') == 'doctor' ?
                                        <div>
                                            <Typography variant="body1" gutterBottom align="left" style={{ color: "#42425E", marginBottom: 0, fontWeight: 'bolder', fontSize: '16px' }}>{prescription.doctor && prescription.patient != null && prescription.patient.firstname && prescription.patient.lastname && prescription.patient.firstname != null && prescription.patient.lastname != null ? prescription.patient.firstname + ' ' + prescription.patient.lastname : '--'}
                                            </Typography>
                                            <Typography variant="body1" gutterBottom align="left" style={{ marginTop: '5px', color: AppConfig.colors.secondary, marginBottom: 0, fontWeight: '400', fontSize: '14px' }}>Age: {prescription.patient && prescription.patient != null && prescription.patient.age && prescription.patient.age != null ? prescription.patient.age : '--'}
                                            </Typography>
                                            <Typography key={"owner"} variant="body1" gutterBottom align="left" style={{ color: AppConfig.colors.secondary, fontSize: '14px', marginBottom: 0, fontWeight: '400', }}>Location: {prescription.patient && prescription.patient != null && prescription.patient.address && prescription.patient.address != null ? prescription.patient.address : '--'}
                                            </Typography>
                                        </div>
                                        :
                                        prescription.doctor && prescription.doctor != null ?
                                            <div>
                                                <Typography variant="body1" gutterBottom align="left" style={{ color: "#42425E", marginBottom: 0, fontWeight: 'bolder', fontSize: '16px' }}>{prescription.doctor && prescription.doctor != null && prescription.doctor.firstname && prescription.doctor.lastname && prescription.doctor.firstname != null && prescription.doctor.lastname != null ? 'Dr. ' + prescription.doctor.firstname + ' ' + prescription.doctor.lastname : '--'}
                                                </Typography>
                                                <Typography variant="body1" gutterBottom align="left" style={{ marginTop: '5px', color: AppConfig.colors.secondary, marginBottom: 0, fontWeight: '400', fontSize: '14px' }}>Age: {prescription.doctor && prescription.doctor != null && prescription.doctor.age && prescription.doctor.age != null ? prescription.doctor.age : '--'}
                                                </Typography>
                                                <Typography key={"owner"} variant="body1" gutterBottom align="left" style={{ color: AppConfig.colors.secondary, fontSize: '14px', marginBottom: 0, fontWeight: '400', }}>Location: {prescription.doctor && prescription.doctor != null && prescription.doctor.address && prescription.doctor.address != null ? prescription.doctor.address : '--'}
                                                </Typography>
                                            </div>
                                            :
                                            <div>
                                                <Typography variant="body1" gutterBottom align="left" style={{ color: "#42425E", marginBottom: 0, fontWeight: 'bolder', fontSize: '16px' }}>{prescription.patient && prescription.patient != null && prescription.patient.firstname && prescription.patient.lastname && prescription.patient.firstname != null && prescription.patient.lastname != null ? prescription.patient.firstname + ' ' + prescription.patient.lastname + ` (${userProfileData._id === prescription.patient._id ? 'You' : ''})` : '--'}
                                                </Typography>
                                                <Typography variant="body1" gutterBottom align="left" style={{ marginTop: '5px', color: AppConfig.colors.secondary, marginBottom: 0, fontWeight: '400', fontSize: '14px' }}>Age: {prescription.patient && prescription.patient != null && prescription.patient.age && prescription.patient.age != null ? prescription.patient.age : '--'}
                                                </Typography>
                                                <Typography key={"owner"} variant="body1" gutterBottom align="left" style={{ color: AppConfig.colors.secondary, fontSize: '14px', marginBottom: 0, fontWeight: '400', }}>Location: {prescription.patient && prescription.patient != null && prescription.patient.address && prescription.patient.address != null ? prescription.patient.address : '--'}
                                                </Typography>
                                            </div>
                                }
                            </Col>
                            <Col xs="4" md="4" sm="4" style={{ textAlign: "right" }}>
                                <div className={classes.rightDiv} >
                                    {
                                        localStorage.getItem('role') == 'user' ? (prescription.patient && prescription.patient != null && prescription.patient._id && userProfileData && userProfileData._id && (userProfileData._id === prescription.patient._id && prescription.doctor == null)) ?
                                            <Avatar alt='Profile' src={croppedImage ? croppedImage : require('../../assets/img/noImage.png')} className={classes.imageDiv}>
                                            </Avatar>
                                            :
                                            <Avatar alt='Profile' src={prescription && prescription.doctor && prescription.doctor.profileImage && prescription.doctor.profileImage != undefined && prescription.doctor.profileImage != '' ? AppConfig.baseUrl + '/users/' + prescription.doctor.profileImage + '.png' : require('../../assets/img/noImage.png')} className={classes.imageDiv}>
                                            </Avatar> :
                                            (prescription.patient && prescription.patient != null && prescription.patient._id && userProfileData && userProfileData._id && (userProfileData._id === prescription.patient._id && prescription.doctor == null)) ?
                                                <Avatar alt='Profile' src={croppedImage ? croppedImage : require('../../assets/img/noImage.png')} className={classes.imageDiv}>
                                                </Avatar>
                                                :
                                                <Avatar alt='Profile' src={prescription && prescription.patient && prescription.patient.profileImage && prescription.patient.profileImage != undefined && prescription.patient.profileImage != '' ? AppConfig.baseUrl + '/users/' + prescription.patient.profileImage + '.png' : require('../../assets/img/noImage.png')} className={classes.imageDiv}>


                                                </Avatar>
                                    }
                                    <Button
                                        style={{ borderRadius: '6px' }}
                                        onClick={(e) => this.goBack(e)}
                                        variant="contained"
                                        className={classes.signup}
                                    > Back</Button>
                                </div>
                            </Col>
                        </Row>
                    </div>
                    <div className={classes.container}>
                        <div className={classes.form}>
                            <Typography style={{ color: '#43415e', fontSize: '14px', float: 'left' }}>Title: {prescription && prescription != null && prescription.title && prescription.title != null ? prescription.title : '--'}</Typography>
                        </div>
                        <div className={classes.form}>
                            <Typography style={{ color: '#43415e', fontSize: '14px', float: 'left' }}>Note: {prescription && prescription != null && prescription.note && prescription.note != null ? prescription.note : '--'}</Typography>
                        </div>

                        <div className={classes.form}>
                            <Typography style={{ color: '#43415e', fontSize: '14px', float: 'left' }}>Orders: </Typography>
                        </div>
                        <div className={classes.form}>
                            {prescription && prescription != null && prescription.otherDetails && prescription.otherDetails != null && prescription.otherDetails.length > 0 ? prescription.otherDetails.map((each, i) => {
                                console.log('eachhhhhh', each.other.A);
                                return <div>
                                    <Row>
                                        <Col xs="6" sm="4">
                                            <div style={{ color: '#43415e', padding: 6, fontSize: '12px', float: 'left', fontWeight: 'bold', display: 'inline-block', paddingLeft: '0px' }}>{(i + 1)}.</div> <Typography style={{ color: '#43415e', padding: 5, fontSize: '12px', float: 'left', fontWeight: 'bold', display: 'inline-block', paddingLeft: '0px' }}>Description: </Typography> <Typography style={{ color: '#43415e', padding: 5, fontSize: '12px', display: 'inline-block', float: 'left' }}> {each.description && each.description != '' ? each.description : '--'} </Typography><br></br>
                                        </Col>
                                        <Col xs="6" sm="4">
                                            <Typography style={{ color: '#43415e', padding: 5, fontSize: '12px', float: 'left', fontWeight: 'bold', display: 'inline-block', paddingLeft: '0px' }}>Duration: </Typography> <Typography style={{ color: '#43415e', padding: 5, fontSize: '12px', display: 'inline-block', float: 'left' }}> {each.duration && each.duration !== '' ? each.duration : '--'} </Typography>
                                        </Col>
                                        <Col xs="6" sm="4">
                                            <Typography style={{ color: '#43415e', padding: 5, fontSize: '12px', float: 'left', fontWeight: 'bold', display: 'inline-block', paddingLeft: '15px' }}>Quantity: </Typography> <Typography style={{ color: '#43415e', padding: 5, fontSize: '12px', display: 'inline-block', float: 'left' }}> {each.qty && each.qty != '' ? each.qty : '--'} </Typography>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col xs="6" sm="4">
                                            <Typography style={{ color: '#43415e', padding: 5, fontSize: '12px', float: 'left', fontWeight: 'bold', display: 'inline-block', paddingLeft: '15px' }}>Order Quantity: </Typography> <Typography style={{ color: '#43415e', padding: 5, fontSize: '12px', display: 'inline-block', float: 'left' }}> {each.orderQty && each.orderQty != '' ? each.orderQty : '--'} </Typography>
                                        </Col>
                                        <Col xs="6" sm="4">
                                            <Typography style={{ color: '#43415e', padding: 5, fontSize: '12px', float: 'left', fontWeight: 'bold', display: 'inline-block', paddingLeft: '0px' }}>Other: </Typography>
                                            <Typography style={{ color: '#43415e', padding: 5, fontSize: '12px', float: 'left' }}> {each.other && each.other != null ? <div>
                                                <div> M: {each.other.M ? 'Yes' : 'No'}</div><div>A: {each.other.A ? 'Yes' : 'No'}</div><div> N: {each.other.N ? 'Yes' : 'No'}</div>
                                            </div> : '--'} </Typography>
                                        </Col>
                                    </Row>
                                </div>
                            }) : ''}
                        </div>

                        <div className={classes.form} style={{ padding: '0 20px' }}>
                            {(prescription && prescription !== null && prescription.images && prescription.images != null && prescription.images.length > 0 ? prescription.images.map((data, index) => {
                                return data.indexOf('.pdf') !== -1 ? <a style={{ color: AppConfig.colors.secondary, fontSize: '14px', textDecoration: 'underline' }} href={url + "prescriptions/" + data}>Download PDF</a> : <img key={index} style={{ width: '100%' }} src={url + "prescriptions/" + data} />
                            }) : '')}
                        </div>
                    </div>
                </div>
            </React.Fragment>);
    }
}

PrescriptionDetails.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = ({ authUser }) => {
    const { userProfileData, profileImage } = authUser
    return { userProfileData, profileImage }
}

export default connect(mapStateToProps, { getuserProfileData })(withStyles(styles)(PrescriptionDetails));