import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import { Table, Col, Row } from 'reactstrap';
import SideBar from '../../components/userSideBar'
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import AppBar from '@material-ui/core/AppBar';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import Create from '@material-ui/icons/Create';
import { red } from '@material-ui/core/colors';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import { Input, FormGroup, Label, CustomInput } from 'reactstrap';
import ListItemText from '@material-ui/core/ListItemText';
import { Cropper } from 'react-image-cropper'
import SweetAlert from 'react-bootstrap-sweetalert'
import MatButton from '@material-ui/core/Button';
import { getuserProfileData, getPrescriptionData, editVitals, cropImage } from '../../actions';
import { connect } from 'react-redux';
import axios from '../../constant/axios';
import moment from 'moment';
import appConfig from '../../constant/config'
import Personal from "../../components/UserDetails/personal";
import MedicalHistory from "../../components/UserDetails/medicalHistory";
import Vitals from "../../components/UserDetails/vitals";

const drawerWidth = 240;
const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    appBar: {
        marginLeft: drawerWidth,
        [theme.breakpoints.up('sm')]: {
            width: `calc(100% - ${drawerWidth}px)`,
        },
    },
    menuButton: {
        marginRight: 20,
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
    userName: {
        color: appConfig.colors.primary,
        marginBottom: 14,
        fontWeight: 'bolder',
        fontSize: '19px'
    },
    userDetails: {
        marginTop: '5px',
        color: appConfig.colors.primary,
        fontSize: '15px',
        marginBottom: 0,
        fontWeight: '400'
    },
    dialogDone: {
        backgroundColor: '#454545',
        color: "#3ddb20",
        '&:hover': {
            background: 'none',
            backgroundColor: '#454545',
            color: '#3ddb20'
        },
        borderColor: '#454545'
    },
    dialogTitle: {
        backgroundColor: '#5a5a5a',
        '& h2': {
            color: 'white',
            textAlign: 'center'
        },
        borderBottom: '1px solid #5a5a5a'
    },
    dialogContent: {
        backgroundColor: '#5a5a5a',
    },
    root2: {
        flexGrow: 1,
        backgroundColor: '#fff'
    },
    signup: {
        backgroundColor: appConfig.colors.primary,
        borderRadius: '5px',
        color: appConfig.colors.white,
        padding: '5px 10px',
        textTransform: 'none',
        textAlign: 'right',
        float: 'right'
    },
    tabsRoot: {
        color: appConfig.colors.primary,
        borderBottom: '1px solid #7a7a7a',
    },
    tabsIndicator: {
        backgroundColor: '#7a7a7a',
        height: '3px'
    },
    rightDiv: {
        color: "#CCCACD",
        marginBottom: 0,
        fontWeight: 'bold',
        fontSize: '16px',
        /*  textAlign: 'right',
         float: 'right' */
    },
    listitems: {
        paddingLeft: 40,
        paddingRight: 10,
        paddingTop: 5,
        paddingBottom: 5
    },
    listitems1: {
        paddingLeft: 40,
        paddingRight: 10,
        paddingTop: 14,
        paddingBottom: 5
    },
    tabRoot: {
        textTransform: 'initial',
        minWidth: '33%',
        fontWeight: theme.typography.fontWeightRegular,
        fontFamily: [
            '-apple-system',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ].join(','),
        fontSize: '15px',
    },
    typography: {
        padding: '15px 0px',
        color: '#CCCACD',
        ['@media (max-width:787px)']: {
            padding: '15px 15px',
        },
    },
    boldSize: {
        fontSize: '16px'
    },
    imageDiv: {
        textAlign: 'right',
        height: '70px',
        width: '70px',

        /*   position: 'fixed', */
        height: '70px',
        width: '70px',
    },
    avatar: {
        margin: theme.spacing.unit * 2,
        marginLeft: theme.spacing.unit * 4,
        width: 70,
        height: 70
    },
    profile: {
        marginTop: theme.spacing.unit * 4,
        lineHeight: 1
    },
    tableDiv: {
        padding: '0 50px',
    },
    tabsFlex: {
        borderBottom: '0px solid #7a7a7a',
    },
    tableThTd: {
        color: appConfig.colors.white,
    },
    infoDiv: {
        padding: '0 0 18px 0',
        backgroundColor: "#fff"
    },
    disableClick: {
        pointerEvents: 'none'
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: appConfig.colors.white,
        '&:hover': {
            backgroundColor: appConfig.colors.white,
        },
        marginRight: theme.spacing(2),
        marginLeft: 0,
        width: '100%',
        borderRadius: '40px',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(3),
            width: 'auto',
        },
    },
    searchIcon: {
        width: theme.spacing(7),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        fontWeight: 'bold',
        opacity: 1
    },
    inputRoot: {
        color: '#c6c6c6',
    },
    patientName: {
        color: appConfig.colors.white,
        font: 'bolder'
    },
    inputInput: {
        color: '#c6c6c6',
        padding: theme.spacing(1, 1, 1, 7),
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: 200,
        },
    },
    Sidebardivide: {
        marginLeft: '12px',
        marginRight: '12px',
        borderBottom: '1px solid #eeeeee'
    },
    icon: {
        position: 'fixed',
        right: '10px',
        color: '#fff'
    },
    iconHover: {
        margin: theme.spacing(2),
        '&:hover': {
            color: red[800],
        },
    },
    avatar: {
        margin: 0,
        padding: 0
    },
    avatarlist: {
        margin: 0,
        padding: 0,
        width: 35,
        height: 35,
    },
    profile: {
        margin: 0,
        height: 100,
        lineHeight: 'normal'
    },
    backBtn: {
        backgroundColor: appConfig.colors.primary,
        borderRadius: '0px',
        color: '#fff',
        marginTop: '5px',
        padding: '5px 22px',
        textTransform: 'none',

    }
});

class PatientDetails extends Component {
    constructor(props) {
        super(props)
        console.log('hey  i m here', this.props.location)
        this.state = {
            value: 0,
            openModal: false,
            isLoading: true,
            mobileOpen: false,
            anchorEl: null,
            fullWidth: true,
            maxWidth: 'md',
            status: false,
            patient: {},
            bloodPressure: '',
            temperature: '',
            pulseRate: '',
            allergies: '',
            diseases: '',
            surgeries: '',
            others: '',
            tool: false,
            croppedImage: '',
            profileimage: '',
            imagePreview: '',
            profileimageMain: '',
            image: '',
            imageLoaded: false,
            alert: false,
            openModal: false,
            isLoading: true,
            mobileOpen: false,
            doctorDetails: '',
            lifeStyle: '',
            habits: '',
            isPhoneEnabled: false
        };
    }

    componentWillReceiveProps = (nextProps) => {
        console.log("this.props.userProfileData", nextProps.profileImage)
        if (this.props.userProfileData !== nextProps.userProfileData) {
            this.setState({ croppedImage: nextProps.profileImage, temperature: nextProps.userProfileData.temperature, pulseRate: nextProps.userProfileData.pulseRate, bloodPressure: nextProps.userProfileData.bloodPressure, allergies: nextProps.userProfileData.allergies, diseases: nextProps.userProfileData.diseases, surgeries: nextProps.userProfileData.surgeries, others: nextProps.userProfileData.others, isPhoneEnabled: nextProps.userProfileData.isPhoneEnabled, habits: nextProps.userProfileData.habits, lifeStyle: nextProps.userProfileData.lifeStyle })
        }
        if (this.props.profileImage !== nextProps.profileImage) {
            this.setState({ croppedImage: nextProps.profileImage })
        }
    }

    componentWillMount() {
        this.props.getuserProfileData({ history: this.props.history });
        axios.get(`/users/patientDetails/${this.props.match.params.id}`, {
            headers: {
                'Content-Type': 'application/json',
                'token': localStorage.getItem('token')
            }
        }).then(result => {
            console.log("patientDetails result###", result.data.detail);
            this.setState({ patient: result.data.detail && result.data.detail ? result.data.detail : {} })
        })
            .catch(error => {
                console.log("error....", error);
            });
        this.props.getPrescriptionData({ data: { patientId: this.props.match.params.id }, history: this.props.history });
        console.log('sadsdasdlldk', this.props.userProfileData);
    }

    handleInputChange = (e, key) => {
        console.log('key,e,', key, e.target.value);
        this.setState({ [key]: e.target.value });
    }

    handleClick = (e) => {
        this.props.history.push(`/user/editPatientDetails`)
    }

    handleOnClick = (e, id) => {
        e.preventDefault();
        console.log('id***', id);
        this.props.history.push(`/${localStorage.getItem('role')}/prescriptionDetails/${id}`)
    }

    openUrl = (e, url) => {
        e.preventDefault();
        window.open(url);
    }

    handleSubmit = (e, userId) => {
        e.preventDefault();
        let data = {
            temperature: this.state.temperature,
            pulseRate: this.state.pulseRate,
            bloodPressure: this.state.bloodPressure,
            allergies: this.state.allergies,
            diseases: this.state.diseases,
            surgeries: this.state.surgeries,
            others: this.state.others,
            profileimageMain: this.state.profileimageMain,
            userId: userId
        }
        console.log('this.props.history', this.props.history);
        this.props.editVitals({ data, history: this.props.history })
        this.setState({ openModal: false, errors: {} });
    }

    handleChange = (event, value) => {
        console.log('value', value)
        this.setState({ value });
    };

    handleDrawerToggle = () => {
        this.setState(state => ({ mobileOpen: !state.mobileOpen }));
    };

    handleModalClose = () => {
        this.setState({ openModal: false, errors: {} });
    }

    handleModal = (e) => {
        e.preventDefault();
        this.setState({ openModal: true })
    }

    render() {
        const { classes, userProfileData, children, prescriptionData } = this.props;
        const { openModal, patient } = this.state;
        const { value } = this.state;
        console.log('prescriptionData', prescriptionData)
        return (
            <React.Fragment >
                <AppBar position="fixed" className={classes.appBar}>
                    <Toolbar>
                        <IconButton
                            color="inherit"
                            aria-label="Open drawer"
                            onClick={this.handleDrawerToggle}
                            className={classes.menuButton}
                        >
                            <MenuIcon />
                        </IconButton>
                        <Typography variant="h6" color="inherit" style={{ fontWeight: 'bold', color: '#fff' }} noWrap>
                            {"Profile"}
                        </Typography>
                        <Create onClick={(e) => { this.handleClick(e) }} className={classes.icon} />
                    </Toolbar>
                </AppBar>
                <div className='page-content' style={{ fontWeight: 'bold', backgroundColor: '#fff', marginTop: '-56px' }}>
                    <SideBar mobileOpen={this.state.mobileOpen} handleDrawerToggle={this.handleDrawerToggle} userProfileData={userProfileData} >
                        {children}
                    </SideBar>
                </div>
                <div className={classes.root}>
                    <div className={classes.infoDiv}>
                        <div style={{ backgroundColor: '#fff', color: appConfig.colors.primary, padding: ' 0 24px 0 24px ' }}>
                            <Tabs
                                value={value}
                                onChange={this.handleChange}
                                classes={{ root: classes.tabsRoot, indicator: classes.tabsIndicator, flexContainer: classes.tabsFlex }} >
                                <Tab
                                    disableRipple
                                    classes={{ root: classes.tabRoot }}
                                    label="PERSONAL"
                                    style={{ fontWeight: 'bolder', color: appConfig.colors.primary }} />
                                <Tab
                                    disableRipple
                                    classes={{ root: classes.tabRoot }}
                                    label="VITALS"
                                    style={{ fontWeight: 'bolder', color: appConfig.colors.primary }} />
                                <Tab
                                    disableRipple
                                    classes={{ root: classes.tabRoot }}
                                    label="MEDICAL HISTORY"
                                    style={{ fontWeight: 'bolder', color: appConfig.colors.primary }} />
                            </Tabs>
                        </div>
                    </div>
                    {
                        value === 0 ?

                            <Personal classes={classes} id={this.props.match.params.id} history={this.props.history}/>
                            :
                            value === 1 ? <Vitals classes={classes} id={this.props.match.params.id} history={this.props.history}/> : <MedicalHistory classes={classes} id={this.props.match.params.id} prescriptionData={prescriptionData} history={this.props.history}/>

                    }
                </div>
                {/* 
            
             <div className={classes.root}>
                    <div className={classes.infoDiv}>
                        <Row>
                            {
                                <Col className={classes.profile} xs="8" md="8" sm="8">
                                    <Typography variant="body1" gutterBottom align="left" style={{ color: "#42425E", marginBottom: 0, fontWeight: 'bolder', fontSize: '16px' }}>
                                        {patient && patient != null && patient.firstname && patient.firstname != null ? patient.firstname + ' ' + patient.lastname : ''}
                                    </Typography>
                                    {
                                        (patient && patient.isPhoneEnabled !== undefined) ? <Typography variant="body1" gutterBottom align="left" style={{ marginTop: '5px', color: '#42425E', marginBottom: 0, fontWeight: '400', fontSize: '14px' }}>Contact: {patient && patient.mobile ? patient.mobile : ''}
                                        </Typography> : ''
                                    }
                                    <Typography variant="body1" gutterBottom align="left" style={{ color: '#42425E', marginBottom: 0, fontWeight: '400', fontSize: '14px' }}>Age: {patient && patient != null && patient.age && patient.age != null ? patient.age : ''}
                                    </Typography>
                                    <Typography key={"owner"} variant="body1" gutterBottom align="left" style={{ color: '#42425E', fontSize: '14px', marginBottom: 0, fontWeight: '400' }}>LifeStyle: {patient && patient != null && patient.lifeStyle && patient.lifeStyle != null ? patient.lifeStyle : ''}
                                    </Typography>
                                    <Typography key={"owner"} variant="body1" gutterBottom align="left" style={{ color: '#42425E', fontSize: '14px', marginBottom: 0, fontWeight: '400' }}>Habits: {patient && patient != null && patient.habits && patient.habits != null ? patient.habits : ''}
                                    </Typography>
                                    <Typography key={"owner"} variant="body1" gutterBottom align="left" style={{ color: '#42425E', fontSize: '14px', marginBottom: 0, fontWeight: '400' }}>Location: {patient && patient != null && patient.address && patient.address != null ? patient.address : ''}
                                    </Typography>
                                    {patient && patient != null && patient.geoLocation && patient.geoLocation.coordinates && patient.geoLocation.coordinates.length > 0 ?
                                        <Button
                                            style={{
                                                backgroundColor: appConfig.colors.primary,
                                                borderRadius: '5px',
                                                color: appConfig.colors.white,
                                                padding: '3px 10px',
                                                textTransform: 'none', float: 'left'
                                            }}
                                            variant="contained"
                                            className={classes.signup} onClick={(e) => this.openUrl(e, `http://www.google.com/maps/place/${patient.geoLocation.coordinates[1]},${patient.geoLocation.coordinates[0]}`)}
                                        > View Location</Button>
                                        : ''}
                                </Col>
                            }
                            <Col xs="4" md="4" sm="4" style={{ textAlign: "right" }}>
                                {
                                    <div className={classes.rightDiv} style={{}}>
                                        {
                                            this.state.croppedImage &&
                                            <Avatar alt='Profile' src={localStorage.getItem('role') == 'user' ? this.state.croppedImage ? this.state.croppedImage : require('../../assets/img/noImage.png') : patient.profileImage && patient.profileImage != undefined && patient.profileImage != '' ? appConfig.baseUrl + '/users/' + patient.profileImage + '.png' : require('../../assets/img/noImage.png')} className={classes.imageDiv} >
                                            </Avatar>
                                        }
                                        {(this.state.croppedImage == undefined || this.state.croppedImage == "") &&
                                            <Avatar alt='Profile' src={require('../../assets/img/noImage.png')} className={classes.imageDiv}>
                                            </Avatar>
                                        }
                                    </div>
                                }
                            </Col>
                        </Row>
                    </div>
                    <div style={{ backgroundColor: '#fff', color: '#42425E', padding: '0 24px 0 24px', marginTop: 65 }}>
                        <Tabs
                            value={value}
                            onChange={this.handleChange}
                            classes={{ root: classes.tabsRoot, indicator: classes.tabsIndicator, flexContainer: classes.tabsFlex }}
                        >
                            <Tab
                                disableRipple
                                classes={{ root: classes.tabRoot }}
                                label="HISTORY"
                                style={{ fontWeight: 'bolder', color: '#42425E' }}
                            />
                            <Tab
                                disableRipple
                                classes={{ root: classes.tabRoot }}
                                label="VITALS"
                                style={{ fontWeight: 'bolder', color: '#42425E' }}
                            />
                        </Tabs>
                    </div>
                    {
                        value === 0 ?
                            <div className={classes.tableDiv}>
                                <div key={'List'} className={classes.tableDiv}>
                                    <List className={classes.root2} >
                                        {
                                            prescriptionData && prescriptionData.length > 0 ?
                                                prescriptionData.map((data, index) => {
                                                    console.log('dataaaa', data);
                                                    return <div key={index}>
                                                        <ListItem className={classes.listitems} onClick={(e) => this.handleOnClick(e, data._id)}>
                                                            <ListItemText primary={<React.Fragment>
                                                                <Typography variant="h5" style={{ color: '#42425E', fontWeight: 'bold', fontSize: '16px', padding: '5px 0 5px 0' }} className={classes.patientName}>{data && data.title ? data.title : '--'}</Typography>
                                                            </React.Fragment>} secondary={<Typography style={{ color: '#42425E', fontWeight: 'bold', fontSize: '11px' }}>Date: {data && data.createdAt ? moment(data.createdAt).format("D MMMM YYYY") : ''}</Typography>} />
                                                        </ListItem>
                                                        <li className={classes.Sidebardivide}></li>
                                                    </div>
                                                }) : ''
                                        }
                                    </List>
                                </div>
                            </div>
                            :
                            <div className={classes.tableDiv}>
                                <div key={'List'} className={classes.tableDiv}>
                                    <List className={classes.root2} >
                                        <ListItem className={classes.listitems1} >
                                            <ListItemText primary={<React.Fragment>
                                                <Typography variant="h5" style={{ color: '#42425E', padding: '0', lineHeight: '0' }} className={classes.patientName}><Typography style={{ fontSize: '15px', display: 'inline-block' }}>Temperature : </Typography> <Typography style={{ fontSize: '15px', fontWeight: 'bold', display: 'inline-block' }}>{userProfileData.temperature}</Typography></Typography>
                                                <Typography variant="h5" style={{ color: '#42425E', padding: '0', lineHeight: '0' }} className={classes.patientName}><Typography style={{ fontSize: '15px', display: 'inline-block' }}>Pulse Rate : </Typography> <Typography style={{ fontSize: '15px', fontWeight: 'bold', display: 'inline-block' }}>{userProfileData.pulseRate}</Typography></Typography>
                                                <Typography variant="h5" style={{ color: '#42425E', padding: '0', lineHeight: '0' }} className={classes.patientName}><Typography style={{ fontSize: '15px', display: 'inline-block' }}>Blood Pressure : </Typography> <Typography style={{ fontSize: '15px', fontWeight: 'bold', display: 'inline-block' }}>{userProfileData.bloodPressure}</Typography></Typography>
                                                <Typography variant="h5" style={{ color: '#42425E', padding: '0', lineHeight: '0' }} className={classes.patientName}><Typography style={{ fontSize: '15px', display: 'inline-block' }}>Allergies : </Typography> <Typography style={{ fontSize: '15px', fontWeight: 'bold', display: 'inline-block' }}>{userProfileData.allergies}</Typography></Typography>
                                                <Typography variant="h5" style={{ color: '#42425E', padding: '0', lineHeight: '0' }} className={classes.patientName}><Typography style={{ fontSize: '15px', display: 'inline-block' }}>Injuries/Disease : </Typography> <Typography style={{ fontSize: '15px', fontWeight: 'bold', display: 'inline-block' }}>{userProfileData.diseases}</Typography></Typography>
                                                <Typography variant="h5" style={{ color: '#42425E', padding: '0', lineHeight: '0' }} className={classes.patientName}><Typography style={{ fontSize: '15px', display: 'inline-block' }}>Surgeries : </Typography> <Typography style={{ fontSize: '15px', fontWeight: 'bold', display: 'inline-block' }}>{userProfileData.surgeries}</Typography></Typography>
                                                <Typography variant="h5" style={{ color: '#42425E', padding: '0', lineHeight: '0' }} className={classes.patientName}><Typography style={{ fontSize: '15px', display: 'inline-block' }}>Others : </Typography> <Typography style={{ fontSize: '15px', fontWeight: 'bold', display: 'inline-block' }}>{userProfileData.others}</Typography></Typography>
                                                <Typography style={{ textAlign: 'right', color: '#42425E', fontWeight: 'bold', fontSize: '12px', marginTop: '10px' }}>Last Updated: {userProfileData && userProfileData != null && userProfileData.updatedAt && userProfileData.updatedAt != null && userProfileData.updatedAt ? moment(userProfileData.updatedAt).format("D MMMM YYYY") : ''}</Typography>
                                            </React.Fragment>} />
                                        </ListItem>
                                    </List>
                                </div>

                            </div>
                    }
                </div>

            */}
            </React.Fragment >);
    }
}

PatientDetails.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = ({ authUser, prescriptionReducer }) => {
    const { userProfileData, profileImage } = authUser
    const { prescriptionData } = prescriptionReducer
    return { userProfileData, prescriptionData, profileImage }
}

export default connect(mapStateToProps, { getuserProfileData, getPrescriptionData, editVitals, cropImage })(withStyles(styles)(PatientDetails));