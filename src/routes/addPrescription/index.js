import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import {
    Row,
    Label,
    Input,
    FormGroup,
    Col,
} from 'reactstrap';
import { Dropdown } from 'semantic-ui-react'
import SideBar from '../../components/userSideBar'
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import AppBar from '@material-ui/core/AppBar';
import { ThemeProvider } from "@material-ui/styles";
import { createMuiTheme } from "@material-ui/core/styles";
import { red } from '@material-ui/core/colors';
import TextField from '@material-ui/core/TextField';
import { getuserProfileData, addPrescription, getDoctorPatientsData, getPatientData, getPharmacyData } from '../../actions';
import { connect } from 'react-redux';
import appConfig from "../../constant/config";

const drawerWidth = 240;
const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    appBar: {
        marginLeft: drawerWidth,
        [theme.breakpoints.up('sm')]: {
            width: `calc(100% - ${drawerWidth}px)`,
        },
    },
    menuButton: {
        marginRight: 20,
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
    dialogDone: {
        backgroundColor: '#454545',
        color: "#3ddb20",
        '&:hover': {
            background: 'none',
            backgroundColor: '#454545',
            color: '#3ddb20'
        },
        borderColor: '#454545'
    },
    dialogTitle: {
        backgroundColor: '#5a5a5a',
        '& h2': {
            color: 'white',
            textAlign: 'center'
        },
        borderBottom: '1px solid #5a5a5a'
    },
    dialogContent: {
        backgroundColor: '#5a5a5a',
    },
    root2: {
        flexGrow: 1,
        backgroundColor: '#fff'
    },
    tabsRoot: {
        color: '#42425E',
        borderBottom: '1px solid #7a7a7a',
    },
    tabsIndicator: {
        backgroundColor: '#7a7a7a',
        height: '3px'
    },
    rightDiv: {
        color: "#CCCACD",
        marginBottom: 0,
        fontWeight: 'bold',
        fontSize: '16px',
        textAlign: 'right',
        float: 'right'

    },
    listitems: {
        paddingLeft: 40,
        paddingRight: 10,
        paddingTop: 5,
        paddingBottom: 5
    },
    listitems1: {
        paddingLeft: 40,
        paddingRight: 10,
        paddingTop: 14,
        paddingBottom: 5
    },
    tabRoot: {
        textTransform: 'initial',
        minWidth: '50%',
        fontWeight: theme.typography.fontWeightRegular,
        fontFamily: [
            '-apple-system',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ].join(','),
    },
    typography: {
        padding: '15px 0px',
        color: '#CCCACD',
        ['@media (max-width:787px)']: {
            padding: '15px 15px',
        },
    },
    imageDiv: {
        // right: '30px',
        textAlign: 'right',
        // position: 'fixed',
        height: '70px',
        width: '70px',
    },
    avatar: {
        margin: theme.spacing.unit * 2,
        marginLeft: theme.spacing.unit * 4,
        width: 70,
        height: 70
    },
    profile: {
        marginTop: theme.spacing.unit * 4,
        lineHeight: 1
    },
    tableDiv: {
        padding: '0px',
    },
    tabsFlex: {
        borderBottom: '0px solid #454545',
    },
    tableThTd: {
        color: '#ffffff',
    },
    infoDiv: {
        padding: 18,
        backgroundColor: "#fff"
    },
    disableClick: {
        pointerEvents: 'none'
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: '#ffffff',
        '&:hover': {
            backgroundColor: '#ffffff',
        },
        marginRight: theme.spacing(2),
        marginLeft: 0,
        width: '100%',
        borderRadius: '40px',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(3),
            width: 'auto',
        },
    },
    searchIcon: {
        width: theme.spacing(7),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        fontWeight: 'bold',
        opacity: 1
    },
    inputRoot: {
        color: '#c6c6c6',
    },
    inputInput: {
        color: '#c6c6c6',
        padding: theme.spacing(1, 1, 1, 7),
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: 200,
        },
    },
    icon: {
        position: 'fixed',
        right: '10px',
        color: '#fff'

    },
    iconHover: {
        margin: theme.spacing(2),
        '&:hover': {
            color: red[800],
        },
    },
    avatar: {
        margin: 0,
        padding: 0,
    },
    avatarlist: {
        margin: 0,
        padding: 0,
        width: 35,
        height: 35,
    },
    profile: {
        margin: 0,
        height: 100,
        lineHeight: 'normal'
    },
    signup: {
        backgroundColor: appConfig.colors.primary,
        borderRadius: '0px',
        color: '#fff',
        marginTop: '85px',
        padding: '5px 22px',
        textTransform: 'none',
        marginRight: '8px',
        ['@media (max-width:707px)']: {
            marginRight: 'unset',
        },
        ['@media (max-width:599px)']: {
            marginRight: '8px',
        },
        ['@media (max-width:436px)']: {
            marginRight: 'unset',
        },
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        borderRadius: '8px',
        color: appConfig.colors.primary
    },
    dense: {
        marginTop: theme.spacing(2),
    },
    menu: {
        width: 200,
    },
    submit: {
        backgroundColor: appConfig.colors.primary,
        color: '#fff',
        marginTop: theme.spacing.unit * 2,
        padding: '8px 30px',
        textTransform: 'none',
        borderRadius: '0',
        '&:hover': {
            background: appConfig.colors.primary,
            backgroundColor: '#292743',
            color: '#fff'
        },
    },
    form: {
        width: '100%',
        paddingLeft: '20px',
        paddingRight: '35px',
        marginTop: theme.spacing.unit,
    },
    error: {
        color: 'red',
        float: 'left',
        paddingLeft: '9px'
    }
});

class AddPrescription extends Component {
    constructor(props) {
        super(props)
        this.state = {
            uploaded_image_path: '',
            value: 0,
            openModal: false,
            isLoading: true,
            imageArray: [],
            imagesArray: [],
            addmoreText: [],
            title: '',
            note: '',
            patient: '',
            /* pharmacy: '', */
            options: [],
            errors: {},
            category : ''
        };
        this.handleValidation = this.handleValidation.bind(this);
    }

    handleValidation = () => {
        let errors = {};
        let formIsValid = true;
        console.log("this.state handleValidation", this.state)
        if (localStorage.getItem('role') == "doctor" && this.state.patient === '') {
            formIsValid = false;
            errors["patient"] = "This field is required";
        }
        if (localStorage.getItem('role') == "user" && this.state.title === '') {
            formIsValid = false;
            errors["title"] = "This field is required";
        }
        if (localStorage.getItem('role') == "doctor" && this.state.title === '') {
            formIsValid = false;
            errors["category"] = "This field is required";
        }
        this.setState({ errors: errors });
        return formIsValid;
    }

    componentWillMount() {
        this.props.getuserProfileData({ history: this.props.history });
        this.props.getPatientData({ history: this.props.history });
        this.props.getPharmacyData({ history: this.props.history });
        this.props.getDoctorPatientsData({ history: this.props.history });
    }

    handleChange = (event, key) => {
        if (key == 'patient' || key == 'pharmacy') {
            console.log('event', event)
            this.setState({ [key]: event });
        } else {
            this.setState({ [key]: event.target.value });
        }
    };

    handleOnSubmit = async (e, userId) => {
        e.preventDefault();
        let data = {
            title: this.state.title,
            note: this.state.note,
            category: this.state.category,
            /*    pharmacy: this.state.pharmacy, */
            patient: localStorage.getItem('role') == "user" ? userId : this.state.patient,
            items: this.state.addmoreText,
            images: this.state.imagesArray
        }

        console.log('prescription data', data);
        let isValid = await this.handleValidation();
        if (isValid === true) {
            this.props.addPrescription({
                data,
                history: this.props.history
            })
        } else {
            return;
        }
    }

    openImageUploader() {
        this.refs.imageUploader.click();
    }

    handleImageChange = name => event => {
        console.log('name=', name, event.target.files[0])
        let arr = [...this.state.imageArray];
        let imagearr = [...this.state.imagesArray];
        if (name === 'uploaded_image_path') {
            if (event.target.files[0]) {
                var file = event.target.files[0];
                var reader = new FileReader();
                var url = reader.readAsDataURL(file);
                reader.onloadend = function (e) {
                    console.log('reader.result', reader.result);
                    imagearr.push(reader.result)
                    this.setState({
                        imageArray: arr,
                        imagesArray: imagearr,
                    });
                }.bind(this);
                console.log('url=,', url)
                arr.push(event.target.files[0].name)
            }
        }
        this.setState({
            imageArray: arr,
            imagesArray: imagearr,
        });
    };

    handleDrawerToggle = () => {
        this.setState(state => ({ mobileOpen: !state.mobileOpen }));
    };

    handleModalClose = () => {
        this.setState({ openModal: false, errors: {} });
    }

    removeItems = (index) => {
        this.props.removeHandler(index)
    }

    addMoreItems = (e) => {
        e.preventDefault()
        let addmoreText = this.state.addmoreText;
        addmoreText.push({ description: '', other: { M: false, A: false, N: false }, duration: '', qty: '', orderQty: '' });
        this.setState({ addmoreText: addmoreText })
    }

    handleItemChange = (e, name, ind) => {
        e.preventDefault()
        var oldState = [...this.state.addmoreText];
        oldState.map((data, i) => {
            if (ind == i) {
                console.log('name', name, e.target.value, e.target.checked);
                if (name == 'other') {
                    if (e.target.checked) {
                        oldState[i][name][e.target.value] = true;
                    } else {
                        oldState[i][name][e.target.value] = false;
                    }
                } else {
                    oldState[i][name] = e.target.value;
                }
            }
        })
        console.log('oldState', oldState);

        this.setState({ addmoreText: oldState });
    }

    removeHandler = (e, ind) => {
        e.preventDefault()
        var oldState = [...this.state.addmoreText];
        var newState = []
        oldState.map((data, i) => {
            if (ind !== i) {
                newState.push(data)
            }
        })
        this.setState({ addmoreText: newState });
    }

    render() {
        const { classes, userProfileData, children, url, patientData, pharmacyData, doctorPatientData } = this.props;
        var patients = [];
        var pharmacy = []
        const theme = createMuiTheme({
            palette: {
                primary: { main: appConfig.colors.primary },
                secondary: { main: appConfig.colors.primary },
            },
            overrides: {
                MuiInputLabel: {
                    root: {
                        color: "#43415e",
                        "&$focused": {
                            color: "#43415e"
                        }
                    }
                },
                MuiInputBase: {
                    root: {
                        color: "#43415e",
                        "&$focused": {
                            color: "#43415e"
                        }
                    },
                },
                MuiFormLabel: {
                    root: {
                        color: "#43415e",
                        "&$focused": {
                            color: "#43415e"
                        }
                    }
                },
                MuiInput: {
                    root: {
                        borderColor: '#fff'
                    },
                    underline: {
                        color: appConfig.colors.primary,
                        "&$focused": {
                            color: "#43415e",
                        },
                        "&:before": {
                            color: "#43415e",
                            borderBottom: '1px solid rgba(231, 231, 231, 1)'
                        },
                        borderBottom: '1px solid rgba(231, 231, 231, 1)',
                    }
                },
                MuiOutlinedInput: {
                    input: {
                        fontSize: '13px',
                    },
                    notchedOutline: {
                        borderColor: "#43415e"
                    }
                }
            }
        });
        console.log('doctorPatientData', doctorPatientData)
        console.log('this.state.addmoreText', this.state.addmoreText);
        return (
            <React.Fragment>
                <AppBar position="fixed" className={classes.appBar}>
                    <Toolbar>
                        <IconButton
                            color="inherit"
                            aria-label="Open drawer"
                            onClick={this.handleDrawerToggle}
                            className={classes.menuButton}>
                            <MenuIcon />
                        </IconButton>
                        <Typography variant="h6" color="inherit" style={{ fontWeight: 'bold', color: '#fff' }} noWrap>
                            {"Consultation"}
                        </Typography>
                    </Toolbar>
                </AppBar>
                <div className='page-content' style={{ fontWeight: 'bold', backgroundColor: '#fff', marginTop: '-56px' }}>
                    <SideBar mobileOpen={this.state.mobileOpen} handleDrawerToggle={this.handleDrawerToggle} userProfileData={userProfileData} >
                        {children}
                    </SideBar>
                </div>
                <div className={classes.root}>
                    {doctorPatientData && doctorPatientData.length > 0 ?
                        doctorPatientData.map((data, index) => {
                            console.log('data%%%', data);
                            patients.push({ key: index, value: data.patients, text: data.patientDetails.firstname + ' ' + data.patientDetails.lastname })
                        }) : []}
                    {pharmacyData && pharmacyData.length > 0 ?
                        pharmacyData.map((data, index) => {
                            console.log('data', data);
                            pharmacy.push({ key: index, value: patients, text: data.firstname + ' ' + data.lastname })
                        }) : []}
                    <div className={classes.container}>
                        <form className={classes.form} onSubmit={(e) => this.handleOnSubmit(e, userProfileData._id)}>
                            <ThemeProvider theme={theme}>
                                <Col sm={12} xs={12} md={12}>
                                    {localStorage.getItem('role') == "user" ? '' :
                                        <div>
                                            <div style={{ padding: '18.5px 0 8px 8px' }}>
                                                <Dropdown
                                                    style={{
                                                        border: '1px solid #43415e',
                                                        font: 'inherit', color: appConfig.colors.primary, display: 'inline-flex',
                                                        fontSize: '1rem',
                                                        boxSizing: 'border-box',
                                                        alignItems: 'center',
                                                    }}
                                                    placeholder='Select Patient'
                                                    fluid
                                                    search
                                                    selection
                                                    onChange={(e, { value }) => this.handleChange(value, 'patient')}
                                                    options={patients.length > 0 ? patients : []}
                                                />
                                                <span className={classes.error} >{this.state.errors["patient"]}</span>
                                            </div>

                                        </div>
                                    }
                                    <TextField
                                        id="title"
                                        label="Title"
                                        fullWidth
                                        name='title'
                                        onChange={(e) => this.handleChange(e, 'title')}
                                        className={classes.textField}
                                        style={{ color: appConfig.colors.primary, }}
                                        margin="normal"
                                        variant="outlined"
                                    />
                                    <span className={classes.error} >{this.state.errors["title"]}</span>
                                    <TextField
                                        id="category"
                                        label="Prescription for"
                                        fullWidth
                                        name='category'
                                        onChange={(e) => this.handleChange(e, 'category')}
                                        className={classes.textField}
                                        style={{ color: appConfig.colors.primary, }}
                                        margin="normal"
                                        variant="outlined"
                                    />
                                    <span className={classes.error} >{this.state.errors["category"]}</span>
                                    {localStorage.getItem('role') == "user" ? '' :
                                        <TextField
                                            id="outlined-multiline-static"
                                            label="Add Note"
                                            fullWidth
                                            onChange={(e) => this.handleChange(e, 'note')}
                                            name='note'
                                            multiline
                                            rows="4"
                                            className={classes.textField}
                                            margin="normal"
                                            variant="outlined"
                                        />
                                    }
                                </Col>
                                {localStorage.getItem('role') == "user" ? '' :
                                    this.state.addmoreText.map((data, index) => {
                                        console.log('data!!!!', data);
                                        return <div key={index}>
                                            <Col sm={12} xs={12} md={12}>
                                                <TextField
                                                    id="description"
                                                    label="Description"
                                                    fullWidth
                                                    name="description"
                                                    className={classes.textField}
                                                    style={{ color: appConfig.colors.primary, }}
                                                    margin="normal"
                                                    variant="outlined"
                                                    onChange={(e) => this.handleItemChange(e, 'description', index)}
                                                />
                                            </Col>
                                            <Col sm={12} xs={12} md={12}>
                                                <Col sm={2} xs={2} md={2} style={{ display: 'inline-block' }}>
                                                    <FormGroup check>
                                                        <Label check style={{ color: appConfig.colors.primary }}>
                                                            <Input type="checkbox" style={{ marginTop: '.1rem' }} value={'M'} onChange={(e) => this.handleItemChange(e, 'other', index)} id={index + 'M'} name="other" checked={this.state.addmoreText[index].other["M"]} />{' '}
                                                            M</Label>
                                                    </FormGroup>
                                                </Col>
                                                <Col sm={2} sm={2} xs={2} md={2} style={{ display: 'inline-block' }}>
                                                    <FormGroup check >
                                                        <Label check style={{ color: appConfig.colors.primary }}>
                                                            <Input type="checkbox" value={'A'} id={index + 'A'} style={{ marginTop: '.1rem' }} onChange={(e) => this.handleItemChange(e, 'other', index)} name="other" checked={this.state.addmoreText[index].other["A"]} />{' '}
                                                            A</Label>
                                                    </FormGroup>
                                                </Col>
                                                <Col sm={2} sm={2} xs={2} md={2} style={{ display: 'inline-block' }}>
                                                    <FormGroup check>
                                                        <Label check style={{ color: appConfig.colors.primary }}>
                                                            <Input type="checkbox" value={'N'} onChange={(e) => this.handleItemChange(e, 'other', index)} style={{ marginTop: '.1rem' }} checked={this.state.addmoreText[index].other["N"]} id={index + 'N'} name="other" />{' '}
                                                            N</Label>
                                                    </FormGroup>
                                                </Col>
                                            </Col>
                                            <Col sm={8} xs={8} md={8} style={{ display: 'inline-block' }}>
                                                <TextField
                                                    id="duration"
                                                    label="Duration"
                                                    fullWidth
                                                    type="text"
                                                    name={'duration'} onChange={(e) => this.handleItemChange(e, 'duration', index)}
                                                    className={classes.textField}
                                                    style={{ color: appConfig.colors.primary, }}
                                                    margin="normal"
                                                    variant="outlined"
                                                />
                                            </Col>
                                            <Col sm={4} xs={4} md={4} style={{ display: 'inline-block' }}>
                                                <TextField
                                                    id="qty"
                                                    label="Qty"
                                                    fullWidth
                                                    type="number"
                                                    name='qty'
                                                    onChange={(e) => this.handleItemChange(e, 'qty', index)}
                                                    className={classes.textField}
                                                    style={{ color: appConfig.colors.primary, }}
                                                    margin="normal"
                                                    variant="outlined"
                                                />
                                            </Col>
                                            {this.state.addmoreText.length > 0 &&
                                                <Col sm={12} xs={12} md={12} style={{ textAlign: 'center' }}>
                                                    <span>
                                                        <Button className={classes.submit} style={{ textAlign: 'center', padding: '6px 10px', borderRadius: '11px', marginLeft: '8px' }} onClick={(e) => this.removeHandler(e, index)}>Remove </Button>
                                                    </span>
                                                </Col>
                                            }
                                        </div>
                                    })
                                }
                                {localStorage.getItem('role') == "user" ? '' :
                                    <Col sm={12} xs={12} md={12} style={{ display: 'inline-block', textAlign: 'center' }}>
                                        <Button color="secondary" style={{ textAlign: 'center', marginTop: '5px', marginRight: '5px', marginLeft: '8px', padding: '6px 15px', borderRadius: '11px' }} className={classes.submit} onClick={(e) => this.addMoreItems(e)}>Add Medicine</Button>
                                    </Col>
                                }
                                {
                                    (this.state.imageArray.length > 0) ?
                                        this.state.imageArray.map((image, key) =>
                                            <Col sm={12}>
                                                <TextField
                                                    id="product_image"
                                                    fullWidth
                                                    style={{ color: appConfig.colors.primary, marginTop: '8px', padding: '10px 14px !important' }}
                                                    className={classes.textField}
                                                    value=
                                                    {
                                                        image ? image : ''} disabled
                                                    margin="normal"
                                                    variant="outlined" />
                                            </Col>) :
                                        <Col sm={12}>
                                            <TextField
                                                id="product_image"
                                                fullWidth
                                                style={{ color: appConfig.colors.primary, marginTop: '8px', padding: '10px 14px !important' }}
                                                className={classes.textField}
                                                value=
                                                {''} disabled
                                                margin="normal"
                                                variant="outlined"
                                            />
                                        </Col>
                                }
                                <Col sm={12}>
                                    <Row>
                                        <Col sm={6}>
                                            <input type="file" id="uploaded_image_path" ref="imageUploader" name="uploaded_image_path" onChange={this.handleImageChange('uploaded_image_path')} style={{ display: 'none' }} />

                                            <Button className={classes.submit} style={{ borderRadius: '11px' }} onClick={() => this.openImageUploader()} >Upload Document</Button>

                                        </Col>
                                        <Col sm={6}>
                                            <Button
                                                type="submit"
                                                variant="contained"
                                                className={classes.submit} style={{ padding: '6px 20px', borderRadius: '11px' }}
                                            >Confirm</Button>
                                        </Col>
                                    </Row>
                                </Col>
                            </ThemeProvider>
                            <br />
                        </form>
                    </div>
                </div>
            </React.Fragment>);
    }
}

AddPrescription.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = ({ authUser, doctorReducer }) => {
    const { userProfileData, patientData, pharmacyData } = authUser
    const { doctorPatientData } = doctorReducer
    return { userProfileData, patientData, pharmacyData, doctorPatientData }
}

export default connect(mapStateToProps, { getuserProfileData, getDoctorPatientsData, addPrescription, getPatientData, getPharmacyData })(withStyles(styles)(AddPrescription));