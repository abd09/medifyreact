import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import SearchBar from 'material-ui-search-bar'
import List from '@material-ui/core/List';
import CallIcon from '@material-ui/icons/CallEnd';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import IconButton from '@material-ui/core/IconButton';
import AppConfig from '../../constant/config';
import Button from '@material-ui/core/Button';
import axios from '../../constant/axios';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContentText from '@material-ui/core/DialogContentText';
import FilterComponent from '../../components/FilterComponent';
import MenuIcon from '@material-ui/icons/Menu';
import Chat from '@material-ui/icons/Chat';
import {
    isMobile
} from "react-device-detect";

import {
    Col,
    Row
} from 'reactstrap';
import Toolbar from '@material-ui/core/Toolbar';
import SideBar from '../../components/userSideBar'
import AppBar from '@material-ui/core/AppBar';
import { getuserProfileData, connectedDoctorsData, getAllDoctorsData, searchDoctors, filterDoctors, getSpecialities, getDoctorDetail } from '../../actions';
import { connect } from 'react-redux';

const drawerWidth = 240;
const styles = theme => ({
    root: {
        flexGrow: 1,
        backgroundColor: '#f5f5f5 !important',
    },
    root2: {
        flexGrow: 1,
        backgroundColor: '#fff'
    },
    appBar: {
        marginLeft: drawerWidth,
        [theme.breakpoints.up('sm')]: {
            width: `calc(100% - ${drawerWidth}px)`,
        },
    },
    menuButton: {
        marginRight: 20,
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
    tabsRoot: {
        borderBottom: '1px solid #454545',
    },
    tabsIndicator: {
        backgroundColor: '#58de45',
        height: '4px'
    },
    listitems: {
        /*  paddingLeft: 40,
         paddingRight: 10, */
    },
    tabRoot: {
        textTransform: 'initial',
        minWidth: '50%',
        fontWeight: theme.typography.fontWeightRegular,
        // marginRight: theme.spacing.unit * 4,
        fontFamily: [
            '-apple-system',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ].join(','),
    },
    tabSelected: {},
    typography: {
        padding: '15px 0px',
        color: '#CCCACD',
        ['@media (max-width:787px)']: {
            padding: '15px 15px',
        },
    },
    avatar: {
        margin: theme.spacing.unit * 2,
        marginLeft: theme.spacing.unit * 4,
        width: 70,
        height: 70
    },
    profile: {
        marginTop: theme.spacing.unit * 4,
        lineHeight: 1
    },
    tableDiv: {
        padding: '5px',
        flexWrap: 'unset !important'
    },
    tabsFlex: {
        borderBottom: '1px solid #454545',
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: '#ffffff',
        '&:hover': {
            backgroundColor: '#ffffff',
        },
        marginRight: 0,
        marginLeft: theme.spacing(2),
        marginTop: theme.spacing(1),
        width: '100%',
        borderRadius: '40px',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(3),
            width: 'auto',
        },
    },
    searchIcon: {
        width: theme.spacing(7),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        fontWeight: 'bold',
        opacity: 1
    },
    inputRoot: {
        color: '#c6c6c6',
    },
    docName: {
        color: '#FFFFFF',
        font: 'bolder'
    },
    inputInput: {
        color: '#c6c6c6',
        padding: theme.spacing(1, 1, 1, 7),
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: 200,
        },
    },
    docdivide: {
        marginLeft: '12px',
        marginRight: '12px',
        borderBottom: '1px solid #eeeeee'
    },
    dialogDone: {
        backgroundColor: AppConfig.colors.darkGrey,
        color: "#3ddb20",
        '&:hover': {
            background: 'none',
            backgroundColor: AppConfig.colors.darkGrey,
            color: '#3ddb20'
        },
        borderColor: AppConfig.colors.darkGrey
    },
    dialogTitle: {
        backgroundColor: '#5a5a5a',
        '& h2': {
            color: 'white',
            textAlign: 'center'
        },
        borderBottom: '1px solid #5a5a5a'
    },
    dialogContent: {
        backgroundColor: '#5a5a5a',
    },
    dailogContentText: {
        height: 300,
        width: 250
    },
});

class PharmacyDoctor extends Component {
    constructor(props) {
        super(props)
        console.log('hey  i m here', this.props.location)
        this.state = {
            value: '',
            isLoading: true,
            mobileOpen: false,
            doctors: [],
            searchText: '',
            doctorDetails: '',
            open: false,
            scroll: 'paper',
            filterStateValue: null
        };
        this.filterRef = React.createRef();
    }

    handleChat = (e, id) => {
        let data = {
            userOne: this.props.userProfileData._id,
            userTwo: id
        }
        axios.post(`/users/connectChat`, data, {
            headers: {
                'Content-Type': 'application/json',
                'token': localStorage.getItem('token')
            }
        }).then(result => {
            console.log("pharmacyDetails result^^^ pharmacy", result.data);
            if (result.data.error === false) {
                this.props.history.push(`/chat/${result.data.detail[0]._id}/${id}`)
            } else {
                this.props.history.push(`/${localStorage.getItem('role')}`)
            }
        }).catch(error => {
            console.log("error....", error);
            this.props.history.push(`/${localStorage.getItem('role')}`)
        });
    }

    componentWillMount() {
        this.props.getuserProfileData({ history: this.props.history });
        this.props.connectedDoctorsData({ history: this.props.history });
        this.props.getAllDoctorsData({ history: this.props.history })
        this.props.getSpecialities({ history: this.props.history })
    }

    handleChange = (event, value) => {
        console.log('value', value)
        this.setState({ value });
    };

    handleDrawerToggle = () => {
        this.setState(state => ({ mobileOpen: !state.mobileOpen }));
    };

    handleModal = (e, isBrowser, docId) => {
        e.preventDefault();
        console.log('docId', docId);
        this.props.getDoctorDetail({ data: { id: this.props.match.params.id }, history: this.props.history });

        this.setState({ openModal: true, isBrowser: isBrowser, doctorId: docId })
    }


    handleModalClose = () => {
        this.setState({ openModal: false, errors: {} });
    }


    handleSearchText = (e, key) => {
        console.log('e searchText key', e);
        this.setState({ [key]: e })
    }

    handleApplyFilter() {
        console.log('asdasdasdasdsad', this.filterRef.current.state.single.value)
        this.props.filterDoctors({ data: this.filterRef.current.state.single, history: this.props.history });
        this.setState({ open: false, filterStateValue: this.filterRef.current.state.single });
    }

    handleClearFilter(e) {
        e.preventDefault();
        this.props.filterDoctors({ data: '', history: this.props.history });
        this.props.getAllDoctorsData({ history: this.props.history })
        this.setState({ open: false, filterStateValue: null });
    }

    handleCloseFilter(e) {
        e.preventDefault();
        this.setState({ open: false, filterStateValue: null });
    }

    handleModalOpen(e) {
        e.preventDefault();
        this.setState({ open: true });
    }

    searchResults = (e, searchText) => {
        console.log('resultssss', searchText.length);
        if (searchText.length > 0) {
            this.props.searchDoctors({ data: { searchText: searchText }, history: this.props.history });
        } else {
            this.props.getAllDoctorsData({ history: this.props.history })
        }
    }

    handleOnClick = (e, id) => {
        e.preventDefault();
        this.props.history.push(`/${localStorage.getItem('role')}/doctorDetails/${id}`)
    }

    render() {
        const { classes, userProfileData, children, doctorsData, specialityList, filterDoctorData, doctorDetails } = this.props;
        const { searchText, open } = this.state;
        console.log('doctorsData', doctorsData);

        return (
            <React.Fragment>
                <AppBar position="fixed" className={classes.appBar}>
                    <Toolbar>
                        <IconButton
                            color="inherit"
                            aria-label="Open drawer"
                            onClick={this.handleDrawerToggle}
                            className={classes.menuButton}
                        >
                            <MenuIcon />
                        </IconButton>
                        <Typography variant="h6" color="inherit" style={{ fontWeight: 'bold', color: '#fff' }} noWrap>
                            {"Doctors"}
                        </Typography>
                    </Toolbar>
                </AppBar>
                <div className='page-content' style={{ fontWeight: 'bold', backgroundColor: '#fff', marginTop: '-56px' }}>
                    <SideBar mobileOpen={this.state.mobileOpen} handleDrawerToggle={this.handleDrawerToggle} userProfileData={userProfileData} >
                        {children}
                    </SideBar>
                </div>
                <div className={classes.root}>
                    <Row className={classes.tableDiv} >
                        <Col className={classes.search} sm={10} xs={10} md={10} lg={11} xl={11}>
                            <SearchBar value={searchText} onRequestSearch={(e) => this.searchResults(e, searchText)} onChange={(e) => this.handleSearchText(e, 'searchText')} style={{ borderRadius: '40px', height: '40px', fontWeight: 'bold', boxShadow: 'none', marginBottom: '8px' }} />
                        </Col>
                        <Col className={'filter'} sm={2} md={2} xs={2} lg={1} xl={1}>
                            <i className="fa fa-filter" aria-hidden="true" onClick={(e) => this.handleModalOpen(e)}></i>
                        </Col>
                    </Row>

                    <div key={'doctorList'} className={classes.tableDiv}>
                        <List className={classes.root2} >
                            {
                                doctorsData && doctorsData.length > 0 ?
                                    doctorsData.map((data, index) => {
                                        console.log('dataaaa', data);
                                        return <div key={index}>
                                            <ListItem className={classes.listitems} >
                                                <Col sm={8} xs={(data && data._id && data.isChatEnabled === true) ? 7 : 8} md={8} lg={8} xl={8} onClick={(e) => this.handleOnClick(e, data._id)}>
                                                    <ListItemText primary={<Typography style={{ color: '#42425E', fontWeight: 'bold', fontSize: '12px' }}>ID: {data && data.userId ? data.userId : ''}</Typography>} secondary={<React.Fragment>
                                                        <Typography style={{ color: AppConfig.colors.primary, fontWeight: 'bold', fontSize: '18px', padding: '5px 0 5px 0', display: 'inline-block' }} className={classes.docName}>Dr. {data && data.firstname ? data.firstname + ' ' + data.lastname : ''}</Typography>
                                                        <Typography style={{ color: AppConfig.colors.grey, fontWeight: 'bold', fontSize: '12px', display: 'inline-block', marginLeft: 10 }}>{data && data.speciality ? `(${data.speciality.title})` : ''}</Typography>
                                                    </React.Fragment>} />
                                                </Col>
                                                <Col sm={(data && data._id && data.isChatEnabled === true) ? 3 : 4} xs={(data && data._id && data.isChatEnabled === true) ? 3 : 4} md={(data && data._id && data.isChatEnabled === true) ? 3 : 4} lg={(data && data._id && data.isChatEnabled === true) ? 3 : 4} xl={(data && data._id && data.isChatEnabled === true) ? 3 : 4} onClick={(e) => this.handleModal(e, isMobile, data._id)}>
                                                    <ListItemText style={{
                                                        textAlign: 'right'
                                                    }} primary={<React.Fragment><Typography style={{ color: '#42425E', fontWeight: 'bold', fontSize: '12px' }}><CallIcon /></Typography><Typography style={{ color: '#42425E', fontWeight: 'bold', fontSize: '12px' }}>Call</Typography></React.Fragment>} />
                                                </Col>
                                                {
                                                    (data && data._id && data.isChatEnabled === true) ? <Col sm={1} xs={2} md={1} lg={1} xl={1} onClick={(e) => this.handleChat(e, data._id)} >
                                                        <ListItemText style={{
                                                            float: 'right', marginTop: '0px', marginBottom: '0px',
                                                        }} primary={<React.Fragment><Typography style={{ color: '#42425E', fontWeight: 'bold', fontSize: '12px' }}><Chat /></Typography><Typography style={{ color: '#42425E', fontWeight: 'bold', fontSize: '12px' }}>Chat</Typography></React.Fragment>} />
                                                    </Col> : ''
                                                }
                                            </ListItem>
                                            <li className={classes.docdivide}></li>
                                        </div>
                                    }) : <div><Typography style={{
                                        backgroundColor: '#fff', color: 'gray', fontWeight: 'bold', fontSize: '25px',
                                    }}>No Doctors</Typography></div>
                            }
                        </List>
                        <Dialog
                            fullWidth={this.state.fullWidth}
                            maxWidth={this.state.maxWidth}
                            open={this.state.openModal}
                            onClose={this.handleModalClose}
                            aria-labelledby="max-width-dialog-title">
                            <DialogTitle id="max-width-dialog-title">
                                <Typography variant="body1" style={{ color: '#000' }}>Mobile Number: </Typography> {doctorDetails.mobile ? isMobile ? <a style={{ color: AppConfig.colors.primary, fontSize: '12px', fontWeight: 'bold' }} href={`tel:+91${doctorDetails.mobile}`}> +91{doctorDetails.mobile}</a> : <Typography key={"owner354"} variant="body1" style={{ color: AppConfig.colors.primary, fontSize: '12px', fontWeight: 'bold' }} gutterBottom align="left" >{doctorDetails.mobile}</Typography> : 'No number'}
                            </DialogTitle>
                            <DialogContent>
                                <form className={classes.form}>
                                    <div style={{ textAlign: 'center' }}>
                                        {this.state.isBrowser ?
                                            <div style={{ textAlign: 'center' }}>
                                                {doctorDetails.mobile ? <Button variant="outlined" style={{ padding: '1px 10px' }} className={classes.submit2} type="submit" >
                                                    <a style={{ color: AppConfig.colors.primary, fontSize: '12px', fontWeight: 'bold', textDecoration: 'none', textAlign: 'center' }} href={`tel:+91${doctorDetails.mobile}`}> Call</a> </Button> :
                                                    <Button variant="outlined" style={{ padding: '1px 10px' }} onClick={this.handleModalClose} className={classes.submit2} type="submit" >
                                                        Ok </Button>}
                                            </div>
                                            :
                                            <div style={{ textAlign: 'center' }}>
                                                <Button variant="outlined" style={{ padding: '1px 10px' }} className={classes.submit2} type="submit" onClick={this.handleModalClose} >
                                                    Ok</Button>
                                            </div>
                                        }
                                    </div>
                                </form>
                            </DialogContent>
                        </Dialog>
                        <Dialog
                            open={open}
                            onClose={(e) => this.handleCloseFilter(e)}
                            scroll={this.state.scroll}
                            aria-labelledby="scroll-dialog-title"
                        >
                            <DialogTitle id="scroll-dialog-title">Filter</DialogTitle>
                            <DialogContent dividers={this.state.scroll === 'paper'} className={classes.dailogContentText}>
                                <FilterComponent ref={this.filterRef} filterFor={'Speciality'} speciality={specialityList} filterStateValue={this.state.filterStateValue} />
                            </DialogContent>
                            <DialogActions>
                                <Button variant="contained" onClick={(e) => this.handleClearFilter(e)} color="primary">
                                    Remove Filter
						</Button>
                                <Button variant="contained" onClick={(e) => this.handleApplyFilter(e)} color="primary">
                                    Apply Filter
						</Button>
                            </DialogActions>
                        </Dialog>
                        <Dialog
                            fullWidth={this.state.fullWidth}
                            maxWidth={this.state.maxWidth}
                            open={this.state.openModal}
                            onClose={this.handleModalClose}
                            aria-labelledby="max-width-dialog-title"
                        >
                            <DialogTitle id="max-width-dialog-title"><Typography variant="body1" style={{ color: '#000' }}>Mobile Number: </Typography>{doctorDetails.mobile ? isMobile ? <a style={{ color: '#42425E', fontSize: '12px', fontWeight: 'bold' }} href={`tel:+91${doctorDetails.mobile}`}> +91{doctorDetails.mobile}</a> : <Typography key={"owner354"} variant="body1" style={{ color: '#42425E', fontSize: '12px', fontWeight: 'bold' }} gutterBottom align="left" >{doctorDetails.mobile}</Typography> : 'No number'} </DialogTitle>
                            <DialogContent>
                                <form className={classes.form}>
                                    <div>
                                        {this.state.isBrowser ?
                                            <div>
                                                {doctorDetails.mobile ? <Button variant="outlined" style={{ padding: '1px 10px' }} className={classes.submit2} type="submit" >
                                                    <a style={{ color: '#42425E', fontSize: '12px', fontWeight: 'bold', textDecoration: 'none' }} href={`tel:+91${doctorDetails.mobile}`}> Call</a> </Button> :
                                                    <Button variant="outlined" style={{ padding: '1px 10px' }} className={classes.submit2} type="submit" >
                                                        Ok </Button>}
                                            </div>
                                            :
                                            <div>
                                                <Button variant="outlined" style={{ padding: '1px 10px' }} className={classes.submit2} type="submit" onClick={this.handleModalClose} >
                                                    Ok</Button>

                                            </div>
                                        }
                                    </div>
                                </form>
                            </DialogContent>
                        </Dialog>
                    </div>
                </div>
            </React.Fragment>);
    }
}

PharmacyDoctor.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = ({ authUser, doctorReducer, specialityReducer }) => {
    const { userProfileData } = authUser
    const { connectedDoctors, doctorsData, filterDoctorData, doctorDetails } = doctorReducer
    const { specialityList } = specialityReducer
    return { userProfileData, connectedDoctors, doctorsData, specialityList, filterDoctorData, doctorDetails }
}

export default connect(mapStateToProps, { getuserProfileData, connectedDoctorsData, getAllDoctorsData, searchDoctors, getSpecialities, filterDoctors, getDoctorDetail })(withStyles(styles)(PharmacyDoctor));