import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { ThemeProvider } from "@material-ui/styles";
import { createMuiTheme } from "@material-ui/core/styles";
import SearchBar from 'material-ui-search-bar'
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import AppBar from '@material-ui/core/AppBar';
import SideBar from '../../components/userSideBar'
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import { getuserProfileData, filterInventory, getInventoriesData } from '../../actions';
import { connect } from 'react-redux';
import Switch from '@material-ui/core/Switch';
import axios from '../../constant/axios';
import { NotificationManager } from 'react-notifications';
import Button from '@material-ui/core/Button';
import FilterComponent from '../../components/FilterComponent';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import {
    Col,
    Row
} from 'reactstrap';
import AppConfig from '../../constant/config';

const drawerWidth = 240;
const styles = theme => ({
    root: {
        flexGrow: 1,
        backgroundColor: '#f5f5f5 !important',
    },
    root2: {
        flexGrow: 1,
        backgroundColor: '#fff'
    },
    appBar: {
        marginLeft: drawerWidth,
        [theme.breakpoints.up('sm')]: {
            width: `calc(100% - ${drawerWidth}px)`,
        },
    },
    menuButton: {
        marginRight: 20,
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
    tabsRoot: {
        borderBottom: '1px solid #454545',
    },
    tabsIndicator: {
        backgroundColor: '#58de45',
        height: '4px'
    },
    listitems: {
        paddingLeft: 40,
        paddingRight: 10,
    },
    tabRoot: {
        textTransform: 'initial',
        minWidth: '50%',
        fontWeight: theme.typography.fontWeightRegular,
        fontFamily: [
            '-apple-system',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ].join(','),
    },
    tabSelected: {},
    typography: {
        padding: '15px 0px',
        color: '#CCCACD',
        ['@media (max-width:787px)']: {
            padding: '15px 15px',
        },
    },
    avatar: {
        margin: theme.spacing.unit * 2,
        marginLeft: theme.spacing.unit * 4,
        width: 70,
        height: 70
    },
    profile: {
        marginTop: theme.spacing.unit * 4,
        lineHeight: 1
    },
    tableDiv: {
        padding: '5px',
        flexWrap: 'unset !important'
    },
    tabsFlex: {
        borderBottom: '1px solid #454545',
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: '#ffffff',
        '&:hover': {
            backgroundColor: '#ffffff',
        },
        marginRight: 0,
        marginLeft: theme.spacing(2),
        marginTop: theme.spacing(1),
        width: '100%',
        borderRadius: '40px',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(3),
            width: 'auto',
        },
    },
    searchIcon: {
        width: theme.spacing(7),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        fontWeight: 'bold',
        opacity: 1
    },
    inputRoot: {
        color: '#c6c6c6',
    },
    patName: {
        color: '#FFFFFF',
        font: 'bolder'
    },
    inputInput: {
        color: '#c6c6c6',
        padding: theme.spacing(1, 1, 1, 7),
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: 200,
        },
    },
    listdivide: {
        marginLeft: '12px',
        marginRight: '12px',
        borderBottom: '1px solid #eeeeee'
    },
    dailogContentText: {
        height: 300,
        width: 250
    }
});

class Inventory extends Component {
    constructor(props) {
        super(props)
        console.log('hey  i m here', this.props.location)
        this.state = {
            checkedA: true,
            checkedB: true,
            value: '',
            isLoading: true,
            mobileOpen: false,
            inventories: [],
            searchText: '',
            selectedInventory: [],
            open: false,
            scroll: 'paper',
            filterStateValue: null,
            connect: null
        };
        this.filterRef = React.createRef();
    }

    // *snip*
    componentWillMount() {
        this.props.getuserProfileData({ history: this.props.history });
        this.props.getInventoriesData({ history: this.props.history })
        axios.get('/users/inventory', {
            headers: {
                'Content-Type': 'application/json',
                'token': localStorage.getItem('token')
            }
        }).then(result => {
            console.log("inventories result###", result.data.detail);
            console.log('this.props.userProfileData._id', this.props.userProfileData._id);
            let array = []
            if (result.data.detail.length > 0) {
                result.data.detail.map((data, index) => {
                    console.log('dattaa', data.pharmacy);
                    console.log('indexxxxx', data.pharmacy.length == 0 ? '' : data.pharmacy.findIndex((e) => (e._id) == this.props.userProfileData._id));
                    data.pharmacy.length == 0 ? array[index] = { inventoryId: data._id, isAvailable: false } : data.pharmacy.findIndex((e) => (e._id) == this.props.userProfileData._id) > -1 ? array[index] = { inventoryId: data._id, isAvailable: true } : array[index] = { inventoryId: data._id, isAvailable: false }
                })
                this.setState({
                    inventories: result.data.detail && result.data.detail.length > 0 ? result.data.detail : [], selectedInventory: array.length > 0 ? array : Array(result.data.count).fill("")
                })
            } else {
                this.setState({
                    inventories: result.data.detail && result.data.detail.length > 0 ? result.data.detail : [], selectedInventory: array.length > 0 ? array : Array(result.data.count).fill("")
                })
            }
        })
            .catch(error => {
                console.log("error....", error);
            });
    }

    handleOnClick = (e) => {
        e.preventDefault();
        this.props.history.push(`/${localStorage.getItem('role')}/inventoryDetails`)
    }

    handleApplyFilter() {
        let data = this.filterRef.current.state.single;
        data.id = this.props.userProfileData._id
        console.log('asdasdasdasdsad', this.filterRef.current.state.single.value)
        this.setState({ open: false, filterStateValue: this.filterRef.current.state.single });
        axios.get(`/users/sortByInventoryCategory/${data.value}/${this.props.userProfileData._id}`, {
            headers: {
                'Content-Type': 'application/json',
                'token': localStorage.getItem('token')
            }
        }).then(result => {
            console.log("inventories result###", result.data.detail);
            console.log('this.props.userProfileData._id', this.props.userProfileData._id);
            let array = []
            if (result.data.detail.length > 0) {
                result.data.detail.map((data, index) => {
                    console.log('dattaa', data.pharmacy.length, this.props.userProfileData._id);
                    console.log('indexxxxx', data.pharmacy.length == 0 ? '' : data.pharmacy.findIndex((e) => (e) == this.props.userProfileData._id));
                    data.pharmacy.length == 0 ? array[index] = { inventoryId: data._id, isAvailable: false } : data.pharmacy.findIndex((e) => (e) == this.props.userProfileData._id) > -1 ? array[index] = { inventoryId: data._id, isAvailable: true } : array[index] = { inventoryId: data._id, isAvailable: false }
                })
                console.log('array>>>>>>', array);
                this.setState({
                    inventories: result.data.detail && result.data.detail.length > 0 ? result.data.detail : [], selectedInventory: array.length > 0 ? array : Array(result.data.count).fill("")
                })
            } else {
                this.setState({
                    inventories: result.data.detail && result.data.detail.length > 0 ? result.data.detail : [], selectedInventory: array.length > 0 ? array : Array(result.data.count).fill("")
                })
            }
        })
            .catch(error => {
                console.log("error....", error);
            });
    }

    handleCancelFilter(e) {
        e.preventDefault();
        this.setState({ open: false });
    }

    handleClearFilter(e) {
        e.preventDefault();
        axios.get('/users/inventory', {
            headers: {
                'Content-Type': 'application/json',
                'token': localStorage.getItem('token')
            }
        }).then(result => {
            console.log("inventories result###", result.data.detail);
            console.log('this.props.userProfileData._id', this.props.userProfileData._id);
            let array = []
            if (result.data.detail.length > 0) {
                result.data.detail.map((data, index) => {
                    console.log('dattaa', data.pharmacy);
                    console.log('indexxxxx', data.pharmacy.length == 0 ? '' : data.pharmacy.findIndex((e) => (e._id) == this.props.userProfileData._id));
                    data.pharmacy.length == 0 ? array[index] = { inventoryId: data._id, isAvailable: false } : data.pharmacy.findIndex((e) => (e._id) == this.props.userProfileData._id) > -1 ? array[index] = { inventoryId: data._id, isAvailable: true } : array[index] = { inventoryId: data._id, isAvailable: false }
                })


                this.setState({
                    inventories: result.data.detail && result.data.detail.length > 0 ? result.data.detail : [], selectedInventory: array.length > 0 ? array : Array(result.data.count).fill("")
                })
            } else {
                this.setState({
                    inventories: result.data.detail && result.data.detail.length > 0 ? result.data.detail : [], selectedInventory: array.length > 0 ? array : Array(result.data.count).fill("")
                })
            }
        })
            .catch(error => {
                console.log("error....", error);
            });
        this.setState({ open: false, filterStateValue: null });
    }


    handleModalOpen(e) {
        e.preventDefault();
        this.setState({ open: true });
    }

    searchResults = (e, searchText, id) => {
        console.log('resultssss');
        let data = {
            searchText: searchText
        }
        axios.post(`/users/searchInventory/${id}`, data, {
            headers: {
                'Content-Type': 'application/json',
                'token': localStorage.getItem('token')
            }
        }
        ).then(result => {
            console.log('result result', result);
            this.setState({ inventories: result.data.detail ? result.data.detail : '' })
        })
            .catch(error => {
                console.log('error', error);
            });
    }

    handleSearchText = (e, key, id) => {
        console.log('e searchText key', e);
        this.setState({ [key]: e })
        let data = {
            searchText: e
        }
        axios.post(`/users/searchInventory/${id}`, data, {
            headers: {
                'Content-Type': 'application/json',
                'token': localStorage.getItem('token')
            }
        }
        ).then(result => {
            console.log('result result', result);
            this.setState({ inventories: result.data.detail ? result.data.detail : '' })
        })
            .catch(error => {
                console.log('error', error);
            });
    }
    /* 
       
     */

    handleChange = (e, index) => {
        console.log("e.target.value", e.target.checked, index)
        let tempSelInv = [...this.state.selectedInventory];
        console.log('tempSelInv', tempSelInv, index);
        let temp = { inventoryId: e.target.value, isAvailable: e.target.checked }
        tempSelInv[index] = temp;
        this.setState({ selectedInventory: tempSelInv })
        let data = {
            selectedInventory: tempSelInv
        }
        axios.post('/users/updateInventory', data, {
            headers: {
                'Content-Type': 'application/json',
                'token': localStorage.getItem('token')
            }
        }
        ).then(result => {
            console.log('result result', result);
            NotificationManager.success(result.data.title)
        })
            .catch(error => {
                NotificationManager.error('Something went wrong please try again.')
            });
    };

    handleDrawerToggle = () => {
        this.setState(state => ({ mobileOpen: !state.mobileOpen }));
    };


    render() {
        const { classes, activeStep, userProfileData, children, inventoryData } = this.props;
        const { inventories, selectedInventory, searchText, open } = this.state;
        console.log('inventoryData', inventoryData)
        console.log('selectedInventory', selectedInventory);
        const theme = createMuiTheme({
            palette: {
                primary: { main: '#43415e' },
                secondary: { main: '#43415e' },
            },
            overrides: {
                MuiInputLabel: {
                    root: {
                        color: "#43415e",
                        "&$focused": {
                            color: "#43415e"
                        }
                    }
                },
                MuiInputBase: {
                    root: {
                        color: "#43415e",
                        "&$focused": {
                            color: "#43415e"
                        }
                    }
                },
                MuiFormLabel: {
                    root: {
                        color: "#43415e",
                        "&$focused": {
                            color: "#43415e"
                        }
                    }
                },
                MuiInput: {
                    root: {
                        borderColor: '#fff'
                    },
                    underline: {
                        color: '#43415e',
                        "&$focused": {
                            color: "#43415e",
                        },
                        "&:before": {
                            color: "#43415e",
                            borderBottom: '1px solid rgba(231, 231, 231, 1)'
                        },
                        borderBottom: '1px solid rgba(231, 231, 231, 1)'
                    }
                }
            }
        });

        return (
            <React.Fragment>
                <AppBar position="fixed" className={classes.appBar}>
                    <Toolbar>
                        <IconButton
                            color="inherit"
                            aria-label="Open drawer"
                            onClick={this.handleDrawerToggle}
                            className={classes.menuButton} >
                            <MenuIcon />
                        </IconButton>
                        <Typography variant="h6" color="inherit" style={{ fontWeight: 'bold', color: '#fff' }} noWrap>
                            {"Inventory"}
                        </Typography>
                    </Toolbar>
                </AppBar>
                <div className='page-content' style={{ fontWeight: 'bold', backgroundColor: '#fff', marginTop: '-56px' }}>
                    <SideBar mobileOpen={this.state.mobileOpen} handleDrawerToggle={this.handleDrawerToggle} userProfileData={userProfileData} >
                        {children}
                    </SideBar>
                </div>
                <div className={classes.root}>
                    {/*  <div className={classes.tableDiv} >
                        <Typography className={classes.typography}>
                            <div className={classes.search}>
                                {<SearchBar value={searchText} onRequestSearch={(e) => this.searchResults(e, searchText, userProfileData._id)} onChange={(e) => this.handleSearchText(e, 'searchText')} style={{ borderRadius: '40px', height: '40px', fontWeight: 'bold', boxShadow: 'none', marginBottom: '8px' }} />}
                            </div>
                        </Typography>
                    </div> */}
                    <Row className={classes.tableDiv} >
                        <Col className={classes.search} sm={10} xs={10} md={10} lg={11} xl={11}>
                            <SearchBar value={searchText} onRequestSearch={(e) => this.searchResults(e, searchText, userProfileData._id)} onChange={(e) => this.handleSearchText(e, 'searchText', userProfileData._id)} style={{ borderRadius: '40px', height: '40px', fontWeight: 'bold', boxShadow: 'none', marginBottom: '8px' }} />
                        </Col>
                        <Col className={'filter'} sm={2} md={2} xs={2} lg={1} xl={1}>
                            <i className="fa fa-filter" aria-hidden="true" onClick={(e) => this.handleModalOpen(e)}></i>
                        </Col>
                    </Row>
                    <ThemeProvider theme={theme}>
                        <div key={'List'} className={classes.tableDiv}>
                            <List className={classes.root2} >
                                {
                                    inventories && inventories.length > 0 ?
                                        inventories.map((data, index) => {
                                            console.log('dataaaa', data);
                                            return <ListItem key={index} className={classes.listitems} >
                                                <ListItemText primary={
                                                    <div>
                                                        <Typography style={{ color: AppConfig.colors.primary, fontWeight: 'bold', fontSize: '16px', display: 'inline-block' }}>{data && data.name ? data.name : ''}</Typography>
                                                        <Typography style={{ color: AppConfig.colors.grey, fontWeight: 'bold', fontSize: '12px', display: 'inline-block', marginLeft: '5px' }}>({data && data.category ? data.category : ''})</Typography>
                                                    </div>
                                                } />
                                                <ListItemText style={{
                                                    textAlign: 'right',
                                                    right: '20px', marginTop: '-1px'
                                                }} primary={<Switch
                                                    color="primary"
                                                    checked={this.state.selectedInventory[index].isAvailable}
                                                    onChange={(e) => { this.handleChange(e, index) }}
                                                    value={data._id}
                                                    inputProps={{ 'aria-label': 'primary checkbox' }}
                                                />} />
                                            </ListItem>
                                        }) : <div><Typography style={{
                                            backgroundColor: '#fff', color: 'gray', fontWeight: 'bold', fontSize: '25px',
                                        }}>No Inventory</Typography></div>
                                }
                            </List>
                        </div>
                        <Dialog
                            open={open}
                            onClose={(e) => this.handleCancelFilter(e)}
                            scroll={this.state.scroll}
                            aria-labelledby="scroll-dialog-title"
                        >
                            <DialogTitle id="scroll-dialog-title">Filter</DialogTitle>
                            <DialogContent dividers={this.state.scroll === 'paper'} className={classes.dailogContentText}>
                                <FilterComponent ref={this.filterRef} filterFor={'Category'} inventory={inventoryData} filterStateValue={this.state.filterStateValue} />
                            </DialogContent>
                            <DialogActions>
                                <Button variant="contained" onClick={(e) => this.handleClearFilter(e)} color="primary">
                                    Remove Filter</Button>
                                <Button variant="contained" onClick={(e) => this.handleApplyFilter(e)} color="primary">
                                    Apply Filter</Button>
                            </DialogActions>
                        </Dialog>
                    </ThemeProvider>
                </div>
            </React.Fragment>);
    }
}

Inventory.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = ({ authUser, inventoryReducer }) => {
    const { userProfileData, profileImage } = authUser;
    const { inventoryData } = inventoryReducer;
    return { userProfileData, profileImage, inventoryData }
}

export default connect(mapStateToProps, { getuserProfileData, filterInventory, getInventoriesData })(withStyles(styles)(Inventory));