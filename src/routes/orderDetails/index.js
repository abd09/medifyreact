import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { Table, Col, Row, Input, FormGroup, Label, CustomInput, Container } from 'reactstrap';
import SideBar from '../../components/userSideBar'
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import AppBar from '@material-ui/core/AppBar';
import { ThemeProvider } from "@material-ui/styles";
import { createMuiTheme } from "@material-ui/core/styles";
import { red } from '@material-ui/core/colors';
import KeyboardArrowLeft from '@material-ui/icons/KeyboardArrowLeft';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import { getuserProfileData, updateOrder } from '../../actions';
import { connect } from 'react-redux';
import Chat from '@material-ui/icons/Chat';
import axios from '../../constant/axios';
import moment from 'moment';

const drawerWidth = 240;
const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    error: {
        color: 'red',
        textAlign: 'left'
    },
    appBar: {
        marginLeft: drawerWidth,
        [theme.breakpoints.up('sm')]: {
            width: `calc(100% - ${drawerWidth}px)`,
        },
    },
    menuButton: {
        marginRight: 20,
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
    dialogDone: {
        backgroundColor: '#454545',
        color: "#3ddb20",
        '&:hover': {
            background: 'none',
            backgroundColor: '#454545',
            color: '#3ddb20'
        },
        borderColor: '#454545'
    },
    dialogTitle: {
        backgroundColor: '#5a5a5a',
        '& h2': {
            color: 'white',
            textAlign: 'center'
        },
        borderBottom: '1px solid #5a5a5a'
    },
    dialogContent: {
        backgroundColor: '#5a5a5a',
    },
    root2: {
        flexGrow: 1,
        backgroundColor: '#fff'
    },
    tabsRoot: {
        color: '#42425E',
        borderBottom: '1px solid #7a7a7a',
    },
    tabsIndicator: {
        backgroundColor: '#7a7a7a',
        height: '3px'
    },
    rightDiv: {
        color: "#CCCACD",
        marginBottom: 0,
        fontWeight: 'bold',
        fontSize: '16px',
        textAlign: 'right',
    },
    listitems: {
        paddingLeft: 40,
        paddingRight: 10,
        paddingTop: 5,
        paddingBottom: 5
    },
    listitems1: {
        paddingLeft: 40,
        paddingRight: 10,
        paddingTop: 14,
        paddingBottom: 5
    },
    tabRoot: {
        textTransform: 'initial',
        minWidth: '50%',
        fontWeight: theme.typography.fontWeightRegular,
        fontFamily: [
            '-apple-system',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ].join(','),
    },
    typography: {
        padding: '15px 0px',
        color: '#CCCACD',
        ['@media (max-width:787px)']: {
            padding: '15px 15px',
        },
    },
    imageDiv: {
        right: '30px',
        textAlign: 'right',
        position: 'fixed',
        height: '70px',
        width: '70px',
    },
    avatar: {
        margin: theme.spacing.unit * 2,
        marginLeft: theme.spacing.unit * 4,
        width: 70,
        height: 70
    },
    profile: {
        marginTop: theme.spacing.unit * 4,
        lineHeight: 1
    },
    tableDiv: {
        padding: '0px',
    },
    tabsFlex: {
        borderBottom: '0px solid #454545',
    },
    tableThTd: {
        color: '#ffffff'
    },
    infoDiv: {
        padding: 18,
        backgroundColor: "#fff"
    },
    disableClick: {
        pointerEvents: 'none'
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: '#ffffff',
        '&:hover': {
            backgroundColor: '#ffffff',
        },
        marginRight: theme.spacing(2),
        marginLeft: 0,
        width: '100%',
        borderRadius: '40px',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(3),
            width: 'auto',
        },
    },
    searchIcon: {
        width: theme.spacing(7),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        fontWeight: 'bold',
        opacity: 1
    },
    inputRoot: {
        color: '#c6c6c6',
    },
    inputInput: {
        color: '#c6c6c6',
        padding: theme.spacing(1, 1, 1, 7),
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: 200,
        },
    },
    icon: {
        position: 'fixed',
        right: '10px',
        color: '#fff'
    },
    iconHover: {
        margin: theme.spacing(2),
        '&:hover': {
            color: red[800],
        },
    },
    avatar: {
        margin: 0,
        padding: 0
    },
    avatarlist: {
        margin: 0,
        padding: 0,
        width: 35,
        height: 35,
    },
    profile: {
        margin: 0,
        height: 100,
        lineHeight: 'normal'
    },
    signup: {
        backgroundColor: '#43415e',
        borderRadius: '0px',
        color: '#fff',
        marginTop: '85px',
        padding: '5px 22px',
        textTransform: 'none',
        marginRight: '8px',
        ['@media (max-width:707px)']: {
            marginRight: 'unset',
        },
        ['@media (max-width:599px)']: {
            marginRight: '8px',
        },
        ['@media (max-width:436px)']: {
            marginRight: 'unset',
        },
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        borderRadius: '8px',
        color: '#43415e'
    },
    dense: {
        marginTop: theme.spacing(2),
    },
    menu: {
        width: 200,
    },
    submit: {
        backgroundColor: '#43415e',
        color: '#fff',
        marginTop: theme.spacing.unit * 2,
        padding: '5px 12px',
        textTransform: 'none',
        borderRadius: '0',
        '&:hover': {
            border: '1px solid #2c2a4b',
            backgroundColor: '#2c2a4b'
        }
    },
    form: {
        width: '100%',
        paddingLeft: '20px',
        paddingRight: '35px',
        marginTop: theme.spacing.unit,
    },
    submit2: {
        padding: '0',
        backgroundColor: '#fff',
        color: '#43415e',
        border: 'none',
        marginTop: theme.spacing(2),
        textTransform: 'none',
        fontWeight: 'bolder',
        borderRadius: '0',
        '&:active': {
            boxShadow: 'none'
        },
        '&:hover': {
            backgroundColor: '#ffffff',
            color: '#43415e',
        },
    },
});

class OrderDetails extends Component {
    constructor(props) {
        super(props)
        console.log('hey  i m here', this.props.location)
        this.state = {
            orderDetails: '',
            mobileOpen: false,
            anchorEl: null,
            fullWidth: false,
            maxWidth: 'md',
            uploaded_image_path: '',
            value: 0,
            openModal: false,
            isLoading: true,
            imageArray: [],
            imagesArray: [],
            url: '',
            amount: '',
            time: '',
            errors: {},
            timeType: 'minutes',
            buttonType: 0,
            reason: ''
        };
        this.handleValidation = this.handleValidation.bind(this);
    }

    handleValidation = () => {
        let errors = {};
        let formIsValid = true;
        console.log("this.state handleValidation", this.state)
        if (this.state.amount === '') {
            formIsValid = false;
            errors["amount"] = "This field is required";
        }

        if (this.state.time === '') {
            formIsValid = false;
            errors["time"] = "This field is required";
        }
        console.log('parseInt(this.state.time)', parseInt(this.state.time));

        if (this.state.amount !== '' && parseInt(this.state.amount) < 1) {
            formIsValid = false;
            errors["amount"] = "Amount can't be less than 1 rupees.";
        }
        if (this.state.time !== '' && parseInt(this.state.time) < 1) {
            formIsValid = false;
            errors["time"] = "Time can't be less than 1 minute / hours.";
        }
        this.setState({ errors: errors });
        return formIsValid;
    }

    handleChat = (e, id) => {
        let data = {
            userOne: this.props.userProfileData._id,
            userTwo: id
        }
        axios.post(`/users/connectChat`, data, {
            headers: {
                'Content-Type': 'application/json',
                'token': localStorage.getItem('token')
            }
        }).then(result => {
            console.log("pharmacyDetails result^^^ pharmacy", result.data);
            if (result.data.error === false) {
                this.props.history.push(`/chat/${result.data.detail[0]._id}/${id}`)
            } else {
                this.props.history.push(`/${localStorage.getItem('role')}`)
            }
        }).catch(error => {
            console.log("error....", error);
            this.props.history.push(`/${localStorage.getItem('role')}`)
        });
    }

    componentWillMount() {
        console.log('this.props.match.params.id', this.props.match.params.id);
        this.props.getuserProfileData({ history: this.props.history });
        axios.get(`/users/orderDetails/${this.props.match.params.id}`, {
            headers: {
                'Content-Type': 'application/json',
                'token': localStorage.getItem('token')
            }
        }).then(result => {
            console.log("result..sdasdasdsa..", result.data.detail);
            this.setState({ orderDetails: result.data.detail ? result.data.detail : {}, url: result.data.url })
        })
            .catch(error => {
                console.log("error....", error);
            });
    }

    handleChange = (event, key) => {
        console.log('key', key, event.target.value)
        this.setState({ [key]: event.target.value });
    }

    handleModal = (e, buttonType) => {
        e.preventDefault();
        this.setState({ openModal: true, buttonType: buttonType })
    }

    handleSubmit = async (e) => {
        e.preventDefault();
        console.log('handleSubmit', this.state.buttonType)
        let data = {
            bills: this.state.imagesArray,
            amount: this.state.amount,
            reason: this.state.reason,
            time: this.state.time + ' ' + this.state.timeType,
            status: this.state.buttonType === 0 ? 'accepted' : 'rejected',
            orderId: this.state.orderDetails && this.state.orderDetails != null && this.state.orderDetails._id ? this.state.orderDetails._id : null
        }
        let isValid = false;
        this.state.buttonType === 0 ? isValid = await this.handleValidation() : isValid = true
        if (isValid === true) {
            this.props.updateOrder({
                data,
                history: this.props.history
            })
            this.setState({ openModal: false, errors: {} });
        } else {
            return;
        }
    }

    handleDrawerToggle = () => {
        this.setState(state => ({ mobileOpen: !state.mobileOpen }));
    };

    handleModalClose = () => {
        this.setState({ openModal: false, errors: {} });
    }

    openImageUploader() {
        this.refs.imageUploader.click();
    }

    handleImageChange = name => event => {
        console.log('name=', name, event.target.files[0])
        let arr = [...this.state.imageArray];
        var imagearr = [...this.state.imagesArray];
        if (name === 'uploaded_image_path') {
            if (event.target.files[0]) {
                var file = event.target.files[0];
                var reader = new FileReader();
                var url = reader.readAsDataURL(file);
                reader.onloadend = function (e) {
                    console.log('reader.result', typeof reader.result);
                    imagearr.push(reader.result)
                    this.setState({
                        imageArray: arr,
                        imagesArray: imagearr,
                    });
                }.bind(this);
                arr.push(event.target.files[0].name)
                this.setState({
                    imageArray: arr,
                    imagesArray: imagearr,
                });
            }
        }
    };

    goBack = (e) => {
        console.log('e', e);
        e.preventDefault();
        console.log('this.props.history', this.props.history);
        this.props.history.goBack();
    }

    handleOnClick = (e, id) => {
        console.log('id', id);
        this.props.history.push(`/${localStorage.getItem('role')}/patientDetails/${id}`)
    }

    render() {
        const { classes, userProfileData, children } = this.props;
        const { url, openModal, orderDetails, imagesArray, imageArray } = this.state;
        const theme = createMuiTheme({
            palette: {
                primary: { main: '#43415e' },
                secondary: { main: '#43415e' },
            },
            overrides: {
                MuiInputLabel: {
                    root: {
                        color: "#43415e",
                        "&$focused": {
                            color: "#43415e"
                        }
                    }
                },
                MuiInputBase: {
                    root: {
                        color: "#43415e",
                        "&$focused": {
                            color: "#43415e"
                        }
                    }
                },
                MuiFormLabel: {
                    root: {
                        color: "#43415e",
                        "&$focused": {
                            color: "#43415e"
                        }
                    }
                },
                MuiInput: {
                    root: {
                        borderColor: '#fff'
                    },
                    underline: {
                        color: '#43415e',
                        "&$focused": {
                            color: "#43415e",
                        },
                        "&:before": {
                            color: "#43415e",
                            borderBottom: '1px solid rgba(231, 231, 231, 1)'
                        },
                        borderBottom: '1px solid rgba(231, 231, 231, 1)'
                    }
                },
                MuiButton: {
                    contained: {

                        boxShadow: 'none'
                    }
                }
            }
        });

        console.log('orderDetails,', orderDetails);
        console.log('imagesArray.....', imagesArray, imagesArray.length);
        return (
            <React.Fragment>
                <AppBar position="fixed" className={classes.appBar}>
                    <Toolbar>
                        <IconButton
                            color="inherit"
                            aria-label="Open drawer"
                            onClick={this.handleDrawerToggle}
                            className={classes.menuButton}>
                            <MenuIcon />
                        </IconButton>
                        <Typography variant="h6" color="inherit" style={{ fontWeight: 'bold', color: '#fff' }} noWrap>
                            {"Orders"}
                        </Typography>
                    </Toolbar>
                </AppBar>
                <div className='page-content' style={{ fontWeight: 'bold', backgroundColor: '#fff', marginTop: '-56px' }}>
                    <SideBar mobileOpen={this.state.mobileOpen} handleDrawerToggle={this.handleDrawerToggle} userProfileData={userProfileData} >
                        {children}
                    </SideBar>
                </div>
                <div className={classes.root}>
                    <ThemeProvider theme={theme}>
                        <div className={classes.infoDiv}>
                            <Row>
                                <Col className={classes.profile} xs="8" md="8" sm="8">
                                    <Typography key={"owner"} variant="body1" gutterBottom align="left" style={{ color: '#42425E', fontSize: '12px', marginBottom: 0, fontWeight: 'bold', }}>Order ID: {'119220'}
                                    </Typography>
                                    {localStorage.getItem('role') == 'user' ?
                                        <div>
                                            <Typography variant="body1" gutterBottom align="left" style={{ color: "#42425E", marginBottom: 0, fontWeight: 'bolder', fontSize: '16px' }}>{orderDetails && orderDetails != null && orderDetails.pharmacy && orderDetails.pharmacy != null && orderDetails.pharmacy.firstname && orderDetails.pharmacy.firstname != null ? orderDetails.pharmacy.firstname + ' ' + orderDetails.pharmacy.lastname : ''}
                                            </Typography>
                                            <Typography variant="body1" gutterBottom align="left" style={{ color: '#42425E', marginBottom: 0, fontWeight: '400', fontSize: '14px' }}>Location: {orderDetails && orderDetails != null && orderDetails.pharmacy && orderDetails.pharmacy != null && orderDetails.pharmacy.address && orderDetails.pharmacy.address != null ? orderDetails.pharmacy.address : ''}
                                            </Typography>
                                        </div>
                                        :
                                        <div>
                                            <Typography variant="body1" gutterBottom align="left" onClick={(e) => this.handleOnClick(e, orderDetails.patient._id)} style={{ color: "#42425E", marginBottom: 0, fontWeight: 'bolder', fontSize: '16px', cursor: 'pointer' }}>{orderDetails && orderDetails != null && orderDetails.patient && orderDetails.patient != null && orderDetails.patient.firstname && orderDetails.patient.firstname != null ? orderDetails.patient.firstname + ' ' + orderDetails.patient.lastname : ''}
                                            </Typography>
                                            <Typography variant="body1" gutterBottom align="left" style={{ color: '#42425E', marginBottom: 0, fontWeight: '400', fontSize: '14px' }}>Location: {orderDetails && orderDetails != null && orderDetails.patient && orderDetails.patient != null && orderDetails.patient.address && orderDetails.patient.address != null ? orderDetails.patient.address : ''}
                                            </Typography>
                                        </div>
                                    }
                                    {(orderDetails && orderDetails.prescription && orderDetails.prescription !== null && orderDetails.prescription.images && orderDetails.prescription.images != null && orderDetails.prescription.images.length > 0 ? <Typography key={"owner1"} variant="body1" gutterBottom align="left" style={{ color: '#42425E', marginTop: '10px', fontSize: '13px', marginBottom: 0, fontWeight: 'bold', }}>Prescription:</Typography> : '')}

                                </Col>
                                <Col xs="4" md="4" sm="4" style={{ textAlign: "right" }}>
                                    <div className={classes.rightDiv} onClick={(e) => this.handleChat(e, orderDetails.patient._id)}>
                                        {
                                            (orderDetails && orderDetails != null && orderDetails.patient && orderDetails.patient != null && orderDetails.patient.isChatEnabled === true) ? <React.Fragment><Typography style={{ color: '#42425E', fontWeight: 'bold', fontSize: '12px' }}><Chat /></Typography><Typography style={{ color: '#42425E', fontWeight: 'bold', fontSize: '12px' }}>Chat</Typography></React.Fragment> : ''

                                        }
                                    </div>
                                    <div className={classes.rightDiv} >
                                        <Typography variant="body1" gutterBottom style={{ color: '#42425E', marginBottom: 0, fontWeight: 'bold', fontSize: '14px' }}>{orderDetails && orderDetails != null && orderDetails.createdAt && orderDetails.createdAt != null ? moment(orderDetails.createdAt).format("D MMMM YYYY") : ''}
                                        </Typography>
                                    </div>
                                </Col>
                            </Row>
                        </div>

                        <div className={classes.form}>
                            {(orderDetails && orderDetails.prescription && orderDetails.prescription !== null && orderDetails.prescription.images && orderDetails.prescription.images != null && orderDetails.prescription.images.length > 0 ? orderDetails.prescription.images.map((data, index) => {
                                return data.indexOf('.pdf') !== -1 ? <a style={{ color: '#42425E', fontSize: '14px', textDecoration: 'underline' }} href={url + "prescriptions/" + data}>Download PDF</a> : <img key={index} style={{ width: '100%' }} src={url + "prescriptions/" + data} />
                            }) : '')}

                        </div>
                        <Col xs="12" md="12" sm="12" style={{ paddingBottom: '15px', marginTop: '8px' }}>
                            <Typography variant="body1" gutterBottom align="left" style={{ color: '#42425E', fontSize: '12px', marginBottom: 0, fontWeight: 'bold' }}>Prescription Title:</Typography>

                            <Typography variant="body1" gutterBottom align="left" style={{ color: '#42425E', fontSize: '12px', marginBottom: 0, fontWeight: '400' }}>
                                {orderDetails && orderDetails.prescription !== null && orderDetails.prescription.title && orderDetails.prescription.title != undefined && orderDetails.prescription.title != '' ? orderDetails.prescription.title : '--'}
                            </Typography>
                            <Typography variant="body1" gutterBottom align="left" style={{ color: '#42425E', fontSize: '12px', marginBottom: 0, fontWeight: 'bold', marginTop: '5px' }}>Prescription Note:</Typography>

                            <Typography variant="body1" gutterBottom align="left" style={{ color: '#42425E', fontSize: '12px', marginBottom: 0, fontWeight: '400' }}>
                                {orderDetails && orderDetails.prescription !== null && orderDetails.prescription.note && orderDetails.prescription.note != undefined && orderDetails.prescription.note != '' ? orderDetails.prescription.note : '--'}
                            </Typography>
                            <Typography variant="body1" gutterBottom align="left" style={{ color: '#42425E', fontSize: '12px', marginBottom: 0, fontWeight: 'bold', marginTop: '5px' }}>Prescription Details:</Typography>
                            {orderDetails && orderDetails.prescription !== null && orderDetails.prescription && orderDetails.prescription.otherDetails && orderDetails.prescription.otherDetails != null && orderDetails.prescription.otherDetails.length > 0 ? orderDetails.prescription.otherDetails.map((each, i) => {
                                return <div key={i}>
                                    <Row>
                                        <Col xs="6" sm="4">
                                            <div style={{ color: '#43415e', padding: 5, fontSize: '12px', float: 'left', fontWeight: 'bold', display: 'inline-block', paddingLeft: '0px', paddingTop: '6px' }}>{i + 1}. </div>  <Typography style={{ color: '#43415e', padding: 5, fontSize: '12px', float: 'left', fontWeight: 'bold', display: 'inline-block', paddingLeft: '0px' }}>Description: </Typography> <Typography style={{ color: '#43415e', padding: 5, fontSize: '12px', display: 'inline-block', float: 'left' }}> {each.description && each.description != '' ? each.description : '--'} </Typography><br></br>
                                        </Col>
                                        <Col xs="6" sm="4">
                                            <Typography style={{ color: '#43415e', padding: 5, fontSize: '12px', float: 'left', fontWeight: 'bold', display: 'inline-block', paddingLeft: '0px' }}>Duration: </Typography> <Typography style={{ color: '#43415e', padding: 5, fontSize: '12px', display: 'inline-block', float: 'left' }}> {each.duration && each.duration !== '' ? each.duration : '--'} </Typography>
                                        </Col>
                                        <Col xs="6" sm="4">
                                            <Typography style={{ color: '#43415e', padding: 5, fontSize: '12px', float: 'left', fontWeight: 'bold', display: 'inline-block', paddingLeft: '15px' }}>Quantity: </Typography> <Typography style={{ color: '#43415e', padding: 5, fontSize: '12px', display: 'inline-block', float: 'left' }}> {each.qty && each.qty != '' ? each.qty : '--'}</Typography>
                                        </Col>
                                    </Row>
                                    <Row>
                                        <Col xs="6" sm="4">
                                            <Typography style={{ color: '#43415e', padding: 5, fontSize: '12px', float: 'left', fontWeight: 'bold', display: 'inline-block', paddingLeft: '15px' }}>Order Quantity: </Typography> <Typography style={{ color: '#43415e', padding: 5, fontSize: '12px', display: 'inline-block', float: 'left' }}> {each.orderQty && each.orderQty != '' ? each.orderQty : '--'}</Typography>
                                        </Col>
                                        <Col xs="6" sm="4">
                                            <Typography style={{ color: '#43415e', padding: 5, fontSize: '12px', float: 'left', fontWeight: 'bold', display: 'inline-block', paddingLeft: '0px' }}>Times: </Typography>
                                            <Typography style={{ color: '#43415e', padding: 5, fontSize: '12px', float: 'left' }}> {each.other && each.other != null ? <div>
                                                <div> M: {each.other.M ? 'Yes' : 'No'},</div> <div>A: {each.other.A ? 'Yes' : 'No'},</div><div> N: {each.other.N ? 'Yes' : 'No'}</div>
                                            </div> : '--'} </Typography>
                                        </Col>
                                    </Row>
                                </div>;
                            }) : ''}

                            <Typography variant="body1" gutterBottom align="left" style={{ color: '#42425E', fontSize: '12px', marginBottom: 0, fontWeight: 'bold' }}>Note:</Typography>

                            <Typography variant="body1" gutterBottom align="left" style={{ color: '#42425E', fontSize: '12px', marginBottom: 0, fontWeight: '400', }}>
                                {orderDetails && orderDetails != null && orderDetails.note && orderDetails.note != null ? orderDetails.note : ''}
                            </Typography>

                            <Typography key={"owner354"} variant="body1" gutterBottom align="left" style={{ color: '#42425E', fontSize: '12px', fontWeight: 'bold', marginTop: '5px' }}>Add On:</Typography>
                            {(orderDetails && orderDetails.items && orderDetails.items !== null && orderDetails.items.length > 0 ? orderDetails.items.map((data, index) => {
                                return <div key={index} style={{ color: '#42425E', fontSize: '12px', fontWeight: '400', textAlign: 'left', }}>
                                    <Row>
                                        <Col xs="6" sm="4">
                                            <div style={{ color: '#43415e', fontSize: '12px', float: 'left', fontWeight: 'bold', display: 'inline-block', paddingTop: '1px' }}>{index + 1}. </div> <Typography style={{ color: '#43415e', fontSize: '12px', fontWeight: 'bold', display: 'inline-block', marginLeft: '5px' }}>Description: </Typography> <Typography style={{ color: '#43415e', fontSize: '12px', display: 'inline-block' }}>{data && data.name && data.name != null ? data.name : '--'}</Typography><br></br>
                                        </Col>
                                        <Col xs="6" sm="4">
                                            <Typography style={{ color: '#43415e', fontSize: '12px', fontWeight: 'bold', display: 'inline-block' }}>Quantity: </Typography> <Typography style={{ color: '#43415e', fontSize: '12px', display: 'inline-block' }}>{data && data.qty && data.qty != null ? data.qty : '--'}</Typography>
                                        </Col>
                                    </Row>
                                </div>
                            }) : '')}
                            {
                                orderDetails.status == 'accepted' || orderDetails.status == 'completed' ?
                                    <div style={{ marginTop: '5px' }}>
                                        <Typography variant="body1" gutterBottom align="left" style={{ color: '#42425E', fontSize: '12px', marginBottom: 0, fontWeight: 'bold' }}>Amount:</Typography>

                                        <Typography variant="body1" gutterBottom align="left" style={{ color: '#42425E', fontSize: '12px', marginBottom: 0, fontWeight: '400', }}>
                                            {orderDetails && orderDetails != null && orderDetails.amount && orderDetails.amount != null ? 'Rs. ' + orderDetails.amount : ''}
                                        </Typography>
                                        <Typography variant="body1" gutterBottom align="left" style={{ color: '#42425E', fontSize: '12px', marginBottom: 0, fontWeight: 'bold', marginTop: '5px' }}>Time:</Typography>

                                        <Typography variant="body1" gutterBottom align="left" style={{ color: '#42425E', fontSize: '12px', marginBottom: 0, fontWeight: '400' }}>
                                            {orderDetails && orderDetails != null && orderDetails.time && orderDetails.time != null ? orderDetails.time : ''}
                                        </Typography>
                                        <div className={classes.form}>
                                            {(orderDetails && orderDetails.bills && orderDetails.bills != null && orderDetails.bills.length > 0 ? orderDetails.bills.map((data, index) => {
                                                return data.indexOf('.pdf') !== -1 ? <a style={{ color: '#42425E', fontSize: '14px', textDecoration: 'underline' }} href={url + "orderBills/" + data}>Download Bill</a> : <img key={index} style={{ width: '100%' }} src={url + "orderBills/" + data} />
                                            }) : '')}

                                        </div>
                                    </div> : ''

                            }

                        </Col>
                        <Col md={6} sm={6} xs={6} style={{ textAlign: 'left', display: 'inline-block' }}>
                            <Button
                                type="button"
                                variant="contained"
                                className={classes.submit2}
                                onClick={(e) => this.goBack(e)}
                            ><KeyboardArrowLeft style={{ fontSize: '35px' }} />Back</Button>
                        </Col>
                        <Col md={6} sm={6} xs={6} style={{ textAlign: 'right', display: 'inline-block' }}>

                            {localStorage.getItem('role') == 'pharmacy' && orderDetails && orderDetails.status && orderDetails.status == 'requested' ?
                                <Button
                                    type="submit"
                                    variant="contained" style={{ textAlign: 'right', borderRadius: '6px', margin: 5 }}
                                    className={classes.submit} onClick={(e) => this.handleModal(e, 0)}
                                >Confirm</Button> : ''}
                            {localStorage.getItem('role') == 'pharmacy' && orderDetails && orderDetails.status && orderDetails.status == 'requested' ?
                                <Button
                                    type="submit"
                                    variant="contained" style={{ textAlign: 'right', borderRadius: '6px', margin: 5 }}
                                    className={classes.submit} onClick={(e) => this.handleModal(e, 1)}
                                >Reject</Button> : ''}

                        </Col>
                        <Dialog
                            fullWidth={this.state.fullWidth}
                            maxWidth={this.state.maxWidth}
                            open={openModal}
                            onClose={this.handleModalClose}
                            aria-labelledby="max-width-dialog-title" >
                            <DialogContent>
                                <form className={classes.form} onSubmit={(e) => this.handleSubmit(e)}>
                                    <div >
                                        {
                                            this.state.buttonType === 0 ? <React.Fragment>
                                                <FormGroup className={classes.lable}>
                                                    <Input type="number" name="amount" onChange={(e) => this.handleChange(e, 'amount')} id="amount" placeholder="Enter Total Cost" min='1' />
                                                    <span className={classes.error} >{this.state.errors["amount"]}</span>
                                                </FormGroup>
                                                <Row>
                                                    <Col xs={12} sm={7} md={7} lg={7} xl={7} >
                                                        <FormGroup className={classes.lable}>
                                                            <Input type="number" name="time" onChange={(e) => this.handleChange(e, 'time')} min='1' id="time" placeholder="Enter Estimated Time" />
                                                        </FormGroup>
                                                    </Col>
                                                    <Col xs={12} sm={5} md={5} lg={5} xl={5}>
                                                        <FormGroup className={classes.lable} style={{ marginBottom: '0px' }}>
                                                            <Input type="select" defaultValue={this.state.timeType} value={this.state.timeType} name="timeType" id="timeType" onChange={(e) => this.handleChange(e, 'timeType')} >
                                                                <option value="" >Select Type</option>
                                                                <option value={'minutes'}>{'Minutes'}</option>
                                                                <option value={'hours'}>{'Hours'}</option>
                                                            </Input>
                                                        </FormGroup>
                                                    </Col>
                                                </Row>
                                                <FormGroup className={classes.lable}>
                                                    <span className={classes.error} >{this.state.errors["time"]}</span></FormGroup>
                                                {
                                                    (this.state.imageArray.length > 0) ?
                                                        this.state.imageArray.map((image, key) =>
                                                            <FormGroup key={key} className={classes.lable}>
                                                                <Input id="product_image" type="text" name="image" value=
                                                                    {
                                                                        image ? image : ''} disabled />
                                                            </FormGroup>
                                                        ) : ''
                                                }
                                            </React.Fragment> : <React.Fragment>
                                                    <FormGroup className={classes.lable}>
                                                        <Input type="textarea" name="reason" onChange={(e) => this.handleChange(e, 'reason')} min='1' id="reason" placeholder="Please enter reason here..." />
                                                    </FormGroup>
                                                </React.Fragment>
                                        }
                                        <div style={{ textAlign: 'center' }}>
                                            <Row>
                                                {
                                                    this.state.buttonType === 0 ? <Col style={{ textAlign: 'center' }}>
                                                        <input type="file" id="uploaded_image_path" ref="imageUploader" name="uploaded_image_path" onChange={this.handleImageChange('uploaded_image_path')} style={{ display: 'none' }} />
                                                        <Button variant="outlined" style={{ padding: '1px 6px', textAlign: 'center', borderRadius: '6px' }} onClick={() => this.openImageUploader()} type="button" className={classes.submit} color="primary">
                                                            Add Bill</Button>
                                                    </Col> : ''
                                                }
                                                <Col style={{ textAlign: 'center' }}>
                                                    <Button type="submit" style={{ padding: '1px 6px', textAlign: 'center', borderRadius: '6px' }} className={classes.submit} variant="outlined" color="secondary">
                                                        Confirm</Button>
                                                </Col>
                                            </Row>
                                        </div>
                                    </div>
                                </form>
                            </DialogContent>
                        </Dialog>
                    </ThemeProvider>
                </div>
            </React.Fragment >);
    }
}

OrderDetails.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = ({ authUser }) => {
    const { userProfileData } = authUser
    return { userProfileData }
}

export default connect(mapStateToProps, { getuserProfileData, updateOrder })(withStyles(styles)(OrderDetails));