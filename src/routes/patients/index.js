import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import SearchBar from 'material-ui-search-bar'
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import AppBar from '@material-ui/core/AppBar';
import SideBar from '../../components/userSideBar'
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import AppConfig from '../../constant/config';
import Toolbar from '@material-ui/core/Toolbar';
import { getuserProfileData, getDoctorPatientsData, searchDoctorPatients } from '../../actions';
import { connect } from 'react-redux';
import Chat from '@material-ui/icons/Chat';
import { Col, Row } from 'reactstrap';
import axios from '../../constant/axios';
import moment from 'moment';

const drawerWidth = 240;
const styles = theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: '#f5f5f5 !important',
  },
  root2: {
    flexGrow: 1,
    backgroundColor: '#fff'
  },
  appBar: {
    marginLeft: drawerWidth,
    [theme.breakpoints.up('sm')]: {
      width: `calc(100% - ${drawerWidth}px)`,
    },
  },
  menuButton: {
    marginRight: 20,
    [theme.breakpoints.up('sm')]: {
      display: 'none',
    },
  },
  tabsRoot: {
    borderBottom: '1px solid #454545',
  },
  tabsIndicator: {
    backgroundColor: '#58de45',
    height: '4px'
  },
  listitems: {
    paddingLeft: 40,
    paddingRight: 20,
  },
  tabRoot: {
    textTransform: 'initial',
    minWidth: '50%',
    fontWeight: theme.typography.fontWeightRegular,
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
  },
  tabSelected: {},
  typography: {
    padding: '15px 0px',
    color: '#CCCACD',
    ['@media (max-width:787px)']: {
      padding: '15px 15px',
    },
  },
  avatar: {
    margin: theme.spacing.unit * 2,
    marginLeft: theme.spacing.unit * 4,
    width: 70,
    height: 70
  },
  profile: {
    marginTop: theme.spacing.unit * 4,
    lineHeight: 1
  },
  tableDiv: {
    padding: '0px',
  },
  tabsFlex: {
    borderBottom: '1px solid #454545',
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: '#ffffff',
    '&:hover': {
      backgroundColor: '#ffffff',
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: '100%',
    borderRadius: '40px',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(3),
      width: 'auto',
    },
  },
  searchIcon: {
    width: theme.spacing(7),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    fontWeight: 'bold',
    opacity: 1
  },
  inputRoot: {
    color: '#c6c6c6',
  },
  patName: {
    color: '#FFFFFF',
    font: 'bolder'
  },
  inputInput: {
    color: '#c6c6c6',
    padding: theme.spacing(1, 1, 1, 7),
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: 200,
    },
  },
  listdivide: {
    marginLeft: '12px',
    marginRight: '12px',
    borderBottom: '1px solid #eeeeee'
  },
});

class Patients extends Component {
  constructor(props) {
    super(props)
    console.log('hey  i m here', this.props.location)
    this.state = {
      mobileOpen: false,
      value: '',
      isLoading: true,
      patients: [],
      searchText: ''
    };
  }

  componentWillMount() {
    this.props.getuserProfileData({ history: this.props.history });
    this.props.getDoctorPatientsData({
      history: this.props.history
    });
  }

  handleOnClick = (e, id) => {
    console.log('id', id);
    e.preventDefault();
    this.props.history.push(`/${localStorage.getItem('role')}/patientDetails/${id}`)
  }

  handleChange = (event, value) => {
    console.log('value', value)
    this.setState({ value });
  };

  handleChat = (e, id) => {
    let data = {
      userOne: this.props.userProfileData._id,
      userTwo: id
    }
    axios.post(`/users/connectChat`, data, {
      headers: {
        'Content-Type': 'application/json',
        'token': localStorage.getItem('token')
      }
    }).then(result => {
      console.log("pharmacyDetails result^^^ pharmacy", result.data);
      if (result.data.error === false) {
        this.props.history.push(`/chat/${result.data.detail[0]._id}/${id}`)
      } else {
        this.props.history.push(`/${localStorage.getItem('role')}`)
      }
    }).catch(error => {
      console.log("error....", error);
      this.props.history.push(`/${localStorage.getItem('role')}`)
    });
  }

  handleSearchText = (e, key) => {
    console.log('e searchText key', e);
    this.setState({ [key]: e })
    if (e.length > 0) {
      let data = {
        searchText: e
      }
      this.props.searchDoctorPatients({
        data: data, history: this.props.history
      });
    } else {
      this.props.getDoctorPatientsData({
        history: this.props.history
      });
    }
  }

  searchResults = (e, searchText) => {
    console.log('resultssss');
    if (searchText.length > 0) {
      let data = {
        searchText: searchText
      }
      this.props.searchDoctorPatients({
        data: data, history: this.props.history
      });

    } else {
      this.props.getDoctorPatientsData({
        history: this.props.history
      });
    }
  }

  handleDrawerToggle = () => {
    this.setState(state => ({ mobileOpen: !state.mobileOpen }));
  };

  render() {
    const { classes, userProfileData, children, doctorPatientData } = this.props;
    const { searchText } = this.state;
    console.log('doctorPatientData', doctorPatientData)

    return (
      <React.Fragment>
        <AppBar position="fixed" className={classes.appBar}>
          <Toolbar>
            <IconButton
              color="inherit"
              aria-label="Open drawer"
              onClick={this.handleDrawerToggle}
              className={classes.menuButton}
            >
              <MenuIcon />
            </IconButton>
            <Typography variant="h6" color="inherit" style={{ fontWeight: 'bold', color: '#fff' }} noWrap>
              {"Patients"}
            </Typography>
          </Toolbar>
        </AppBar>
        <div className='page-content' style={{ fontWeight: 'bold', backgroundColor: '#fff', marginTop: '-56px' }}>
          <SideBar mobileOpen={this.state.mobileOpen} handleDrawerToggle={this.handleDrawerToggle} userProfileData={userProfileData} >
            {children}
          </SideBar>
        </div>
        <div className={classes.root}>
          <div className={classes.tableDiv} >
            <Typography className={classes.typography}>
              {<div className={classes.search}>
                {<SearchBar value={searchText} onRequestSearch={(e) => this.searchResults(e, searchText)} onChange={(e) => this.handleSearchText(e, 'searchText')} style={{ borderRadius: '40px', height: '40px', fontWeight: 'bold', boxShadow: 'none', marginBottom: '8px' }} />}
              </div>}
            </Typography>
          </div>
          <div key={'List'} className={classes.tableDiv}>
            <List className={classes.root2} >
              {
                doctorPatientData && doctorPatientData.length > 0 ?
                  doctorPatientData.map((data, index) => {
                    console.log('dataaaa', data);
                    return <div>
                      <ListItem className={classes.listitems} >
                        <Col sm={8} md={8} lg={8} xs={8} onClick={(e) => data && data.patients && data.patients != null ? this.handleOnClick(e, data.patients) : this.handleOnClick(e, '')}>
                          <ListItemText primary={<Typography style={{ color: AppConfig.colors.primary, fontWeight: 'bold', fontSize: '12px' }}>ID: {data && data.patientDetails && data.patientDetails.userId}</Typography>} secondary={<React.Fragment>
                            <Typography variant="body1" style={{ color: AppConfig.colors.primary, fontWeight: 'bold', fontSize: '18px', padding: '5px 0 5px 0' }} className={classes.patName}>{data && data.patientDetails && data.patientDetails.firstname && data.patientDetails.firstname != '' ? data.patientDetails.firstname + ' ' + data.patientDetails.lastname : ''}</Typography>
                            <Typography style={{ color: AppConfig.colors.grey, fontWeight: 'bold', fontSize: '12px' }}>Age: {data && data.age && data.age !== '' ? `${data.age} years` : '--'}, Sex: {data && data.patientDetails && data.patientDetails.gender !== '' ? `${data.patientDetails.gender.charAt(0).toUpperCase()}${data.patientDetails.gender.slice(1)}` : '--'}</Typography>
                          </React.Fragment>} />
                        </Col>

                        <Col sm={4} md={4} lg={4} xs={4} onClick={(e) => this.handleChat(e, data.patientDetails._id)}>
                          {
                            (data.patientDetails !== null && data.patientDetails.isChatEnabled === true) ? <div style={{ float: 'right' }}>
                              <Typography style={{ color: AppConfig.colors.primary, fontWeight: 'bold', fontSize: '12px' }}><Chat /></Typography>
                              <Typography style={{ color: AppConfig.colors.primary, fontWeight: 'bold', fontSize: '12px' }}>Chat</Typography>
                            </div> : ''
                          }
                        </Col>

                      </ListItem>
                      <li className={classes.listdivide}></li>
                    </div>
                  }) : <div><Typography style={{
                    backgroundColor: '#fff', color: 'gray', fontWeight: 'bold', fontSize: '25px',
                  }}>No Patients</Typography></div>
              }
            </List>
          </div>
        </div>
      </React.Fragment>);
  }
}

Patients.propTypes = {
  classes: PropTypes.object.isRequired,
};

const mapStateToProps = ({ authUser, doctorReducer }) => {
  const { userProfileData } = authUser
  const { doctorPatientData } = doctorReducer
  return { userProfileData, doctorPatientData }
}

export default connect(mapStateToProps, { getuserProfileData, getDoctorPatientsData, searchDoctorPatients })(withStyles(styles)(Patients));