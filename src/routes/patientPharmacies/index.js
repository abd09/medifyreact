import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import SearchBar from 'material-ui-search-bar'
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import AppBar from '@material-ui/core/AppBar';
import AppConfig from '../../constant/config';
import SideBar from '../../components/userSideBar'
import IconButton from '@material-ui/core/IconButton';
import Button from '@material-ui/core/Button';
import MenuIcon from '@material-ui/icons/Menu';
import CallIcon from '@material-ui/icons/CallEnd';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import DialogActions from '@material-ui/core/DialogActions';
import Toolbar from '@material-ui/core/Toolbar';
import ListItemText from '@material-ui/core/ListItemText';
import { getuserProfileData, searchPharmacyData, getPharmacyData } from '../../actions';
import { connect } from 'react-redux';
import axios from '../../constant/axios';
import Chat from '@material-ui/icons/Chat';
import { Col, Row } from 'reactstrap';
import {
    isBrowser,
    isMobile
} from "react-device-detect";
const drawerWidth = 240;
const styles = theme => ({
    root: {
        flexGrow: 1,
        backgroundColor: '#f5f5f5 !important',
    },
    root2: {
        flexGrow: 1,
        backgroundColor: '#fff'
    },
    appBar: {
        marginLeft: drawerWidth,
        [theme.breakpoints.up('sm')]: {
            width: `calc(100% - ${drawerWidth}px)`,
        },
    },
    menuButton: {
        marginRight: 20,
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
    tabsRoot: {
        borderBottom: '1px solid #454545',
    },
    tabsIndicator: {
        backgroundColor: '#58de45',
        height: '4px'
    },
    listitems: {
        paddingLeft: 20,
        paddingRight: 10,
    },
    tabRoot: {
        textTransform: 'initial',
        minWidth: '50%',
        fontWeight: theme.typography.fontWeightRegular,
        fontFamily: [
            '-apple-system',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ].join(','),
    },
    tabSelected: {},
    typography: {
        padding: '15px 0px',
        color: '#CCCACD',
        ['@media (max-width:787px)']: {
            padding: '15px 15px',
        },
    },
    avatar: {
        margin: theme.spacing.unit * 2,
        marginLeft: theme.spacing.unit * 4,
        width: 70,
        height: 70
    },
    profile: {
        marginTop: theme.spacing.unit * 4,
        lineHeight: 1
    },
    tableDiv: {
        padding: '0px',
    },
    tabsFlex: {
        borderBottom: '1px solid #454545',
    },
    dialogDone: {
        backgroundColor: AppConfig.colors.darkGrey,
        color: "#3ddb20",
        '&:hover': {
            background: 'none',
            backgroundColor: AppConfig.colors.darkGrey,
            color: '#3ddb20'
        },
        borderColor: AppConfig.colors.darkGrey
    },
    dialogTitle: {
        backgroundColor: '#5a5a5a',
        '& h2': {
            color: 'white',
            textAlign: 'center'
        },
        borderBottom: '1px solid #5a5a5a'
    },
    dialogContent: {
        backgroundColor: '#5a5a5a',
    },
    dailogContentText: {
        height: 300,
        width: 250
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: '#ffffff',
        '&:hover': {
            backgroundColor: '#ffffff',
        },
        marginRight: theme.spacing(2),
        marginLeft: 0,
        width: '100%',
        borderRadius: '40px',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(3),
            width: 'auto',
        },
    },
    searchIcon: {
        width: theme.spacing(7),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        fontWeight: 'bold',
        opacity: 1
    },
    inputRoot: {
        color: '#c6c6c6',
    },
    pharmacyName: {
        color: '#FFFFFF',
        font: 'bolder'
    },
    inputInput: {
        color: '#c6c6c6',
        padding: theme.spacing(1, 1, 1, 7),
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: 200,
        },
    },
    Listdivide: {
        marginLeft: '12px',
        marginRight: '12px',
        borderBottom: '1px solid #eeeeee'
    },
});

class Pharmacy extends Component {
    constructor(props) {
        super(props)
        console.log('hey  i m here', this.props.location)
        this.state = {
            mobileOpen: false,
            value: '',
            searchText: '',
            isLoading: true,
            connect: [],
            pharmacies: [],
            pharmacyDetails: ''
        };
    }

    componentWillMount() {
        this.setState({ searchText: '' })
        this.props.getuserProfileData({ history: this.props.history });
        if (this.state.searchText == '') {
            this.props.getPharmacyData({ history: this.props.history });
        }
        this.props.getPharmacyData({ history: this.props.history });
    }

    handleChange = (event, value) => {
        console.log('value', value)
        this.setState({ value });
    };

    handleChat = (e, id) => {
        let data = {
            userOne: this.props.userProfileData._id,
            userTwo: id
        }
        axios.post(`/users/connectChat`, data, {
            headers: {
                'Content-Type': 'application/json',
                'token': localStorage.getItem('token')
            }
        }).then(result => {
            console.log("pharmacyDetails result^^^ pharmacy", result.data);
            if (result.data.error === false) {
                this.props.history.push(`/chat/${result.data.detail[0]._id}/${id}`)
            } else {
                this.props.history.push(`/${localStorage.getItem('role')}`)
            }
        }).catch(error => {
            console.log("error....", error);
            this.props.history.push(`/${localStorage.getItem('role')}`)
        });
    }

    handleDrawerToggle = () => {
        this.setState(state => ({ mobileOpen: !state.mobileOpen }));
    };

    handleSearchText = (e, key) => {
        console.log('e searchText key', e);
        this.setState({ [key]: e })

        if (e.length > 0) {
            this.props.searchPharmacyData({ data: { searchText: e }, history: this.props.history });
        } else {
            this.props.getPharmacyData({ history: this.props.history });
        }

    }
    handleModal = (e, isBrowser, pharId) => {
        e.preventDefault();
        console.log('pharId', pharId);

        axios.get(`/users/pharmacyDetails/${pharId}`, {
            headers: {
                'Content-Type': 'application/json',
                'token': localStorage.getItem('token')
            }
        }).then(result => {
            console.log("pharmacyDetails result^^^", result.data.detail);
            this.setState({ pharmacyDetails: result.data.detail && result.data.detail ? result.data.detail : {} })
        })
            .catch(error => {
                console.log("error....", error);
            });
        this.setState({ openModal: true, isBrowser: isBrowser, pharmacyId: pharId })
    }
    searchResults = (e, searchText) => {
        console.log('resultssss', searchText.length);
        if (searchText.length > 0) {
            this.props.searchPharmacyData({ data: { searchText: searchText }, history: this.props.history });
        } else {
            this.props.getPharmacyData({ history: this.props.history });
        }
    }

    handleOnClick = (e, id) => {
        e.preventDefault();
        this.props.history.push(`/${localStorage.getItem('role')}/pharmacyDetails/${id}`)
    }
    handleModalClose = () => {
        this.setState({ openModal: false, errors: {} });
    }
    render() {
        const { classes, userProfileData, children, pharmacyData, searchpharmacyData } = this.props;
        console.log('AppConfig', AppConfig.colors.primary)
        const { searchText, pharmacies, pharmacyDetails } = this.state;
        console.log('searchText,searchpharmacyData', searchText, searchpharmacyData)
        return (
            <React.Fragment>
                <AppBar position="fixed" className={classes.appBar}>
                    <Toolbar>
                        <IconButton
                            color="inherit"
                            aria-label="Open drawer"
                            onClick={this.handleDrawerToggle}
                            className={classes.menuButton}>
                            <MenuIcon />
                        </IconButton>
                        <Typography variant="h6" color="inherit" style={{ fontWeight: 'bold', color: '#fff' }} noWrap>
                            {"Pharmacy"}
                        </Typography>
                    </Toolbar>
                </AppBar>
                <div className='page-content' style={{ fontWeight: 'bold', backgroundColor: '#fff', marginTop: '-65px' }}>
                    <SideBar mobileOpen={this.state.mobileOpen} handleDrawerToggle={this.handleDrawerToggle} userProfileData={userProfileData} >
                        {children}
                    </SideBar>
                </div>
                <div className={classes.root}>
                    <div className={classes.tableDiv} >
                        <Typography className={classes.typography}>
                            {<div className={classes.search}>
                                {<SearchBar value={searchText} onRequestSearch={(e) => this.searchResults(e, searchText)} onChange={(e) => this.handleSearchText(e, 'searchText')} style={{ borderRadius: '40px', height: '40px', fontWeight: 'bold', boxShadow: 'none', marginBottom: '8px' }} />}
                            </div>}
                        </Typography>
                    </div>

                    <div key={'List'} className={classes.tableDiv}>
                        <List className={classes.root2} >
                            {
                                searchText != '' && searchpharmacyData && searchpharmacyData.length > 0 ?
                                    searchpharmacyData.map((data, index) => {
                                        console.log('dataa&&&&', data);
                                        return <div key={index}>
                                            <ListItem className={classes.listitems}  >
                                                <Col sm={8} xs={(data && data._id && data.isChatEnabled === true) ? 7 : 8} md={8} lg={8} xl={8} onClick={(e) => this.handleOnClick(e, data._id)}>
                                                    {/*   <Col sm={10} xs={10} md={11} lg={11} xl={11} onClick={(e) => this.handleOnClick(e, data._id)} className={classes.listitems}> */}
                                                    <ListItemText primary={<Typography style={{ color: AppConfig.colors.primary, fontWeight: 'bold', fontSize: '12px' }}>ID: {data && data !== null && data.userId ? data.userId : '--'}</Typography>} secondary={<React.Fragment>
                                                        <Typography variant="body1" style={{ color: AppConfig.colors.primary, fontWeight: 'bold', fontSize: '18px', padding: '5px 0 5px 0' }} className={classes.pharmacyName}>{data && data != null && data.firstname && data.firstname != null ? data.firstname + ' ' + data.lastname : ''}</Typography>
                                                        {data.connected ?
                                                            <Typography variant={"caption"} style={{
                                                                color: AppConfig.colors.success, fontWeight: 'bold', fontSize: '12px', border: '1px solid',
                                                                padding: '2px 6px',
                                                                borderRadius: '10px'
                                                            }}>{data.connected ? 'Connected' : ''}</Typography>

                                                            :
                                                            ''}

                                                    </React.Fragment>} />
                                                </Col>
                                                <Col sm={(data && data._id && data.isChatEnabled === true) ? 3 : 4} xs={(data && data._id && data.isChatEnabled === true) ? 3 : 4} md={(data && data._id && data.isChatEnabled === true) ? 3 : 4} lg={(data && data._id && data.isChatEnabled === true) ? 3 : 4} xl={(data && data._id && data.isChatEnabled === true) ? 3 : 4} onClick={(e) => this.handleModal(e, isMobile, data._id)}>

                                                    <ListItemText style={{
                                                        textAlign: 'right'
                                                        , marginTop: '0px', marginBottom: '0px',
                                                    }} primary={<React.Fragment><Typography style={{ color: AppConfig.colors.primary, fontWeight: 'bold', fontSize: '12px' }}><CallIcon /></Typography><Typography style={{ color: AppConfig.colors.primary, fontWeight: 'bold', fontSize: '12px' }}>Call</Typography></React.Fragment>} />
                                                </Col>
                                                {
                                                    (data && data._id && data.isChatEnabled === true) ? <Col sm={1} xs={2} md={1} lg={1} xl={1} onClick={(e) => this.handleChat(e, data._id)} >
                                                        <ListItemText style={{
                                                            float: 'right', marginTop: '0px', marginBottom: '0px',
                                                        }} primary={<React.Fragment><Typography style={{ color: '#42425E', fontWeight: 'bold', fontSize: '12px' }}><Chat /></Typography><Typography style={{ color: '#42425E', fontWeight: 'bold', fontSize: '12px' }}>Chat</Typography></React.Fragment>} />
                                                    </Col> : ''
                                                }
                                                {
                                                    (data && data._id && data.isChatEnabled === true) ? <Col sm={2} xs={2} md={1} lg={1} xl={1} onClick={(e) => this.handleChat(e, data._id)} >
                                                        <ListItemText style={{
                                                            float: 'right', marginTop: '0px', marginBottom: '0px',
                                                        }} primary={<React.Fragment><Typography style={{ color: '#42425E', fontWeight: 'bold', fontSize: '12px' }}><Chat /></Typography><Typography style={{ color: '#42425E', fontWeight: 'bold', fontSize: '12px' }}>Chat</Typography></React.Fragment>} />
                                                    </Col> : ''
                                                }
                                            </ListItem>
                                            <Dialog
                                                fullWidth={this.state.fullWidth}
                                                maxWidth={this.state.maxWidth}
                                                open={this.state.openModal}
                                                onClose={this.handleModalClose}
                                                aria-labelledby="max-width-dialog-title">
                                                <DialogTitle id="max-width-dialog-title">
                                                    <Typography variant="body1" style={{ color: '#000' }}>Mobile Number: </Typography> {pharmacyDetails.mobile ? isMobile ? <a style={{ color: AppConfig.colors.primary, fontSize: '12px', fontWeight: 'bold' }} href={`tel:+91${pharmacyDetails.mobile}`}> +91{pharmacyDetails.mobile}</a> : <Typography key={"owner354"} variant="body1" style={{ color: AppConfig.colors.primary, fontSize: '12px', fontWeight: 'bold' }} gutterBottom align="left" >{pharmacyDetails.mobile}</Typography> : 'No number'}
                                                </DialogTitle>
                                                <DialogContent>
                                                    <form className={classes.form}>
                                                        <div style={{ textAlign: 'center' }}>
                                                            {this.state.isBrowser ?
                                                                <div style={{ textAlign: 'center' }}>
                                                                    {pharmacyDetails.mobile ? <Button variant="outlined" style={{ padding: '1px 10px' }} className={classes.submit2} type="submit" >
                                                                        <a style={{ color: AppConfig.colors.primary, fontSize: '12px', fontWeight: 'bold', textDecoration: 'none', textAlign: 'center' }} href={`tel:+91${pharmacyDetails.mobile}`}> Call</a> </Button> :
                                                                        <Button variant="outlined" style={{ padding: '1px 10px' }} onClick={this.handleModalClose} className={classes.submit2} type="submit" >
                                                                            Ok </Button>}
                                                                </div>
                                                                :
                                                                <div style={{ textAlign: 'center' }}>
                                                                    <Button variant="outlined" style={{ padding: '1px 10px' }} className={classes.submit2} type="submit" onClick={this.handleModalClose} >
                                                                        Ok</Button>
                                                                </div>
                                                            }
                                                        </div>
                                                    </form>
                                                </DialogContent>
                                            </Dialog>

                                            <li className={classes.Listdivide}></li>
                                        </div>

                                    }) : searchText == '' && pharmacyData && pharmacyData.length > 0 ?
                                        pharmacyData.map((data, index) => {
                                            console.log('dataaaa', data);
                                            return <div key={index}>
                                                <ListItem className={classes.listitems}  >
                                                    <Col sm={8} xs={(data && data._id && data.isChatEnabled === true) ? 7 : 8} md={8} lg={8} xl={8} onClick={(e) => this.handleOnClick(e, data._id)}>
                                                        <ListItemText primary={<Typography style={{ color: AppConfig.colors.primary, fontWeight: 'bold', fontSize: '12px' }}>ID: {data && data !== null && data.userId ? data.userId : '--'}</Typography>} secondary={<React.Fragment>
                                                            <Typography variant="body1" style={{ color: AppConfig.colors.primary, fontWeight: 'bold', fontSize: '18px', padding: '5px 0 5px 0' }} className={classes.pharmacyName}>{data && data != null && data.firstname && data.firstname != null ? data.firstname + ' ' + data.lastname : ''}</Typography>
                                                            {data.connected ?
                                                                <Typography variant={"caption"} style={{
                                                                    color: AppConfig.colors.success, fontWeight: 'bold', fontSize: '12px',/*  border: '1px solid',
                                                                    padding: '2px 6px',
                                                                    borderRadius: '10px' */
                                                                }}>{data.connected ? 'Connected' : ''}</Typography>

                                                                :
                                                                ''}

                                                        </React.Fragment>} />
                                                    </Col>

                                                    <Col sm={(data && data._id && data.isChatEnabled === true) ? 3 : 4} xs={(data && data._id && data.isChatEnabled === true) ? 3 : 4} md={(data && data._id && data.isChatEnabled === true) ? 3 : 4} lg={(data && data._id && data.isChatEnabled === true) ? 3 : 4} xl={(data && data._id && data.isChatEnabled === true) ? 3 : 4} onClick={(e) => this.handleModal(e, isMobile, data._id)}>

                                                        <ListItemText style={{
                                                            textAlign: 'right'
                                                            , marginTop: '0px', marginBottom: '0px',
                                                        }} primary={<React.Fragment><Typography style={{ color: AppConfig.colors.primary, fontWeight: 'bold', fontSize: '12px' }}><CallIcon /></Typography><Typography style={{ color: AppConfig.colors.primary, fontWeight: 'bold', fontSize: '12px' }}>Call</Typography></React.Fragment>} />
                                                    </Col>
                                                    {
                                                        (data && data._id && data.isChatEnabled === true) ? <Col sm={1} xs={2} md={1} lg={1} xl={1} onClick={(e) => this.handleChat(e, data._id)} >
                                                            <ListItemText style={{
                                                                float: 'right', marginTop: '0px', marginBottom: '0px',
                                                            }} primary={<React.Fragment><Typography style={{ color: '#42425E', fontWeight: 'bold', fontSize: '12px' }}><Chat /></Typography><Typography style={{ color: '#42425E', fontWeight: 'bold', fontSize: '12px' }}>Chat</Typography></React.Fragment>} />
                                                        </Col> : ''
                                                    }
                                                    {
                                                        (data && data._id && data.isChatEnabled === true) ? <Col sm={2} xs={2} md={1} lg={1} xl={1} onClick={(e) => this.handleChat(e, data._id)} >
                                                            <ListItemText style={{
                                                                float: 'right', marginTop: '0px', marginBottom: '0px',
                                                            }} primary={<React.Fragment><Typography style={{ color: '#42425E', fontWeight: 'bold', fontSize: '12px' }}><Chat /></Typography><Typography style={{ color: '#42425E', fontWeight: 'bold', fontSize: '12px' }}>Chat</Typography></React.Fragment>} />
                                                        </Col> : ''
                                                    }
                                                </ListItem>
                                                <Dialog
                                                    fullWidth={this.state.fullWidth}
                                                    maxWidth={this.state.maxWidth}
                                                    open={this.state.openModal}
                                                    onClose={this.handleModalClose}
                                                    aria-labelledby="max-width-dialog-title">
                                                    <DialogTitle id="max-width-dialog-title">
                                                        <Typography variant="body1" style={{ color: '#000' }}>Mobile Number: </Typography> {pharmacyDetails.mobile ? isMobile ? <a style={{ color: AppConfig.colors.primary, fontSize: '12px', fontWeight: 'bold' }} href={`tel:+91${pharmacyDetails.mobile}`}> +91{pharmacyDetails.mobile}</a> : <Typography key={"owner354"} variant="body1" style={{ color: AppConfig.colors.primary, fontSize: '12px', fontWeight: 'bold' }} gutterBottom align="left" >{pharmacyDetails.mobile}</Typography> : 'No number'}
                                                    </DialogTitle>
                                                    <DialogContent>
                                                        <form className={classes.form}>
                                                            <div style={{ textAlign: 'center' }}>
                                                                {this.state.isBrowser ?
                                                                    <div style={{ textAlign: 'center' }}>
                                                                        {pharmacyDetails.mobile ? <Button variant="outlined" style={{ padding: '1px 10px' }} className={classes.submit2} type="submit" >
                                                                            <a style={{ color: AppConfig.colors.primary, fontSize: '12px', fontWeight: 'bold', textDecoration: 'none', textAlign: 'center' }} href={`tel:+91${pharmacyDetails.mobile}`}> Call</a> </Button> :
                                                                            <Button variant="outlined" style={{ padding: '1px 10px' }} onClick={this.handleModalClose} className={classes.submit2} type="submit" >
                                                                                Ok </Button>}
                                                                    </div>
                                                                    :
                                                                    <div style={{ textAlign: 'center' }}>
                                                                        <Button variant="outlined" style={{ padding: '1px 10px' }} className={classes.submit2} type="submit" onClick={this.handleModalClose} >
                                                                            Ok</Button>
                                                                    </div>
                                                                }
                                                            </div>
                                                        </form>
                                                    </DialogContent>
                                                </Dialog>
                                                <li className={classes.Listdivide}></li>
                                            </div>
                                        }) : <div><Typography style={{
                                            backgroundColor: '#fff', color: 'gray', fontWeight: 'bold', fontSize: '25px',
                                        }}>No Pharmacies</Typography></div>
                            }
                        </List>

                    </div>
                </div>
            </React.Fragment>);
    }
}

Pharmacy.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = ({ authUser }) => {
    const { userProfileData, pharmacyData, searchpharmacyData } = authUser
    return { userProfileData, pharmacyData, searchpharmacyData }
}

export default connect(mapStateToProps, { getuserProfileData, searchPharmacyData, getPharmacyData })(withStyles(styles)(Pharmacy));