import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import SideBar from '../../components/userSideBar'
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import { Button, FormGroup, Input, Row, Col } from 'reactstrap';
import AppBar from '@material-ui/core/AppBar';
import { ThemeProvider } from "@material-ui/styles";
import { createMuiTheme } from "@material-ui/core/styles";
import appConfig from '../../constant/config'
import KeyboardArrowRight from '@material-ui/icons/KeyboardArrowRight';
import { red } from '@material-ui/core/colors';
import axios from '../../constant/axios';
import { getuserProfileData, getPharmacyData, addOrder, connectedPharmaciesData } from '../../actions';
import { connect } from 'react-redux';

const drawerWidth = 240;
const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120
    },
    lable: {
        textAlign: 'left'
    },
    selectEmpty: {
        marginTop: theme.spacing(2)
    },
    error: {
        color: 'red',
        textAlign: 'left'
    },
    appBar: {
        marginLeft: drawerWidth,
        [theme.breakpoints.up('sm')]: {
            width: `calc(100% - ${drawerWidth}px)`,
        },
    },
    menuButton: {
        marginRight: 20,
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
    dialogDone: {
        backgroundColor: '#454545',
        color: "#3ddb20",
        '&:hover': {
            background: 'none',
            backgroundColor: '#454545',
            color: '#3ddb20'
        },
        borderColor: '#454545'
    },
    dialogTitle: {
        backgroundColor: '#5a5a5a',
        '& h2': {
            color: 'white',
            textAlign: 'center'
        },
        borderBottom: '1px solid #5a5a5a'
    },
    dialogContent: {
        backgroundColor: '#5a5a5a',
    },
    root2: {
        flexGrow: 1,
        backgroundColor: '#fff'
    },
    tabsRoot: {
        color: '#42425E',
        borderBottom: '1px solid #7a7a7a',
    },
    tabsIndicator: {
        backgroundColor: '#7a7a7a',
        height: '3px'
    },
    rightDiv: {
        color: "#CCCACD",
        marginBottom: 0,
        fontWeight: 'bold',
        fontSize: '16px',
        float: 'right',
        textAlign: 'right',

    },
    listitems: {
        paddingLeft: 40,
        paddingRight: 10,
        paddingTop: 5,
        paddingBottom: 5
    },
    listitems1: {
        paddingLeft: 40,
        paddingRight: 10,
        paddingTop: 14,
        paddingBottom: 5
    },
    tabRoot: {
        textTransform: 'initial',
        minWidth: '50%',
        fontWeight: theme.typography.fontWeightRegular,
        fontFamily: [
            '-apple-system',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ].join(','),
    },
    typography: {
        padding: '15px 0px',
        color: '#CCCACD',
        ['@media (max-width:787px)']: {
            padding: '15px 15px',
        },
    },
    input: {
        '::-webkit-input-placeholder': {
            color: 'red'
        }
    },
    imageDiv: {
        textAlign: 'right',
        height: '70px',
        width: '70px',
    },
    avatar: {
        margin: theme.spacing(2),
        marginLeft: theme.spacing(4),
        width: 70,
        height: 70
    },
    profile: {
        marginTop: theme.spacing(4),
        lineHeight: 1
    },
    tableDiv: {
        padding: '0px',
    },
    tabsFlex: {
        borderBottom: '0px solid #454545',
    },
    tableThTd: {
        color: '#ffffff',
    },
    infoDiv: {
        padding: 18,
        backgroundColor: "#fff"
    },
    disableClick: {
        pointerEvents: 'none'
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: '#ffffff',
        '&:hover': {
            backgroundColor: '#ffffff',
        },
        marginRight: theme.spacing(2),
        marginLeft: 0,
        width: '100%',
        borderRadius: '40px',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(3),
            width: 'auto',
        },
    },
    searchIcon: {
        width: theme.spacing(7),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        fontWeight: 'bold',
        opacity: 1
    },
    inputRoot: {
        color: '#c6c6c6',
    },
    inputInput: {
        color: '#c6c6c6',
        padding: theme.spacing(1, 1, 1, 7),
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: 200,
        },
    },
    icon: {
        position: 'fixed',
        right: '10px',
        color: '#fff'
    },
    iconHover: {
        margin: theme.spacing(2),
        '&:hover': {
            color: red[800],
        },
    },
    avatar: {
        margin: 0,
        padding: 0,
    },
    avatarlist: {
        margin: 0,
        padding: 0,
        width: 35,
        height: 35,
    },
    profile: {
        margin: 0,
        height: 100,
        lineHeight: 'normal'
    },
    signup: {
        backgroundColor: appConfig.colors.primary,
        borderRadius: '0px',
        color: '#fff',
        marginTop: '85px',
        padding: '5px 22px',
        textTransform: 'none',
        marginRight: '8px',
        ['@media (max-width:707px)']: {
            marginRight: 'unset',
        },
        ['@media (max-width:599px)']: {
            marginRight: '8px',
        },
        ['@media (max-width:436px)']: {
            marginRight: 'unset',
        },
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        borderRadius: '8px',
        color: appConfig.colors.primary
    },
    dense: {
        marginTop: theme.spacing(2),
    },
    menu: {
        width: 200,
    },
    submit: {
        backgroundColor: appConfig.colors.primary,
        color: '#fff',
        marginTop: '3px',
        textTransform: 'none',
        borderRadius: '0'
    },
    submit2: {
        backgroundColor: '#fff',
        color: appConfig.colors.primary,
        border: 'none',
        marginTop: theme.spacing(2),
        textTransform: 'none',
        fontWeight: 'bolder',
        borderRadius: '0',
        '&:hover': {
            backgroundColor: '#ffffff',
            color: appConfig.colors.primary,
        },
    },
    form: {
        width: '100%',
        paddingLeft: '20px',
        paddingRight: '35px',
        marginTop: theme.spacing.unit,
    },
});

class AddOrder extends Component {
    constructor(props) {
        super(props)
        console.log('hey  i m here', this.props.location)
        this.state = {
            uploaded_image_path: '',
            value: 0,
            openModal: false,
            isLoading: true,
            prescription: '',
            pharmacy: '',
            prescriptions: [],
            note: '',
            errors: {},
            prescriptionDetails: '',
            addmoreText: [{ name: '', qty: '' }],
            qty: [{ description: '', qty: '' }]
        };

        this.handleValidation = this.handleValidation.bind(this);
    }

    componentWillMount() {
        this.props.getuserProfileData({ history: this.props.history });
        this.props.getPharmacyData({ history: this.props.history });
        this.props.connectedPharmaciesData({ history: this.props.history })
        console.log('k,dsfkdsfkd', localStorage.getItem('token'));
        axios.get('/users/orderPrescriptionsById', {
            headers: {
                'Content-Type': 'application/json',
                'token': localStorage.getItem('token')
            }
        }).then(result => {
            console.log("orderPrescriptionsById result###", result.data.detail);

            this.setState({ prescriptions: result.data.detail && result.data.detail.length > 0 ? result.data.detail : [] })
        })
            .catch(error => {
                console.log("error....", error);
            });
    }

    addMoreItems = (e) => {
        e.preventDefault()
        let addmoreText = this.state.addmoreText;
        addmoreText.push({ name: '', qty: '' });
        this.setState({ addmoreText: addmoreText })
    }

    handleItemChange = (e, key, ind) => {
        console.log('e, key,ind', e.target.value, key, ind);
        e.preventDefault()
        var oldState = [...this.state.addmoreText];
        oldState.map((data, i) => {
            if (ind == i) {
                oldState[i][key] = e.target.value;
            }
        })
        this.setState({ addmoreText: oldState });
    }

    handleValidation = () => {
        let errors = {};
        let formIsValid = true;
        console.log("this.state handleValidation", this.state)
        if (this.state.prescription === '') {
            formIsValid = false;
            errors["prescription"] = "This field is required";
        }
        if (this.state.pharmacy === '') {
            formIsValid = false;
            errors["pharmacy"] = "This field is required";
        }

        this.setState({ errors: errors });
        return formIsValid;
    }

    handleOnSubmit = async (e) => {
        e.preventDefault()
        let data = {
            items: this.state.addmoreText,
            note: this.state.note,
            pharmacy: this.state.pharmacy,
            prescription: this.state.prescription
        }
        console.log('order data', data);
        /* this.props.addOrder({
            data,
            history: this.props.history
        }) */

        let isValid = await this.handleValidation();
        if (isValid === true) {
            this.props.addOrder({
                data,
                history: this.props.history
            })
        } else {
            return;
        }
    }

    handleQtyChange = (event, key, index) => {
        console.log('handleQtyChange key', key, event.target.value)
        let qtyArry = [...this.state.qty]
        qtyArry[index][key] = event.target.value
        this.setState({ [key]: qtyArry });
    }

    removeHandler = (e, ind) => {
        e.preventDefault()
        var oldState = [...this.state.addmoreText];
        var newState = []
        oldState.map((data, i) => {
            if (ind !== i) {

                newState.push(data)
            }
        })
        this.setState({ addmoreText: newState });
    }

    handleChange = (event, key) => {
        console.log('handleChange key', key, event.target.value)
        this.setState({ [key]: event.target.value });
    };

    handlePrescriptionChange = (event, key) => {
        console.log('key', key)
        axios.get(`/users/prescriptionDetails/${event.target.value}`, {
            headers: {
                'Content-Type': 'application/json',
                'token': localStorage.getItem('token')
            }
        }).then(result => {
            console.log("prescriptionDetails result###", result.data.detail);
            let arry = []
            result.data.detail.otherDetails.map((each, i) => {
                arry.push({ qty: each.qty, description: each.description })
            })
            this.setState({ prescriptionDetails: result.data.detail && result.data.detail != undefined ? result.data.detail : '', qty: arry })
        })
            .catch(error => {
                console.log("error....", error);
            });

        this.setState({ [key]: event.target.value });
    };

    openImageUploader() {
        this.refs.imageUploader.click();
    }

    handleImageChange = name => event => {
        console.log('name=', name, event.target.files[0].name)
        if (name === 'uploaded_image_path') {
            this.setState({
                [name]: event.target.files,
            });
        }
        this.setState({
            [name]: event.target.files[0].name,
        });
    };

    handleDrawerToggle = () => {
        this.setState(state => ({ mobileOpen: !state.mobileOpen }));
    };

    handleModalClose = () => {
        this.setState({ openModal: false, errors: {} });
    }

    render() {
        const { classes, userProfileData, pharmacyData, children, connectedPharmacies } = this.props;
        const theme = createMuiTheme({
            palette: {
                primary: { main: appConfig.colors.primary },
                secondary: { main: appConfig.colors.primary },
            },
            overrides: {
                MuiInputLabel: {
                    root: {
                        color: "#43415e",
                        "&$focused": {
                            color: "#43415e"
                        }
                    }
                },
                MuiInputBase: {
                    root: {
                        color: "#43415e",
                        "&$focused": {
                            color: "#43415e"
                        }
                    }
                },
                MuiFormLabel: {
                    root: {
                        color: "#43415e",
                        "&$focused": {
                            color: "#43415e"
                        }
                    }
                },
                MuiInput: {
                    root: {
                        borderColor: '#fff'
                    },
                    underline: {
                        color: appConfig.colors.primary,
                        "&$focused": {
                            color: "#43415e",
                        },
                        "&:before": {
                            color: "#43415e",
                            borderBottom: '1px solid rgba(231, 231, 231, 1)'
                        },
                        borderBottom: '1px solid rgba(231, 231, 231, 1)'
                    }
                }
            }
        });
        const { prescriptions } = this.state;
        console.log('connectedPharmacies', connectedPharmacies)

        return (
            <React.Fragment>
                <AppBar position="fixed" className={classes.appBar}>
                    <Toolbar>
                        <IconButton
                            color="inherit"
                            aria-label="Open drawer"
                            onClick={this.handleDrawerToggle}
                            className={classes.menuButton}
                        >
                            <MenuIcon />
                        </IconButton>
                        <Typography variant="h6" color="inherit" style={{ fontWeight: 'bold', color: '#fff' }} noWrap>
                            {"Orders"}
                        </Typography>
                    </Toolbar>
                </AppBar>
                <div className='page-content' style={{ fontWeight: 'bold', backgroundColor: '#fff', marginTop: '-56px' }}>
                    <SideBar mobileOpen={this.state.mobileOpen} handleDrawerToggle={this.handleDrawerToggle} userProfileData={userProfileData} >
                        {children}
                    </SideBar>
                </div>
                <div className={classes.root}>
                    <div className={classes.container}>
                        <form className={classes.form} onSubmit={(e) => this.handleOnSubmit(e)}>
                            <ThemeProvider theme={theme}>
                                <FormGroup className={classes.lable}>
                                    <Input type="select" value={this.state.prescription} name="prescription" id="prescription" onChange={(e) => this.handlePrescriptionChange(e, 'prescription')} style={{ borderColor: appConfig.colors.primary, fontSize: '18px' }} value={this.state.prescription} >
                                        <option value="" >Select Prescription</option>
                                        {prescriptions && prescriptions.length > 0 ?
                                            prescriptions.map((each, ind) => {
                                                console.log('each ^^^', each);
                                                return <option value={each._id} key={ind}>{each.title != '' ? each.title : each._id}</option>
                                            })
                                            : ''}
                                    </Input>
                                    <span className={classes.error} >{prescriptions.length > 0 && this.state.errors["prescription"]}</span>
                                </FormGroup>
                                <FormGroup className={classes.lable}>
                                    <Input type="select" value={this.state.pharmacy} name="pharmacy" id="pharmacy" style={{ borderColor: appConfig.colors.primary, fontSize: '18px' }} onChange={(e) => this.handleChange(e, 'pharmacy')} >
                                        <option value="" >Select Pharmacy</option>
                                        {connectedPharmacies && connectedPharmacies.length > 0 ?
                                            connectedPharmacies.map((each, ind) => {
                                                console.log('each', each);
                                                return <option value={each && each.pharmacy && each.pharmacy != null ? each.pharmacy._id : ''} key={ind}>{each.pharmacy.firstname} {each.pharmacy.lastname}</option>
                                            })
                                            : ''}
                                    </Input>
                                    <span className={classes.error} >{connectedPharmacies.length > 0 && this.state.errors["pharmacy"]}</span>
                                </FormGroup>
                                <FormGroup>
                                    <Input type="textarea" style={{ borderColor: appConfig.colors.primary, fontSize: '18px' }} onChange={(e) => this.handleChange(e, 'note')} className={classes.input} placeholder="Note for Pharmacy" name="note" id="note" />

                                </FormGroup>
                                {this.state.addmoreText.map((data, index) => {
                                    console.log('data!!!!', data);
                                    return <Row key={index}>
                                        <Col xl={6} lg={6} md={6} sm={6} xs={6}>
                                            <FormGroup>
                                                <Input type="text" style={{ borderColor: appConfig.colors.primary, fontSize: '18px' }} className={classes.input} placeholder="Items" onChange={(e) => this.handleItemChange(e, 'name', index)} name="name" id={`items${index}`} />
                                            </FormGroup>
                                        </Col>
                                        <Col xl={3} lg={3} md={3} sm={5} xs={6}>
                                            <FormGroup>
                                                <Input type="number" style={{ borderColor: appConfig.colors.primary, fontSize: '18px' }} className={classes.input} placeholder="Qty" onChange={(e) => this.handleItemChange(e, 'qty', index)} name="qty" id={`qty${index}`} />
                                            </FormGroup>
                                        </Col>
                                        {this.state.addmoreText.length > 1 &&
                                            <Col xl={3} lg={3} md={3} sm={3} xs={3}>
                                                <FormGroup>
                                                    <Button className={classes.submit} style={{ borderRadius: '11px' }} onClick={(e) => this.removeHandler(e, index)}>Remove </Button>
                                                </FormGroup>
                                            </Col>
                                        }
                                    </Row>
                                })}
                                <Col md={6} sm={6} xs={6} style={{ textAlign: 'left', display: 'inline-block' }}>
                                    <Button
                                        type="submit"
                                        variant="contained" style={{ textAlign: 'left', borderRadius: '11px' }}
                                        className={classes.submit} onClick={(e) => this.addMoreItems(e)}
                                    >Add Items</Button>
                                </Col>
                                <Col md={6} sm={6} xs={6} style={{ textAlign: 'right', display: 'inline-block' }}>
                                    <Button
                                        type="submit"
                                        variant="contained"
                                        className={classes.submit2}
                                    >Request<KeyboardArrowRight style={{ fontSize: '35px' }} /></Button>
                                </Col>
                            </ThemeProvider>
                            <br />
                        </form>
                    </div>
                </div>
            </React.Fragment>);
    }
}

AddOrder.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = ({ authUser, pharmacyReducer }) => {
    const { userProfileData, pharmacyData } = authUser
    const { connectedPharmacies } = pharmacyReducer
    return { userProfileData, pharmacyData, connectedPharmacies }
}

export default connect(mapStateToProps, { getuserProfileData, getPharmacyData, addOrder, connectedPharmaciesData })(withStyles(styles)(AddOrder));