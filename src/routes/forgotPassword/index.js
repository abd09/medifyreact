import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControl from '@material-ui/core/FormControl';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import { createMuiTheme } from "@material-ui/core/styles";
import { forgotPassword } from '../../actions';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form'
import { ThemeProvider } from "@material-ui/styles";
import Box from '@material-ui/core/Box';
import { Col, Row } from 'reactstrap';
import axios from '../../constant/axios';
import appConfig from "../../constant/config";
import Link from '@material-ui/core/Link';

const styles = theme => ({
    main: {
        marginTop: theme.spacing.unit * 8,
        width: 'auto',
        display: 'block',
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3,
        [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
            width: 400,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    heading: {
        color: appConfig.colors.primary,
    },
    paper: {
        marginTop: theme.spacing.unit * 8,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
    },
    heading2: {
        marginTop: '25px',
        fontSize: '16px',
        /*  color: '#A6A7AD' */
        color: '#808188'
    },
    avatar: {
        height: "25px",
        width: "114px"
    },
    form: {
        width: '100%',
        marginTop: theme.spacing.unit,
    },
    label: {
        color: appConfig.colors.primary,
        "&$focused": {
            color: appConfig.colors.primary
        }
    },
    signup: {
        backgroundColor: '#fff',
        border: '1px solid #43415e',
        color: appConfig.colors.primary,
        marginTop: theme.spacing.unit * 3,
        padding: '10px 50px'
    },
    submit: {
        backgroundColor: appConfig.colors.primary,
        color: '#fff',
        marginTop: theme.spacing.unit * 3,
        padding: '10px 50px',
        textTransform: 'none',
        fontSize: '16px'
    },
    error: {
        color: 'red',
        textAlign: 'left'
    },
    font: {
        fontFamily: [
            '"Tangerine"',
            'cursive'
        ].join(','),
    }
});

class ForgotPassword extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email: '',
            checkedB: false,
            errors: {},
        }
        this.onSubmit = this.onSubmit.bind(this);
        this.handleEmailValidation = this.handleEmailValidation.bind(this);
        this.handleValidation = this.handleValidation.bind(this);
    }

    onSubmit = async (e) => {
        e.preventDefault()
        let postData = {
            email: this.state.email,
            role: this.props.match.params.category
        }
        console.log("postData", postData)
        let isValid = await this.handleValidation();
        if (isValid === true) {
            this.props.forgotPassword({ data: postData, history: this.props.history });
        } else {
            return;
        }
    }

    handleValidation = () => {
        let errors = {};
        let formIsValid = true;
        console.log("this.state handleValidation", this.state)
        //email
        if (typeof this.state.email !== "") {
            let lastAtPos = this.state.email.lastIndexOf('@');
            let lastDotPos = this.state.email.lastIndexOf('.');
            if (!(lastAtPos < lastDotPos && lastAtPos > 0 && this.state.email.indexOf('@@') == -1 && lastDotPos > 2 && (this.state.email.length - lastDotPos) > 2)) {
                formIsValid = false;
                errors["email"] = "Please enter valid email address.";
            }
        }
        //email
        if (this.state.email === '') {
            formIsValid = false;
            errors["email"] = "This field is required";
        }

        this.setState({ errors: errors });
        return formIsValid;
    }

    handleEmailValidation = async () => {
        console.log('this.state....', this.state);
        let errors = {};
        let formIsValid = true;
        //email
        if (this.state.email === '') {
            formIsValid = false;
            errors["email"] = "This field is required";
        }
        /*  if (this.state.email !== '' || this.state.email !== undefined) {
             let data = {
                 email: this.state.email,
                 role: this.props.match.params.category
             }
             await axios.post('/users/emailExist', data, {
                 headers: {
                     'Content-Type': 'application/json',
                     'token': localStorage.getItem('token')
                 }
             }
             ).then(result => {
                 console.log("result", result)
                 if (result.data.error === false) {
                     formIsValid = false;
                     errors["email"] = "Invalid email!";
                 } else {
                     formIsValid = true;
                 }
             })
                 .catch(error => {
                     console.log("error", error)
                     formIsValid = false;
                     errors["email"] = "Invalid email!";
                 });
         } */
        this.setState({ errors: errors });
        return formIsValid;
    }

    handleChange = (e, key) => {
        console.log("e.....", key)
        if (key == 'checkedB') {
            this.setState({ [key]: e.target.checked })
        } else {
            this.setState({ [key]: e.target.value })
        }
    }

    render() {
        const { classes } = this.props;
        const theme = createMuiTheme({
            palette: {
                primary: { main: appConfig.colors.primary },
                secondary: { main: appConfig.colors.primary },
            },
            overrides: {
                MuiInputLabel: {
                    root: {
                        color: "orange",
                        "&$focused": {
                            color: "#43415e",
                            borderColor: '#fff'
                        }
                    }
                },
                MuiInput: {
                    root: {
                        color: appConfig.colors.primary,
                        borderColor: '#fff'
                    },
                    underline: {
                        color: appConfig.colors.primary,
                        "&$focused": {
                            color: "#43415e",
                        },
                        "&:before": {
                            color: "#43415e",
                            borderBottom: '1px solid rgba(231, 231, 231, 1)'
                        },
                        borderBottom: '1px solid rgba(231, 231, 231, 1)'
                    }
                },
                MuiInputBase: {
                    root: {
                        padding: '8px 0 7px'
                    }
                },
            },
        });

        return (
            <main className={classes.main}>
                <CssBaseline />
                <Typography className={classes.heading} component="h1" variant="h5">
                    <img className={classes.avatar} src={require('../../assets/img/logo.png')} />
                </Typography>
                <Typography className={classes.heading2} component="h1" variant="h5">
                    Enter your email and we send you a password reset link.</Typography>
                <Row>
                    <Col md={12} sm={12} xs={12} lg={12} xl={12}>
                        <form className={classes.form} onSubmit={(e) => this.onSubmit(e)}>
                            <ThemeProvider theme={theme}>
                                <Col md={12} sm={12} xs={12} lg={12} xl={12}>
                                    <FormControl margin="normal" fullWidth>
                                        <InputLabel htmlFor="email" className={classes.label}>Email</InputLabel>
                                        <Input value={this.state.email} id="email" type="email" name="email" onBlur={this.handleEmailValidation} onChange={(e) => this.handleChange(e, 'email')} />
                                        <span className={classes.error} >{this.state.errors["email"]}</span>
                                    </FormControl>
                                </Col>
                                <br />
                                <Button
                                    type="submit"
                                    variant="contained"
                                    className={classes.submit}
                                >Send request</Button>
                            </ThemeProvider>
                        </form>
                    </Col>
                </Row>
                <Box mt={5} style={{ marginTop: '150px' }}>
                    {/* <Typography variant="body2" align="center" className={classes.heading} style={{ fontSize: '11px' }}>
                        {'Terms of use. Privacy policy'}
                    </Typography> */}
                      <Link color="inherit" className={classes.heading} style={{ fontSize: '12px' }} target={'_blank'} href="http://medify.net/page-privacy.html">Terms of use. Privacy policy</Link>
                </Box>
            </main>
        );
    }
}

ForgotPassword.propTypes = {
    classes: PropTypes.object.isRequired,
};

ForgotPassword = reduxForm({
    form: 'ForgotPasswordValidation'  // a unique identifier for this form
})(ForgotPassword)

ForgotPassword = connect(
    state => ({
        initialValues: state // pull initial values from account reducer
    }),
    { forgotPassword }               // bind account loading action creator
)(ForgotPassword)

export default withStyles(styles)(ForgotPassword);