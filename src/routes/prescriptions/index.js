import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { fade, makeStyles, withStyles } from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Typography from '@material-ui/core/Typography';
import Tab from '@material-ui/core/Tab';
import InputBase from '@material-ui/core/InputBase';
import SearchIcon from '@material-ui/icons/Search';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';
import SideBar from '../../components/userSideBar'
import AppBar from '@material-ui/core/AppBar';
import SearchBar from 'material-ui-search-bar'
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import Icon from '@material-ui/core/Icon';
import { red } from '@material-ui/core/colors';
import { filterPrescription, getuserProfileData } from '../../actions';
import { NotificationManager } from 'react-notifications';
import FilterComponent from '../../components/FilterComponent';
import { connect } from 'react-redux';
import { Col, Row, Label } from 'reactstrap';
import axios from '../../constant/axios';
import moment from 'moment';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';

const drawerWidth = 240;
const styles = theme => ({
	root: {
		flexGrow: 1,
		backgroundColor: '#f5f5f5 !important',
		// backgroundColor: '#bdbdcc !important'
	},
	root2: {
		flexGrow: 1,
		backgroundColor: '#fff'
	},
	appBar: {
		marginLeft: drawerWidth,
		[theme.breakpoints.up('sm')]: {
			width: `calc(100% - ${drawerWidth}px)`,
		},
	},
	menuButton: {
		marginRight: 20,
		[theme.breakpoints.up('sm')]: {
			display: 'none',
		},
	},
	dialogDone: {
		backgroundColor: '#454545',
		color: "#3ddb20",
		'&:hover': {
			background: 'none',
			backgroundColor: '#454545',
			color: '#3ddb20'
		},
		borderColor: '#454545'
	},
	dialogTitle: {
		backgroundColor: '#5a5a5a',
		'& h2': {
			color: 'white',
			textAlign: 'center'
		},
		borderBottom: '1px solid #5a5a5a'
	},
	dialogContent: {
		backgroundColor: '#5a5a5a',
	},
	tabsRoot: {
		borderBottom: '1px solid #454545',
	},
	tabsIndicator: {
		backgroundColor: '#58de45',
		height: '4px'
	},
	listitems: {
		paddingLeft: 40,
		paddingRight: 20,
	},
	tabRoot: {
		textTransform: 'initial',
		minWidth: '50%',
		fontWeight: theme.typography.fontWeightRegular,
		// marginRight: theme.spacing.unit * 4,
		fontFamily: [
			'-apple-system',
			'BlinkMacSystemFont',
			'"Segoe UI"',
			'Roboto',
			'"Helvetica Neue"',
			'Arial',
			'sans-serif',
			'"Apple Color Emoji"',
			'"Segoe UI Emoji"',
			'"Segoe UI Symbol"',
		].join(','),
	},
	tabSelected: {},
	typography: {
		padding: '15px 0px',
		color: '#CCCACD',
		['@media (max-width:787px)']: {
			padding: '15px 15px',
		},
	},
	avatar: {
		margin: theme.spacing.unit * 2,
		marginLeft: theme.spacing.unit * 4,
		width: 70,
		height: 70
	},
	profile: {
		marginTop: theme.spacing.unit * 4,
		lineHeight: 1
	},
	tableDiv: {
		padding: '5px',
		flexWrap: 'unset !important'
	},
	tabsFlex: {
		borderBottom: '1px solid #454545',
	},
	search: {
		position: 'relative',
		borderRadius: theme.shape.borderRadius,
		backgroundColor: '#ffffff',
		'&:hover': {
			backgroundColor: '#ffffff',
		},
		marginRight: 0,
		marginLeft: theme.spacing(2),
		marginTop: theme.spacing(1),
		width: '100%',
		borderRadius: '40px',
		[theme.breakpoints.up('sm')]: {
			marginLeft: theme.spacing(3),
			width: 'auto',
		},
	},
	searchIcon: {
		width: theme.spacing(7),
		height: '100%',
		position: 'absolute',
		pointerEvents: 'none',
		display: 'flex',
		alignItems: 'center',
		justifyContent: 'center',
		fontWeight: 'bold',
		opacity: 1,
		marginTop: theme.spacing(1)
	},
	inputRoot: {
		color: '#c6c6c6',
	},
	presName: {
		color: '#FFFFFF',
		font: 'bolder'
	},
	inputInput: {
		color: '#c6c6c6',
		padding: theme.spacing(1, 1, 1, 7),
		transition: theme.transitions.create('width'),
		width: '100%',
		[theme.breakpoints.up('md')]: {
			width: 200,
		},
	},
	icon: {
		// margin: theme.spacing(2),
		position: 'fixed',
		right: '10px',
		color: '#fff'

	},
	iconHover: {
		margin: theme.spacing(2),
		'&:hover': {
			color: red[800],
		},
	},
	listdivide: {
		marginLeft: '12px',
		marginRight: '12px',
		borderBottom: '1px solid #eeeeee'
	},
	dailogContentText: {
		height: 300,
		width: 250
	}
});

class Prescription extends Component {
	constructor(props) {
		super(props)
		this.state = {
			mobileOpen: false,
			value: '',
			searchText: '',
			isLoading: true,
			prescriptions: [],
			searchResults: [],
			open: false,
			scroll: 'paper',
			filterStateValue: null
		};
		this.filterRef = React.createRef();
	}
	// *snip*

	componentWillMount() {
		this.props.getuserProfileData({ history: this.props.history });
		axios.get('/users/prescriptionsById', {
			headers: {
				'Content-Type': 'application/json',
				'token': localStorage.getItem('token')
			}
		}).then(result => {
			console.log("getPrescriptionsById result###", result.data.detail);
			this.setState({ prescriptions: result.data.detail && result.data.detail.length > 0 ? result.data.detail : [] })
			if (result.data.detail.title == '') {
				NotificationManager.success('Account Verified Successfully')
			}
		})
			.catch(error => {
				console.log("error....", error);
			});

		axios.get('/users/prescriptionsByCategory', {
			headers: {
				'Content-Type': 'application/json',
				'token': localStorage.getItem('token')
			}
		}).then(result => {
			console.log("prescriptionsByCategory result###", result.data.detail);
			this.setState({ categoryPrescriptions: result.data.detail && result.data.detail.length > 0 ? result.data.detail : [] })
		})
			.catch(error => {
				console.log("error....", error);
			});
		/* axios.get('/users/prescriptionsCategory', {
			headers: {
				'Content-Type': 'application/json',
				'token': localStorage.getItem('token')
			}
		}).then(result => {
			console.log("prescriptionsByCategory result###", result.data.detail);
			this.setState({ categoryPrescriptions: result.data.detail && result.data.detail.length > 0 ? result.data.detail : [] })
		})
			.catch(error => {
				console.log("error....", error);
			}); */
	}

	handleApplyFilter() {
		console.log('asdasdasdasdsad', this.filterRef.current.state.single.value)
		this.props.filterPrescription({ data: this.filterRef.current.state.single, history: this.props.history });
		this.setState({ open: false, filterStateValue: this.filterRef.current.state.single });
	}

	handleClearFilter(e) {
		e.preventDefault();
		this.props.filterPrescription({ data: '', history: this.props.history });
		this.setState({ open: false, filterStateValue: null });
	}

	handleModalOpen(e) {
		e.preventDefault();
		this.setState({ open: true });
	}

	handleChange = (event, value) => {
		console.log('value', value)
		this.setState({ value });
	};

	handleDrawerToggle = () => {
		this.setState(state => ({ mobileOpen: !state.mobileOpen }));
	};

	handleOnClick = (e, id) => {
		e.preventDefault();
		console.log('id', id);
		this.props.history.push(`/${localStorage.getItem('role')}/prescriptionDetails/${id}`)
	}

	handleClick = (e) => {
		e.preventDefault();
		this.props.history.push(`/${localStorage.getItem('role')}/addPrescription`)
	}

	searchResults = (e, searchText) => {
		console.log('resultssss');
		let data = {
			searchText: searchText
		}

		if (searchText != '') {
			axios.post('/users/searchPatientPrescriptions', data, {
				headers: {
					'Content-Type': 'application/json',
					'token': localStorage.getItem('token')
				}
			}
			).then(result => {
				console.log('searchPatientPrescriptions', result);
				this.setState({ searchResults: result.data.detail ? result.data.detail : '' })
			})
				.catch(error => {
					console.log('error', error);
				});
		} else {
			axios.get('/users/prescriptionsById', {
				headers: {
					'Content-Type': 'application/json',
					'token': localStorage.getItem('token')
				}
			}).then(result => {
				console.log("getPrescriptionsById result###", result.data.detail);
				this.setState({ prescriptions: result.data.detail && result.data.detail.length > 0 ? result.data.detail : [] })
			})
				.catch(error => {
					console.log("error....", error);
				});
		}
	}

	handleSearchText = (e, key) => {
		console.log('e searchText key', e, key);
		this.setState({ [key]: e })
		let data = {
			searchText: e
		}
		if (e != '') {
			axios.post('/users/searchPatientPrescriptions', data, {
				headers: {
					'Content-Type': 'application/json',
					'token': localStorage.getItem('token')
				}
			}
			).then(result => {
				console.log('searchPatientPrescriptions', result);
				this.setState({ searchResults: result.data.detail ? result.data.detail : '' })
			})
				.catch(error => {
					console.log('error', error);
				});
		} else {
			axios.get('/users/prescriptionsById', {
				headers: {
					'Content-Type': 'application/json',
					'token': localStorage.getItem('token')
				}
			}).then(result => {
				console.log("getPrescriptionsById result###", result.data.detail);
				this.setState({ prescriptions: result.data.detail && result.data.detail.length > 0 ? result.data.detail : [] })
			})
				.catch(error => {
					console.log("error....", error);
				});
		}
	}

	render() {
		const { classes, userProfileData, children, filterPrescriptionData } = this.props;
		const { prescriptions, searchResults, searchText, open, categoryPrescriptions } = this.state;
		console.log('filterPrescriptionData', filterPrescriptionData, this.filterRef)
		return (
			<React.Fragment>
				<AppBar position="fixed" className={classes.appBar}>
					<Toolbar>
						<IconButton
							color="inherit"
							aria-label="Open drawer"
							onClick={this.handleDrawerToggle}
							className={classes.menuButton}
						><MenuIcon />
						</IconButton>
						<Typography variant="h6" color="inherit" style={{ fontWeight: 'bold', color: '#fff' }} noWrap>{'Medical History'}</Typography>
						<Icon className={classes.icon} color="disabled" fontSize="large" onClick={(e) => this.handleClick(e)}>add</Icon>
					</Toolbar>
				</AppBar>
				<div className='page-content' style={{ fontWeight: 'bold', backgroundColor: '#fff', marginTop: '-65px' }}>
					<SideBar mobileOpen={this.state.mobileOpen} handleDrawerToggle={this.handleDrawerToggle} userProfileData={userProfileData} >
						{children}
					</SideBar>
				</div>
				<div className={classes.root}>
					<Row className={classes.tableDiv} >
						<Col className={classes.search} sm={10} xs={10} md={10} lg={11} xl={11}>
							<SearchBar value={searchText} onRequestSearch={(e) => this.searchResults(e, searchText)} onChange={(e) => this.handleSearchText(e, 'searchText')} style={{ borderRadius: '40px', height: '40px', fontWeight: 'bold', boxShadow: 'none', marginBottom: '8px' }} />
						</Col>
						<Col className={'filter'} sm={2} md={2} xs={2} lg={1} xl={1}>
							<i className="fa fa-filter" aria-hidden="true" onClick={(e) => this.handleModalOpen(e)}></i>
						</Col>
					</Row>
					<div key={'List'} className={classes.tableDiv}>
						<List className={classes.root2} >
							{
								filterPrescriptionData != undefined && filterPrescriptionData.length > 0 ? filterPrescriptionData.map((data, index) => {
									console.log('dataaaa', data);
									return <div key={index}>
										<ListItem onClick={(e) => this.handleOnClick(e, data._id)} className={classes.listitems} >
											<ListItemText primary={<Typography style={{ color: '#42425E', fontWeight: 'bold', fontSize: '12px' }}>{data && data.doctor && data.doctor.firstname && data.doctor.firstname != '' ? 'Dr.  ' + data.doctor.firstname + ' ' + data.doctor.lastname : '--'}</Typography>} secondary={<React.Fragment>
												<Typography variant="h5" className={classes.presName} style={{ color: '#42425E', fontWeight: 'bold', fontSize: '18px', padding: '5px 0 5px 0' }}>{data.title ? data.title : '--'}</Typography>

												<Typography variant="h5" style={{ color: '#646464', fontWeight: 'bold', fontSize: '12px' }} className={classes.presName}>Prescription No.: {data.prescriptionNo && data.prescriptionNo ? data.prescriptionNo : ''}</Typography>
											</React.Fragment>} />
											<ListItemText style={{
												textAlign: 'right',
												right: '20px', marginTop: '-24px'
											}} primary={<Typography style={{ color: '#42425E', fontWeight: 'bold', fontSize: '12px' }}>{data && data != null && data.createdAt && data.createdAt != null && data.createdAt ? moment(data.createdAt).format("D MMMM YYYY") : ''}</Typography>} />
										</ListItem>
										<li className={classes.listdivide}></li>
									</div>
								}) : searchText !== '' && searchResults && searchResults.length > 0 ?
										searchResults.map((data, index) => {
											console.log('searchdata ', data);
											return <div key={index}>
												<ListItem onClick={(e) => this.handleOnClick(e, data.prescriptions._id)} className={classes.listitems} >
													<ListItemText primary={<Typography style={{ color: '#42425E', fontWeight: 'bold', fontSize: '12px' }}  >{data && data.doctorDetails && data.doctorDetails.length > 0 && data.doctorDetails[0].firstname && data.doctorDetails[0].firstname != '' ? 'Dr. ' + data.doctorDetails[0].firstname + ' ' + data.doctorDetails[0].lastname : '--'}</Typography>} secondary={<React.Fragment>
														<Typography variant="h5" className={classes.presName} style={{ color: '#42425E', fontWeight: 'bold', fontSize: '18px', padding: '5px 0 5px 0' }}>{data && data.prescriptions && data.prescriptions && data.prescriptions.title ? data.prescriptions.title : '--'}</Typography>
														<Typography variant="h5" style={{ color: '#646464', fontWeight: 'bold', fontSize: '12px' }} className={classes.presName}>Prescription No.: {data && data.prescriptions && data.prescriptions && data.prescriptions.prescriptionNo ? data.prescriptions.prescriptionNo : '--'}</Typography>
													</React.Fragment>} />
													<ListItemText style={{
														position: 'fixed',
														right: '20px', marginTop: '-24px'
													}} primary={<Typography style={{ color: '#42425E', fontWeight: 'bold', fontSize: '12px' }}>{data && data != null && data.prescriptions.createdAt && data.prescriptions.createdAt != null && data.prescriptions.createdAt ? moment(data.prescriptions.createdAt).format("D MMMM YYYY") : ''}</Typography>} />
												</ListItem>
												<li className={classes.listdivide}></li>
											</div>
										}) : searchText != '' && searchResults.length == 0 ? <div><Typography style={{
											backgroundColor: '#fff', color: 'gray', fontWeight: 'bold', fontSize: '25px',
										}}>No Medical History</Typography></div> : searchText == '' && prescriptions && prescriptions.length > 0 ?
												prescriptions.map((data, index) => {
													console.log('dataaaa', data);
													return <div key={index}>
														<ListItem onClick={(e) => this.handleOnClick(e, data._id)} className={classes.listitems} >
															<ListItemText primary={<Typography style={{ color: '#42425E', fontWeight: 'bold', fontSize: '12px' }}>{data && data.doctor && data.doctor.firstname && data.doctor.firstname != '' ? 'Dr.  ' + data.doctor.firstname + ' ' + data.doctor.lastname : '--'}</Typography>} secondary={<React.Fragment>
																<Typography variant="h5" className={classes.presName} style={{ color: '#42425E', fontWeight: 'bold', fontSize: '18px', padding: '5px 0 5px 0' }}>{data.title ? data.title : '--'}</Typography>

																<Typography variant="h5" style={{ color: '#646464', fontWeight: 'bold', fontSize: '12px' }} className={classes.presName}>Prescription No.: {data.prescriptionNo && data.prescriptionNo ? data.prescriptionNo : ''}</Typography>
															</React.Fragment>} />
															<ListItemText style={{
																textAlign: 'right',
																right: '20px', marginTop: '-24px'
															}} primary={<Typography style={{ color: '#42425E', fontWeight: 'bold', fontSize: '12px' }}>{data && data != null && data.createdAt && data.createdAt != null && data.createdAt ? moment(data.createdAt).format("D MMMM YYYY") : ''}</Typography>} />
														</ListItem>
														<li className={classes.listdivide}></li>
													</div>
												}) : <div><Typography style={{
													backgroundColor: '#fff', color: 'gray', fontWeight: 'bold', fontSize: '25px',
												}}>No Medical History</Typography></div>
							}
						</List>
					</div>
				</div>
				<Dialog
					open={open}
					onClose={(e) => this.handleClearFilter(e)}
					scroll={this.state.scroll}
					aria-labelledby="scroll-dialog-title"
				>
					<DialogTitle id="scroll-dialog-title">Filter</DialogTitle>
					<DialogContent dividers={this.state.scroll === 'paper'} className={classes.dailogContentText}>
						<FilterComponent ref={this.filterRef} filterFor={'Category'} prescriptions={categoryPrescriptions} filterStateValue={this.state.filterStateValue} />
					</DialogContent>
					<DialogActions>
						<Button variant="contained" onClick={(e) => this.handleClearFilter(e)} color="primary">
							Remove Filter
						</Button>
						<Button variant="contained" onClick={(e) => this.handleApplyFilter(e)} color="primary">
							Apply Filter
						</Button>
					</DialogActions>
				</Dialog>
			</React.Fragment>);
	}
}

Prescription.propTypes = {
	classes: PropTypes.object.isRequired,
};

const mapStateToProps = ({ authUser, prescriptionReducer }) => {
	const { userProfileData } = authUser
	const { filterPrescriptionData } = prescriptionReducer
	return { userProfileData, filterPrescriptionData }
}

export default connect(mapStateToProps, { getuserProfileData, filterPrescription })(withStyles(styles)(Prescription));