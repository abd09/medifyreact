import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import { Redirect } from 'react-router-dom';
import Box from '@material-ui/core/Box';
import Link from '@material-ui/core/Link';

const styles = theme => ({
    main: {
        marginTop: theme.spacing.unit * 8,
        width: 'auto',
        display: 'block',
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3,
        [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
            width: 400,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    heading: {
        color: '#43415e',
    },
    paper: {
        marginTop: theme.spacing.unit * 8,
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
    },
    heading2: {
        marginTop: '25px',
        fontSize: '17px',
        /* color: '#A6A7AD' */
        color: '#808188'
    },
    avatar: {
        height: "25px",
        width: "114px"
    },
    form: {
        width: '100%',
        marginTop: theme.spacing.unit,
    },
    label: {
        color: '#43415e',
        "&$focused": {
            color: '#43415e'
        }
    },
    signup: {
        backgroundColor: '#fff',
        border: '1px solid #43415e',
        color: '#43415e',
        marginTop: theme.spacing.unit * 3,
        padding: '10px 50px'
    },
    submit: {
        backgroundColor: '#43415e',
        color: '#fff',
        marginTop: theme.spacing.unit * 3,
        padding: '10px 78px',
        fontSize: '16px',
        textTransform: 'none'
    },
    error: {
        color: 'red',
        textAlign: 'left'
    }
});

class Category extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email: '',
            password: '',
            confirm: '',
            mobile: '',
            firstname: '',
            lastname: '',
            userType: '',
            aadharNo: '',
            checkedB: false,
            errors: {},
        }
        this.onSubmit = this.onSubmit.bind(this);
    }

    onSubmit = async (e) => {
        e.preventDefault()
        let postData = {
            email: this.state.email,
            password: this.state.password,
            mobile: this.state.mobile,
            username: this.state.username,
            name: this.state.name
        }
    }

    handleChange = (key) => {
        console.log("userType.....", key)
        this.setState({ userType: key })
    }
    renderRedirect = () => {
        if (this.state.userType == '') {
            return <Redirect to='/forgotPassword' />
        } else {
            return <Redirect to={`/forgotPassword/${this.state.userType}`} />
        }
    }

    render() {
        const { classes } = this.props;
        console.log('this.state.userType', this.state.userType);
        return (
            <main className={classes.main}>
                {this.renderRedirect()}
                <CssBaseline />
                <Typography className={classes.heading} component="h1" variant="h5">
                    <img className={classes.avatar} src={require('../../assets/img/logo.png')} />
                </Typography>
                <Typography className={classes.heading2} component="h1" variant="h5">
                    Please select the category you represent. </Typography>
                <form className={classes.form} onSubmit={(e) => this.onSubmit(e)}>
                    <Button
                        type="submit"
                        variant="contained"
                        style={{ padding: '10px 91px' }}
                        value={'doctor'}
                        name={'userType'}
                        className={classes.submit}
                        onClick={(e) => this.handleChange('doctor')}
                    >Doctor</Button>
                    <br />
                    <Button
                        type="submit"
                        variant="contained"
                        value={'pharmacy'}
                        name={'userType'}
                        className={classes.submit}
                        onClick={(e) => this.handleChange('pharmacy')}
                    >Pharmacy</Button>
                    <br />
                    <Button
                        type="submit"
                        variant="contained"
                        name={'userType'}
                        style={{ padding: '10px 89px' }}

                        className={classes.submit}
                        value={'user'}
                        onClick={(e) => this.handleChange('user')}
                    >Patient</Button>
                </form>
                <Box mt={5} style={{ marginTop: '150px' }}>
                   {/*  <Typography variant="body2" align="center" className={classes.heading} style={{ fontSize: '11px' }}>
                        {'Terms of use. Privacy policy'}
                    </Typography> */}
                      <Link color="inherit" className={classes.heading} style={{ fontSize: '12px' }} target={'_blank'} href="http://medify.net/page-privacy.html">Terms of use. Privacy policy</Link>
                </Box>
            </main>
        );
    }
}

Category.propTypes = {
    classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Category);