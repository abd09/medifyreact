import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import FormControl from '@material-ui/core/FormControl';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Input from '@material-ui/core/Input';
import { Col, Row } from 'reactstrap';
import InputLabel from '@material-ui/core/InputLabel';
import Typography from '@material-ui/core/Typography';
import withStyles from '@material-ui/core/styles/withStyles';
import Checkbox from '@material-ui/core/Checkbox';
import Box from '@material-ui/core/Box';
import { createMuiTheme } from "@material-ui/core/styles";
import { ThemeProvider } from "@material-ui/styles";
import Link from '@material-ui/core/Link';
import { Redirect } from 'react-router-dom';
import { signin } from '../../actions';
import { connect } from 'react-redux';
import { reduxForm } from 'redux-form'
import axios from '../../constant/axios';
import moment from 'moment';

const styles = theme => ({
    main: {
        width: 'auto',
        display: 'block',
        marginTop: theme.spacing.unit * 8,
        marginLeft: theme.spacing.unit * 3,
        marginRight: theme.spacing.unit * 3,
        [theme.breakpoints.up(400 + theme.spacing.unit * 3 * 2)]: {
            width: 400,
            marginLeft: 'auto',
            marginRight: 'auto',
        },
    },
    heading: {
        color: '#43415e',
    },
    paper: {

        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        padding: `${theme.spacing.unit * 2}px ${theme.spacing.unit * 3}px ${theme.spacing.unit * 3}px`,
    },
    heading2: {
        marginTop: '25px',
        fontSize: '16px',
        /*  color: '#A6A7AD' */
        color: '#808188'
    },
    avatar: {
        height: "25px",
        width: "114px"
    },
    form: {
        width: '100%',
        marginTop: theme.spacing.unit,
    },
    label: {
        color: '#43415e',
        "&$focused": {
            color: '#43415e'
        }
    },
    signup: {
        backgroundColor: '#fff',
        border: '1px solid #43415e',
        color: '#43415e',
        marginTop: theme.spacing.unit * 4,
        padding: '10px 50px',
        textTransform: 'none'
    },
    submit: {
        backgroundColor: '#43415e',
        color: '#fff',
        marginTop: theme.spacing.unit * 2,
        padding: '10px 60px',
        textTransform: 'none'
    },
    error: {
        color: 'red',
        textAlign: 'left'
    },
    heading1: {
        marginTop: '29px',
        marginLeft: '0px',
        marginRight: '0px'
    },
    media: {
        ['@media (max-width:787px)']: {
            paddingLeft: '0px !important',
        },
    }
});

class SignIn extends Component {
    constructor(props) {
        super(props)
        this.state = {
            email: '',
            password: '',
            checkedB: localStorage.getItem('rememberMe') && localStorage.getItem('rememberMe') == 'true' ? true : false,
            errors: {},
            redirect: false,
            redirect2: false
        }
        this.onSubmit = this.onSubmit.bind(this);
        /*  this.handleValidation = this.handleValidation.bind(this); */
    }

    /*  handleValidation = () => {
         let errors = {};
         let formIsValid = true;
         console.log("this.state handleValidation", this.state)
         if (this.state.email === '') {
             formIsValid = false;
             errors["email"] = "This field is required";
         }
         if (this.state.password === '') {
             formIsValid = false;
             errors["password"] = "This field is required";
         }
         this.setState({ errors: errors });
         return formIsValid;
     } */

    componentWillMount() {
        console.log("localStorage.getItem('token')", localStorage.getItem('token'));

        axios.get(`/users/userDetails`, {
            headers: {
                'Content-Type': 'application/json',
                'token': localStorage.getItem('token')
            }
        }).then(result => {
            console.log("result..sdasdasdsa..", result.data.detail);
            this.setState({ email: result.data.detail && localStorage.getItem('rememberMe') == 'true' ? result.data.detail.email : '', password: result.data.detail && localStorage.getItem('rememberMe') == 'true' ? localStorage.getItem('ref') : '' })
        })
            .catch(error => {
                console.log("error....", error);
            })
    }
    onSubmit = async (e) => {
        e.preventDefault()
        let postData = {
            email: this.state.email,
            password: this.state.password,
            role: this.props.match.params.category,
            rememberMe: this.state.checkedB
        }
        this.props.signin({ data: postData, history: this.props.history });
        /*  let isValid = await this.handleValidation();
         if (isValid === true) {
             this.props.signin({ data: postData, history: this.props.history });
         } else {
             return;
         } */
    }

    renderRedirect = () => {
        if (this.state.redirect) {
            return <Redirect to='/category' />
        }
    }

    setRedirect = () => {
        this.setState({
            redirect: true
        })
    }

    handleChange = (e, key) => {
        console.log("e.....", key)
        if (key == 'checkedB') {
            this.setState({ [key]: e.target.checked })
        } else {
            this.setState({ [key]: e.target.value })
        }
    }

    render() {
        const { classes } = this.props;
        const theme = createMuiTheme({
            palette: {
                primary: { main: '#43415e' },
                secondary: { main: '#43415e' },
            },
            overrides: {
                MuiInputLabel: {
                    root: {
                        color: "#43415e",
                        "&$focused": {
                            color: "#43415e"
                        }
                    }
                },
                MuiInputBase: {
                    root: {
                        color: "#43415e",
                        "&$focused": {
                            color: "#43415e"
                        }
                    }
                },
                MuiFormLabel: {
                    root: {
                        color: "#43415e",
                        "&$focused": {
                            color: "#43415e"
                        }
                    }
                },
                MuiInputBase: {
                    root: {
                        padding: '8px 0 7px'
                    }
                },
                MuiInput: {
                    root: {
                        color: '#43415e',
                        borderColor: '#fff'
                    },
                    underline: {
                        color: '#43415e',
                        "&$focused": {
                            color: "#43415e",
                        },
                        "&:before": {
                            color: "#43415e",
                            borderBottom: '1px solid rgba(231, 231, 231, 1)'
                        },
                        borderBottom: '1px solid rgba(231, 231, 231, 1)'
                    }
                },
                MuiTypography: {
                    body1: {
                        fontSize: '15px'
                    },
                }
            }
        });

        return (
            <main className={classes.main}>
                {this.renderRedirect()}
                <CssBaseline />
                <div className="container">
                    <Typography className={classes.heading} component="h1" variant="h5">
                        <img className={classes.avatar} src={require('../../assets/img/logo.png')} />
                    </Typography>
                    <Typography className={classes.heading2} component="h1" variant="h5">
                        Welcome back! Please login to your account. </Typography>
                    <Row>
                        <Col md={12} sm={12} xs={12} lg={12} xl={12}>
                            <form className={classes.form} onSubmit={(e) => this.onSubmit(e)}>
                                <ThemeProvider theme={theme}>
                                    <Col md={12} sm={12} xs={12} lg={12} xl={12}>
                                        <FormControl margin="normal" fullWidth>
                                            <InputLabel htmlFor="email" className={classes.label}>Email</InputLabel>
                                            <Input value={this.state.email} id="email" type="email" name="email" onChange={(e) => this.handleChange(e, 'email')} required={true} />
                                        </FormControl>
                                        <span className={classes.error} >{this.state.errors["email"]}</span>
                                    </Col>
                                    <Col md={12} sm={12} xs={12} lg={12} xl={12}>
                                        <FormControl margin="normal" fullWidth>
                                            <InputLabel htmlFor="password" className={classes.label}>Password</InputLabel>
                                            <Input value={this.state.password} name="password" onChange={(e) => this.handleChange(e, 'password')} type="password" id="password" required={true} />
                                        </FormControl>
                                        <span className={classes.error} >{this.state.errors["password"]}</span>
                                    </Col>
                                    <Row>
                                        <Col md={6} sm={6} xs={6} lg={6} xl={6} className={`${classes.media}`} >
                                            <FormControlLabel className={`${classes.heading}`} style={{ marginRight: '0px', marginTop: '20px', fontSize: '15px' }}
                                                control={
                                                    <Checkbox
                                                        checked={this.state.checkedB}
                                                        onChange={(e) => this.handleChange(e, 'checkedB')}
                                                        value="checkedB"
                                                        color="primary"
                                                    />
                                                }
                                                label="Remember me"
                                            />
                                        </Col>
                                        <Col md={6} sm={6} xs={6} lg={6} xl={6} className={`${classes.media}`}  >
                                            <FormControlLabel className={`${classes.heading} ${classes.heading1}`}
                                                control={
                                                    <Link color="inherit" className={classes.heading} style={{ fontSize: '15px' }} href="/medify/medifyweb/#/forgotPassword">Forgot Password?</Link>
                                                    /* <Link color="inherit" className={classes.heading} style={{ fontSize: '15px' }} href="/medifyweb/#/forgotPassword">Forgot Password?</Link> */
                                                }
                                            />
                                        </Col>
                                    </Row>
                                </ThemeProvider>
                                <br />
                                <Button
                                    type="submit"
                                    variant="contained"
                                    className={classes.submit}
                                    style={{ fontSize: '16px' }}
                                >Login</Button>
                                <br />
                                <Button
                                    variant="contained"
                                    className={classes.signup}
                                    onClick={(e) => this.setRedirect(e)}
                                    style={{ fontSize: '16px' }}
                                > Sign Up</Button>
                                <Box mt={5} style={{ marginTop: '50px' }}>
                                    <Link color="inherit" className={classes.heading} style={{ fontSize: '12px' }} target={'_blank'} href="http://medify.net/page-privacy.html">Terms of use. Privacy policy</Link>
                                    {/* <Typography variant="body2" align="center" className={classes.heading} style={{ fontSize: '12px' }}>
                                        {'Terms of use. Privacy policy'}
                                    </Typography> */}
                                </Box>
                            </form>
                        </Col>
                    </Row>
                </div>
            </main>
        );
    }
}

SignIn.propTypes = {
    classes: PropTypes.object.isRequired,
};

SignIn = reduxForm({
    form: 'signInValidation'  // a unique identifier for this form
})(SignIn)

SignIn = connect(
    state => ({
        initialValues: state // pull initial values from account reducer
    }),
    { signin }               // bind account loading action creator
)(SignIn)


export default withStyles(styles)(SignIn);