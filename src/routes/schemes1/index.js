import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { Col, Row } from 'reactstrap';
import SideBar from '../../components/userSideBar'
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import { Button, FormGroup, Label, Input } from 'reactstrap';
import AppBar from '@material-ui/core/AppBar';
import { EditorState, convertToRaw, convertFromRaw } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import ReactDOM from 'react-dom';
import TinyMCE from 'react-tinymce';
import appConfig from '../../constant/config';

import { red } from '@material-ui/core/colors';

import { getuserProfileData, cropImage, addScheme } from '../../actions';
import { connect } from 'react-redux';

const drawerWidth = 240;
const styles = theme => ({
    root: {
        flexGrow: 1
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120
    },
    lable: {
        textAlign: 'left'
    },
    selectEmpty: {
        marginTop: theme.spacing(2)
    },
    appBar: {
        marginLeft: drawerWidth,
        [theme.breakpoints.up('sm')]: {
            width: `calc(100% - ${drawerWidth}px)`,
        },
    },
    menuButton: {
        marginRight: 20,
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
    dialogDone: {
        backgroundColor: appConfig.colors.darkGrey,
        color: "#3ddb20",
        '&:hover': {
            background: 'none',
            backgroundColor: appConfig.colors.darkGrey,
            color: '#3ddb20'
        },
        borderColor: appConfig.colors.darkGrey
    },
    dialogTitle: {
        backgroundColor: '#5a5a5a',
        '& h2': {
            color: 'white',
            textAlign: 'center'
        },
        borderBottom: '1px solid #5a5a5a'
    },
    dialogContent: {
        backgroundColor: '#5a5a5a',
    },
    root2: {
        flexGrow: 1,
        backgroundColor: appConfig.colors.white
    },
    tabsRoot: {
        color: '#42425E',
        borderBottom: '1px solid #7a7a7a',
    },
    tabsIndicator: {
        backgroundColor: '#7a7a7a',
        height: '3px'
    },
    rightDiv: {
        color: "#CCCACD",
        marginBottom: 0,
        fontWeight: 'bold',
        fontSize: '16px',
        textAlign: '-webkit-center',
    },
    listitems: {
        paddingLeft: 40,
        paddingRight: 10,
        paddingTop: 5,
        paddingBottom: 5
    },
    listitems1: {
        paddingLeft: 40,
        paddingRight: 10,
        paddingTop: 14,
        paddingBottom: 5
    },
    signup: {
        backgroundColor: appConfig.colors.primary,
        color: appConfig.colors.white,
        marginTop: '5px',
        padding: '5px 22px',
        textTransform: 'none',
        borderRadius: '5px',
    },
    tabRoot: {
        textTransform: 'initial',
        minWidth: '50%',
        fontWeight: theme.typography.fontWeightRegular,
        fontFamily: [
            '-apple-system',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ].join(','),
    },
    typography: {
        padding: '15px 0px',
        color: '#CCCACD',
        ['@media (max-width:787px)']: {
            padding: '15px 15px',
        },
    },
    input: {
        '::-webkit-input-placeholder': {
            color: 'red'
        }
    },
    imageDiv: {
        height: '70px',
        width: '70px',
    },
    avatar: {
        margin: theme.spacing(2),
        marginLeft: theme.spacing(4),
        width: 70,
        height: 70
    },
    profile: {
        textAlign: '-webkit-center',
        margin: theme.spacing.unit * 3,
        lineHeight: 'normal'
    },
    tableDiv: {
        padding: '0px',
    },
    tabsFlex: {
        borderBottom: '0px solid #454545',
    },
    tableThTd: {
        color: appConfig.colors.white,
    },
    infoDiv: {
        padding: 18,
        backgroundColor: appConfig.colors.white,
    },
    disableClick: {
        pointerEvents: 'none'
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: appConfig.colors.white,
        '&:hover': {
            backgroundColor: appConfig.colors.white,
        },
        marginRight: theme.spacing(2),
        marginLeft: 0,
        width: '100%',
        borderRadius: '40px',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(3),
            width: 'auto',
        },
    },
    searchIcon: {
        width: theme.spacing(7),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        fontWeight: 'bold',
        opacity: 1
    },
    inputRoot: {
        color: '#c6c6c6',
    },
    inputInput: {
        color: '#c6c6c6',
        padding: theme.spacing(1, 1, 1, 7),
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: 200,
        },
    },
    icon: {
        position: 'fixed',
        right: '10px',
        color: appConfig.colors.white
    },
    iconHover: {
        margin: theme.spacing(2),
        '&:hover': {
            color: red[800],
        },
    },
    avatar: {
        margin: 0,
        padding: 0,
    },
    avatarlist: {
        margin: 0,
        padding: 0,
        width: 35,
        height: 35,
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        borderRadius: '8px',
        color: appConfig.colors.primary
    },
    dense: {
        marginTop: theme.spacing(2),
    },
    menu: {
        width: 200,
    },
    submit: {
        backgroundColor: appConfig.colors.primary,
        color: appConfig.colors.white,
        marginTop: theme.spacing(1),
        padding: '5px 10px',
        textTransform: 'none',
        borderRadius: '8px'
    },
    submit2: {
        backgroundColor: appConfig.colors.white,
        color: appConfig.colors.primary,
        border: 'none',
        marginTop: theme.spacing(2),
        textTransform: 'none',
        fontWeight: 'bolder',
        borderRadius: '0',
        '&:hover': {
            backgroundColor: appConfig.colors.white,
            color: appConfig.colors.primary,
        },
    },
    form: {
        width: '100%',
        paddingLeft: '20px',
        paddingRight: '35px',
        marginTop: theme.spacing.unit,
    },
});

class EditPharmacyDetail extends Component {
    constructor(props) {
        super(props)
        console.log('hey  i m here', this.props.location)
        this.state = {
            uploaded_image_path: '',
            certificate_image: '',
            value: 0,
            openModal: false,
            isLoading: true,

            alert: false,
            mobileOpen: false,

            errors: {},
            schemes: '',
            title: '',
            editorState: EditorState.createEmpty(),
        };
        this.handleEditorChange = this.handleEditorChange.bind(this);

    }

    componentWillMount() {
        this.props.getuserProfileData({ history: this.props.history });
    }

    componentWillReceiveProps = (nextProps) => {
        console.log("this.props.userProfileData", nextProps.profileImage)
        if (this.props.userProfileData !== nextProps.userProfileData) {
            console.log(' nextProps.availability', nextProps.userProfileData.availability);

            this.setState({
                title: nextProps.userProfileData.schemes
            })
        }
        if (this.props.profileImage !== nextProps.profileImage) {
            this.setState({ croppedImage: nextProps.profileImage })
        }
    }
    onEditorStateChange = (editorState) => {
        this.setState({
            editorState,
            title: draftToHtml(convertToRaw(editorState.getCurrentContent()))
        });
    };



    handleEditorChange(e) {
        console.log('e.target.getContent()', e.target.getContent());
        this.setState({
            title: e.target.getContent()
        })
    }

    handleChange = (event, key) => {
        console.log('key', key)
        this.setState({ [key]: event.target.value });
    };

    handleDrawerToggle = () => {
        this.setState(state => ({ mobileOpen: !state.mobileOpen }));
    };

    handleModalClose = () => {
        this.setState({ openModal: false, errors: {} });
    }

    openUrl = (e, url) => {
        e.preventDefault();
        window.open(url);
    }

    saveProfile = (e) => {
        e.preventDefault();
        let data = {
            title: this.state.title,
            pharmacy: this.props.userProfileData._id
        }
        console.log("this.state.profileimageMain", data)
        this.props.addScheme({
            data,
            history: this.props.history
        })
    }

    render() {
        const { classes, userProfileData, children } = this.props;
        const { title, editorState } = this.state;

        return (
            <React.Fragment>
                <AppBar position="fixed" className={classes.appBar}>
                    <Toolbar>
                        <IconButton
                            color="inherit"
                            aria-label="Open drawer"
                            onClick={this.handleDrawerToggle}
                            className={classes.menuButton}>
                            <MenuIcon />
                        </IconButton>
                        <Typography variant="h6" color="inherit" style={{ fontWeight: 'bold', color: appConfig.colors.white }} noWrap>
                            {"Schemes"}
                        </Typography>
                    </Toolbar>
                </AppBar>
                <div className='page-content' style={{ fontWeight: 'bold', backgroundColor: appConfig.colors.white, marginTop: '-56px', borderRight: '1px solid #655368' }}>
                    <SideBar mobileOpen={this.state.mobileOpen} handleDrawerToggle={this.handleDrawerToggle} userProfileData={userProfileData} >
                        {children}
                    </SideBar>
                </div>
                <div className={classes.root}>
                    <div className={classes.container}>
                        <Col xs="12" md="12" sm="12" >

                            <div className={classes.profile}>
                                <div className={classes.tableDiv}>
                                    <form className={classes.form} onSubmit={(e) => this.saveProfile(e)}>
                                        <FormGroup className={`${classes.lable} mt-4`}>
                                            <Label for="schemes" style={{ color: appConfig.colors.primary, fontSize: '16px' }}>Schemes / Discounts:</Label>
                                            <Editor
                                                style={{ borderColor: appConfig.colors.primary, fontSize: '18px', color: appConfig.colors.primary }}
                                                toolbar={{

                                                    image: { inDropdown: false },
                                                    inline: { inDropdown: true },
                                                    list: { inDropdown: true },
                                                    textAlign: { inDropdown: true },
                                                    link: { inDropdown: false },
                                                    history: { inDropdown: true },
                                                    colorPicker: {
                                                        className: 'editor-color',
                                                        colors: ['rgb(97,189,109)', 'rgb(26,188,156)', 'rgb(84,172,210)', 'rgb(44,130,201)',
                                                            'rgb(147,101,184)', 'rgb(71,85,119)', 'rgb(204,204,204)', 'rgb(65,168,95)', 'rgb(0,168,133)',
                                                            'rgb(61,142,185)', 'rgb(41,105,176)', 'rgb(85,57,130)', 'rgb(40,50,78)', 'rgb(0,0,0)',
                                                            'rgb(247,218,100)', 'rgb(251,160,38)', 'rgb(235,107,86)', 'rgb(226,80,65)', 'rgb(163,143,132)',
                                                            'rgb(239,239,239)', 'rgb(255,255,255)', 'rgb(250,197,28)', 'rgb(243,121,52)', 'rgb(209,72,65)',
                                                            'rgb(184,49,47)', 'rgb(124,112,107)', 'rgb(209,213,216)'],
                                                    },
                                                }}
                                                wrapperClassName="demo-wrapper"
                                                editorClassName="demo-editor"
                                                defaultEditorState={editorState}
                                                onEditorStateChange={this.onEditorStateChange}
                                            />
                                            
                                        </FormGroup>

                                        <Col md={12} sm={12} xs={12} style={{ textAlign: 'right' }}>
                                            <Button variant="outlined" className={classes.submit} type="submit" >
                                                Submit </Button>
                                        </Col>
                                    </form>
                                </div>
                            </div>
                        </Col>
                    </div>
                </div>
            </React.Fragment >);
    }
}

EditPharmacyDetail.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = ({ authUser }) => {
    const { userProfileData, profileImage } = authUser
    return { userProfileData, profileImage }
}

export default connect(mapStateToProps, { getuserProfileData, cropImage, addScheme })(withStyles(styles)(EditPharmacyDetail));