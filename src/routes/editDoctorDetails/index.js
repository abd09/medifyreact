import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import { Col, Row } from 'reactstrap';
import SideBar from '../../components/userSideBar'
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import { Button, FormGroup, Label, Input } from 'reactstrap';
import AppBar from '@material-ui/core/AppBar';
import { Cropper } from 'react-image-cropper'
import SweetAlert from 'react-bootstrap-sweetalert'
import TextField from '@material-ui/core/TextField';
import MatButton from '@material-ui/core/Button';
import Autocomplete from 'react-google-autocomplete';
import appConfig from '../../constant/config';
import Checkbox from '@material-ui/core/Checkbox';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import {
    isMobile
} from "react-device-detect";
import { red } from '@material-ui/core/colors';

import axios from '../../constant/axios';
import { getuserProfileData, cropImage, getSpecialities } from '../../actions';
import { connect } from 'react-redux';

const drawerWidth = 240;
const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120
    },
    lable: {
        textAlign: 'left'
    },
    selectEmpty: {
        marginTop: theme.spacing(2)
    },
    appBar: {
        marginLeft: drawerWidth,
        [theme.breakpoints.up('sm')]: {
            width: `calc(100% - ${drawerWidth}px)`,
        },
    },
    menuButton: {
        marginRight: 20,
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
    dialogDone: {
        backgroundColor: appConfig.colors.darkGrey,
        color: "#3ddb20",
        '&:hover': {
            background: 'none',
            backgroundColor: appConfig.colors.darkGrey,
            color: '#3ddb20'
        },
        borderColor: appConfig.colors.darkGrey
    },
    dialogTitle: {
        backgroundColor: '#5a5a5a',
        '& h2': {
            color: 'white',
            textAlign: 'center'
        },
        borderBottom: '1px solid #5a5a5a'
    },
    dialogContent: {
        backgroundColor: '#5a5a5a',
    },
    root2: {
        flexGrow: 1,
        backgroundColor: '#fff'
    },
    tabsRoot: {
        color: '#42425E',
        borderBottom: '1px solid #7a7a7a',
    },
    tabsIndicator: {
        backgroundColor: '#7a7a7a',
        height: '3px'
    },
    rightDiv: {
        color: "#CCCACD",
        marginBottom: 0,
        fontWeight: 'bold',
        fontSize: '16px',
        textAlign: '-webkit-center',
    },
    listitems: {
        paddingLeft: 40,
        paddingRight: 10,
        paddingTop: 5,
        paddingBottom: 5
    },
    listitems1: {
        paddingLeft: 40,
        paddingRight: 10,
        paddingTop: 14,
        paddingBottom: 5
    },
    tabRoot: {
        textTransform: 'initial',
        minWidth: '50%',
        fontWeight: theme.typography.fontWeightRegular,
        fontFamily: [
            '-apple-system',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ].join(','),
    },
    typography: {
        padding: '15px 0px',
        color: '#CCCACD',
        ['@media (max-width:787px)']: {
            padding: '15px 15px',
        },
    },
    input: {
        '::-webkit-input-placeholder': {
            color: 'red'
        },
        borderColor: appConfig.colors.primary,
        fontSize: '18px'
    },
    imageDiv: {
        height: '70px',
        width: '70px',
    },
    avatar: {
        margin: theme.spacing(2),
        marginLeft: theme.spacing(4),
        width: 70,
        height: 70
    },
    profile: {
        textAlign: '-webkit-center',
        margin: theme.spacing.unit * 3,
        lineHeight: 'normal'
    },
    tableDiv: {
        padding: '0px',
    },
    tabsFlex: {
        borderBottom: '0px solid #454545',
    },
    tableThTd: {
        color: '#ffffff',
    },
    infoDiv: {
        padding: 18,
        backgroundColor: "#fff"
    },
    disableClick: {
        pointerEvents: 'none'
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: '#ffffff',
        '&:hover': {
            backgroundColor: '#ffffff',
        },
        marginRight: theme.spacing(2),
        marginLeft: 0,
        width: '100%',
        borderRadius: '40px',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(3),
            width: 'auto',
        },
    },
    searchIcon: {
        width: theme.spacing(7),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        fontWeight: 'bold',
        opacity: 1
    },
    inputRoot: {
        color: '#c6c6c6',
    },
    inputInput: {
        color: '#c6c6c6',
        padding: theme.spacing(1, 1, 1, 7),
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: 200,
        },
    },
    icon: {
        position: 'fixed',
        right: '10px',
        color: '#fff'
    },
    iconHover: {
        margin: theme.spacing(2),
        '&:hover': {
            color: red[800],
        },
    },
    avatar: {
        margin: 0,
        padding: 0,
    },
    avatarlist: {
        margin: 0,
        padding: 0,
        width: 35,
        height: 35,
    },
    signup: {
        backgroundColor: appConfig.colors.primary,
        color: '#fff',
        marginTop: '5px',
        padding: '5px 22px',
        textTransform: 'none',
        borderRadius: '5px',
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        borderRadius: '8px',
        color: appConfig.colors.primary
    },
    dense: {
        marginTop: theme.spacing(2),
    },
    menu: {
        width: 200,
    },
    submit: {
        backgroundColor: appConfig.colors.primary,
        color: '#fff',
        marginTop: theme.spacing(1),
        padding: '5px 10px',
        textTransform: 'none',
        borderRadius: '8px'
    },
    submit2: {
        backgroundColor: '#fff',
        color: appConfig.colors.primary,
        border: 'none',
        marginTop: theme.spacing(2),
        textTransform: 'none',
        fontWeight: 'bolder',
        borderRadius: '0',
        '&:hover': {
            backgroundColor: '#ffffff',
            color: appConfig.colors.primary,
        },
    },
    form: {
        width: '100%',
        paddingLeft: '20px',
        paddingRight: '35px',
        marginTop: theme.spacing.unit,
    },
    lableStyle: {
        color: appConfig.colors.primary,
        fontSize: '16px'
    },
    inputStyle: {
        borderColor: appConfig.colors.primary,
        fontSize: '18px'
    }
});

class EditDoctorDetail extends Component {
    constructor(props) {
        super(props)
        this.state = {
            uploaded_image_path: '',
            certificate_image: '',
            value: 0,
            openModal: false,
            isLoading: true,
            prescription: '',
            pharmacy: '',
            prescriptions: [],
            note: '',
            addmoreText: [{
                location: '', days: [
                    { name: 'Sunday', value: 'Sunday', isSelected: false },
                    { name: 'Monday', value: 'Monday', isSelected: false },
                    { name: 'Tuesday', value: 'Tuesday', isSelected: false },
                    { name: 'Wednesday', value: 'Wednesday', isSelected: false },
                    { name: 'Thursday', value: 'Thursday', isSelected: false },
                    { name: 'Friday', value: 'Friday', isSelected: false },
                    { name: 'Saturday', value: 'Saturday', isSelected: false }
                ], from: '', to: ''
            }],
            tool: false,
            croppedImage: '',
            profileimage: '',
            imagePreview: '',
            profileimageMain: '',
            image: '',
            imageLoaded: false,
            alert: false,
            mobileOpen: false,
            isChatEnabled: false,
            age: 0,
            firstname: '',
            lastname: '',
            mobile: '',
            gender: '',
            address: '',
            waitingTime: '',
            fees: '',
            experience: '',
            speciality: '',
            serviceProvided: '',
            dob: '',
            licenseNo: '',
            imagesArray: [],
            imageArray: [],
            errors: {},
            qualification: '',
            isPhoneEnabled: ''
        };
    }

    componentWillMount() {
        this.props.getuserProfileData({ history: this.props.history });
        axios.get('/users/prescriptionsById', {
            headers: {
                'Content-Type': 'application/json',
                'token': localStorage.getItem('token')
            }
        }).then(result => {
            console.log("getPrescriptionsById result###", result.data.detail);
            this.setState({ prescriptions: result.data.detail && result.data.detail.length > 0 ? result.data.detail : [] })
        })
            .catch(error => {
                console.log("error....", error);
            });
        this.props.getSpecialities({ history: this.props.history })
    }

    componentWillReceiveProps = (nextProps) => {
        console.log("this.props.userProfileData", nextProps)
        if (this.props.userProfileData !== nextProps.userProfileData) {
            console.log(' nextProps.availability', nextProps.userProfileData.availability);
            this.setState({
                croppedImage: nextProps.profileImage,
                firstname: nextProps.userProfileData.firstname,
                lastname: nextProps.userProfileData.lastname,
                mobile: nextProps.userProfileData.mobile,
                diseases: nextProps.userProfileData.diseases,
                surgeries: nextProps.userProfileData.surgeries,
                others: nextProps.userProfileData.others, addmoreText: (nextProps.userProfileData.availability.length === 0 ? [{
                    location: '', days: [
                        { name: 'Sunday', value: 'Sunday', isSelected: false },
                        { name: 'Monday', value: 'Monday', isSelected: false },
                        { name: 'Tuesday', value: 'Tuesday', isSelected: false },
                        { name: 'Wednesday', value: 'Wednesday', isSelected: false },
                        { name: 'Thursday', value: 'Thursday', isSelected: false },
                        { name: 'Friday', value: 'Friday', isSelected: false },
                        { name: 'Saturday', value: 'Saturday', isSelected: false }
                    ], from: '', to: ''
                }] : nextProps.userProfileData.availability),
                age: nextProps.userProfileData.age,
                dob: nextProps.userProfileData.dob,
                licenseNo: nextProps.userProfileData.licenseNo,
                address: nextProps.userProfileData.address ? nextProps.userProfileData.address : '',
                gender: nextProps.userProfileData.gender, lifeStyle: nextProps.userProfileData.lifeStyle, habits: nextProps.userProfileData.habits, deliveryTime: nextProps.userProfileData.deliveryTime, deliveryDistance: nextProps.userProfileData.deliveryDistance, isDeliveryEnabled: nextProps.userProfileData.isDeliveryEnabled,
                imageArray: nextProps.userProfileData.certificates,
                schemes: nextProps.userProfileData.schemes,
                terms_policy: nextProps.userProfileData.terms_policy,
                speciality: nextProps.userProfileData.speciality && nextProps.userProfileData.speciality._id ? nextProps.userProfileData.speciality._id : '',
                serviceProvided: nextProps.userProfileData.serviceProvided,
                experience: nextProps.userProfileData.experience,
                fees: nextProps.userProfileData.fees,
                waitingTime: nextProps.userProfileData.waitingTime,
                imageArray: nextProps.userProfileData.certificates,
                isChatEnabled: nextProps.userProfileData.isChatEnabled,
                isPhoneEnabled: nextProps.userProfileData.isPhoneEnabled,
                lat: nextProps.userProfileData.geoLocation && nextProps.userProfileData.geoLocation.coordinates && nextProps.userProfileData.geoLocation.coordinates.length > 0 ? nextProps.userProfileData.geoLocation.coordinates[1] : '',
                lng: nextProps.userProfileData.geoLocation && nextProps.userProfileData.geoLocation.coordinates && nextProps.userProfileData.geoLocation.coordinates.length > 0 ? nextProps.userProfileData.geoLocation.coordinates[0] : '',
                qualification: nextProps.userProfileData.qualification
            })
        }
        if (this.props.profileImage !== nextProps.profileImage) {
            this.setState({ croppedImage: nextProps.profileImage })
        }
    }

    getCity = (addressArray) => {
        console.log('addressArray', addressArray);

        let city = '';
        if (addressArray && addressArray.length > 0) {
            for (let i = 0; i < addressArray.length; i++) {
                if (addressArray[i].types[0] && 'administrative_area_level_2' === addressArray[i].types[0]) {
                    city = addressArray[i].long_name;
                    return city;
                }
            }
        }

    };
    /**
      * Get the area and set the area input value to the one selected
      *
      * @param addressArray
      * @return {string}
      */
    getArea = (addressArray) => {
        let area = '';
        if (addressArray && addressArray.length > 0) {
            for (let i = 0; i < addressArray.length; i++) {
                if (addressArray[i].types[0]) {
                    for (let j = 0; j < addressArray[i].types.length; j++) {
                        if ('sublocality_level_1' === addressArray[i].types[j] || 'locality' === addressArray[i].types[j]) {
                            area = addressArray[i].long_name;
                            return area;
                        }
                    }
                }
            }
        }
    };
    handleCheck = (e, index, locationI) => {
        e.preventDefault();
        let tempPermission = this.state.addmoreText[locationI].days;
        console.log('tempPermission', tempPermission);
        console.log('index', index);
        tempPermission[index].isSelected = !tempPermission[index].isSelected;
        this.setState({ options: tempPermission })
        var oldState = [...this.state.addmoreText];


        oldState.map((data, i) => {
            console.log('locationI', locationI, i);

            if (locationI == i) {
                oldState[i]['days'] = tempPermission;
            }
        })
        console.log('oldState', oldState);
        this.setState({ addmoreText: oldState });
    }
    /**
      * Get the address and set the address input value to the one selected
      *
      * @param addressArray
      * @return {string}
      */
    getState = (addressArray) => {
        let state = '';
        if (addressArray && addressArray !== undefined && addressArray.length > 0) {
            for (let i = 0; i < addressArray.length; i++) {
                for (let i = 0; i < addressArray.length; i++) {
                    if (addressArray[i].types[0] && 'administrative_area_level_1' === addressArray[i].types[0]) {
                        state = addressArray[i].long_name;
                        return state;
                    }
                }
            }
        }
    };

    SelectedPlace = (place, index) => {
        console.log('place', place, index);
        const address = place.formatted_address ? place.formatted_address : place.name;
        console.log('address', address);

        const addressArray = place.address_components ? place.address_components : '',
            city = addressArray !== '' ? this.getCity(addressArray) : '',
            area = addressArray !== '' ? this.getArea(addressArray) : '',
            state = addressArray !== '' ? this.getState(addressArray) : '',
            latValue = place.geometry && place.geometry.location ? place.geometry.location.lat() : 0,
            lngValue = place.geometry && place.geometry.location ? place.geometry.location.lng() : 0;
        var oldState = [...this.state.addmoreText];
        oldState.map((data, i) => {
            if (index == i) {
                oldState[i]['location'] = address;
            }
        })
        this.setState({ addmoreText: oldState });
        this.setState({
            address: (address) ? address : '',
            area: (area) ? area : '',
            city: (city) ? city : '',
            state: (state) ? state : '',
            lat: latValue !== 0 ? latValue : '',
            lng: lngValue !== 0 ? lngValue : '',
        })
    }


    addMoreItems = (e) => {
        e.preventDefault()
        let addmoreText = this.state.addmoreText;
        addmoreText.push({
            location: '', days: [
                { name: 'Sunday', value: 'Sunday', isSelected: false },
                { name: 'Monday', value: 'Monday', isSelected: false },
                { name: 'Tuesday', value: 'Tuesday', isSelected: false },
                { name: 'Wednesday', value: 'Wednesday', isSelected: false },
                { name: 'Thursday', value: 'Thursday', isSelected: false },
                { name: 'Friday', value: 'Friday', isSelected: false },
                { name: 'Saturday', value: 'Saturday', isSelected: false }
            ], from: '', to: ''
        });
        this.setState({ addmoreText: addmoreText })
    }

    handleItemChange = (e, name, ind) => {
        e.preventDefault()
        var oldState = [...this.state.addmoreText];
        oldState.map((data, i) => {
            if (ind == i) {
                oldState[i][name] = e.target.value;
            }
        })
        this.setState({ addmoreText: oldState });
    }
    removeHandler = (e, ind) => {
        e.preventDefault()
        var oldState = [...this.state.addmoreText];
        var newState = []
        oldState.map((data, i) => {
            if (ind !== i) {
                newState.push(data)
            }
        })
        this.setState({ addmoreText: newState });
    }

    handleChange = (event, key) => {
        console.log('key', key)
        this.setState({ [key]: event.target.value });
        if (key === 'dob') {
            var today = new Date();
            var birthDate = new Date(event.target.value);  // create a date object directly from `dob1` argument
            var age_now = today.getFullYear() - birthDate.getFullYear();
            var m = today.getMonth() - birthDate.getMonth();
            if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) {
                age_now--;
            }
            console.log('age_now', age_now)
            if (age_now === NaN) {
                this.setState({ age: 0 });
            } else {
                this.setState({ age: age_now });
            }

        }

    };

    handleUploadCertificateChange = name => event => {
        console.log('name=', name, event.target.files[0])
        let arr = [...this.state.imageArray];
        let imagearr = [...this.state.imagesArray];
        console.log("this.state imagearr", imagearr)
        if (name === 'certificate_image') {
            let errors = {};
            let formIsValid = true;
            let validExtension = false;
            if (event.target.files[0]) {
                var file = event.target.files[0];
                var reader = new FileReader();
                var url = reader.readAsDataURL(file);
                let name = event.target.files[0].name
                reader.onloadend = function (e) {
                    if (reader.result.substring("data:image/".length, reader.result.indexOf(";base64")) === 'jpeg' || reader.result.substring("data:image/".length, reader.result.indexOf(";base64")) === 'png') {
                        arr.push(name);
                        imagearr.push(reader.result);
                        this.setState({
                            imageArray: arr,
                            imagesArray: imagearr,
                        });
                    } else {
                        formIsValid = false;
                        errors['certificates'] = "Only jpeg and png file supported."
                        this.setState({ errors: errors });
                    }
                    return formIsValid;
                }.bind(this);
            }
        }
    };

    handleDrawerToggle = () => {
        this.setState(state => ({ mobileOpen: !state.mobileOpen }));
    };

    handleModalClose = () => {
        this.setState({ openModal: false, errors: {} });
    }

    saveProfile = (e) => {
        e.preventDefault();
        console.log("this.state", this.state.imagesArray)
        let data = {
            profileimageMain: this.state.profileimageMain,
            availability: this.state.addmoreText,
            firstname: this.state.firstname,
            lastname: this.state.lastname,
            address: this.state.address,
            mobile: this.state.mobile,
            age: this.state.age,
            gender: this.state.gender,
            speciality: this.state.speciality,
            serviceProvided: this.state.serviceProvided,
            experience: this.state.experience,
            fees: this.state.fees,
            waitingTime: this.state.waitingTime,
            certificates: this.state.imagesArray,
            lat: this.state.lat,
            lng: this.state.lng,
            habits: this.state.habits,
            lifeStyle: this.state.lifeStyle,
            isChatEnabled: this.state.isChatEnabled,
            dob: this.state.dob,
            licenseNo: this.state.licenseNo,
            qualification: this.state.qualification,
            isPhoneEnabled: this.state.isPhoneEnabled
        }
        console.log("this.state.profileimageMain", data)
        this.props.cropImage(data);
        this.setState({ imagesArray: [] });
    }

    openUrl = (e, url) => {
        e.preventDefault();
        window.open(url);
    }

    onConfirm(key, image) {
        let node = this[image]
        console.log('nodes=', node.crop())
        let img = node.crop()
        this.setState({ [key]: false, croppedImage: img, profileimageMain: img })
        console.log('image=', this.state)
    }

    onCancel(key) {
        console.log("key", key)
        this.setState({ [key]: false })
    }

    handleImageLoaded(state) {
        this.setState({
            [state + 'Loaded']: true
        })
    }

    openAlert(e, key) {
        this.setState({ images: e.target.files, image: '' })
        console.log('files=', e.target.files[0], e.target.files)
        var file = e.target.files[0];
        var reader = new FileReader();
        var url = reader.readAsDataURL(file);
        console.log('url=,', url)
        reader.onloadend = function (e) {
            this.setState({
                imagePreview: [reader.result]
            })
        }.bind(this);
        console.log('url=,', url)
        this.setState({ [key]: true });
        console.log('files=', e.target.files[0])
    }

    uploadFile(e) {
        e.preventDefault();
        this.setState({ tool: true })
    }

    openImageUploader() {
        this.setState({ tool: false, errors: {} })
        this.refs.imageUploader.click();
    }

    openCertificateImageUploader() {
        this.setState({ tool: false, errors: {} })
        this.refs.CertificateImageUploader.click();
    }

    removeProfilePhoto = () => {
        this.setState({ profileimageMain: null, croppedImage: null, tool: false });
    }

    render() {
        const { classes, userProfileData, children, specialityList } = this.props;
        const { tool, alert, firstname, lastname, mobile, age, address, fees, waitingTime, experience, speciality, serviceProvided, isPhoneEnabled, isChatEnabled, dob, licenseNo, qualification } = this.state;
        console.log('specialityList', specialityList)
        return (
            <React.Fragment>
                <AppBar position="fixed" className={classes.appBar}>
                    <Toolbar>
                        <IconButton
                            color="inherit"
                            aria-label="Open drawer"
                            onClick={this.handleDrawerToggle}
                            className={classes.menuButton} >
                            <MenuIcon />
                        </IconButton>
                        <Typography variant="h6" color="inherit" style={{ fontWeight: 'bold', color: '#fff' }} noWrap>
                            {"Edit Profile"}
                        </Typography>
                    </Toolbar>
                </AppBar>
                <div className='page-content' style={{ fontWeight: 'bold', backgroundColor: '#fff', marginTop: '-56px', borderRight: '1px solid #655368' }}>
                    <SideBar mobileOpen={this.state.mobileOpen} handleDrawerToggle={this.handleDrawerToggle} userProfileData={userProfileData} >
                        {children}
                    </SideBar>
                </div>
                <div className={classes.root}>
                    <div className={classes.container}>
                        <Col xs="12" md="12" sm="12" >
                            <div className={classes.rightDiv}>
                                {
                                    this.state.croppedImage &&
                                    <Avatar alt='Profile' src={this.state.croppedImage ? this.state.croppedImage : require('../../assets/img/noImage.png')} className={classes.imageDiv} onClick={(e) => this.uploadFile(e)}>
                                    </Avatar>
                                }
                                {(this.state.croppedImage == undefined || this.state.croppedImage == "") &&
                                    <Avatar alt='Profile' src={require('../../assets/img/noImage.png')} className={classes.imageDiv} onClick={(e) => this.uploadFile(e)}>
                                    </Avatar>
                                }
                                <input type="file" id="file" ref="imageUploader" accept="image/*" style={{ display: "none" }} onChange={(e) => this.openAlert(e, 'alert')} />
                                <SweetAlert
                                    btnSize="sm"
                                    show={tool}
                                    showCancel
                                    cancelBtnBsStyle="danger"
                                    title="Choose an option"
                                    confirmBtnStyle={{ display: 'none' }}
                                    onConfirm={() => this.onConfirm()}
                                    onCancel={() => this.onCancel('tool')}>
                                    <MatButton variant="contained" color="primary" onClick={this.removeProfilePhoto}>Remove Image</MatButton>
                                    <MatButton variant="contained" style={{ backgroundColor: '#58de45', marginLeft: 20 }} onClick={() => this.openImageUploader()}>Select Image</MatButton>
                                </SweetAlert>
                            </div>
                            <div className={classes.profile}>
                                <SweetAlert
                                    style={{ position: 'absolute' }}
                                    btnSize="sm"
                                    show={alert}
                                    showCancel
                                    cancelBtnBsStyle="danger"
                                    title="Image Cropper"
                                    onConfirm={() => this.onConfirm('alert', 'image')}
                                    onCancel={() => this.onCancel('alert')}>
                                    {
                                        this.state.imagePreview &&
                                        <Cropper src={this.state.imagePreview[0]} newItem={300} width={300} height={300}
                                            ref={ref => { this.image = ref }}
                                            fixedRatio={true}
                                            onImgLoad={() => this.handleImageLoaded('image')}
                                        />
                                    }
                                </SweetAlert>
                            </div>
                            <div className={classes.profile}>
                                <div className={classes.tableDiv}>
                                    <form className={classes.form} onSubmit={(e) => this.saveProfile(e)}>
                                        <FormGroup className={classes.lable}>
                                            <Label for="firstname" style={{ color: appConfig.colors.primary, fontSize: '16px' }}>First name:</Label>
                                            <Input type="text" style={{ borderColor: appConfig.colors.primary, fontSize: '18px' }} value={firstname} name="firstname" id="firstname" placeholder="First name" onChange={(e) => this.handleChange(e, 'firstname')} />
                                        </FormGroup>
                                        <FormGroup className={classes.lable}>
                                            <Label for="name" style={{ color: appConfig.colors.primary, fontSize: '16px' }}>Last name:</Label>
                                            <Input type="text" style={{ borderColor: appConfig.colors.primary, fontSize: '18px' }} value={lastname} name="name" id="name" placeholder="Last name" onChange={(e) => this.handleChange(e, 'lastname')} />
                                        </FormGroup>
                                        <FormGroup className={classes.lable}>
                                            <Label for="name" style={{ color: appConfig.colors.primary, fontSize: '16px' }}>Date of Birth:</Label>
                                            <Input type="date" style={{ borderColor: appConfig.colors.primary, fontSize: '18px' }} value={dob} name="dob" id="dob" placeholder="DOB" maxDate={new Date()} onChange={(e) => this.handleChange(e, 'dob')} />
                                        </FormGroup>
                                        <FormGroup className={classes.lable}>
                                            <Label for="name" style={{ color: appConfig.colors.primary, fontSize: '16px' }}>Age:</Label>
                                            <Input type="number" style={{ borderColor: appConfig.colors.primary, fontSize: '18px' }} value={age} name="age" id="age" placeholder="Age" onChange={(e) => this.handleChange(e, 'age')} readOnly />
                                        </FormGroup>
                                        <FormGroup className={classes.lable}>
                                            <Label for="name" style={{ color: appConfig.colors.primary, fontSize: '16px' }}>Gender:</Label>
                                            <div>
                                                <FormGroup check style={{ display: 'inline-block' }}>
                                                    <Label check style={{ color: appConfig.colors.primary, fontSize: '16px' }}>
                                                        <Input type="radio" style={{ marginTop: '.1rem' }} name="gender" value="female" onChange={(e) => this.handleChange(e, 'gender')} checked={this.state.gender == 'female'} />{' '}Female </Label>
                                                </FormGroup>
                                                <FormGroup check style={{ display: 'inline-block', marginLeft: '15px' }}>
                                                    <Label check style={{ color: appConfig.colors.primary, fontSize: '16px' }}>
                                                        <Input style={{ marginTop: '.1rem' }} onChange={(e) => this.handleChange(e, 'gender')} type="radio" name="gender" value="male" checked={this.state.gender == 'male'} />{' '}Male </Label>
                                                </FormGroup>
                                                <FormGroup check style={{ display: 'inline-block', marginLeft: '15px' }}>
                                                    <Label check style={{ color: appConfig.colors.primary, fontSize: '16px' }}>
                                                        <Input style={{ marginTop: '.1rem' }} onChange={(e) => this.handleChange(e, 'gender')} type="radio" name="gender" value="other" checked={this.state.gender == 'other'} />{' '}Other </Label>
                                                </FormGroup>
                                            </div>
                                        </FormGroup>
                                        {/* <FormGroup className={classes.lable}>
                                            <Label for="location" style={{ color: appConfig.colors.primary, fontSize: '16px' }}>Clinic Location:</Label>
                                            <Autocomplete
                                                className="form-control"
                                                style={{ borderColor: appConfig.colors.primary, fontSize: '18px' }} placeholder="Address" name="address"
                                                onChange={(e) => this.handleChange(e, 'address')}
                                                onPlaceSelected={(place) => this.SelectedPlace(place)}
                                                types={['(cities)']}
                                                fields={['address_components', 'geometry.location', 'place_id', 'formatted_address', 'name', 'vicinity']}
                                                value={address}
                                                componentRestrictions={{ country: "in" }}
                                            />
                                            {this.state.lat !== '' && this.state.lng !== '' ?
                                                <Button
                                                    variant="contained"
                                                    className={classes.signup} onClick={(e) => this.openUrl(e, `http://www.google.com/maps/place/${this.state.lat},${this.state.lng}`)}
                                                > View location</Button>
                                                : ''}
                                        </FormGroup> */}
                                        <FormGroup className={classes.lable}>
                                            <Label for="mobile" style={{ color: appConfig.colors.primary, fontSize: '16px' }}>Phone Number:</Label>
                                            <Input type="text" style={{ borderColor: appConfig.colors.primary, fontSize: '18px' }} value={mobile} name="mobile" id="mobile" onChange={(e) => this.handleChange(e, 'mobile')}
                                                placeholder="Contact" />
                                        </FormGroup>
                                        <FormGroup className={classes.lable}>
                                            <Label for="email" style={{ color: appConfig.colors.primary, fontSize: '16px' }}>Email:</Label>
                                            <Input type="email" style={{ borderColor: appConfig.colors.primary, fontSize: '18px' }} value={userProfileData ? userProfileData.email : ''} name="email" id="email"
                                                placeholder="Email" disabled />
                                        </FormGroup>
                                        <FormGroup className={classes.lable}>
                                            <Label for="licenseNo" style={{ color: appConfig.colors.primary, fontSize: '16px' }}>License Number:</Label>
                                            <Input type="licenseNo" style={{ borderColor: appConfig.colors.primary, fontSize: '18px' }} onChange={(e) => this.handleChange(e, 'licenseNo')} value={licenseNo} name="licenseNo" id="licenseNo"
                                                placeholder="License number" />
                                        </FormGroup>
                                        <FormGroup className={classes.lable}>
                                            <Label for="qualification" style={{ color: appConfig.colors.primary, fontSize: '16px' }}>Qualifications:</Label>
                                            <Input type="qualification" style={{ borderColor: appConfig.colors.primary, fontSize: '18px' }} onChange={(e) => this.handleChange(e, 'qualification')} value={qualification} name="qualification" id="qualification"
                                                placeholder="Qualifications" />
                                        </FormGroup>
                                        <FormGroup className={classes.lable}>
                                            <Label for="speciality" style={{ color: appConfig.colors.primary, fontSize: '16px' }}>Speciality:</Label>
                                            <Input type="select" name="speciality" id="speciality" onChange={(e) => this.handleChange(e, 'speciality')} className={classes.input} value={speciality} style={{ borderColor: appConfig.colors.primary, fontSize: '18px' }} >
                                                <option value="" >Select Speciality</option>
                                                {specialityList && specialityList.length > 0 ?
                                                    specialityList.map((data, index) => {
                                                        return <option value={data && data._id ? data._id : ''} key={data._id}>{data && data.title ? data.title : ''}</option>
                                                    }) : ''
                                                }
                                            </Input>
                                        </FormGroup>
                                        <FormGroup className={classes.lable}>
                                            <Label for="isPhoneEnabled" style={{ color: appConfig.colors.primary, fontSize: '16px' }}>Display phone number:</Label>
                                            <Input type="select" value={isPhoneEnabled} name="isPhoneEnabled" id="isPhoneEnabled" onChange={(e) => this.handleChange(e, 'isPhoneEnabled')} style={{ borderColor: appConfig.colors.primary, fontSize: '18px' }}>
                                                <option value="" >Select Status</option>
                                                <option value={true}>Enable</option>
                                                <option value={false}>Disable</option>
                                            </Input>
                                        </FormGroup>
                                        <FormGroup className={classes.lable}>
                                            <Label for="isChatEnabled" style={{ color: appConfig.colors.primary, fontSize: '16px' }}>Chat Status:</Label>
                                            <Input type="select" value={isChatEnabled} name="isChatEnabled" id="isChatEnabled" onChange={(e) => this.handleChange(e, 'isChatEnabled')} style={{ borderColor: appConfig.colors.primary, fontSize: '18px' }}>
                                                <option value={true}>Enabled</option>
                                                <option value={false}>Disabled</option>
                                            </Input>
                                        </FormGroup>
                                        <FormGroup className={classes.lable}>
                                            <Label for="serviceProvided" style={{ color: appConfig.colors.primary, fontSize: '16px' }}>Services Provided:</Label>
                                            <Input type="textarea" style={{ borderColor: appConfig.colors.primary, fontSize: '18px' }} onChange={(e) => this.handleChange(e, 'serviceProvided')} value={serviceProvided} className={classes.input} placeholder="Services Provided" name="serviceProvided" id="serviceProvided" />
                                        </FormGroup>
                                        <React.Fragment>
                                            <Row>
                                                <Col md={4} sm={4} xs={12} >
                                                    <FormGroup className={classes.lable}>
                                                        <Label for="experience" className={classes.lableStyle}>Experience:</Label>
                                                        <Input type="number" onChange={(e) => this.handleChange(e, 'experience')} value={experience} className={classes.input} placeholder="Experience" name="experience" id="experience" />
                                                    </FormGroup>
                                                </Col>
                                                <Col md={4} sm={4} xs={12} >
                                                    <FormGroup className={classes.lable}>
                                                        <Label for="fees" className={classes.lableStyle}>Fees:</Label>
                                                        <Input type="number" min='1' onChange={(e) => this.handleChange(e, 'fees')} value={fees} className={classes.input} placeholder="Fees" name="fees" id="fees" />
                                                    </FormGroup>
                                                </Col>
                                                <Col md={4} sm={4} xs={12} >
                                                    <FormGroup className={classes.lable}>
                                                        <Label for="waitingTime" className={classes.lableStyle}>Waiting Time:</Label>
                                                        <Input type="text" onChange={(e) => this.handleChange(e, 'waitingTime')} value={waitingTime} className={classes.input} placeholder="Waiting Time" name="waitingTime" id="waitingTime" />
                                                    </FormGroup>
                                                </Col>
                                            </Row>
                                        </React.Fragment>
                                        {
                                            (this.state.imageArray.length > 0) ?
                                                this.state.imageArray.map((image, key) =>
                                                    <React.Fragment key={key + 'certificateImage'}>
                                                        <Row>
                                                            <Col sm={12}>
                                                                <TextField
                                                                    id="certificate_image"
                                                                    fullWidth
                                                                    style={{ color: appConfig.colors.primary, marginTop: '8px', padding: '10px 14px !important' }}
                                                                    value=
                                                                    {
                                                                        image ? image : ''} disabled
                                                                    margin="normal"
                                                                    variant="outlined" />
                                                                {
                                                                    this.state.imageArray.length === (key + 1) ? <span style={{ color: 'red', float: 'left' }} >{this.state.errors["certificates"]}</span> : ''
                                                                }
                                                            </Col>
                                                            {
                                                                this.state.imageArray.length === (key + 1) ? <Col sm={2}>
                                                                    <input type="file" id="certificate_image" ref="CertificateImageUploader" name="certificate_image" onChange={this.handleUploadCertificateChange('certificate_image')} style={{ display: 'none' }} />
                                                                    <Button className={classes.submit} style={{ borderRadius: '11px', margin: 10 }} onClick={() => this.openCertificateImageUploader()} >Upload More</Button>
                                                                </Col> : ''
                                                            }
                                                        </Row>
                                                    </React.Fragment>) :
                                                <React.Fragment>
                                                    <Row>
                                                        <Col sm={10}>
                                                            <TextField
                                                                id="certificate_image"
                                                                fullWidth
                                                                style={{ color: appConfig.colors.primary, marginTop: '8px', padding: '10px 14px !important' }}
                                                                value=
                                                                {''} disabled
                                                                margin="normal"
                                                                variant="outlined"
                                                            />
                                                            <span style={{ color: 'red', float: 'left' }} >{this.state.errors["certificates"]}</span>
                                                        </Col>
                                                        <Col sm={2}>
                                                            <input type="file" id="certificate_image" ref="CertificateImageUploader" name="certificate_image" onChange={this.handleUploadCertificateChange('certificate_image')} style={{ display: 'none' }} />
                                                            <Button className={classes.submit} style={{ borderRadius: '11px' }} onClick={() => this.openCertificateImageUploader()} >Upload Certificates</Button>

                                                        </Col>
                                                    </Row>
                                                </React.Fragment>
                                        }
                                        {isMobile ? '' :
                                            <div>
                                                <Col md={3} sm={3} xs={12} style={{ display: 'inline-block', paddingLeft: '0px', paddingRight: '0px', }} >
                                                    <Label for="location" style={{ color: appConfig.colors.primary, fontSize: '16px' }}>Location:</Label>
                                                </Col>
                                                <Col md={3} sm={3} xs={12} style={{ display: 'inline-block' }} >
                                                    <Label for="day" style={{ color: appConfig.colors.primary, fontSize: '16px', paddingLeft: '0px', paddingRight: '0px' }}>Day:</Label>
                                                </Col>
                                                <Col md={3} sm={3} xs={12} style={{ display: 'inline-block' }}>
                                                    <Label for="email" style={{ color: appConfig.colors.primary, fontSize: '16px', paddingLeft: '0px', paddingRight: '0px' }}>From:</Label>
                                                </Col>
                                                <Col md={3} sm={3} xs={12} style={{ display: 'inline-block', paddingRight: '0px' }}>
                                                    <Label for="to" style={{ color: appConfig.colors.primary, fontSize: '16px', paddingLeft: '0px', paddingRight: '0px' }}>To:</Label>
                                                </Col>
                                            </div>}
                                        {this.state.addmoreText.map((data, index) => {
                                            console.log('data!!!!', data);
                                            return <div key={index}>
                                                <Col md={3} sm={3} xs={12} style={{ display: 'inline-block', paddingLeft: '0px', paddingRight: '0px' }} >
                                                    <FormGroup>
                                                        {isMobile ? <Label for="location" style={{ color: appConfig.colors.primary, fontSize: '16px' }}>Location:</Label> : ''}
                                                        <Autocomplete
                                                            className="form-control"
                                                            style={{ borderColor: appConfig.colors.primary, fontSize: '18px' }} onChange={(e) => this.handleItemChange(e, 'location', index)}
                                                            onPlaceSelected={(place) => this.SelectedPlace(place, index)}
                                                            types={['(regions)']}

                                                            value={data.location}
                                                            componentRestrictions={{ country: "in" }}
                                                        />
                                                        {/*  <Input type="text" style={{ borderColor: appConfig.colors.primary, fontSize: '18px' }} className={classes.input} value={data.location} placeholder="Location" onChange={(e) => this.handleItemChange(e, 'location', index)} name="location" id={`location${index}`} /> */}
                                                    </FormGroup>
                                                </Col>
                                                <Col md={3} sm={3} xs={12} style={{ display: 'inline-block', paddingLeft: '0px', paddingRight: '0px' }} >
                                                    <FormGroup style={{marginBottom:'0'}}>
                                                        {isMobile ? <Label for="day" style={{ color: appConfig.colors.primary, fontSize: '16px' }}>Day:</Label> : ''}

                                                        <Row>
                                                            {
                                                                (data.days !== undefined && data.days.length > 0) ? data.days.map((value, key) => {
                                                                    console.log('value', value)
                                                                    return <Col xs={6} md={6} sm={6} xl={6} lg={6}>
                                                                        <FormControlLabel
                                                                            style={{ color: appConfig.colors.primary }}
                                                                            control={
                                                                                <Checkbox
                                                                                    checked={value.isSelected}
                                                                                    onChange={(e) => this.handleCheck(e, key, index)}
                                                                                    value={value.value}
                                                                                    color="primary"
                                                                                />
                                                                            }
                                                                            label={value.name}
                                                                        />
                                                                    </Col>
                                                                }) : ''
                                                            }
                                                        </Row>

                                                        {/*  <Input type="select" value={data.day} name="day" id="day" onChange={(e) => this.handleItemChange(e, 'day', index)} style={{ borderColor: appConfig.colors.primary, fontSize: '18px' }}>
                                                            <option value="" >Select day</option>
                                                            <option value={'Sunday'}>{'Sunday'}</option>
                                                            <option value={'Monday'}>{'Monday'}</option>
                                                            <option value={'Tuesday'}>{'Tuesday'}</option>
                                                            <option value={'Wednesday'}>{'Wednesday'}</option>
                                                            <option value={'Thursday'}>{'Thursday'}</option>
                                                            <option value={'Friday'}>{'Friday'}</option>
                                                            <option value={'Saturday'}>{'Saturday'}</option>
                                                        </Input> */}
                                                    </FormGroup>
                                                </Col>
                                                <Col md={3} sm={3} xs={12} style={{ display: 'inline-block', paddingLeft: '0px', paddingRight: '0px' }}>
                                                    <FormGroup>
                                                        {isMobile ? <Label for="email" style={{ color: appConfig.colors.primary, fontSize: '16px' }}>From:</Label> : ''}
                                                        <Input type="select" name="from" id="from" onChange={(e) => this.handleItemChange(e, 'from', index)} style={{ borderColor: appConfig.colors.primary, fontSize: '18px' }} value={data.from} >
                                                            <option value="" >From Time</option>
                                                            <option value={'09:00 AM'}>{'09:00 AM'}</option>
                                                            <option value={'10:00 AM'}>{'10:00 AM'}</option>
                                                            <option value={'11:00 AM'}>{'11:00 AM'}</option>
                                                            <option value={'12:00 PM'}>{'12:00 PM'}</option>
                                                            <option value={'01:00 PM'}>{'01:00 PM'}</option>
                                                            <option value={'02:00 PM'}>{'02:00 PM'}</option>
                                                            <option value={'03:00 PM'}>{'03:00 PM'}</option>
                                                            <option value={'04:00 PM'}>{'04:00 PM'}</option>
                                                            <option value={'05:00 PM'}>{'05:00 PM'}</option>
                                                            <option value={'06:00 PM'}>{'06:00 PM'}</option>
                                                            <option value={'07:00 PM'}>{'07:00 PM'}</option>
                                                            <option value={'08:00 PM'}>{'08:00 PM'}</option>
                                                            <option value={'09:00 PM'}>{'09:00 PM'}</option>
                                                            <option value={'10:00 PM'}>{'10:00 PM'}</option>
                                                            <option value={'11:00 PM'}>{'11:00 PM'}</option>
                                                            <option value={'12:00 PM'}>{'12:00 PM'}</option>
                                                        </Input>
                                                    </FormGroup>
                                                </Col>
                                                <Col md={3} sm={3} xs={12} style={{ display: 'inline-block', paddingLeft: '0px', paddingRight: '0px' }}>
                                                    <FormGroup>
                                                        {isMobile ? <Label for="to" style={{ color: appConfig.colors.primary, fontSize: '16px' }}>To:</Label> : ''}
                                                        <Input type="select" name="to" id="to" onChange={(e) => this.handleItemChange(e, 'to', index)} style={{ borderColor: appConfig.colors.primary, fontSize: '18px' }} value={data.to} >
                                                            <option value="" >To Time</option>
                                                            <option value={'10:00 AM'}>{'10:00 AM'}</option>
                                                            <option value={'11:00 AM'}>{'11:00 AM'}</option>
                                                            <option value={'12:00 PM'}>{'12:00 PM'}</option>
                                                            <option value={'01:00 PM'}>{'01:00 PM'}</option>
                                                            <option value={'02:00 PM'}>{'02:00 PM'}</option>
                                                            <option value={'03:00 PM'}>{'03:00 PM'}</option>
                                                            <option value={'04:00 PM'}>{'04:00 PM'}</option>
                                                            <option value={'05:00 PM'}>{'05:00 PM'}</option>
                                                            <option value={'06:00 PM'}>{'06:00 PM'}</option>
                                                            <option value={'07:00 PM'}>{'07:00 PM'}</option>
                                                            <option value={'08:00 PM'}>{'08:00 PM'}</option>
                                                            <option value={'09:00 PM'}>{'09:00 PM'}</option>
                                                            <option value={'10:00 PM'}>{'10:00 PM'}</option>
                                                            <option value={'11:00 PM'}>{'11:00 PM'}</option>
                                                            <option value={'12:00 PM'}>{'12:00 PM'}</option>
                                                        </Input>
                                                    </FormGroup>
                                                </Col>
                                                {this.state.addmoreText.length > 1 &&
                                                    <FormGroup>
                                                        <span>
                                                            <Button className={classes.submit} style={{}} onClick={(e) => this.removeHandler(e, index)}>Remove </Button>
                                                        </span>
                                                    </FormGroup>
                                                }
                                            </div>
                                        })}
                                        <Col md={6} sm={6} xs={6} style={{ textAlign: 'left', display: 'inline-block' }}>
                                            <Button
                                                type="button"
                                                variant="contained" style={{ textAlign: 'left', display: 'inline-block' }}
                                                className={classes.submit} onClick={(e) => this.addMoreItems(e)}
                                            >Add More</Button>
                                        </Col>
                                        <Col md={6} sm={6} xs={6} style={{ textAlign: 'right', display: 'inline-block' }}>
                                            <Button variant="outlined" className={classes.submit} type="submit" >
                                                Submit </Button>
                                        </Col>
                                    </form>
                                </div>
                            </div>
                        </Col>
                    </div>
                </div>
            </React.Fragment>);
    }
}

EditDoctorDetail.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = ({ authUser, specialityReducer }) => {
    const { userProfileData, profileImage } = authUser
    const { specialityList } = specialityReducer
    return { userProfileData, profileImage, specialityList }
}

export default connect(mapStateToProps, { getuserProfileData, cropImage, getSpecialities })(withStyles(styles)(EditDoctorDetail));