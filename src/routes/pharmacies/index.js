import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import SearchBar from 'material-ui-search-bar'
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import AppBar from '@material-ui/core/AppBar';
import SideBar from '../../components/userSideBar'
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import ListItemText from '@material-ui/core/ListItemText';
import { getuserProfileData, getPharmacyData, getConnectData, searchPharmacyData } from '../../actions';
import axios from '../../constant/axios';
import { connect } from 'react-redux';
import AppConfig from '../../constant/config'
import Chat from '@material-ui/icons/Chat';
import { Col, Row } from 'reactstrap';

const drawerWidth = 240;
const styles = theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: '#f5f5f5 !important',
  },
  root2: {
    flexGrow: 1,
    backgroundColor: '#fff'
  },
  appBar: {
    marginLeft: drawerWidth,
    [theme.breakpoints.up('sm')]: {
      width: `calc(100% - ${drawerWidth}px)`,
    },
  },
  menuButton: {
    marginRight: 20,
    [theme.breakpoints.up('sm')]: {
      display: 'none',
    },
  },
  tabsRoot: {
    borderBottom: '1px solid #454545',
  },
  tabsIndicator: {
    backgroundColor: '#58de45',
    height: '4px'
  },
  listitems: {
    paddingLeft: 40,
    paddingRight: 20,
  },
  tabRoot: {
    textTransform: 'initial',
    minWidth: '50%',
    fontWeight: theme.typography.fontWeightRegular,
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
  },
  tabSelected: {},
  typography: {
    padding: '15px 0px',
    color: '#CCCACD',
    ['@media (max-width:787px)']: {
      padding: '15px 15px',
    },
  },
  avatar: {
    margin: theme.spacing.unit * 2,
    marginLeft: theme.spacing.unit * 4,
    width: 70,
    height: 70
  },
  profile: {
    marginTop: theme.spacing.unit * 4,
    lineHeight: 1
  },
  tableDiv: {
    padding: '0px',
  },
  tabsFlex: {
    borderBottom: '1px solid #454545',
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: '#ffffff',
    '&:hover': {
      backgroundColor: '#ffffff',
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: '100%',
    borderRadius: '40px',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(3),
      width: 'auto',
    },
  },
  searchIcon: {
    width: theme.spacing(7),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    fontWeight: 'bold',
    opacity: 1
  },
  inputRoot: {
    color: '#c6c6c6',
  },
  pharmacyName: {
    color: '#FFFFFF',
    font: 'bolder'
  },
  inputInput: {
    color: '#c6c6c6',
    padding: theme.spacing(1, 1, 1, 7),
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: 200,
    },
  },
  Listdivide: {
    marginLeft: '12px',
    marginRight: '12px',
    borderBottom: '1px solid #eeeeee'
  },
});

class Pharmacy extends Component {
  constructor(props) {
    super(props)
    console.log('hey  i m here', this.props.location)
    this.state = {
      mobileOpen: false,
      value: '',
      searchText: '',
      isLoading: true,
      connect: [],
      pharmacies: []
    };
  }

  componentWillMount() {
    this.props.getuserProfileData({ history: this.props.history });
    this.props.getPharmacyData({ history: this.props.history });

    this.props.getConnectData({ history: this.props.history });
  }

  handleChange = (event, value) => {
    console.log('value', value)
    this.setState({ value });
  };

  handleDrawerToggle = () => {
    this.setState(state => ({ mobileOpen: !state.mobileOpen }));
  };

  handleSearchText = (e, key) => {
    console.log('e searchText key', e.length, key);
    this.setState({ [key]: e })
    if (e.length > 0) {
      this.props.searchPharmacyData({ data: { searchText: e }, history: this.props.history });
    } else {
      this.props.getPharmacyData({ history: this.props.history });
    }

  }

  searchResults = (e, searchText) => {
    console.log('resultssss', searchText.length);
    if (searchText.length > 0) {
      this.props.searchPharmacyData({ data: { searchText: searchText }, history: this.props.history });
    } else {
      this.props.getPharmacyData({ history: this.props.history });
    }
  }

  handleOnClick = (e, id) => {
    e.preventDefault();
    this.props.history.push(`/${localStorage.getItem('role')}/pharmacyDetails/${id}`)
  }

  handleChat = (e, id) => {
    let data = {
      userOne: this.props.userProfileData._id,
      userTwo: id
    }
    axios.post(`/users/connectChat`, data, {
      headers: {
        'Content-Type': 'application/json',
        'token': localStorage.getItem('token')
      }
    }).then(result => {
      console.log("pharmacyDetails result^^^ doctor", result.data);
      if (result.data.error === false) {
        this.props.history.push(`/chat/${result.data.detail[0]._id}/${id}`)
      } else {

      }
    }).catch(error => {
      console.log("error....", error);
    });
  }

  render() {
    const { classes, userProfileData, children, pharmacyData, searchpharmacyData } = this.props;
    const { searchText, pharmacies } = this.state;

    return (
      <React.Fragment>
        <AppBar position="fixed" className={classes.appBar}>
          <Toolbar>
            <IconButton
              color="inherit"
              aria-label="Open drawer"
              onClick={this.handleDrawerToggle}
              className={classes.menuButton}>
              <MenuIcon />
            </IconButton>
            <Typography variant="h6" color="inherit" style={{ fontWeight: 'bold', color: '#fff' }} noWrap>
              {"Pharmacy"}
            </Typography>
          </Toolbar>
        </AppBar>
        <div className='page-content' style={{ fontWeight: 'bold', backgroundColor: '#fff', marginTop: '-56px' }}>
          <SideBar mobileOpen={this.state.mobileOpen} handleDrawerToggle={this.handleDrawerToggle} userProfileData={userProfileData} >
            {children}
          </SideBar>
        </div>
        <div className={classes.root}>
          <div className={classes.tableDiv} >
            <Typography className={classes.typography}>
              {<div className={classes.search}>
                {<SearchBar value={searchText} onRequestSearch={(e) => this.searchResults(e, searchText)} onChange={(e) => this.handleSearchText(e, 'searchText')} style={{ borderRadius: '40px', height: '40px', fontWeight: 'bold', boxShadow: 'none', marginBottom: '8px' }} autoFocus />}
              </div>}
            </Typography>
          </div>
          <div key={'List'} className={classes.tableDiv}>
            <List className={classes.root2} >

              {
                searchText != '' && searchpharmacyData && searchpharmacyData.length > 0 ?
                  searchpharmacyData.map((data, index) => {
                    console.log('dataa&&&&', data);
                    return <Row key={index}>
                      <Col sm={12} xs={12} lg={12} md={12}>
                        <Row style={{ alignItems: 'center' }} >
                          <Col sm={10} xs={10} lg={11} md={11}>
                            <ListItem onClick={(e) => this.handleOnClick(e, data._id)} className={classes.listitems} >
                              <ListItemText primary={<Typography style={{ color: AppConfig.colors.secondary, fontWeight: 'bold', fontSize: '12px' }}>ID: {data && data != null && data.userId ? data.userId : '--'}</Typography>} secondary={<React.Fragment>
                                <Typography variant="body1" style={{ color: AppConfig.colors.secondary, fontWeight: 'bold', fontSize: '18px', padding: '5px 0 5px 0' }} className={classes.pharmacyName}>{data && data != null && data.firstname && data.firstname != null ? data.firstname + ' ' + data.lastname : ''} <Typography style={{ color: AppConfig.colors.success, fontWeight: 'bold', fontSize: '12px' }}>{data.connected ? 'Connected' : ''}</Typography></Typography>
                              </React.Fragment>} />

                            </ListItem>
                          </Col>
                          {
                            (data && data.isChatEnabled === true) ?
                              <Col sm={2} xs={2} lg={1} md={1} onClick={(e) => this.handleChat(e, data._id)}>
                                <React.Fragment>
                                  <Typography style={{ color: AppConfig.colors.secondary, fontWeight: 'bold', fontSize: '12px' }}><Chat /></Typography>
                                  <Typography style={{ color: AppConfig.colors.secondary, fontWeight: 'bold', fontSize: '12px' }}>Chat</Typography>
                                </React.Fragment>
                              </Col> : ''
                          }

                        </Row>
                        <li className={classes.Listdivide}></li>
                      </Col>
                    </Row>
                  }) : searchText == '' && pharmacyData && pharmacyData.length > 0 ?
                    pharmacyData.map((data, index) => {
                      console.log('dataaaa', data);
                      return <Row key={index}>
                        <Col sm={12} xs={12} lg={12} md={12}>
                          <Row style={{ alignItems: 'center' }} >
                            <Col sm={10} xs={10} lg={11} md={11}>
                              <ListItem onClick={(e) => this.handleOnClick(e, data._id)} className={classes.listitems} >
                                <ListItemText primary={<Typography style={{ color: AppConfig.colors.secondary, fontWeight: 'bold', fontSize: '12px' }}>ID: {data && data != null && data.userId ? data.userId : '--'}</Typography>} secondary={<React.Fragment>
                                  <Typography variant="body1" style={{ color: AppConfig.colors.secondary, fontWeight: 'bold', fontSize: '18px', padding: '5px 0 5px 0' }} className={classes.pharmacyName}>{data && data != null && data.firstname && data.firstname != null ? data.firstname + ' ' + data.lastname : ''} <Typography style={{ color: AppConfig.colors.success, fontWeight: 'bold', fontSize: '12px' }}>{data.connected ? 'Connected' : ''}</Typography></Typography>
                                </React.Fragment>} />

                              </ListItem>
                            </Col>
                            {
                              (data && data.isChatEnabled === true) ?
                                <Col sm={2} xs={2} lg={1} md={1} onClick={(e) => this.handleChat(e, data._id)}>
                                  <React.Fragment>
                                    <Typography style={{ color: AppConfig.colors.secondary, fontWeight: 'bold', fontSize: '12px' }}><Chat /></Typography>
                                    <Typography style={{ color: AppConfig.colors.secondary, fontWeight: 'bold', fontSize: '12px' }}>Chat</Typography>
                                  </React.Fragment>
                                </Col> : ''
                            }

                          </Row>
                          <li className={classes.Listdivide}></li>
                        </Col>
                      </Row>
                    }) : <div><Typography style={{
                      backgroundColor: '#fff', color: 'gray', fontWeight: 'bold', fontSize: '25px',
                    }}>No Pharmacies</Typography></div>
              }
            </List>
          </div>
        </div>
      </React.Fragment>);
  }
}

Pharmacy.propTypes = {
  classes: PropTypes.object.isRequired,
};

const mapStateToProps = ({ authUser }) => {
  const { userProfileData, pharmacyData, connectData, searchpharmacyData } = authUser
  return { userProfileData, pharmacyData, connectData, searchpharmacyData }
}

export default connect(mapStateToProps, { getuserProfileData, searchPharmacyData, getPharmacyData, getConnectData })(withStyles(styles)(Pharmacy));