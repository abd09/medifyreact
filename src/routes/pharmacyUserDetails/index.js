import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import { Col, Row, Label } from 'reactstrap';
import SideBar from '../../components/userSideBar'
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import { Badge } from 'reactstrap';
import Toolbar from '@material-ui/core/Toolbar';
import AppBar from '@material-ui/core/AppBar';
import SearchBar from 'material-ui-search-bar'
import Create from '@material-ui/icons/Create';
import { red } from '@material-ui/core/colors';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import axios from '../../constant/axios';
import { getuserProfileData, connectPharmacy, getReviewDetail, filterInventory, getPharmacyInventoryData, searchInventories, getPharmacyDetail, getInventoriesData } from '../../actions';
import { connect } from 'react-redux';
import appConfig from '../../constant/config';
import FilterComponent from '../../components/FilterComponent';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import ShowCertificate from '../../components/ShowCertificate';
import ReviewComponent from '../../components/ReviewComponent';
import Inventory from "../../components/PharmacyDetails/inventory";
import Personal from "../../components/PharmacyDetails/personal";
import Offers from "../../components/PharmacyDetails/offers";
import { EditorState, ContentState } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';
import {
    isMobile
} from "react-device-detect";

const drawerWidth = 240;
const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    icon: {
        position: 'fixed',
        right: '15px',
        color: appConfig.colors.white

    },
    iconHover: {
        margin: theme.spacing(2),
        '&:hover': {
            color: red[800],
        },
    },
    appBar: {
        marginLeft: drawerWidth,
        [theme.breakpoints.up('sm')]: {
            width: `calc(100% - ${drawerWidth}px)`,
        },
    },
    menuButton: {
        marginRight: 20,
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
    dialogDone: {
        backgroundColor: '#454545',
        color: "#3ddb20",
        '&:hover': {
            background: 'none',
            backgroundColor: '#454545',
            color: '#3ddb20'
        },
        borderColor: '#454545'
    },
    dialogTitle: {
        backgroundColor: '#5a5a5a',
        '& h2': {
            color: 'white',
            textAlign: 'center'
        },
        borderBottom: '1px solid #5a5a5a'
    },
    dialogContent: {
        backgroundColor: '#5a5a5a',
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: '#ffffff',
        '&:hover': {
            backgroundColor: '#ffffff',
        },
        marginRight: 0,
        marginLeft: theme.spacing(2),
        marginTop: theme.spacing(1),
        width: '100%',
        borderRadius: '40px',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(3),
            width: 'auto',
        },
    },
    searchBar: {
        /*   padding: '5px', */
        flexWrap: 'unset !important',
        backgroundColor: '#f5f5f5 !important',
        padding: '15px 0 14px 0',
        ['@media (max-width:787px)']: {
            padding: '0px 15px',
        },
    },
    searchIcon: {
        width: theme.spacing(7),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        fontWeight: 'bold',
        opacity: 1
    },
    root2: {
        flexGrow: 1,
        paddingTop: '0',
        backgroundColor: appConfig.colors.white
    },
    tabsRoot: {
        color: appConfig.colors.primary,
        borderBottom: '1px solid #7a7a7a',
    },
    tabsIndicator: {
        backgroundColor: '#7a7a7a',
        height: '3px'
    },
    userName: {
        color: appConfig.colors.primary,
        marginBottom: 14,
        fontWeight: 'bolder',
        fontSize: '19px'
    },
    userDetails: {
        marginTop: '5px',
        color: appConfig.colors.primary,
        fontSize: '15px',
        marginBottom: 0,
        fontWeight: '400'
    },
    rightDiv: {
        color: "#CCCACD",
        marginBottom: 0,
        fontWeight: 'bold',
        fontSize: '16px',
        ['@media (max-width:767px)']: {
            paddingTop: '50px'
        }
    },
    listitems: {
        paddingLeft: 40,
        paddingRight: 10,
        paddingTop: 5,
        paddingBottom: 5
    },
    listitems1: {
        paddingLeft: 40,
        paddingRight: 10,
        paddingTop: 14,
        paddingBottom: 5
    },
    tabRoot: {
        textTransform: 'initial',
        minWidth: '33%',
        fontWeight: theme.typography.fontWeightRegular,
        fontFamily: [
            '-apple-system',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ].join(','),
        fontSize: '14px',
    },
    typography: {
        padding: '15px 0px',
        color: '#CCCACD',
        ['@media (max-width:787px)']: {
            padding: '15px 15px',
        },
    },
    /*  imageDiv: {
         textAlign: 'right',
         right: '-6px',
         height: '70px',
         width: '70px',
     }, */
    avatar: {
        margin: theme.spacing.unit * 2,
        marginLeft: theme.spacing.unit * 4,
        width: 70,
        height: 70
    },
    profile: {
        marginTop: theme.spacing.unit * 4,
        lineHeight: 1
    },
    tableDiv: {
        padding: '0 40px',
    },
    tabsFlex: {
        borderBottom: '0px solid #7a7a7a',
    },
    tableThTd: {
        color: '#ffffff'
    },
    infoDiv: {
        padding: 10,
        backgroundColor: "#fff"
    },
    disableClick: {
        pointerEvents: 'none'
    },
    inputRoot: {
        color: '#c6c6c6',
    },
    patientName: {
        color: appConfig.colors.secondary,
        font: 'bolder'
    },
    boldSize: {
        fontSize: '16px'
    },
   /*  availabilityBox: {
        backgroundColor: appConfig.colors.primary,
        padding: '10px',
        borderRadius: '7px',
        margin: '12px 0'
    }, */
    imageDiv: {
        textAlign: 'right',
        height: '70px',
        width: '70px',
    },
    inputInput: {
        color: '#c6c6c6',
        padding: theme.spacing(1, 1, 1, 7),
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: 200,
        },
    },
    listdivide: {
        marginLeft: '12px',
        marginRight: '12px',
        borderBottom: '1px solid #eeeeee'
    },
    avatar: {
        margin: 0,
        padding: 0
    },
    avatarlist: {
        margin: 0,
        padding: 0,
        width: 35,
        height: 35,
    },
    profile: {
        margin: 0,
        height: 100,
        lineHeight: 'normal'
    },
    signup: {
        backgroundColor: appConfig.colors.primary,
        borderRadius: '7px',
        color: appConfig.colors.white,
        padding: '6px 10px',
        textTransform: 'none',
        textAlign: 'right',
        float: 'right',
        display: 'inline-block',
        marginLeft: '5px'
    },
    viewLocation: {
        textAlign: 'right',
        float: 'right',
        backgroundColor: appConfig.colors.primary,
        borderRadius: '7px',
        color: appConfig.colors.white,
        padding: '6px 10px',
        textTransform: 'none',
        display: 'inline-block',
    },
    dailogContentText: {
        height: 300,
        width: 250
    }
});

class PatientDetails extends Component {
    constructor(props) {
        super(props)
        console.log('hey  i m here', this.props.location)
        this.state = {
            value: 0,
            openModal: false,
            isLoading: true,
            mobileOpen: false,
            anchorEl: null,
            fullWidth: true,
            maxWidth: 'md',
            status: false,
            pharmacy: {},
            tool: false,
            croppedImage: '',
            profileimage: '',
            imagePreview: '',
            profileimageMain: '',
            image: '',
            imageLoaded: false,
            alert: false,
            doctorDetails: '',
            searchText: '',
            inventory: [],
            review: false,
            role: '',
            open: false,
            scroll: 'paper',
            filterStateValue: null,
            connect: null
        };
    }

    componentWillReceiveProps = (nextProps) => {
        console.log("this.props.userProfileData", nextProps.profileImage)
        if (this.props.userProfileData !== nextProps.userProfileData) {
            this.setState({ croppedImage: nextProps.profileImage, temperature: nextProps.userProfileData.temperature, pulseRate: nextProps.userProfileData.pulseRate, bloodPressure: nextProps.userProfileData.bloodPressure, allergies: nextProps.userProfileData.allergies, diseases: nextProps.userProfileData.diseases, surgeries: nextProps.userProfileData.surgeries, others: nextProps.userProfileData.others, role: nextProps.userProfileData.role })
        }
        if (this.props.profileImage !== nextProps.profileImage) {
            this.setState({ croppedImage: nextProps.profileImage })
        }
    }

    componentWillMount() {
        this.props.getuserProfileData({ history: this.props.history });
        this.props.getReviewDetail({ data: this.props.match.params.id, history: this.props.history });
        this.props.getInventoriesData({ history: this.props.history })
        this.props.getPharmacyDetail({ data: { id: this.props.match.params.id }, history: this.props.history })
        this.props.getPharmacyInventoryData({ data: { id: this.props.match.params.id }, history: this.props.history })

        axios.get(`/users/getConnectDetails/${this.props.match.params.id}`, {
            headers: {
                'Content-Type': 'application/json',
                'token': localStorage.getItem('token')
            }
        }).then(result => {
            console.log("getConnectDetails result^^^", result.data.detail);
            this.setState({ connect: result.data.detail && result.data.detail != null ? true : false, review: result.data.detail && result.data.detail.isReview != null ? result.data.detail.isReview : false })
        }).catch(error => {
            console.log("error....", error);
        });
    }
    handleClick = (e) => {
        this.props.history.push(`/pharmacy/editPharmacyDetails`)
    }

    goBack = (e) => {
        console.log('e', e);
        e.preventDefault();
        console.log('this.props.history', this.props.history);
        this.props.history.goBack();
    }

    handleConnect = (e) => {
        e.preventDefault();
        let data = {
            doctor: this.state.role == 'doctor' ? this.props.userProfileData._id : null,
            pharmacy: this.props.match.params.id,
            connect: true,
            user: this.state.role == 'user' ? this.props.userProfileData._id : null
        }
        this.setState({ connect: true })
        this.props.connectPharmacy({
            data,
            history: this.props.history
        })
    }

    handleDisconnect = (e) => {
        e.preventDefault();
        let data = {
            doctor: this.state.role == 'doctor' ? this.props.userProfileData._id : null,
            pharmacy: this.props.match.params.id,
            connect: false,
            user: this.state.role == 'user' ? this.props.userProfileData._id : null
        }
        this.props.connectPharmacy({
            data,
            history: this.props.history
        })
        this.setState({ connect: false })
    }

    handleReview = (e) => {
        e.preventDefault();
    }

    handleInputChange = (e, key) => {
        console.log('key,e,', key, e.target.value);
        this.setState({ [key]: e.target.value });
    }

    saveProfile = (e) => {
        e.preventDefault();
        let data = {
            profileimageMain: this.state.profileimageMain
        }
        console.log("this.state.profileimageMain", this.state.profileimageMain)
        this.props.cropImage(data)
    }

    onConfirm(key, image) {
        let node = this[image]
        console.log('nodes=', node.crop())
        let img = node.crop()
        this.setState({ [key]: false, croppedImage: img, profileimageMain: img })
        console.log('image=', this.state)
    }

    onCancel(key) {
        console.log("key", key)
        this.setState({ [key]: false })
    }

    handleImageLoaded(state) {
        this.setState({
            [state + 'Loaded']: true
        })
    }

    openAlert(e, key) {
        this.setState({ images: e.target.files, image: '' })
        console.log('files=', e.target.files[0], e.target.files)
        var file = e.target.files[0];
        var reader = new FileReader();
        var url = reader.readAsDataURL(file);
        console.log('url=,', url)
        reader.onloadend = function (e) {
            this.setState({
                imagePreview: [reader.result]
            })
        }.bind(this);
        console.log('url=,', url)
        this.setState({ [key]: true });
        console.log('files=', e.target.files[0])
    }

    uploadFile(e) {
        e.preventDefault();
        this.setState({ tool: true })
    }

    openImageUploader() {
        this.setState({ tool: false })
        this.refs.imageUploader.click();
    }

    removeProfilePhoto = () => {
        this.setState({ profileimageMain: null, croppedImage: null, tool: false });
    }

    openUrl = (e, url) => {
        e.preventDefault();
        window.open(url);
    }

    handleSubmit = (e, userId) => {
        let data = {
            temperature: this.state.temperature,
            pulseRate: this.state.pulseRate,
            bloodPressure: this.state.bloodPressure,
            allergies: this.state.allergies,
            diseases: this.state.diseases,
            surgeries: this.state.surgeries,
            others: this.state.others,
            profileimageMain: this.state.profileimageMain,
            userId: userId
        }
        console.log('this.props.history', this.props.history);
        this.props.editVitals({ history: this.props.history, data })
        this.setState({ openModal: false, errors: {} });
    }

    handleChange = (event, value) => {
        console.log('value', value)
        this.setState({ value });
    };

    handleDrawerToggle = () => {
        this.setState(state => ({ mobileOpen: !state.mobileOpen }));
    };

    handleModalClose = () => {
        this.setState({ openModal: false, errors: {} });
    }

    handleModal = (e) => {
        e.preventDefault();
        this.setState({ openModal: true })
    }

    searchResults = (e, searchText, id) => {
        console.log('resultssss');
        let data = {
            searchText: searchText
        }
        if (searchText != '') {
            axios.post(`/users/searchInventory/${id}`, data, {
                headers: {
                    'Content-Type': 'application/json',
                    'token': localStorage.getItem('token')
                }
            }
            ).then(result => {
                console.log('result result', result);
                this.setState({ inventory: result.data.detail ? result.data.detail : '' })
            })
                .catch(error => {
                    console.log('error', error);
                });
        } else {
            axios.get(`/users/pharmacyInventory/${this.props.match.params.id}`, {
                headers: {
                    'Content-Type': 'application/json',
                    'token': localStorage.getItem('token')
                }
            }).then(result => {
                console.log("pharmacyInventory result^^^", result.data.detail);
                this.setState({ inventory: result.data.detail && result.data.detail ? result.data.detail : {} })
            })
                .catch(error => {
                    console.log("error....", error);
                });
        }
    }

    handleSearchText = (e, id, key) => {
        console.log('e searchText key', e);
        this.setState({ [key]: e })
        let data = {
            searchText: e
        }
        if (e != '') {
            axios.post(`/users/searchInventory/${id}`, data, {
                headers: {
                    'Content-Type': 'application/json',
                    'token': localStorage.getItem('token')
                }
            }
            ).then(result => {
                console.log('result result', result);
                this.setState({ inventory: result.data.detail ? result.data.detail : '' })
            })
                .catch(error => {
                    console.log('error', error);
                });
        } else {
            axios.get(`/users/pharmacyInventory/${this.props.match.params.id}`, {
                headers: {
                    'Content-Type': 'application/json',
                    'token': localStorage.getItem('token')
                }
            }).then(result => {
                console.log("pharmacyInventory result^^^", result.data.detail);
                this.setState({ inventory: result.data.detail && result.data.detail ? result.data.detail : {} })
            })
                .catch(error => {
                    console.log("error....", error);
                });
        }
    }

    render() {
        const { classes, userProfileData, children, url, reviewDetail, pharmacyInventoryData, pharmacyDetail, inventoryData } = this.props;
        const { inventory, value, pharmacy, searchText, connect, review, croppedImage, schemes, open } = this.state;

        return (
            <React.Fragment >
                <AppBar position="fixed" className={classes.appBar}>
                    <Toolbar>
                        <IconButton
                            color="inherit"
                            aria-label="Open drawer"
                            onClick={this.handleDrawerToggle}
                            className={classes.menuButton}>
                            <MenuIcon />
                        </IconButton>
                        <Typography variant="h6" color="inherit" style={{ fontWeight: 'bold', color: appConfig.colors.white }} noWrap>
                            {"Profile"}
                        </Typography>
                        <Create onClick={(e) => { this.handleClick(e) }} className={classes.icon} />
                    </Toolbar>
                </AppBar>
                <div className='page-content' style={{ fontWeight: 'bold', backgroundColor: appConfig.colors.white, marginTop: '-56px' }}>
                    <SideBar mobileOpen={this.state.mobileOpen} handleDrawerToggle={this.handleDrawerToggle} userProfileData={userProfileData} >
                        {children}
                    </SideBar>
                </div>
                <div className={classes.root}>
                    <div className={classes.infoDiv}>
                        <div style={{ backgroundColor: appConfig.colors.white, color: appConfig.colors.primary, padding: ' 0 24px 0 24px ' }}>
                            <Tabs
                                value={value}
                                onChange={this.handleChange}
                                classes={{ root: classes.tabsRoot, indicator: classes.tabsIndicator, flexContainer: classes.tabsFlex }} >
                                <Tab
                                    disableRipple
                                    classes={{ root: classes.tabRoot }}
                                    label="PERSONAL"
                                    style={{ fontWeight: 'bolder', color: appConfig.colors.primary }} />
                                <Tab
                                    disableRipple
                                    classes={{ root: classes.tabRoot }}
                                    label="INVENTORY"
                                    style={{ fontWeight: 'bolder', color: appConfig.colors.primary }}
                                />
                                <Tab
                                    disableRipple
                                    classes={{ root: classes.tabRoot }}
                                    label="OFFERS"
                                    style={{ fontWeight: 'bolder', color: appConfig.colors.primary }} />
                            </Tabs>
                        </div>
                    </div>
                    {value === 0 ?
                        <div className={classes.tableDiv} >
                            <div>

                                {pharmacyDetail && pharmacyDetail != null && pharmacyDetail.geoLocation && pharmacyDetail.geoLocation.coordinates && pharmacyDetail.geoLocation.coordinates.length > 0 ?
                                    <Button
                                        variant="contained"
                                        className={classes.viewLocation} onClick={(e) => this.openUrl(e, `http://www.google.com/maps/place/${pharmacyDetail.geoLocation.coordinates[1]},${pharmacyDetail.geoLocation.coordinates[0]}`)}
                                    > View Location</Button>
                                    : ''}
                            </div>

                            <Personal classes={classes} pharmacyDetail={pharmacyDetail} connect={connect} id={this.props.match.params.id} userProfileData={userProfileData} />
                        </div>
                        :
                        value === 1 ?
                            <Inventory pharmacyInventoryData={pharmacyInventoryData} id={this.props.match.params.id} classes={classes} inventoryData={inventoryData} />
                            :
                            <Offers classes={classes} pharmacyDetail={pharmacyDetail} connect={connect} id={this.props.match.params.id} userProfileData={userProfileData} />
                    }
                </div>
                {/* 
        
        
                <div className={classes.root}>
                    <div className={classes.infoDiv}>
                        <Row>
                            <Col className={classes.profile} xs="8" md="8" sm="8">
                                <Typography variant="body1" gutterBottom align="left" style={{ color: "#42425E", marginBottom: 0, fontWeight: 'bolder', fontSize: '16px' }}>
                                    {pharmacy && pharmacy.firstname && pharmacy.firstname != null ? pharmacy.firstname + ' ' + pharmacy.lastname : 'N/A'}
                                </Typography>
                                {
                                    pharmacy && pharmacy.isPhoneEnabled && pharmacy.isPhoneEnabled != null ? <Typography variant="body1" gutterBottom align="left" style={{ marginTop: '5px', color: appConfig.colors.secondary, marginBottom: 0, fontWeight: '400', fontSize: '14px' }}>Phone: {pharmacy && pharmacy.mobile && pharmacy.mobile != null ? pharmacy.mobile : 'N/A'}
                                    </Typography> : ''
                                }
                                <Typography variant="body1" gutterBottom align="left" style={{ marginTop: '5px', color: appConfig.colors.secondary, marginBottom: 0, fontWeight: '400', fontSize: '14px' }}>Delivery Distance: {pharmacy && pharmacy.deliveryDistance && pharmacy.deliveryDistance != null ? pharmacy.deliveryDistance : 'N/A'}
                                </Typography>
                                <Typography variant="body1" gutterBottom align="left" style={{ marginTop: '5px', color: appConfig.colors.secondary, marginBottom: 0, fontWeight: '400', fontSize: '14px' }}>Delivery Time: {pharmacy && pharmacy.deliveryTime && pharmacy.deliveryTime != null ? pharmacy.deliveryTime : 'N/A'}
                                </Typography>
                                <Typography variant="body1" gutterBottom align="left" style={{ marginTop: '5px', color: appConfig.colors.secondary, marginBottom: 0, fontWeight: '400', fontSize: '14px' }}>Delivery Status: {pharmacy && pharmacy.isDeliveryEnabled && pharmacy.isDeliveryEnabled === true ? 'Enabled' : 'Disabled'}
                                </Typography>
                                <Typography variant="body1" gutterBottom align="left" style={{ marginTop: '5px', color: appConfig.colors.secondary, marginBottom: 0, fontWeight: '400', fontSize: '14px' }}>Phone Status: {pharmacy && pharmacy.isPhoneEnabled && pharmacy.isPhoneEnabled === true ? 'Enabled' : 'Disabled'}
                                </Typography>
                                <Typography variant="body1" gutterBottom align="left" style={{ marginTop: '5px', color: appConfig.colors.secondary, marginBottom: 0, fontWeight: '400', fontSize: '14px' }}>Scheme/Discounts: {pharmacy && pharmacy.schemes && pharmacy.schemes !== undefined ? pharmacy.schemes : 'N/A'}
                                </Typography>
                                <Typography key={"owner"} variant="body1" gutterBottom align="left" style={{ color: appConfig.colors.secondary, marginTop: '5px', fontSize: '14px', marginBottom: 0, fontWeight: '400' }}>Location: {pharmacy && pharmacy.address && pharmacy.address != null ? pharmacy.address : 'N/A'}
                                </Typography>

                                {pharmacy && pharmacy != null && pharmacy.geoLocation && pharmacy.geoLocation.coordinates && pharmacy.geoLocation.coordinates.length > 0 ?
                                    <Button
                                        variant="contained"
                                        className={classes.signup} onClick={(e) => this.openUrl(e, `http://www.google.com/maps/place/${pharmacy.geoLocation.coordinates[1]},${pharmacy.geoLocation.coordinates[0]}`)}
                                    > View Location</Button>
                                    : ''}
                            </Col>
                            <Col xs="4" md="4" sm="4" style={{ textAlign: "right" }}>
                                {
                                    <div className={classes.rightDiv} style={{}}>
                                        {
                                            this.state.croppedImage &&
                                            <Avatar alt='Profile' src={localStorage.getItem('role') == 'pharmacy' ? this.state.croppedImage ? this.state.croppedImage : require('../../assets/img/noImage.png') : pharmacy.profileImage && pharmacy.profileImage != undefined && pharmacy.profileImage != '' ? appConfig.baseUrl + '/users/' + pharmacy.profileImage + '.png' : require('../../assets/img/noImage.png')} className={classes.imageDiv} >
                                            </Avatar>
                                        }
                                        {(this.state.croppedImage == undefined || this.state.croppedImage == "") &&
                                            <Avatar alt='Profile' src={require('../../assets/img/noImage.png')} className={classes.imageDiv}>
                                            </Avatar>
                                        }
                                    </div>
                                }
                            </Col>
                        </Row>
                    </div>
                    <Row style={{ marginTop: (pharmacy.availability && pharmacy.availability.length > 0 && pharmacy.availability[0].location != '') ? 100 : 75, height: (pharmacy.availability && pharmacy.availability.length > 0 && pharmacy.availability[0].location != '') ? 100 : (isMobile ? 80 : 30) }}>
                        <Col sm={12} xs={12} md={12} className={classes.profile}>
                            {
                                pharmacy.availability && pharmacy.availability.length > 0 && pharmacy.availability[0].location != '' ?
                                    < div >
                                        <Label for="certificates" style={{ color: appConfig.colors.primary, fontWeight: 'bold', fontSize: '12px', margin: 0 }} >Timing:</Label>
                                        <div style={{ backgroundColor: appConfig.colors.primary, padding: '10px 0', margin: '10px 0 10px 0' }}>
                                            {
                                                pharmacy.availability.map((each, ind) => {
                                                    console.log('each', each);
                                                    return <Typography style={{ color: appConfig.colors.white, fontSize: '17px' }}>{each.location.charAt(0).toUpperCase() + each.location.slice(1)} - {each.day.toUpperCase()}  -  {each.from} TO {each.to}</Typography>
                                                })
                                            }
                                        </div>
                                    </div> : ''
                            }
                        </Col>
                    </Row>
                    <div style={{ backgroundColor: appConfig.colors.secondary, color: appConfig.colors.white, padding: '0 24px 0 24px', }}>
                        <Tabs
                            value={value}
                            onChange={this.handleChange}
                            classes={{ root: classes.tabsRoot, indicator: classes.tabsIndicator, flexContainer: classes.tabsFlex }}>
                            <Tab
                                disableRipple
                                classes={{ root: classes.tabRoot }}
                                label="Inventory"
                                style={{ fontWeight: 'bolder', color: appConfig.colors.white }} />
                        </Tabs>
                    </div>
                    {
                        value === 0 ?
                            <div className={classes.tableDiv}>
                                <div key={'List'} className={classes.tableDiv}>
                                    <List className={classes.root2} >
                                        {<div className={classes.searchBar}>
                                            <div className={classes.search}>
                                                {<SearchBar value={searchText} onRequestSearch={(e) => this.searchResults(e, searchText, this.props.match.params.id)} onChange={(e) => this.handleSearchText(e, this.props.match.params.id, 'searchText')} style={{ borderRadius: '40px', height: '40px', fontWeight: 'bold', boxShadow: 'none', marginBottom: '8px' }} />}
                                            </div>
                                        </div>}
                                        <li className={classes.listdivide}></li>
                                        {
                                            inventory && inventory.length > 0 ?
                                                inventory.map((data, index) => {
                                                    console.log('dataaaa!!', data);
                                                    return <ListItem key={index} className={classes.listitems} >
                                                        <ListItemText primary={<React.Fragment>
                                                            <div style={{ float: 'none', display: 'inline-block' }}><Badge color={data.isAvailable ? "success" : "danger"} style={{ display: 'block', borderRadius: 10, padding: 4, marginRight: 5, marginTop: 5 }}></Badge></div>  <Typography variant="h5" style={{ color: appConfig.colors.white, fontWeight: 'bold', display: 'inline-block', fontSize: '17px', padding: '5px 0 5px 0' }} className={classes.patientName}>{data.name}</Typography>
                                                        </React.Fragment>} />
                                                    </ListItem>
                                                }) : <div><Typography style={{
                                                    color: appConfig.colors.white, fontWeight: 'bold', fontSize: '15px',
                                                }}> Inventory not found.</Typography></div>
                                        }
                                    </List>
                                </div>
                            </div>
                            :
                            <div className={classes.tableDiv}>
                            </div>
                    }
                    <Row>
                        <Col sm={12} xs={12} md={12} style={{ marginTop: 20 }}>

                            {
                                pharmacy.certificates && pharmacy.certificates.length > 0 ? <Label for="certificates" style={{ color: appConfig.colors.primary, fontWeight: 'bold', fontSize: '12px' }}>Certificates:</Label> : ''
                            }
                            {
                                pharmacy.certificates && pharmacy.certificates.length > 0 ? <ShowCertificate certificate={pharmacy.certificates} /> : ''
                            }

                        </Col>
                    </Row>
                </div>
            
        */}
            </React.Fragment >);
    }
}

PatientDetails.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = ({ authUser, ReviewReducer, inventoryReducer }) => {
    const { userProfileData, profileImage, pharmacyDetail } = authUser;
    const { reviewDetail } = ReviewReducer;
    const { pharmacyInventoryData, inventoryData } = inventoryReducer;
    return { userProfileData, profileImage, reviewDetail, pharmacyInventoryData, pharmacyDetail, inventoryData }
}

export default connect(mapStateToProps, { getuserProfileData, connectPharmacy, getReviewDetail, getPharmacyInventoryData, searchInventories, getPharmacyDetail, filterInventory, getInventoriesData })(withStyles(styles)(PatientDetails));