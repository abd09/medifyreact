import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import { Col, Row, Label } from 'reactstrap';
import SideBar from '../../components/userSideBar'
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Button from '@material-ui/core/Button';
import Toolbar from '@material-ui/core/Toolbar';
import AppBar from '@material-ui/core/AppBar';
import { red } from '@material-ui/core/colors';
import { getuserProfileData, getReviewDetail, getDoctorDetail } from '../../actions';
import appConfig from '../../constant/config';
import { connect } from 'react-redux';
import ReviewComponent from '../../components/ReviewComponent';
import ShowCertificate from '../../components/ShowCertificate';
import Personal from "../../components/DoctorDetails/personal";
import Professional from "../../components/DoctorDetails/professional";
import axios from '../../constant/axios';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';

const drawerWidth = 240;
const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    backBtn: {
        backgroundColor: appConfig.colors.primary,
        borderRadius: '0px',
        color: '#fff',
        marginTop: '5px',
        padding: '5px 22px',
        textTransform: 'none',
        marginRight: '8px',
        ['@media (max-width:707px)']: {
            marginRight: 'unset',
        },
        ['@media (max-width:599px)']: {
            marginRight: '8px',
        },
        ['@media (max-width:436px)']: {
            marginRight: 'unset',
        },
    },
    appBar: {
        marginLeft: drawerWidth,
        [theme.breakpoints.up('sm')]: {
            width: `calc(100% - ${drawerWidth}px)`,
        },
    },
    menuButton: {
        marginRight: 20,
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
    connectDiv: {
        backgroundColor: appConfig.colors.primary,
        borderRadius: '7px',
        color: appConfig.colors.white,
        padding: '6px 10px',
        textTransform: 'none',
        textAlign: 'right',
        float: 'right'
    },
    dialogDone: {
        backgroundColor: '#454545',
        color: "#3ddb20",
        '&:hover': {
            background: 'none',
            backgroundColor: '#454545',
            color: '#3ddb20'
        },
        borderColor: '#454545'
    },
    dialogTitle: {
        backgroundColor: '#5a5a5a',
        '& h2': {
            color: 'white',
            textAlign: 'center'
        },
        borderBottom: '1px solid #5a5a5a'
    },
    dialogContent: {
        backgroundColor: '#5a5a5a',
    },
    root2: {
        flexGrow: 1,
        backgroundColor: '#fff'
    },
    tabsRoot: {
        color: appConfig.colors.primary,
        borderBottom: '1px solid #7a7a7a',
    },
    tabsIndicator: {
        backgroundColor: '#7a7a7a',
        height: '3px'
    },
    rightDiv: {
        color: "#CCCACD",
        marginBottom: 0,
        fontWeight: 'bold',
        fontSize: '16px',
    },
    userName: {
        color: appConfig.colors.primary,
        marginBottom: 14,
        fontWeight: 'bolder',
        fontSize: '19px'
    },
    userDetails: {
        marginTop: '5px',
        color: appConfig.colors.primary,
        fontSize: '15px',
        marginBottom: 0,
        fontWeight: '400'
    },
    patientName: {
        color: appConfig.colors.secondary,
        font: 'bolder'
    },
    boldSize: {
        fontSize: '16px'
    },
    availabilityBox: {
        backgroundColor: appConfig.colors.primary,
        padding: '10px',
        borderRadius: '7px',
        margin: '12px 0'
    },
    listitems: {
        paddingLeft: 40,
        paddingRight: 10,
        paddingTop: 5,
        paddingBottom: 5
    },
    listitems1: {
        paddingLeft: 40,
        paddingRight: 10,
        paddingTop: 14,
        paddingBottom: 5
    },
    tabRoot: {
        textTransform: 'initial',
        minWidth: '50%',
        fontWeight: theme.typography.fontWeightRegular,
        fontFamily: [
            '-apple-system',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ].join(','),
        fontSize: '15px',
    },
    typography: {
        padding: '15px 0px',
        color: '#CCCACD',
        ['@media (max-width:787px)']: {
            padding: '15px 15px',
        },
    },
    imageDiv: {
        textAlign: 'right',
        height: '70px',
        width: '70px',
    },
    avatar: {
        margin: theme.spacing.unit * 2,
        marginLeft: theme.spacing.unit * 4,
        width: 70,
        height: 70
    },
    tableDiv: {
        padding: '20px 50px 10px 50px',
    },
    tabsFlex: {
        borderBottom: '0px solid #7a7a7a',
    },
    tableThTd: {
        color: '#ffffff',
    },
    infoDiv: {
        padding: '0 0 18px 0',
        backgroundColor: "#fff"
    },
    disableClick: {
        pointerEvents: 'none'
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: '#ffffff',
        '&:hover': {
            backgroundColor: '#ffffff',
        },
        marginRight: theme.spacing(2),
        marginLeft: 0,
        width: '100%',
        borderRadius: '40px',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(3),
            width: 'auto',
        },
    },
    searchIcon: {
        width: theme.spacing(7),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        fontWeight: 'bold',
        opacity: 1
    },
    inputRoot: {
        color: '#c6c6c6',
    },
    inputInput: {
        color: '#c6c6c6',
        padding: theme.spacing(1, 1, 1, 7),
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: 200,
        },
    },
    icon: {
        position: 'fixed',
        right: '15px',
        color: '#fff'
    },
    iconHover: {
        margin: theme.spacing(2),
        '&:hover': {
            color: red[800],
        },
    },
    avatar: {
        margin: 0,
        padding: 0,
    },
    avatarlist: {
        margin: 0,
        padding: 0,
        width: 35,
        height: 35,
    },
    profile: {
        margin: '10px 0 0 0',
        lineHeight: 'normal'
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        borderRadius: '8px',
        color: appConfig.colors.primary
    },
    dense: {
        marginTop: theme.spacing(2),
    },
    menu: {
        width: 200,
    },
    submit: {
        backgroundColor: appConfig.colors.primary,
        color: '#fff',
        marginTop: theme.spacing.unit * 2,
        padding: '8px 30px',
        textTransform: 'none',
        borderRadius: '0'
    },
    form: {
        width: '100%',
        paddingLeft: '20px',
        paddingRight: '35px',
        marginTop: theme.spacing.unit,
    },
});

class DoctorDetails extends Component {
    constructor(props) {
        super(props)
        console.log('hey  i m here', this.props.location)
        this.state = {
            uploaded_image_path: '',
            value: 0,
            openModal: false,
            isLoading: true,
            mobileOpen: false,
            doctorDetails: '',
            croppedImage: '',
            review: false,
            rates: '',
            speciality: '',
            experience: '',
            serviceProvided: '',
            waitingTime: '',
            value: 0
        };
    }

    componentWillReceiveProps = (nextProps) => {
        console.log("this.props.userProfileData", nextProps.profileImage)
        if (this.props.userProfileData !== nextProps.userProfileData) {
            this.setState({ croppedImage: nextProps.profileImage, temperature: nextProps.userProfileData.temperature, pulseRate: nextProps.userProfileData.pulseRate, bloodPressure: nextProps.userProfileData.bloodPressure, allergies: nextProps.userProfileData.allergies, diseases: nextProps.userProfileData.diseases, surgeries: nextProps.userProfileData.surgeries, others: nextProps.userProfileData.others, rates: nextProps.userProfileData.rates, speciality: nextProps.userProfileData.speciality, experience: nextProps.userProfileData.experience, serviceProvided: nextProps.userProfileData.serviceProvided, waitingTime: nextProps.userProfileData.waitingTime })
        }
        if (this.props.profileImage !== nextProps.profileImage) {
            this.setState({ croppedImage: nextProps.profileImage })
        }
    }

    componentWillMount() {
        this.props.getReviewDetail({ data: this.props.match.params.id, history: this.props.history });
        this.props.getuserProfileData({ history: this.props.history });
        this.props.getDoctorDetail({ data: { id: this.props.match.params.id }, history: this.props.history });

     
    }

    openUrl = (e, url) => {
        e.preventDefault();
        window.open(url);
    }

    handleChange = (event, value) => {
        console.log('value', value)
        this.setState({ value });
    };

    handleDrawerToggle = () => {
        this.setState(state => ({ mobileOpen: !state.mobileOpen }));
    };

    handleModalClose = () => {
        this.setState({ openModal: false, errors: {} });
    }

    render() {
        const { classes, userProfileData, children, url, reviewDetail, doctorDetails } = this.props;
        const {review, value } = this.state;
        console.log('doctorDetailsreviewDetail', doctorDetails)

        return (
            <React.Fragment>
                <AppBar position="fixed" className={classes.appBar}>
                    <Toolbar>
                        <IconButton
                            color="inherit"
                            aria-label="Open drawer"
                            onClick={this.handleDrawerToggle}
                            className={classes.menuButton}>
                            <MenuIcon />
                        </IconButton>
                        <Typography variant="h6" color="inherit" style={{ fontWeight: 'bold', color: '#fff' }} noWrap>
                            {"Doctors"}
                        </Typography>
                    </Toolbar>
                </AppBar>
                <div className='page-content' style={{ fontWeight: 'bold', backgroundColor: '#fff', marginTop: '-56px' }}>
                    <SideBar mobileOpen={this.state.mobileOpen} handleDrawerToggle={this.handleDrawerToggle} userProfileData={userProfileData} >
                        {children}
                    </SideBar>
                </div>
                <div className={classes.root}>
                    <div className={classes.infoDiv}>

                        <div style={{ backgroundColor: '#fff', color: appConfig.colors.primary, padding: ' 0 24px 0 24px ' }}>
                            <Tabs
                                value={value}
                                onChange={this.handleChange}
                                classes={{ root: classes.tabsRoot, indicator: classes.tabsIndicator, flexContainer: classes.tabsFlex }} >
                                <Tab
                                    disableRipple
                                    classes={{ root: classes.tabRoot }}
                                    label="PERSONAL"
                                    style={{ fontWeight: 'bolder', color: appConfig.colors.primary }} />
                                <Tab
                                    disableRipple
                                    classes={{ root: classes.tabRoot }}
                                    label="PROFESSIONAL"
                                    style={{ fontWeight: 'bolder', color: appConfig.colors.primary }} />
                            </Tabs>
                        </div>
                        {
                            value === 0 ?
                                <div className={classes.tableDiv} style={{ textAlign: "center" }}>
                                    <Personal classes={classes} doctorDetails={doctorDetails} id={this.props.match.params.id} />
                                </div>
                                :
                                <Professional classes={classes} doctorDetails={doctorDetails} id={this.props.match.params.id} />
                        }


                    </div>
                </div>
            </React.Fragment>);
    }
}

DoctorDetails.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = ({ authUser, ReviewReducer, doctorReducer }) => {
    const { userProfileData, profileImage } = authUser;
    const { reviewDetail } = ReviewReducer;
    const { doctorDetails } = doctorReducer;
    return { userProfileData, profileImage, reviewDetail, doctorDetails }
}

export default connect(mapStateToProps, { getuserProfileData, getReviewDetail, getDoctorDetail })(withStyles(styles)(DoctorDetails));