import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import List from '@material-ui/core/List';
import SideBar from '../../components/userSideBar'
import IconButton from '@material-ui/core/IconButton';
import { Col } from 'reactstrap';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import AppBar from '@material-ui/core/AppBar';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { getuserProfileData, getPharmacyOrders } from '../../actions';
import { connect } from 'react-redux';
import { red } from '@material-ui/core/colors';
import moment from 'moment';
import appconfig from '../../constant/config';

const drawerWidth = 240;

const styles = theme => ({
    root: {
        flexGrow: 1,
        backgroundColor: '#f5f5f5 !important',
    },
    appBar: {
        marginLeft: drawerWidth,
        [theme.breakpoints.up('sm')]: {
            width: `calc(100% - ${drawerWidth}px)`,
        },
        boxShadow: 'none'
    },
    menuButton: {
        marginRight: 20,
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
    dialogDone: {
        backgroundColor: '#454545',
        color: "#3ddb20",
        '&:hover': {
            background: 'none',
            backgroundColor: '#454545',
            color: '#3ddb20'
        },
        borderColor: '#454545'
    },
    dialogTitle: {
        backgroundColor: '#5a5a5a',
        '& h2': {
            color: 'white',
            textAlign: 'center'
        },
        borderBottom: '1px solid #5a5a5a'
    },
    dialogContent: {
        backgroundColor: '#5a5a5a',
    },
    root2: {
        flexGrow: 1,
        backgroundColor: '#fff'
    },
    tabsRoot: {
        borderBottom: '1px solid #454545',
    },
    tabsIndicator: {
        backgroundColor: '#58de45',
        height: '4px'
    },
    tabRoot: {
        textTransform: 'initial',
        minWidth: '50%',
        fontWeight: theme.typography.fontWeightRegular,
        fontFamily: [
            '-apple-system',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ].join(','),
    },
    tabSelected: {},
    typography: {
        padding: '15px 0px',
        color: '#CCCACD',
        ['@media (max-width:787px)']: {
            padding: '15px 15px',
        },
    },
    avatar: {
        margin: theme.spacing.unit * 2,
        marginLeft: theme.spacing.unit * 4,
        width: 70,
        height: 70
    },
    profile: {
        marginTop: theme.spacing.unit * 4,
        lineHeight: 1
    },
    tableDiv: {
        padding: '0px',
    },
    tabsFlex: {
        borderBottom: '1px solid #454545',
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: '#ffffff',
        '&:hover': {
            backgroundColor: '#ffffff',
        },
        marginRight: theme.spacing(2),
        marginLeft: 0,
        width: '100%',
        borderRadius: '40px',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(3),
            width: 'auto',
        },
    },
    searchIcon: {
        width: theme.spacing(7),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        fontWeight: 'bold',
        opacity: 1
    },
    inputRoot: {
        color: '#c6c6c6',
    },
    icon: {
        position: 'fixed',
        right: '10px',
        color: '#fff'
    },
    iconHover: {
        margin: theme.spacing(2),
        '&:hover': {
            color: red[800],
        },
    },
    docName: {
        color: '#FFFFFF',
        font: 'bolder'
    },
    inputInput: {
        color: '#c6c6c6',
        padding: theme.spacing(1, 1, 1, 7),
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: 200,
        },
    },
    docdivide: {
        marginLeft: '12px',
        marginRight: '12px',
        borderBottom: '1px solid #eeeeee'
    },
});

class PharmacyOrder extends Component {
    constructor(props) {
        super(props)
        console.log('hey  i m here', this.props.location)
        this.state = {
            docs: [],
            value: '',
            isLoading: true,
            mobileOpen: false,
        };
    }

    // *snip*
    componentDidMount() {
        this.props.getuserProfileData({ history: this.props.history });
        this.props.getPharmacyOrders({ history: this.props.history });
    }

    handleChange = (event, value) => {
        console.log('value', value)
        this.setState({ value });
    };

    handleOnClick = (e, orderId) => {
        e.preventDefault();
        this.props.history.push(`/${localStorage.getItem('role')}/orderDetails/${orderId}`)
    }

    handleDrawerToggle = () => {
        this.setState(state => ({ mobileOpen: !state.mobileOpen }));
    };

    render() {
        const { classes, userProfileData, children, pharmacyOrderData } = this.props;
        console.log('pharmacyOrderData', pharmacyOrderData)
        return (
            <React.Fragment>
                <AppBar position="fixed" className={classes.appBar}>
                    <Toolbar>
                        <IconButton
                            color="inherit"
                            aria-label="Open drawer"
                            onClick={this.handleDrawerToggle}
                            className={classes.menuButton}
                        ><MenuIcon />
                        </IconButton>
                        <Typography variant="h6" color="inherit" style={{ fontWeight: 'bold', color: '#fff' }} noWrap>
                            Orders
                        </Typography>
                    </Toolbar>
                </AppBar>
                <div className='page-content' style={{ marginTop: '-60px' }}>
                    <SideBar mobileOpen={this.state.mobileOpen} handleDrawerToggle={this.handleDrawerToggle} userProfileData={userProfileData}>
                        {children}
                    </SideBar>
                </div>
                <div className={classes.root}>
                    <div key={'doctorList'} className={classes.infoDiv}>
                        <List className={classes.root2} >
                            {
                                pharmacyOrderData && pharmacyOrderData.length > 0 ?
                                    pharmacyOrderData.map((data, index) => {
                                        console.log('dataaaa', data);
                                        return <div key={index}>
                                            <ListItem onClick={(e) => { data && data != null && data.patient && data.patient != null && data.patient._id && data.patient._id != null ? this.handleOnClick(e, data._id) : this.handleOnClick(e, '') }} className={classes.listitems} >
                                                <Col sm={7} xs={7} md={7} lg={7} xl={7}>
                                                    <ListItemText primary={<Typography style={{ color: '#42425E', fontWeight: 'bold', fontSize: '11px' }}>Order ID: {data.orderNo}</Typography>} secondary={<React.Fragment>
                                                        <Typography variant="h5" style={{ color: '#42425E', fontWeight: 'bold', fontSize: '16px', padding: '2px 0 0px 0px', letterSpacing: '0.04em' }} className={classes.docName}>{data && data != null && data.patient && data.patient != null && data.patient.firstname && data.patient.firstname != null ? data.patient.firstname + ' ' + data.patient.lastname : '--'}</Typography>
                                                        <Typography variant="body1" style={{ color: '#6b6b78', fontSize: '17px', fontWeight: 'lighter' }} className={classes.docName}>Location : {data && data != null && data.patient && data.patient != null && data.patient.address && data.patient.address != null ? data.patient.address : '--'}</Typography>
                                                    </React.Fragment>} />
                                                </Col>
                                                <Col sm={5} xs={5} md={5} lg={5} xl={5}>
                                                    <ListItemText style={{
                                                        float: 'right',
                                                        right: '20px', marginTop: '10px'
                                                    }} primary={<Typography style={{ color: '#42425E', fontWeight: 'bold', fontSize: '12px' }}>{data && data != null && data.createdAt && data.createdAt != null && data.createdAt ? moment(data.createdAt).format("D MMMM YYYY") : ''}</Typography>} />
                                                    <ListItemText style={{
                                                        textAlign: 'right',
                                                        right: '15px',
                                                        marginTop: '45px'
                                                    }} primary={<Typography style={{
                                                        backgroundColor: '#fff', color: (data && data != null && data.status && data.status != null && (data.status == 'accepted') ? appconfig.colors.info : data.status == 'cancelled' ? appconfig.colors.grey : data.status == 'completed' ? appconfig.colors.success : data.status == 'rejected' ? appconfig.colors.danger : appconfig.colors.warning), fontWeight: 'bold', fontSize: '12px',
                                                    }}>{data && data != null && data.status && data.status != null && (data.status == 'accepted') ? 'Confirmed' : data.status == 'cancelled' ? 'Cancelled' : data.status == 'completed' ? 'Completed' : data.status == 'rejected' ? 'Rejected' : 'Pending'}</Typography>} />
                                                </Col>
                                            </ListItem>
                                            <li className={classes.docdivide}></li>
                                        </div>
                                    }) : <div>
                                        <Typography style={{
                                            backgroundColor: '#fff', color: 'gray', fontWeight: 'bold', fontSize: '25px',
                                        }}>No Orders</Typography></div>
                            }
                        </List>
                    </div>
                </div>
            </React.Fragment>);
    }
}

PharmacyOrder.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = ({ authUser, orderReducer }) => {
    const { userProfileData } = authUser
    const { pharmacyOrderData } = orderReducer
    return { userProfileData, pharmacyOrderData }
}

export default connect(mapStateToProps, { getuserProfileData, getPharmacyOrders })(withStyles(styles)(PharmacyOrder));