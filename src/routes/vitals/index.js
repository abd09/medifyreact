import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import { Col } from 'reactstrap';
import SideBar from '../../components/userSideBar'
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import { Button, FormGroup, Label, Input } from 'reactstrap';
import AppBar from '@material-ui/core/AppBar';
import { Cropper } from 'react-image-cropper'
import SweetAlert from 'react-bootstrap-sweetalert'
import MatButton from '@material-ui/core/Button';

import { red } from '@material-ui/core/colors';

import axios from '../../constant/axios';
import { getuserProfileData, getPharmacyData, addOrder, cropImage, editVitals } from '../../actions';
import { connect } from 'react-redux';

const drawerWidth = 240;
const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    formControl: {
        margin: theme.spacing(1),
        minWidth: 120
    },
    lable: {
        textAlign: 'left'
    },
    selectEmpty: {
        marginTop: theme.spacing(2)
    },
    appBar: {
        marginLeft: drawerWidth,
        [theme.breakpoints.up('sm')]: {
            width: `calc(100% - ${drawerWidth}px)`,
        },
    },
    menuButton: {
        marginRight: 20,
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
    dialogDone: {
        backgroundColor: '#454545',
        color: "#3ddb20",
        '&:hover': {
            background: 'none',
            backgroundColor: '#454545',
            color: '#3ddb20'
        },
        borderColor: '#454545'
    },
    dialogTitle: {
        backgroundColor: '#5a5a5a',
        '& h2': {
            color: 'white',
            textAlign: 'center'
        },
        borderBottom: '1px solid #5a5a5a'
    },
    dialogContent: {
        backgroundColor: '#5a5a5a',
    },
    root2: {
        flexGrow: 1,
        backgroundColor: '#fff'
    },
    tabsRoot: {
        color: '#42425E',
        borderBottom: '1px solid #7a7a7a',
    },
    tabsIndicator: {
        backgroundColor: '#7a7a7a',
        height: '3px'
    },
    rightDiv: {
        color: "#CCCACD",
        marginBottom: 0,
        fontWeight: 'bold',
        fontSize: '16px',
        textAlign: '-webkit-center',
    },
    listitems: {
        paddingLeft: 40,
        paddingRight: 10,
        paddingTop: 5,
        paddingBottom: 5
    },
    listitems1: {
        paddingLeft: 40,
        paddingRight: 10,
        paddingTop: 14,
        paddingBottom: 5
    },
    tabRoot: {
        textTransform: 'initial',
        minWidth: '50%',
        fontWeight: theme.typography.fontWeightRegular,
        fontFamily: [
            '-apple-system',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ].join(','),
    },
    typography: {
        padding: '15px 0px',
        color: '#CCCACD',
        ['@media (max-width:787px)']: {
            padding: '15px 15px',
        },
    },
    input: {
        '::-webkit-input-placeholder': {
            color: 'red'
        }
    },
    imageDiv: {
        height: '70px',
        width: '70px',
    },
    avatar: {
        margin: theme.spacing(2),
        marginLeft: theme.spacing(4),
        width: 70,
        height: 70
    },
    profile: {
        textAlign: '-webkit-center',
        margin: theme.spacing.unit * 3,
        lineHeight: 'normal'
    },
    tableDiv: {
        padding: '0px',
    },
    tabsFlex: {
        borderBottom: '0px solid #454545',
    },
    tableThTd: {
        color: '#ffffff',
    },
    infoDiv: {
        padding: 18,
        backgroundColor: "#fff"
    },
    disableClick: {
        pointerEvents: 'none'
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: '#ffffff',
        '&:hover': {
            backgroundColor: '#ffffff',
        },
        marginRight: theme.spacing(2),
        marginLeft: 0,
        width: '100%',
        borderRadius: '40px',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(3),
            width: 'auto',
        },
    },
    searchIcon: {
        width: theme.spacing(7),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        fontWeight: 'bold',
        opacity: 1
    },
    inputRoot: {
        color: '#c6c6c6',
    },
    inputInput: {
        color: '#c6c6c6',
        padding: theme.spacing(1, 1, 1, 7),
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: 200,
        },
    },
    icon: {
        position: 'fixed',
        right: '10px',
        color: '#fff'
    },
    iconHover: {
        margin: theme.spacing(2),
        '&:hover': {
            color: red[800],
        },
    },
    avatar: {
        margin: 0,
        padding: 0,
    },
    avatarlist: {
        margin: 0,
        padding: 0,
        width: 35,
        height: 35,
    },
    signup: {
        backgroundColor: '#43415e',
        borderRadius: '0px',
        color: '#fff',
        marginTop: '85px',
        padding: '5px 22px',
        textTransform: 'none',
        marginRight: '8px',
        ['@media (max-width:707px)']: {
            marginRight: 'unset',
        },
        ['@media (max-width:599px)']: {
            marginRight: '8px',
        },
        ['@media (max-width:436px)']: {
            marginRight: 'unset',
        },
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        borderRadius: '8px',
        color: '#43415e'
    },
    dense: {
        marginTop: theme.spacing(2),
    },
    menu: {
        width: 200,
    },
    submit: {
        backgroundColor: '#43415e',
        color: '#fff',
        marginTop: theme.spacing(1),
        padding: '5px 10px',
        textTransform: 'none',
        borderRadius: '8px'
    },
    submit2: {
        backgroundColor: '#fff',
        color: '#43415e',
        border: 'none',
        marginTop: theme.spacing(2),
        textTransform: 'none',
        fontWeight: 'bolder',
        borderRadius: '0',
        '&:hover': {
            backgroundColor: '#ffffff',
            color: '#43415e',
        },
    },
    form: {
        width: '100%',
        paddingLeft: '20px',
        paddingRight: '35px',
        marginTop: theme.spacing.unit,
    },
});

class EditPatientDetail extends Component {
    constructor(props) {
        super(props)
        this.state = {
            uploaded_image_path: '',
            value: 0,
            openModal: false,
            isLoading: true,
            prescription: '',
            pharmacy: '',
            prescriptions: [],
            note: '',
            addmoreText: [{ location: '', day: '', from: '', to: '' }],
            tool: false,
            croppedImage: '',
            profileimage: '',
            imagePreview: '',
            profileimageMain: '',
            image: '',
            imageLoaded: false,
            alert: false,
            mobileOpen: false,
            firstname: '',
            lastname: '',
            mobile: '',
            about: '',
            address: '',
            age: '',
            gender: '',
            temperature: '',
            pulseRate: '',
            bloodPressure: '',
            allergies: '',
            diseases: '',
            surgeries: '',
            others: '',

        };
    }

    componentWillMount() {
        this.props.getuserProfileData({ history: this.props.history });
        this.props.getPharmacyData({ history: this.props.history });
        console.log('k,dsfkdsfkd', localStorage.getItem('token'));
        axios.get('/users/prescriptionsById', {
            headers: {
                'Content-Type': 'application/json',
                'token': localStorage.getItem('token')
            }
        }).then(result => {
            console.log("getPrescriptionsById result###", result.data.detail);
            this.setState({ prescriptions: result.data.detail && result.data.detail.length > 0 ? result.data.detail : [] })
        })
            .catch(error => {
                console.log("error....", error);
            });
    }

    handleSubmit = (e, userId) => {
        e.preventDefault();
        let data = {
            temperature: this.state.temperature,
            pulseRate: this.state.pulseRate,
            bloodPressure: this.state.bloodPressure,
            allergies: this.state.allergies,
            diseases: this.state.diseases,
            surgeries: this.state.surgeries,
            others: this.state.others,
            userId: userId
        }
        console.log('this.props.history', this.props.history);
        this.props.editVitals({ data, history: this.props.history })
       
    }


    componentWillReceiveProps = (nextProps) => {
        console.log("this.props.userProfileData", nextProps.profileImage)
        if (this.props.userProfileData !== nextProps.userProfileData) {
            console.log(' nextProps.availability', nextProps.userProfileData.availability);
            this.setState({ croppedImage: nextProps.profileImage, temperature: nextProps.userProfileData.temperature, pulseRate: nextProps.userProfileData.pulseRate, bloodPressure: nextProps.userProfileData.bloodPressure, allergies: nextProps.userProfileData.allergies, diseases: nextProps.userProfileData.diseases, surgeries: nextProps.userProfileData.surgeries, others: nextProps.userProfileData.others })
        }

        if (this.props.profileImage !== nextProps.profileImage) {
            this.setState({ croppedImage: nextProps.profileImage })
        }
    }

    addMoreItems = (e) => {
        e.preventDefault()
        let addmoreText = [...this.state.addmoreText];
        addmoreText.push({ location: '', day: '', from: '', to: '' });
        this.setState({ addmoreText: addmoreText })
    }

    handleItemChange = (e, name, ind) => {
        e.preventDefault()
        var oldState = [...this.state.addmoreText];
        oldState.map((data, i) => {
            if (ind == i) {
                oldState[i][name] = e.target.value;
            }
        })
        this.setState({ addmoreText: oldState });
    }

    removeHandler = (e, ind) => {
        e.preventDefault()
        var oldState = [...this.state.addmoreText];
        var newState = []
        oldState.map((data, i) => {
            if (ind !== i) {
                newState.push(data)
            }
        })
        this.setState({ addmoreText: newState });
    }

    handleChange = (event, key) => {
        console.log('key', key, event.target.value)
        this.setState({ [key]: event.target.value });
    };


    handleDrawerToggle = () => {
        this.setState(state => ({ mobileOpen: !state.mobileOpen }));
    };



    render() {
        const { classes, userProfileData, pharmacyData, children, url } = this.props;
        const { prescriptions, temperature, pulseRate, diseases, others, bloodPressure, surgeries, allergies } = this.state;
        const { addmoreText } = this.state;
        console.log('prescriptions,addmoreText', prescriptions, addmoreText)
        return (
            <React.Fragment>
                <AppBar position="fixed" className={classes.appBar}>
                    <Toolbar>
                        <IconButton
                            color="inherit"
                            aria-label="Open drawer"
                            onClick={this.handleDrawerToggle}
                            className={classes.menuButton}>
                            <MenuIcon />
                        </IconButton>
                        <Typography variant="h6" color="inherit" style={{ fontWeight: 'bold', color: '#fff' }} noWrap>
                            {"Vitals"}
                        </Typography>
                    </Toolbar>
                </AppBar>
                <div className='page-content' style={{ fontWeight: 'bold', backgroundColor: '#fff', marginTop: '-56px', borderRight: '1px solid #655368' }}>
                    <SideBar mobileOpen={this.state.mobileOpen} handleDrawerToggle={this.handleDrawerToggle} userProfileData={userProfileData} >
                        {children}
                    </SideBar>
                </div>
                <div className={classes.root}>
                    <div className={classes.container}>
                        <Col xs="12" md="12" sm="12" >

                            <div className={classes.profile}>
                                <div className={classes.tableDiv}>
                                    <form className={classes.form} onSubmit={(e) => this.handleSubmit(e, userProfileData._id)}>
                                        <FormGroup className={classes.lable}>
                                            <Label for="temperature" style={{ color: '#43415e', fontSize: '16px' }}>Temperature:</Label>
                                            <Input type="text" value={temperature} style={{ borderColor: '#43415e', fontSize: '18px' }} onChange={(e) => this.handleChange(e, 'temperature')} name="temperature" id="temperature" placeholder="Temperature" />

                                        </FormGroup>
                                        <FormGroup className={classes.lable}>
                                            <Label for="pulseRate" style={{ color: '#43415e', fontSize: '16px' }}>Pulse Rate:</Label>
                                            <Input type="text" style={{ borderColor: '#43415e', fontSize: '18px' }} value={pulseRate} name="pulseRate" id="name" placeholder="Pulse Rate" onChange={(e) => this.handleChange(e, 'pulseRate')} />
                                        </FormGroup>
                                        <FormGroup className={classes.lable}>
                                            <Label for="bloodPressure" style={{ color: '#43415e', fontSize: '16px' }}>Blood Pressure:</Label>
                                            <Input type="text" style={{ borderColor: '#43415e', fontSize: '18px' }} value={bloodPressure} name="bloodPressure" id="bloodPressure" placeholder="Blood Pressure" onChange={(e) => this.handleChange(e, 'bloodPressure')} />
                                        </FormGroup>
                                        <FormGroup className={classes.lable}>
                                            <Label for="allergies" style={{ color: '#43415e', fontSize: '16px' }}>Allergies:</Label>

                                            <Input type="text" style={{ borderColor: '#43415e', fontSize: '18px' }} value={allergies} name="allergies" id="allergies" placeholder="Allergies" onChange={(e) => this.handleChange(e, 'allergies')} />
                                        </FormGroup>

                                        <FormGroup className={classes.lable}>
                                            <Label for="diseases" style={{ color: '#43415e', fontSize: '16px' }}>Injuries/ Disease:</Label>
                                            <Input type="text" style={{ borderColor: '#43415e', fontSize: '18px' }} value={diseases} name="diseases" id="diseases" onChange={(e) => this.handleChange(e, 'diseases')}
                                                placeholder="Injuries/ Disease" />
                                        </FormGroup>

                                        <FormGroup className={classes.lable}>
                                            <Label for="surgeries" style={{ color: '#43415e', fontSize: '16px' }}>Surgeries:</Label>
                                            <Input type="textarea" style={{ borderColor: '#43415e', fontSize: '18px' }} onChange={(e) => this.handleChange(e, 'surgeries')} value={surgeries} className={classes.input} placeholder="Surgeries" name="surgeries" id="surgeries" />
                                        </FormGroup>
                                        <FormGroup className={classes.lable}>
                                            <Label for="others" style={{ color: '#43415e', fontSize: '16px' }}>Others:</Label>
                                            <Input type="textarea" style={{ borderColor: '#43415e', fontSize: '18px' }} onChange={(e) => this.handleChange(e, 'others')} value={others} className={classes.input} placeholder="Others" name="others" id="others" />
                                        </FormGroup>
                                        <Button variant="outlined" className={classes.submit} type="submit" >Submit</Button>
                                    </form>
                                </div>
                            </div>
                        </Col>
                    </div>
                </div>
            </React.Fragment>);
    }
}

EditPatientDetail.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = ({ authUser }) => {
    const { userProfileData, pharmacyData, profileImage } = authUser
    return { userProfileData, pharmacyData, profileImage }
}

export default connect(mapStateToProps, { getuserProfileData, getPharmacyData, addOrder, cropImage, editVitals })(withStyles(styles)(EditPatientDetail));