import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import { Col, Row, Label } from 'reactstrap';
import SideBar from '../../components/userSideBar'
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import AppBar from '@material-ui/core/AppBar';
import Create from '@material-ui/icons/Create';
import { red } from '@material-ui/core/colors';
import { getuserProfileData, cropImage, connectDoctor, getReviewDetail } from '../../actions';
import { connect } from 'react-redux';
import appConfig from '../../constant/config'
import ReviewComponent from '../../components/ReviewComponent';
import ShowCertificate from '../../components/ShowCertificate';
import Personal from "../../components/DoctorDetails/personal";
import Professional from "../../components/DoctorDetails/professional";
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import {
    isMobile
} from "react-device-detect";
import axios from '../../constant/axios';

const drawerWidth = 240;
const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    icon: {
        position: 'fixed',
        right: '15px',
        color: appConfig.colors.white

    },
    iconHover: {
        margin: theme.spacing(2),
        '&:hover': {
            color: red[800],
        },
    },
    backBtn: {
        backgroundColor: appConfig.colors.primary,
        borderRadius: '0px',
        color: '#fff',
        marginTop: '5px',
        padding: '5px 22px',
        textTransform: 'none',
        marginRight: '8px',
        ['@media (max-width:707px)']: {
            marginRight: 'unset',
        },
        ['@media (max-width:599px)']: {
            marginRight: '8px',
        },
        ['@media (max-width:436px)']: {
            marginRight: 'unset',
        },
    },
    appBar: {
        marginLeft: drawerWidth,
        [theme.breakpoints.up('sm')]: {
            width: `calc(100% - ${drawerWidth}px)`,
        },
    },
    menuButton: {
        marginRight: 20,
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
    connectDiv: {
     /*    backgroundColor: appConfig.colors.primary,
        borderRadius: '7px',
        color: appConfig.colors.white,
        padding: '6px 10px',
        textTransform: 'none',
        textAlign: 'right',
        float: 'right' */
    },
    dialogDone: {
        backgroundColor: '#454545',
        color: "#3ddb20",
        '&:hover': {
            background: 'none',
            backgroundColor: '#454545',
            color: '#3ddb20'
        },
        borderColor: '#454545'
    },
    dialogTitle: {
        backgroundColor: '#5a5a5a',
        '& h2': {
            color: 'white',
            textAlign: 'center'
        },
        borderBottom: '1px solid #5a5a5a'
    },
    dialogContent: {
        backgroundColor: '#5a5a5a',
    },
    root2: {
        flexGrow: 1,
        backgroundColor: '#fff'
    },
    tabsRoot: {
        color: appConfig.colors.primary,
        borderBottom: '1px solid #7a7a7a',
    },
    tabsIndicator: {
        backgroundColor: '#7a7a7a',
        height: '3px'
    },
    rightDiv: {
        color: "#CCCACD",
        marginBottom: 0,
        fontWeight: 'bold',
        fontSize: '16px',
    },
    userName: {
        color: appConfig.colors.primary,
     /*    marginBottom: 14, */
        fontWeight: 'bolder',
        fontSize: '19px'
    },
    userDetails: {
        marginTop: '5px',
        color: appConfig.colors.primary,
        fontSize: '15px',
        marginBottom: 0,
        fontWeight: '400'
    },
    patientName: {
        color: appConfig.colors.secondary,
        font: 'bolder'
    },
    boldSize: {
        fontSize: '16px'
    },
    availabilityBox: {
        /* backgroundColor: appConfig.colors.primary,
        padding: '10px',
        borderRadius: '7px',
        margin: '12px 0' */
    },
    listitems: {
        paddingLeft: 40,
        paddingRight: 10,
        paddingTop: 5,
        paddingBottom: 5
    },
    listitems1: {
        paddingLeft: 40,
        paddingRight: 10,
        paddingTop: 14,
        paddingBottom: 5
    },
    tabRoot: {
        textTransform: 'initial',
        minWidth: '50%',
        fontWeight: theme.typography.fontWeightRegular,
        fontFamily: [
            '-apple-system',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ].join(','),
        fontSize: '15px',
    },
    typography: {
        padding: '15px 0px',
        color: '#CCCACD',
        ['@media (max-width:787px)']: {
            padding: '15px 15px',
        },
    },
    imageDiv: {
        textAlign: 'right',
        height: '70px',
        width: '70px',
    },
    avatar: {
        margin: theme.spacing.unit * 2,
        marginLeft: theme.spacing.unit * 4,
        width: 70,
        height: 70
    },
    tableDiv: {
        padding: '0 50px',
    },
    tabsFlex: {
        borderBottom: '0px solid #7a7a7a',
    },
    tableThTd: {
        color: '#ffffff',
    },
    infoDiv: {
        padding: '0 0 18px 0',
        backgroundColor: "#fff"
    },
    disableClick: {
        pointerEvents: 'none'
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: '#ffffff',
        '&:hover': {
            backgroundColor: '#ffffff',
        },
        marginRight: theme.spacing(2),
        marginLeft: 0,
        width: '100%',
        borderRadius: '40px',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(3),
            width: 'auto',
        },
    },
    searchIcon: {
        width: theme.spacing(7),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        fontWeight: 'bold',
        opacity: 1
    },
    inputRoot: {
        color: '#c6c6c6',
    },
    inputInput: {
        color: '#c6c6c6',
        padding: theme.spacing(1, 1, 1, 7),
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: 200,
        },
    },
    icon: {
        position: 'fixed',
        right: '15px',
        color: '#fff'
    },
    iconHover: {
        margin: theme.spacing(2),
        '&:hover': {
            color: red[800],
        },
    },
    avatar: {
        margin: 0,
        padding: 0,
    },
    avatarlist: {
        margin: 0,
        padding: 0,
        width: 35,
        height: 35,
    },
    profile: {
        margin: '10px 0 0 0',
        lineHeight: 'normal'
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        borderRadius: '8px',
        color: appConfig.colors.primary
    },
    dense: {
        marginTop: theme.spacing(2),
    },
    menu: {
        width: 200,
    },
    submit: {
        backgroundColor: appConfig.colors.primary,
        color: '#fff',
        marginTop: theme.spacing.unit * 2,
        padding: '8px 30px',
        textTransform: 'none',
        borderRadius: '0'
    },
    form: {
        width: '100%',
        paddingLeft: '20px',
        paddingRight: '35px',
        marginTop: theme.spacing.unit,
    },
});

class DoctorDetails extends Component {
    constructor(props) {
        super(props)
        this.state = {
            uploaded_image_path: '',
            value: 0,
            tool: false,
            croppedImage: '',
            profileimage: '',
            imagePreview: '',
            profileimageMain: '',
            image: '',
            imageLoaded: false,
            alert: false,
            openModal: false,
            isLoading: true,
            mobileOpen: false,
            doctorDetails: '',
            connect: false,
            doctor: '',
            review: false,
            rates: '',
            speciality: '',
            experience: '',
            serviceProvided: '',
            waitingTime: '',
            value: 0
        };
    }

    componentWillReceiveProps = (nextProps) => {
        console.log("this.props.userProfileData", nextProps.profileImage)
        if (this.props.userProfileData !== nextProps.userProfileData) {
            this.setState({ croppedImage: nextProps.profileImage, lat: nextProps.userProfileData.geoLocation && nextProps.userProfileData.geoLocation.coordinates && nextProps.userProfileData.geoLocation.coordinates.length > 0 ? nextProps.userProfileData.geoLocation.coordinates[1] : '', lng: nextProps.userProfileData.geoLocation && nextProps.userProfileData.geoLocation.coordinates && nextProps.userProfileData.geoLocation.coordinates.length > 0 ? nextProps.userProfileData.geoLocation.coordinates[0] : '' })
        }
        if (this.props.profileImage !== nextProps.profileImage) {
            this.setState({ croppedImage: nextProps.profileImage })
        }
    }

    componentWillMount() {
        this.props.getuserProfileData({ history: this.props.history });
        axios.get(`/users/doctorDetails/${this.props.match.params.id}`, {
            headers: {
                'Content-Type': 'application/json',
                'token': localStorage.getItem('token')
            }
        }).then(result => {
            console.log("pharmacyDetails result^^^", result.data.detail);
            this.setState({ doctorDetails: result.data.detail && result.data.detail ? result.data.detail : {} })
        }).catch(error => {
            console.log("error....", error);
        });

        axios.get(`/users/getConnectDetails/${this.props.match.params.id}`, {
            headers: {
                'Content-Type': 'application/json',
                'token': localStorage.getItem('token')
            }
        }).then(result => {
            console.log("getConnectDetails result^^^", result.data.detail);
            this.setState({ connect: result.data.detail && result.data.detail != null ? true : false, review: result.data.detail && result.data.detail.isReview != null ? result.data.detail.isReview : false })
        }).catch(error => {
            console.log("error....", error);
        });

    }

    handleClick = (e) => {
        this.props.history.push(`/doctor/editDoctorDetails`)
    }

    openUrl = (e, url) => {
        e.preventDefault();
        window.open(url);
    }

    saveProfile = (e) => {
        e.preventDefault();
        let data = {
            profileimageMain: this.state.profileimageMain,
        }
        console.log("this.state.profileimageMain", this.state.profileimageMain)
        this.props.cropImage(data)
    }

    handleChange = (event, value) => {
        console.log('value', value)
        this.setState({ value });
    };

    handleDrawerToggle = () => {
        this.setState(state => ({ mobileOpen: !state.mobileOpen }));
    };

    handleModalClose = () => {
        this.setState({ openModal: false, errors: {} });
    }

    onConfirm(key, image) {
        let node = this[image]
        console.log('nodes=', node.crop())
        let img = node.crop()
        this.setState({ [key]: false, croppedImage: img, profileimageMain: img })
        console.log('image=', this.state)
    }
    /**
    * Open Alert
    * @param {key} key
    */
    onCancel(key) {
        console.log("key", key)
        this.setState({ [key]: false })
    }

    handleImageLoaded(state) {
        this.setState({
            [state + 'Loaded']: true
        })
    }
    /**
    * Open Alert
    * @param {key} key
    */
    openAlert(e, key) {
        this.setState({ images: e.target.files, image: '' })
        console.log('files=', e.target.files[0], e.target.files)
        var file = e.target.files[0];
        var reader = new FileReader();
        var url = reader.readAsDataURL(file);
        console.log('url=,', url)
        reader.onloadend = function (e) {
            this.setState({
                imagePreview: [reader.result]
            })
        }.bind(this);
        console.log('url=,', url)
        this.setState({ [key]: true });
        console.log('files=', e.target.files[0])
    }

    uploadFile(e) {
        e.preventDefault();
        this.setState({ tool: true })
    }

    openImageUploader() {
        this.setState({ tool: false })
        this.refs.imageUploader.click();
    }

    removeProfilePhoto = () => {
        this.setState({ profileimageMain: null, croppedImage: null, tool: false });
    }

    render() {
        const { classes, userProfileData, children, reviewDetail } = this.props;
        const { tool, doctorDetails, review, value } = this.state;
        console.log('this.state.croppedImage', this.state.croppedImage)
        console.log('doctorDetails.profileImage', doctorDetails.profileImage)
        return (
            <React.Fragment>
                <AppBar position="fixed" className={classes.appBar}>
                    <Toolbar>
                        <IconButton
                            color="inherit"
                            aria-label="Open drawer"
                            onClick={this.handleDrawerToggle}
                            className={classes.menuButton}>
                            <MenuIcon />
                        </IconButton>
                        <Typography variant="h6" color="inherit" style={{ fontWeight: 'bold', color: appConfig.colors.white }} noWrap>
                            {"Profile"}
                        </Typography>
                        <Create onClick={(e) => { this.handleClick(e) }} className={classes.icon} />
                    </Toolbar>
                </AppBar>
                <div className='page-content' style={{ fontWeight: 'bold', backgroundColor: appConfig.colors.white, marginTop: '-56px' }}>
                    <SideBar mobileOpen={this.state.mobileOpen} handleDrawerToggle={this.handleDrawerToggle} userProfileData={userProfileData} >
                        {children}
                    </SideBar>
                </div>
                <div className={classes.root}>
                    <div className={classes.infoDiv}>
                        <div style={{ backgroundColor: '#fff', color: appConfig.colors.primary, padding: ' 0 24px 0 24px ' }}>
                            <Tabs
                                value={value}
                                onChange={this.handleChange}
                                classes={{ root: classes.tabsRoot, indicator: classes.tabsIndicator, flexContainer: classes.tabsFlex }} >
                                <Tab
                                    disableRipple
                                    classes={{ root: classes.tabRoot }}
                                    label="PERSONAL"
                                    style={{ fontWeight: 'bolder', color: appConfig.colors.primary }} />
                                <Tab
                                    disableRipple
                                    classes={{ root: classes.tabRoot }}
                                    label="PROFESSIONAL"
                                    style={{ fontWeight: 'bolder', color: appConfig.colors.primary }} />
                            </Tabs>
                        </div>

                    </div>
                    {
                        value === 0 ?
                            <div className={classes.tableDiv} style={{ textAlign: "center" }}>
                                
                                <Personal classes={classes} doctorDetails={doctorDetails} id={this.props.match.params.id} />
                            </div>
                            :
                            <Professional classes={classes} doctorDetails={doctorDetails} id={this.props.match.params.id} />
                    }
                    {/* 

 <div className={classes.root}>
                    <div className={classes.infoDiv}>
                        <Row>
                            <Col className={classes.profile} xs="8" md="8" sm="8">
                                <Typography variant="h4" gutterBottom align="left" style={{ color: "#42425E", marginBottom: 0, fontWeight: 'bolder', fontSize: '18px' }}>
                                    Dr. {doctorDetails && doctorDetails.firstname ? doctorDetails.firstname + ' ' + doctorDetails.lastname : ''}
                                </Typography>
                                <Typography variant="body1" gutterBottom align="left" style={{ marginTop: '5px', color: appConfig.colors.secondary, fontWeight: '400', marginBottom: 0 }}><Typography style={{ fontWeight: 'bold', display: 'inline-block', fontSize: '14px' }}>Contact :</Typography> <Typography style={{ display: 'inline-block', fontSize: '14px' }}>{doctorDetails && doctorDetails.mobile ? doctorDetails.mobile : ''}</Typography>
                                </Typography>
                                <Typography variant="body1" gutterBottom align="left" style={{ marginTop: '0px', color: appConfig.colors.secondary, fontWeight: '400', lineHeight: '0', marginBottom: 0 }}><Typography style={{ fontWeight: 'bold', display: 'inline-block', fontSize: '14px' }}>Email :</Typography> <Typography style={{ display: 'inline-block', fontSize: '14px' }}>{doctorDetails && doctorDetails.email ? doctorDetails.email : ''}</Typography>
                                </Typography>
                                <Typography variant="body1" gutterBottom align="left" style={{ marginTop: '0px', color: appConfig.colors.secondary, fontWeight: '400', lineHeight: '0', marginBottom: 0 }}><Typography style={{ fontWeight: 'bold', display: 'inline-block', fontSize: '14px' }}>Location :</Typography> <Typography style={{ display: 'inline-block', fontSize: '14px' }}>{doctorDetails && doctorDetails.address ? doctorDetails.address : ''}</Typography>
                                    {this.state.lat !== '' && this.state.lng !== '' ?
                                        <Button
                                            style={{ display: 'inline-block' }}
                                            variant="contained"
                                            className={classes.signup} onClick={(e) => this.openUrl(e, `http://www.google.com/maps/place/${this.state.lat},${this.state.lng}`)}
                                        > View Location</Button>
                                        : ''}

                                </Typography>
                                <Typography variant="body1" gutterBottom align="left" style={{ marginTop: '0px', marginBottom: 0, color: appConfig.colors.secondary, fontWeight: '400', lineHeight: '0' }}><Typography style={{ fontWeight: 'bold', display: 'inline-block', fontSize: '14px' }}>Availability:</Typography>
                                </Typography>
                            </Col>
                            <Col xs="4" md="4" sm="4" style={{ textAlign: "right" }}>
                                <div className={classes.rightDiv}>
                                    {
                                        this.state.croppedImage &&
                                        <Avatar alt='Profile' src={localStorage.getItem('role') == 'doctor' ? this.state.croppedImage ? this.state.croppedImage : require('../../assets/img/noImage.png') : doctorDetails.profileImage && doctorDetails.profileImage != undefined && doctorDetails.profileImage != '' ? appConfig.baseUrl + '/users/' + doctorDetails.profileImage + '.png' : require('../../assets/img/noImage.png')} className={classes.imageDiv}>
                                        </Avatar>
                                    }
                                    {(this.state.croppedImage == undefined || this.state.croppedImage == "") &&
                                        <Avatar alt='Profile' src={require('../../assets/img/noImage.png')} className={classes.imageDiv} >
                                        </Avatar>
                                    }
                                </div>
                            </Col>
                            <Col sm={12} xs={12} md={12} className={classes.profile}>
                                {
                                    userProfileData.availability && userProfileData.availability.length > 0 && userProfileData.availability[0].location != '' ?
                                        <div style={{ backgroundColor: appConfig.colors.primary, padding: '10px 0', margin: '10px 0 10px 0' }}>
                                            {
                                                userProfileData.availability.map((each, ind) => {
                                                    console.log('each', each);
                                                    return <Typography style={{ color: appConfig.colors.white, fontSize: '17px' }}>{each.location.charAt(0).toUpperCase() + each.location.slice(1)} - {each.day.toUpperCase()}  -  {each.from} TO {each.to}</Typography>
                                                })
                                            }
                                        </div> : ''
                                }
                            </Col>
                            <Col sm={12} xs={12} md={12} align="left">
                                <div>
                                    <Typography variant="body1" gutterBottom align="left" style={{ marginTop: '0px', color: appConfig.colors.secondary, fontWeight: '400', lineHeight: '0', marginBottom: 0 }}><Typography style={{ fontWeight: 'bold', display: 'inline-block', fontSize: '14px' }}>Rate :</Typography> <Typography style={{ display: 'inline-block', fontSize: '14px' }}>{doctorDetails && doctorDetails.rates ? doctorDetails.rates : ''}</Typography>
                                    </Typography>
                                    <Typography variant="body1" gutterBottom align="left" style={{ marginTop: '0px', color: appConfig.colors.secondary, fontWeight: '400', lineHeight: '0', marginBottom: 0 }}><Typography style={{ fontWeight: 'bold', display: 'inline-block', fontSize: '14px' }}>Speciality :</Typography> <Typography style={{ display: 'inline-block', fontSize: '14px' }}>{doctorDetails && doctorDetails.speciality ? doctorDetails.speciality.title : ''}</Typography>
                                    </Typography>
                                    <Typography variant="body1" gutterBottom align="left" style={{ marginTop: '0px', color: appConfig.colors.secondary, fontWeight: '400', lineHeight: '0', marginBottom: 0 }}><Typography style={{ fontWeight: 'bold', display: 'inline-block', fontSize: '14px' }}>Experience :</Typography> <Typography style={{ display: 'inline-block', fontSize: '14px' }}>{doctorDetails && doctorDetails.experience ? doctorDetails.experience : ''}</Typography>
                                    </Typography>
                                    <Typography variant="body1" gutterBottom align="left" style={{ marginTop: '0px', color: appConfig.colors.secondary, fontWeight: '400', lineHeight: '0', marginBottom: 0 }}><Typography style={{ fontWeight: 'bold', display: 'inline-block', fontSize: '14px' }}>Service Provided :</Typography> <Typography style={{ display: 'inline-block', fontSize: '14px' }}>{doctorDetails && doctorDetails.serviceProvided ? doctorDetails.serviceProvided : ''}</Typography>
                                    </Typography>
                                    <Typography variant="body1" gutterBottom align="left" style={{ marginTop: '0px', color: appConfig.colors.secondary, fontWeight: '400', lineHeight: '0', marginBottom: 0 }}><Typography style={{ fontWeight: 'bold', display: 'inline-block', fontSize: '14px' }}>Waiting Time :</Typography> <Typography style={{ display: 'inline-block', fontSize: '14px' }}>{doctorDetails && doctorDetails.waitingTime ? doctorDetails.waitingTime : ''}</Typography>
                                    </Typography>
                                    <Typography style={{ color: appConfig.colors.primary, fontSize: '14px', fontWeight: 'bold', display: 'inline-block' }}>About :</Typography> <Typography style={{ color: appConfig.colors.primary, fontSize: '14px', display: 'inline-block' }}>{userProfileData.about}</Typography>
                                </div>
                            </Col>
                            {
                                doctorDetails.certificates && doctorDetails.certificates.length > 0 ?
                                    <Col sm={12} xs={12} md={12} style={{ marginTop: 20 }}>
                                        <Label for="certificates" style={{ color: appConfig.colors.primary, fontWeight: 'bold', fontSize: '16px' }}>Certificates:</Label>
                                        <ShowCertificate certificate={doctorDetails.certificates} />

                                    </Col>
                                    : ''
                            }
                        </Row>
                    </div>
                </div>
            
*/}
                </div>

            </React.Fragment>);
    }
}

DoctorDetails.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = ({ authUser, ReviewReducer }) => {
    const { userProfileData, profileImage } = authUser
    const { reviewDetail } = ReviewReducer;
    return { userProfileData, profileImage, reviewDetail }
}

export default connect(mapStateToProps, { getuserProfileData, cropImage, connectDoctor, getReviewDetail })(withStyles(styles)(DoctorDetails));