import React, { Component } from 'react';
import { Route, withRouter } from 'react-router-dom';
import UserLayout from '../RctUserLayout';
import Chat from '../../routes/ChatComponent';

class ChatComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }
  render() {
    const { match } = this.props;
    console.log('match=', match)
    return (
      <UserLayout>
        <Route path={`${match.url}/:id/:to`} component={Chat} />
      </UserLayout>
    );
  }
}

export default withRouter(ChatComponent);