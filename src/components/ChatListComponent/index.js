import React, { Component } from 'react';
import { getuserProfileData, getChatConnection } from '../../actions';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';
import Chat from '@material-ui/icons/Chat';
import { Col, Row } from 'reactstrap';
import { connect } from 'react-redux';
import appConfig from "../../constant/config";
class ChatListComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  componentWillMount = async () => {
    console.log('chatConnectionList componentWillMount')
    this.props.getChatConnection({ history: this.props.history })
  }

  handleChat = (e, userId, connectionId) => {
    e.preventDefault();
    console.log('chatConnectionList userId', userId)
    console.log('chatConnectionList connectionId', connectionId)
    console.log('chatConnectionList userProfledata', this.props.userProfileData._id)
    this.props.history.push(`/chat/${connectionId}/${userId}`)
  }

  render() {
    let { chatConnectionList, userProfileData } = this.props;
    console.log('chatConnectionList', chatConnectionList)
    return (
      <React.Fragment>
        <List>
          {
            (chatConnectionList && chatConnectionList.length > 0) ?
              chatConnectionList.map((connection, index) => {
                return <React.Fragment>
                  <ListItem key={index} onClick={(e) => this.handleChat(e, connection.userOne._id !== userProfileData._id ? connection.userOne._id : connection.userTwo._id, connection._id)}>
                    <Col sm={10} xs={10} md={11} lg={11} xl={11}>
                      <ListItemText primary={<Typography style={{ color: appConfig.colors.primary, fontWeight: 'bold', fontSize: '18px' }}>{(connection.userOne._id !== userProfileData._id) ? connection.userOne.firstname : (connection.userTwo._id !== userProfileData._id) ? connection.userTwo.firstname : ''}</Typography>} secondary={<React.Fragment>
                        <span style={{ color: appConfig.colors.grey, fontWeight: 'bold', fontSize: '12px' }}>{(connection.userOne._id !== userProfileData._id) ? connection.userOne.role === 'doctor' ? '(Doctor)' : connection.userOne.role === 'pharmacy' ? '(Pharmacy)' : '(Patient)' : (connection.userTwo._id !== userProfileData._id) ? connection.userTwo.role === 'doctor' ? '(Doctor)' : connection.userTwo.role === 'pharmacy' ? '(Pharmacy)' : '(Patient)' : ''}</span>
                      </React.Fragment>} />
                    </Col>
                    <Col sm={2} xs={2} md={1} lg={1} xl={1} >
                      <ListItemText style={{
                        float: 'right', marginTop: '0px', marginBottom: '0px',
                      }} primary={<React.Fragment><Typography style={{ color: appConfig.colors.primary, fontWeight: 'bold', fontSize: '12px' }}><Chat /></Typography><Typography style={{ color: appConfig.colors.primary, fontWeight: 'bold', fontSize: '12px' }}>Chat</Typography></React.Fragment>} />
                    </Col>
                  </ListItem>
                  <li className="listdivide"></li>
                </React.Fragment>
              }) : <div><Typography style={{
                backgroundColor: '#fff', color: appConfig.colors.grey, fontWeight: 'bold', fontSize: '25px', opacity: 0.6
              }}>No Messages</Typography></div>
          }
        </List>
      </React.Fragment>
    );
  }
}

const mapStateToProps = ({ authUser, ChatReducer }) => {
  const { userProfileData } = authUser
  const { chatConnectionList } = ChatReducer;
  return { userProfileData, chatConnectionList }
}

export default connect(mapStateToProps, { getuserProfileData, getChatConnection })(ChatListComponent);