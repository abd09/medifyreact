import React, { Component } from 'react'
import { Route, withRouter } from 'react-router-dom'
import UserLayout from '../RctUserLayout';
import Prescriptions from '../../routes/doctorPrescriptions';
import Patients from '../../routes/patients';
import Pharmacies from '../../routes/pharmacies';
import PatientDetail from '../../routes/patientDetails';
import AddPrescription from '../../routes/addPrescription';
import PrescriptionDetail from '../../routes/prescriptionDetails';
import PharmacyDetail from '../../routes/pharmacyDetails';
import DoctorDetail from '../../routes/doctorDetails';
import EditDoctorDetail from '../../routes/editDoctorDetails';
import ReviewListComponent from '../ReviewListComponent';
import ChatListComponent from '../ChatListComponent';

class UserComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }
    render() {
        const { match } = this.props;
        console.log('match=', match)
        return (
            <UserLayout>
                <Route path={`${match.url}/`} exact component={Prescriptions} />
                <Route path={`${match.url}/prescriptions`} component={Prescriptions} />
                <Route path={`${match.url}/patients`} component={Patients} />
                <Route path={`${match.url}/pharmacies`} component={Pharmacies} />
                <Route path={`${match.url}/profile/:id`} component={DoctorDetail} />
                <Route path={`${match.url}/editDoctorDetails`} component={EditDoctorDetail} />
                <Route path={`${match.url}/addPrescription`} component={AddPrescription} />
                <Route path={`${match.url}/patientDetails/:id`} component={PatientDetail} />
                <Route path={`${match.url}/prescriptionDetails/:id`} component={PrescriptionDetail} />
                <Route path={`${match.url}/pharmacyDetails/:id`} component={PharmacyDetail} />
                <Route path={`${match.url}/reviews`} component={ReviewListComponent} />
                <Route path={`${match.url}/messages`} component={ChatListComponent} />
            </UserLayout>
        );
    }
}

export default withRouter(UserComponent);