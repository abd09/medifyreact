import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import { Col, Row, Label } from 'reactstrap';
import { getuserProfileData, connectPharmacy, getReviewDetail, filterInventory, getPharmacyInventoryData, searchInventories, getPharmacyDetail, getInventoriesData } from '../../actions';
import { connect } from 'react-redux';
import axios from '../../constant/axios';
import appConfig from '../../constant/config';
import ShowCertificate from '../../components/ShowCertificate';
import ReviewComponent from '../../components/ReviewComponent';

import Button from '@material-ui/core/Button';

const drawerWidth = 240;
const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    appBar: {
        marginLeft: drawerWidth,
        [theme.breakpoints.up('sm')]: {
            width: `calc(100% - ${drawerWidth}px)`,
        },
    },
    menuButton: {
        marginRight: 20,
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
    dialogDone: {
        backgroundColor: '#454545',
        color: "#3ddb20",
        '&:hover': {
            background: 'none',
            backgroundColor: '#454545',
            color: '#3ddb20'
        },
        borderColor: '#454545'
    },
    dialogTitle: {
        backgroundColor: '#5a5a5a',
        '& h2': {
            color: 'white',
            textAlign: 'center'
        },
        borderBottom: '1px solid #5a5a5a'
    },
    dialogContent: {
        backgroundColor: '#5a5a5a',
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: '#ffffff',
        '&:hover': {
            backgroundColor: '#ffffff',
        },
        marginRight: 0,
        marginLeft: theme.spacing(2),
        marginTop: theme.spacing(1),
        width: '100%',
        borderRadius: '40px',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(3),
            width: 'auto',
        },
    },
    searchBar: {
        /*   padding: '5px', */
        flexWrap: 'unset !important',
        backgroundColor: '#f5f5f5 !important',
        padding: '15px 0 14px 0',
        ['@media (max-width:787px)']: {
            padding: '0px 15px',
        },
    },
    searchIcon: {
        width: theme.spacing(7),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        fontWeight: 'bold',
        opacity: 1
    },
    root2: {
        flexGrow: 1,
        paddingTop: '0',
        backgroundColor: appConfig.colors.white
    },
    tabsRoot: {
        color: appConfig.colors.primary,
        borderBottom: '1px solid #7a7a7a',
    },
    tabsIndicator: {
        backgroundColor: '#7a7a7a',
        height: '3px'
    },
    userName: {
        color: appConfig.colors.primary,
        marginBottom: 14,
        fontWeight: 'bolder',
        fontSize: '19px'
    },
    userDetails: {
        marginTop: '5px',
        color: appConfig.colors.primary,
        fontSize: '15px',
        marginBottom: 0,
        fontWeight: '400'
    },
    rightDiv: {
        color: "#CCCACD",
        marginBottom: 0,
        fontWeight: 'bold',
        fontSize: '16px',
        ['@media (max-width:767px)']: {
            paddingTop: '50px'
        }
    },
    listitems: {
        paddingLeft: 40,
        paddingRight: 10,
        paddingTop: 5,
        paddingBottom: 5
    },
    listitems1: {
        paddingLeft: 40,
        paddingRight: 10,
        paddingTop: 14,
        paddingBottom: 5
    },
    tabRoot: {
        textTransform: 'initial',
        minWidth: '33%',
        fontWeight: theme.typography.fontWeightRegular,
        fontFamily: [
            '-apple-system',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ].join(','),
        fontSize: '14px',
    },
    typography: {
        padding: '15px 0px',
        color: '#CCCACD',
        ['@media (max-width:787px)']: {
            padding: '15px 15px',
        },
    },
    /*  imageDiv: {
         textAlign: 'right',
         right: '-6px',
         height: '70px',
         width: '70px',
     }, */
    avatar: {
        margin: theme.spacing.unit * 2,
        marginLeft: theme.spacing.unit * 4,
        width: 70,
        height: 70
    },
    profile: {
        marginTop: theme.spacing.unit * 4,
        lineHeight: 1
    },
    tableDiv: {
        padding: '0 40px',
    },
    tabsFlex: {
        borderBottom: '0px solid #7a7a7a',
    },
    tableThTd: {
        color: '#ffffff'
    },
    infoDiv: {
        padding: 10,
        backgroundColor: "#fff"
    },
    disableClick: {
        pointerEvents: 'none'
    },
    inputRoot: {
        color: '#c6c6c6',
    },
    patientName: {
        color: appConfig.colors.secondary,
        font: 'bolder'
    },
    boldSize: {
        fontSize: '16px'
    },
    availabilityBox: {
        backgroundColor: appConfig.colors.white,
        border: `1px solid ${appConfig.colors.primary}`,
        color: appConfig.colors.primary,
        padding: '10px',
        borderRadius: '7px',
        margin: '12px 0',
        textAlign: 'left'
    },
    imageDiv: {
        textAlign: 'right',
        height: '70px',
        width: '70px',
    },
    inputInput: {
        color: '#c6c6c6',
        padding: theme.spacing(1, 1, 1, 7),
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: 200,
        },
    },
    listdivide: {
        marginLeft: '12px',
        marginRight: '12px',
        borderBottom: '1px solid #eeeeee'
    },
    avatar: {
        margin: 0,
        padding: 0
    },
    avatarlist: {
        margin: 0,
        padding: 0,
        width: 35,
        height: 35,
    },
    profile: {
        margin: 0,
        height: 100,
        lineHeight: 'normal'
    },
    signup: {
        backgroundColor: appConfig.colors.primary,
        borderRadius: '7px',
        color: appConfig.colors.white,
        padding: '6px 10px',
        textTransform: 'none',
        display: 'flex',
        /*  textAlign: 'right', */
        /*  float: 'right', */
        /*  display: 'inline-block', */
        marginLeft: '5px'
    },
    viewLocation: {
       /*  textAlign: 'right',
        float: 'right', */
        backgroundColor: appConfig.colors.primary,
        borderRadius: '7px',
        color: appConfig.colors.white,
        padding: '6px 10px',
        textTransform: 'none',
        display: 'flex',
    },
    dailogContentText: {
        height: 300,
        width: 250
    }
});

class Personal extends Component {
    constructor(props) {
        super(props)
        console.log('hey  i m here', this.props.location)
        this.state = {
            value: 0,
            openModal: false,
            isLoading: true,
            pharmacy: {},
            s: false,
            inventory: [],
            searchText: '',
            croppedImage: '',
            review: false,
            role: '',
            open: false,
            scroll: 'paper',
            filterStateValue: null,
            connect: null
        };
        this.filterRef = React.createRef();
    }

    componentWillReceiveProps = (nextProps) => {
        console.log("this.props.userProfileData", nextProps.profileImage)
        if (this.props.userProfileData !== nextProps.userProfileData) {
            this.setState({ croppedImage: nextProps.profileImage, temperature: nextProps.userProfileData.temperature, pulseRate: nextProps.userProfileData.pulseRate, bloodPressure: nextProps.userProfileData.bloodPressure, allergies: nextProps.userProfileData.allergies, diseases: nextProps.userProfileData.diseases, surgeries: nextProps.userProfileData.surgeries, others: nextProps.userProfileData.others, role: nextProps.userProfileData.role })
        }
        if (this.props.profileImage !== nextProps.profileImage) {
            this.setState({ croppedImage: nextProps.profileImage })
        }
    }

    componentWillMount() {
        this.props.getuserProfileData({ history: this.props.history });
        this.props.getReviewDetail({ data: this.props.id, history: this.props.history });
        this.props.getInventoriesData({ history: this.props.history })
        this.props.getPharmacyDetail({ data: { id: this.props.id }, history: this.props.history })
        this.props.getPharmacyInventoryData({ data: { id: this.props.id }, history: this.props.history })

        axios.get(`/users/getConnectDetails/${this.props.id}`, {
            headers: {
                'Content-Type': 'application/json',
                'token': localStorage.getItem('token')
            }
        }).then(result => {
            console.log("getConnectDetails result^^^", result.data.detail);
            this.setState({ connect: result.data.detail && result.data.detail != null ? true : false, review: result.data.detail && result.data.detail.isReview != null ? result.data.detail.isReview : false })
        }).catch(error => {
            console.log("error....", error);
        });
    }

    openUrl = (e, url) => {
        e.preventDefault();
        window.open(url);
    }

    handleApplyFilter() {
        console.log('asdasdasdasdsad', this.filterRef.current.state.single.value)
        this.props.filterInventory({ data: this.filterRef.current.state.single, history: this.props.history });
        this.setState({ open: false, filterStateValue: this.filterRef.current.state.single });
    }

    handleCancelFilter(e) {
        e.preventDefault();
        this.setState({ open: false });
    }

    handleClearFilter(e) {
        e.preventDefault();
        this.props.getPharmacyInventoryData({ data: { id: this.props.id }, history: this.props.history })
        this.setState({ open: false, filterStateValue: null });
    }

    handleModalOpen(e) {
        e.preventDefault();
        this.setState({ open: true });
    }

    searchResults = (e, searchText, id) => {
        console.log('resultssss');
        if (searchText != '') {
            this.props.searchInventories({ data: { id: id, searchText: searchText }, history: this.props.history });
        } else {
            this.props.getPharmacyInventoryData({ data: { id: this.props.id }, history: this.props.history })
        }
    }

    handleSearchText = (e, key, id) => {
        console.log('e searchText key', e, id);
        this.setState({ [key]: e })
        if (e != '') {
            this.props.searchInventories({ data: { id: id, searchText: e }, history: this.props.history });
        } else {
            this.props.getPharmacyInventoryData({ data: { id: this.props.id }, history: this.props.history })
        }
    }

    handleChange = (event, value) => {
        console.log('value', value)
        this.setState({ value });
    };

    handleDrawerToggle = () => {
        this.setState(state => ({ mobileOpen: !state.mobileOpen }));
    };

    handleModalClose = () => {
        this.setState({ openModal: false, errors: {} });
    }

    goBack = (e) => {
        console.log('e', e);
        e.preventDefault();
        console.log('this.props.history', this.props.history);
        this.props.history.goBack();
    }

    handleConnect = (e) => {
        e.preventDefault();
        let data = {
            doctor: this.state.role == 'doctor' ? this.props.userProfileData._id : null,
            pharmacy: this.props.id,
            connect: true,
            user: this.state.role == 'user' ? this.props.userProfileData._id : null
        }
        this.setState({ connect: true })
        this.props.connectPharmacy({
            data,
            history: this.props.history
        })
    }

    handleDisconnect = (e) => {
        e.preventDefault();
        let data = {
            doctor: this.props.userProfileData._id,
            pharmacy: this.props.id,
            connect: false,
            user: null
        }
        this.props.connectPharmacy({
            data,
            history: this.props.history
        })
        this.setState({ connect: false })
    }

    handleReview = (e) => {
        e.preventDefault();
    }

    goBack = (e) => {
        console.log('e', e);
        e.preventDefault();
        console.log('this.props.history', this.props.history);
        this.props.history.goBack();
    }

    render() {
        const { classes, pharmacyDetail, reviewDetail } = this.props;
        const { croppedImage, schemes, open, review } = this.state;
        console.log('croppedImage', croppedImage == true);

        return (
            <React.Fragment>


                <div  >

                    <div className={classes.rightDiv} style={{ textAlign: "center" }}>
                        {
                            localStorage.getItem('role') == 'pharmacy' ?
                                <Avatar alt='Profile' src={croppedImage ? croppedImage : require('../../assets/img/noImage.png')} className={classes.imageDiv}>
                                </Avatar>
                                :
                                <Avatar alt='Profile' src={pharmacyDetail.profileImage && pharmacyDetail.profileImage != undefined && pharmacyDetail.profileImage != '' ? appConfig.baseUrl + '/users/' + pharmacyDetail.profileImage + '.png' : require('../../assets/img/noImage.png')} className={classes.imageDiv}>


                                </Avatar>
                        }

                    </div>
                    <Typography variant="body1" gutterBottom align="left" className={classes.userName}>
                        {pharmacyDetail && pharmacyDetail.firstname && pharmacyDetail.firstname != null ? pharmacyDetail.firstname + ' ' + pharmacyDetail.lastname : ''}
                    </Typography>
                    <div>
                        {this.state.connect ? <Button
                            variant="contained"
                            className={classes.signup} onClick={(e) => this.handleDisconnect(e)} /* style={{ display: 'inline-block' }} */
                        > Disconnect</Button> : <Button
                            variant="contained"
                            className={classes.signup} onClick={(e) => this.handleConnect(e)} style={{ /* display: 'inline-block', */ padding: '6px 17px' }}
                        > Connect</Button>}

                    </div>
                    <div>
                        {
                            this.state.connect ? (reviewDetail && Object.keys(reviewDetail).length === 0) ? <ReviewComponent isReview={review} from={this.props.userProfileData._id} to={this.props.match.params.id} /> : '' : ''
                        }
                    </div>
                    {
                        (pharmacyDetail && pharmacyDetail.isPhoneEnabled) ? <Typography variant="body1" gutterBottom align="left" className={classes.userDetails}><b className={classes.boldSize}>Phone: </b>{pharmacyDetail && pharmacyDetail.mobile && pharmacyDetail.mobile != null ? pharmacyDetail.mobile : ''}
                        </Typography> : ''
                    }
                    <Typography variant="body1" gutterBottom align="left" className={classes.userDetails}><b className={classes.boldSize}>Email: </b>{pharmacyDetail && pharmacyDetail.email && pharmacyDetail.email != null ? pharmacyDetail.email : 'N/A'}
                    </Typography>
                    <Typography variant="body1" gutterBottom align="left" className={classes.userDetails}><b className={classes.boldSize}>License No.: </b>{pharmacyDetail && pharmacyDetail.licenseNo && pharmacyDetail.licenseNo != null ? pharmacyDetail.licenseNo : ''}
                    </Typography>
                    <Typography variant="body1" gutterBottom align="left" className={classes.userDetails}><b className={classes.boldSize}>GST No.: </b>{pharmacyDetail && pharmacyDetail.GSTNo && pharmacyDetail.GSTNo != null ? pharmacyDetail.GSTNo : ''}
                    </Typography>
                    {
                        pharmacyDetail && pharmacyDetail.availability && pharmacyDetail.availability.length > 0 && pharmacyDetail.availability[0].location != '' ?
                            <div>
                                <Typography variant="body1" gutterBottom align="left" className={classes.userDetails}><b className={classes.boldSize}>Timings: </b>
                                </Typography>
                                {
                                    pharmacyDetail.availability && pharmacyDetail.availability.length > 0 && pharmacyDetail.availability[0].location != '' ?
                                        <div className={classes.availabilityBox}>
                                            {
                                                pharmacyDetail.availability.map((each, i) => {
                                                    console.log('each', each);
                                                    return <Typography style={{ color: appConfig.colors.primary, fontSize: '17px' }}>{each.location.charAt(0).toUpperCase() + each.location.slice(1)} - {each.from} to {each.to}  ({each.days.map((day, ind) => {
                                                        console.log('each.days.length', ind !== (each.days.length - 1));
                                                        return day.isSelected ? (`${day.name}${((ind + 1) === each.days.length) ? '.' : ', '}`) : ''
                                                    }
                                                    )})
                                                    </Typography>
                                                })
                                            }
                                        </div> : ''
                                }
                            </div>
                            : ''
                    }
                    <Typography variant="body1" gutterBottom align="left" className={classes.userDetails}><b className={classes.boldSize}>Deliveries: </b>{pharmacyDetail && pharmacyDetail.isDeliveryEnabled && pharmacyDetail.isDeliveryEnabled === true ? 'Available' : 'Unvailable'}
                    </Typography>
                    <Typography variant="body1" gutterBottom align="left" className={classes.userDetails}><b className={classes.boldSize}>Delivery Time: </b>{pharmacyDetail && pharmacyDetail.deliveryTime && pharmacyDetail.deliveryTime != null ? pharmacyDetail.deliveryTime : 'N/A'}
                    </Typography>
                    <Typography key={"owner"} variant="body1" gutterBottom align="left" className={classes.userDetails}><b className={classes.boldSize}>Location: </b>{pharmacyDetail && pharmacyDetail != null && pharmacyDetail.address && pharmacyDetail.address != null ? pharmacyDetail.address : ''}
                    </Typography>
                    {pharmacyDetail && pharmacyDetail != null && pharmacyDetail.geoLocation && pharmacyDetail.geoLocation.coordinates && pharmacyDetail.geoLocation.coordinates.length > 0 ?
                        <Button
                            variant="contained"
                            className={classes.viewLocation} onClick={(e) => this.openUrl(e, `http://www.google.com/maps/place/${pharmacyDetail.geoLocation.coordinates[1]},${pharmacyDetail.geoLocation.coordinates[0]}`)}
                        > View Location</Button>
                        : ''}
                    {
                        pharmacyDetail && pharmacyDetail.certificates && pharmacyDetail.certificates.length > 0 ?
                            <Label for="certificates" style={{ color: appConfig.colors.primary, fontWeight: 'bold', fontSize: '17px', textDecoration: 'underline', marginTop: '10px' }}>Certificates:</Label> : ''
                    }
                    {
                        pharmacyDetail && pharmacyDetail.certificates && pharmacyDetail.certificates.length > 0 ? <ShowCertificate certificate={pharmacyDetail.certificates} /> : ''
                    }

                </div>




            </React.Fragment >);
    }
}

Personal.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = ({ authUser, ReviewReducer, inventoryReducer }) => {
    const { userProfileData, profileImage, pharmacyDetail } = authUser;
    const { reviewDetail } = ReviewReducer;
    const { pharmacyInventoryData, inventoryData } = inventoryReducer;
    return { userProfileData, profileImage, reviewDetail, pharmacyInventoryData, pharmacyDetail, inventoryData }
}

export default connect(mapStateToProps, { getuserProfileData, connectPharmacy, getReviewDetail, getPharmacyInventoryData, searchInventories, getPharmacyDetail, filterInventory, getInventoriesData })(withStyles(styles)(Personal));