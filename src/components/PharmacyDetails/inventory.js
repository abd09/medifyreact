import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { Badge } from 'reactstrap';
import { Col, Row } from 'reactstrap';
import Button from '@material-ui/core/Button';
import SearchBar from 'material-ui-search-bar'
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import { getuserProfileData, connectPharmacy, getReviewDetail, filterInventory, getPharmacyInventoryData, searchInventories, getPharmacyDetail, getInventoriesData } from '../../actions';
import { connect } from 'react-redux';
import axios from '../../constant/axios';
import appConfig from '../../constant/config';
import FilterComponent from '../../components/FilterComponent';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import { EditorState, ContentState } from 'draft-js';
import { Editor } from 'react-draft-wysiwyg';
import draftToHtml from 'draftjs-to-html';
import htmlToDraft from 'html-to-draftjs';

const drawerWidth = 240;
const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    appBar: {
        marginLeft: drawerWidth,
        [theme.breakpoints.up('sm')]: {
            width: `calc(100% - ${drawerWidth}px)`,
        },
    },
    menuButton: {
        marginRight: 20,
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
    dialogDone: {
        backgroundColor: '#454545',
        color: "#3ddb20",
        '&:hover': {
            background: 'none',
            backgroundColor: '#454545',
            color: '#3ddb20'
        },
        borderColor: '#454545'
    },
    dialogTitle: {
        backgroundColor: '#5a5a5a',
        '& h2': {
            color: 'white',
            textAlign: 'center'
        },
        borderBottom: '1px solid #5a5a5a'
    },
    dialogContent: {
        backgroundColor: '#5a5a5a',
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: '#ffffff',
        '&:hover': {
            backgroundColor: '#ffffff',
        },
        marginRight: 0,
        marginLeft: theme.spacing(2),
        marginTop: theme.spacing(1),
        width: '100%',
        borderRadius: '40px',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(3),
            width: 'auto',
        },
    },
    searchBar: {
        /*   padding: '5px', */
        flexWrap: 'unset !important',
        backgroundColor: '#f5f5f5 !important',
        padding: '15px 0 14px 0',
        ['@media (max-width:787px)']: {
            padding: '0px 15px',
        },
    },
    searchIcon: {
        width: theme.spacing(7),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        fontWeight: 'bold',
        opacity: 1
    },
    root2: {
        flexGrow: 1,
        paddingTop: '0',
        backgroundColor: appConfig.colors.white
    },
    tabsRoot: {
        color: appConfig.colors.primary,
        borderBottom: '1px solid #7a7a7a',
    },
    tabsIndicator: {
        backgroundColor: '#7a7a7a',
        height: '3px'
    },
    userName: {
        color: appConfig.colors.primary,
        marginBottom: 14,
        fontWeight: 'bolder',
        fontSize: '19px'
    },
    userDetails: {
        marginTop: '5px',
        color: appConfig.colors.primary,
        fontSize: '15px',
        marginBottom: 0,
        fontWeight: '400'
    },
    rightDiv: {
        color: "#CCCACD",
        marginBottom: 0,
        fontWeight: 'bold',
        fontSize: '16px',
        ['@media (max-width:767px)']: {
            paddingTop: '50px'
        }
    },
    listitems: {
        paddingLeft: 40,
        paddingRight: 10,
        paddingTop: 5,
        paddingBottom: 5
    },
    listitems1: {
        paddingLeft: 40,
        paddingRight: 10,
        paddingTop: 14,
        paddingBottom: 5
    },
    tabRoot: {
        textTransform: 'initial',
        minWidth: '33%',
        fontWeight: theme.typography.fontWeightRegular,
        fontFamily: [
            '-apple-system',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ].join(','),
        fontSize: '14px',
    },
    typography: {
        padding: '15px 0px',
        color: '#CCCACD',
        ['@media (max-width:787px)']: {
            padding: '15px 15px',
        },
    },
    /*  imageDiv: {
         textAlign: 'right',
         right: '-6px',
         height: '70px',
         width: '70px',
     }, */
    avatar: {
        margin: theme.spacing.unit * 2,
        marginLeft: theme.spacing.unit * 4,
        width: 70,
        height: 70
    },
    profile: {
        marginTop: theme.spacing.unit * 4,
        lineHeight: 1
    },
    tableDiv: {
        padding: '0 40px',
    },
    tabsFlex: {
        borderBottom: '0px solid #7a7a7a',
    },
    tableThTd: {
        color: '#ffffff'
    },
    infoDiv: {
        padding: 10,
        backgroundColor: "#fff"
    },
    disableClick: {
        pointerEvents: 'none'
    },
    inputRoot: {
        color: '#c6c6c6',
    },
    patientName: {
        color: appConfig.colors.secondary,
        font: 'bolder'
    },
    boldSize: {
        fontSize: '16px'
    },
    availabilityBox: {
        backgroundColor: appConfig.colors.primary,
        padding: '10px',
        borderRadius: '7px',
        margin: '12px 0'
    },
    imageDiv: {
        textAlign: 'right',
        height: '70px',
        width: '70px',
    },
    inputInput: {
        color: '#c6c6c6',
        padding: theme.spacing(1, 1, 1, 7),
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: 200,
        },
    },
    listdivide: {
        marginLeft: '12px',
        marginRight: '12px',
        borderBottom: '1px solid #eeeeee'
    },
    avatar: {
        margin: 0,
        padding: 0
    },
    avatarlist: {
        margin: 0,
        padding: 0,
        width: 35,
        height: 35,
    },
    profile: {
        margin: 0,
        height: 100,
        lineHeight: 'normal'
    },
    signup: {
        backgroundColor: appConfig.colors.primary,
        borderRadius: '7px',
        color: appConfig.colors.white,
        padding: '6px 10px',
        textTransform: 'none',
        textAlign: 'right',
        float: 'right',
        display: 'inline-block',
        marginLeft: '5px'
    },
    viewLocation: {
        textAlign: 'right',
        float: 'right',
        backgroundColor: appConfig.colors.primary,
        borderRadius: '7px',
        color: appConfig.colors.white,
        padding: '6px 10px',
        textTransform: 'none',
        display: 'inline-block',
    },
    dailogContentText: {
        height: 300,
        width: 250
    }
});

class PharmacyDetails extends Component {
    constructor(props) {
        super(props)
        console.log('hey  i m here', this.props.location)
        this.state = {
            value: 0,
            openModal: false,
            isLoading: true,
            pharmacy: {},
            s: false,
            inventory: [],
            searchText: '',
            croppedImage: '',
            review: false,
            role: '',
            open: false,
            scroll: 'paper',
            filterStateValue: null,
            connect: null
        };
        this.filterRef = React.createRef();
    }

    componentWillReceiveProps = (nextProps) => {
        console.log("this.props.userProfileData", nextProps.profileImage)
        if (this.props.userProfileData !== nextProps.userProfileData) {
            this.setState({ croppedImage: nextProps.profileImage, temperature: nextProps.userProfileData.temperature, pulseRate: nextProps.userProfileData.pulseRate, bloodPressure: nextProps.userProfileData.bloodPressure, allergies: nextProps.userProfileData.allergies, diseases: nextProps.userProfileData.diseases, surgeries: nextProps.userProfileData.surgeries, others: nextProps.userProfileData.others, role: nextProps.userProfileData.role })
        }
        if (this.props.profileImage !== nextProps.profileImage) {
            this.setState({ croppedImage: nextProps.profileImage })
        }
    }

    componentWillMount() {
        this.props.getuserProfileData({ history: this.props.history });
        this.props.getReviewDetail({ data: this.props.id, history: this.props.history });
        this.props.getInventoriesData({ history: this.props.history })
        this.props.getPharmacyDetail({ data: { id: this.props.id }, history: this.props.history })
        this.props.getPharmacyInventoryData({ data: { id: this.props.id }, history: this.props.history })

        axios.get(`/users/getConnectDetails/${this.props.id}`, {
            headers: {
                'Content-Type': 'application/json',
                'token': localStorage.getItem('token')
            }
        }).then(result => {
            console.log("getConnectDetails result^^^", result.data.detail);
            this.setState({ connect: result.data.detail && result.data.detail != null ? true : false, review: result.data.detail && result.data.detail.isReview != null ? result.data.detail.isReview : false })
        }).catch(error => {
            console.log("error....", error);
        });
    }

    openUrl = (e, url) => {
        e.preventDefault();
        window.open(url);
    }

    handleApplyFilter() {
        let data = this.filterRef.current.state.single;
        data.id = this.props.id
        console.log('asdasdasdasdsad', this.filterRef.current.state.single.value)
        this.props.filterInventory({ data: data, history: this.props.history });
        this.setState({ open: false, filterStateValue: this.filterRef.current.state.single });
    }

    handleCancelFilter(e) {
        e.preventDefault();
        this.setState({ open: false });
    }

    handleClearFilter(e) {
        e.preventDefault();
        this.props.getPharmacyInventoryData({ data: { id: this.props.id }, history: this.props.history })
        this.setState({ open: false, filterStateValue: null });
    }

    handleModalOpen(e) {
        e.preventDefault();
        this.setState({ open: true });
    }

    searchResults = (e, searchText, id) => {
        console.log('resultssss');
        if (searchText != '') {
            this.props.searchInventories({ data: { id: id, searchText: searchText }, history: this.props.history });
        } else {
            this.props.getPharmacyInventoryData({ data: { id: this.props.id }, history: this.props.history })
        }
    }

    handleSearchText = (e, key, id) => {
        console.log('e searchText key', e, id);
        this.setState({ [key]: e })
        if (e != '') {
            this.props.searchInventories({ data: { id: id, searchText: e }, history: this.props.history });
        } else {
            this.props.getPharmacyInventoryData({ data: { id: this.props.id }, history: this.props.history })
        }
    }

    handleChange = (event, value) => {
        console.log('value', value)
        this.setState({ value });
    };

    handleDrawerToggle = () => {
        this.setState(state => ({ mobileOpen: !state.mobileOpen }));
    };

    handleModalClose = () => {
        this.setState({ openModal: false, errors: {} });
    }

    goBack = (e) => {
        console.log('e', e);
        e.preventDefault();
        console.log('this.props.history', this.props.history);
        this.props.history.goBack();
    }

    handleConnect = (e) => {
        e.preventDefault();
        let data = {
            doctor: this.state.role == 'doctor' ? this.props.userProfileData._id : null,
            pharmacy: this.props.id,
            connect: true,
            user: this.state.role == 'user' ? this.props.userProfileData._id : null
        }
        this.setState({ connect: true })
        this.props.connectPharmacy({
            data,
            history: this.props.history
        })
    }

    handleDisconnect = (e) => {
        e.preventDefault();
        let data = {
            doctor: this.props.userProfileData._id,
            pharmacy: this.props.id,
            connect: false,
            user: null
        }
        this.props.connectPharmacy({
            data,
            history: this.props.history
        })
        this.setState({ connect: false })
    }

    handleReview = (e) => {
        e.preventDefault();
    }

    goBack = (e) => {
        console.log('e', e);
        e.preventDefault();
        console.log('this.props.history', this.props.history);
        this.props.history.goBack();
    }

    render() {
        const { classes, pharmacyInventoryData, inventoryData } = this.props;
        const { searchText, open } = this.state;

        return (
            <React.Fragment>
                <div className={classes.root}>
                    <div className={classes.tableDiv}>
                        <List className={classes.root2} >
                            <Row className={classes.searchBar} >
                                <Col className={classes.search} sm={10} xs={10} md={10} lg={11} xl={11}>
                                    <SearchBar value={searchText} onRequestSearch={(e) => this.searchResults(e, searchText, this.props.id)} onChange={(e) => this.handleSearchText(e, 'searchText', this.props.id)} style={{ borderRadius: '40px', height: '40px', fontWeight: 'bold', boxShadow: 'none', marginBottom: '8px' }} />
                                </Col>
                                <Col className={'filter'} sm={2} md={2} xs={2} lg={1} xl={1}>
                                    <i className="fa fa-filter" aria-hidden="true" onClick={(e) => this.handleModalOpen(e)}></i>
                                </Col>
                            </Row>
                            <li className={classes.listdivide}></li>
                            {
                                pharmacyInventoryData && pharmacyInventoryData.length > 0 ?
                                    pharmacyInventoryData.map((data, index) => {
                                        console.log('dataaaa!!', data);
                                        return <ListItem key={index} className={classes.listitems} >
                                            <ListItemText primary={<React.Fragment>
                                                <div style={{ float: 'none', display: 'inline-block' }}><Badge color={data.isAvailable ? "success" : "danger"} style={{ display: 'block', borderRadius: 10, padding: 4, marginRight: 5, marginTop: 5 }}></Badge></div>  <Typography variant="h5" style={{ color: appConfig.colors.secondary, fontWeight: 'bold', display: 'inline-block', fontSize: '17px', padding: '5px 0 5px 0' }} className={classes.patientName}>{data.name}</Typography>
                                                <div style={{ display: 'inline-block', fontSize: '17px', padding: '5px 0 5px 8px', color: appConfig.colors.grey }}>{data.category ? `(${data.category})` : ''}</div>
                                            </React.Fragment>} />
                                        </ListItem>
                                    }) : <div><Typography style={{
                                        color: 'gray', fontWeight: 'bold', fontSize: '15px',
                                    }}> Inventory not found.</Typography></div>
                            }
                        </List>
                    </div>
                </div>
                <Dialog
                    open={open}
                    onClose={(e) => this.handleCancelFilter(e)}
                    scroll={this.state.scroll}
                    aria-labelledby="scroll-dialog-title"
                >
                    <DialogTitle id="scroll-dialog-title">Filter</DialogTitle>
                    <DialogContent dividers={this.state.scroll === 'paper'} className={classes.dailogContentText}>
                        <FilterComponent ref={this.filterRef} filterFor={'Category'} inventory={inventoryData} filterStateValue={this.state.filterStateValue} />
                    </DialogContent>
                    <DialogActions>
                        <Button variant="contained" onClick={(e) => this.handleClearFilter(e)} color="primary">
                            Remove Filter
						</Button>
                        <Button variant="contained" onClick={(e) => this.handleApplyFilter(e)} color="primary">
                            Apply Filter
						</Button>
                    </DialogActions>
                </Dialog>
            </React.Fragment >);
    }
}

PharmacyDetails.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = ({ authUser, ReviewReducer, inventoryReducer }) => {
    const { userProfileData, profileImage, pharmacyDetail } = authUser;
    const { reviewDetail } = ReviewReducer;
    const { pharmacyInventoryData, inventoryData } = inventoryReducer;
    return { userProfileData, profileImage, reviewDetail, pharmacyInventoryData, pharmacyDetail, inventoryData }
}

export default connect(mapStateToProps, { getuserProfileData, connectPharmacy, getReviewDetail, getPharmacyInventoryData, searchInventories, getPharmacyDetail, filterInventory, getInventoriesData })(withStyles(styles)(PharmacyDetails));