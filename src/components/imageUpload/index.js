import React from 'react'
import Avatar from '@material-ui/core/Avatar';
import axios from '../../constant/axios';
const headers = {
  'Content-Type': 'multipart/form-data',
  'token': localStorage.getItem('token')
}

class ReactUploadImage extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      file: null
    };
    this.onChange = this.onChange.bind(this);
  }

  onChange(e) {
    e.preventDefault();
    let files;
    if (e.dataTransfer) {
      files = e.dataTransfer.files;
    } else if (e.target) {
      files = e.target.files;
    }
    const reader = new FileReader();
    reader.onload = () => {
      this.setState({ file: reader.result });
    };
    reader.readAsDataURL(files[0]);
    const formData = new FormData();
    console.log(this.state.file, 'this.state.file......')
    formData.append('myImage', this.state.file);
    axios.post("/upload", formData, headers)
      .then((response) => {
        alert("The file is successfully uploaded");
      }).catch((error) => {
      });
  }

  render() {
    return (
      <form>
        <Avatar style={{ height: 120, width: 120 }}>
          <img src={this.state.file}></img>
        </Avatar>
        <div style={{ width: '100%', paddingRight: "61%", paddingTop: 10 }}>
          <input type="file" name="myImage" onChange={this.onChange} />
          <button type="submit">Upload</button>
        </div>
      </form>
    )
  }
}

export default ReactUploadImage