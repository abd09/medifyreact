import React from 'react';
import PropTypes from 'prop-types';
import CssBaseline from '@material-ui/core/CssBaseline';
import Divider from '@material-ui/core/Divider';
import Drawer from '@material-ui/core/Drawer';
import Hidden from '@material-ui/core/Hidden';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Typography from '@material-ui/core/Typography';
import { withStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import ImageIcon from '@material-ui/icons/Image';
import appConfig from '../../constant/config'
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom'
import { getuserProfileData, getPharmacyOrders, getPatientDoctorsData, getOrders, connectedDoctorsData, getDoctorPrescriptionData, connectedPharmaciesData, getDoctorPatientsData } from '../../actions'
const drawerWidth = 240;

const styles = theme => ({
    root: {
        display: 'flex',
    },
    drawer: {
        [theme.breakpoints.up('sm')]: {
            width: drawerWidth,
            flexShrink: 0,
        },
    },
    appBar: {
        marginLeft: drawerWidth,
        [theme.breakpoints.up('sm')]: {
            width: `calc(100% - ${drawerWidth}px)`,
        },
    },
    menuButton: {
        marginRight: 20,
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
    toolbar: theme.mixins.toolbar,
    drawerPaper: {
        width: drawerWidth,
    },
    content: {
        flexGrow: 1,
        overflowX: 'auto'
    },
    avatar: {
        width: 90,
        height: 90,
        cursor: 'pointer'
    },
    divide2: {
        marginLeft: theme.spacing.unit * 2,
        marginTop: 2,
    },
    profile: {
        textAlign: 'center',
        marginTop: theme.spacing.unit * 1,
        lineHeight: 1
    },
    ListItem: {
        color: '#43415e'
    }
});

class ResponsiveDrawer extends React.Component {
    state = {
        mobileOpen: false,
        activeTab: null
    };

    handleDrawerToggle = () => {
        if (this.props.mobileOpen) {
            this.props.handleDrawerToggle()
        }
    };

    handleRedirect = (e, url) => {
        e.preventDefault();
        console.log('urll', url);
        this.props.history.push(url)
    }

    handleHistory = (e, sidebarVal) => {
        e.preventDefault();
        console.log('sidebarVal', sidebarVal, this.props)
        let path = localStorage.getItem('role')
        this.setState({ activeTab: sidebarVal })
        if (this.props.mobileOpen) {
            this.props.handleDrawerToggle()
        }
        switch (sidebarVal) {
            case 'Medical History':
                this.props.history.push({
                    pathname: `/${path}/prescriptions`
                })
                break;
            case 'Patients':
                this.props.history.push({
                    pathname: `/${path}/patients`
                })
                break;
            case 'Pharmacies':
                this.props.history.push({
                    pathname: `/${path}/pharmacies`
                })
                break;
            case 'Doctors':
                this.props.history.push({
                    pathname: `/${path}/doctors`
                })
                break;
            case 'Orders':
                this.props.history.push({
                    pathname: `/${path}/orders`
                })
                break;
            case 'Inventory':
                this.props.history.push({
                    pathname: `/${path}/inventory`
                })
                break;
            case 'Consultations':
                this.props.history.push({
                    pathname: `/${path}/prescriptions`
                })
                break;
            case 'Vitals':
                this.props.history.push({
                    pathname: `/${path}/vitals`
                })
                break;
            case 'Reviews':
                this.props.history.push({
                    pathname: `/${path}/reviews`
                })
                break;
            case 'Messages':
                this.props.history.push({
                    pathname: `/${path}/messages`
                })
                break;
            case 'Schemes':
                this.props.history.push({
                    pathname: `/${path}/schemes`
                })
                break;
            case 'Profile':
                let componentName = '';
                if (path === 'user') {
                    componentName = 'editPatientDetails'
                } else if (path === 'doctor') {
                    componentName = 'editDoctorDetails'
                } else if (path === 'pharmacy') {
                    componentName = 'editPharmacyDetails'
                }
                this.props.history.push({
                    pathname: `/${path}/${componentName}`
                })
                break;
        }
    }

    componentWillMount() {
        this.props.getuserProfileData({ history: this.props.history });
        if (localStorage.getItem('role') === 'doctor') {
            this.props.getDoctorPatientsData({ history: this.props.history })
            this.props.getDoctorPrescriptionData({ history: this.props.history })
            this.props.connectedPharmaciesData({ history: this.props.history })
        } else if (localStorage.getItem('role') === 'pharmacy') {
            this.props.connectedDoctorsData({ history: this.props.history })
            this.props.getPharmacyOrders({ history: this.props.history });
        } else {
            this.props.getOrders({ history: this.props.history });
            this.props.getPatientDoctorsData({ history: this.props.history })
        }
    }


    handleLogout = () => {
        localStorage.removeItem('loggedIn');
        localStorage.removeItem('token');
        localStorage.removeItem('ref');
        localStorage.removeItem('email');
        localStorage.removeItem('role');
        localStorage.removeItem('rememberMe');
        this.props.history.push('/categorySignin');
    }


    render() {
        const { classes, theme, children, mobileOpen, doctorPatientData, userProfileData, profileImage, patientDoctorData, orderData, connectedDoctors, doctorPrescriptionData, connectedPharmacies } = this.props;
        let listMenu = [];
        console.log("listMenu...................", userProfileData, profileImage, mobileOpen)
        console.log('connectedPharmacies', connectedPharmacies);
        let isLogged = localStorage.getItem("loggedIn");
        let path = ''
        let link = ''
        if (localStorage.getItem('role') === "doctor") {
            path = '/doctor';
            link = `profile/${userProfileData._id}`
            listMenu = ['Consultations', 'Patients', 'Pharmacies', 'Reviews', 'Messages', 'Profile'];
        } else if (localStorage.getItem('role') === "pharmacy") {
            link = `profile/${userProfileData._id}`
            listMenu = ['Orders', 'Doctors', 'Inventory', 'Reviews', 'Messages', 'Profile', 'Schemes'];
            path = '/pharmacy';
        } else {
            link = `profile/${userProfileData._id}`
            listMenu = ['Medical History', 'Doctors', 'Pharmacies', 'Orders', 'Vitals', 'Messages', 'Profile'];
            path = '/user';
        }

        const drawer = (
            <div className="sidebar" >
                <Divider />

                <div style={{ backgroundColor: '#43425D', height: '270px', textAlign: '-webkit-center', paddingTop: '20px' }}>
                    <div onClick={(e) => this.handleRedirect(e, `${path}/${link}`)} >
                        {
                            this.props.profileImage &&
                            <Avatar alt="Profile Pic" src={this.props.profileImage ? this.props.profileImage :`${appConfig.baseUrl}users/noImage.png` /* appConfig.baseUrl + 'users/' + 'noImage.png' */} className={classes.avatar}>
                            </Avatar>
                        }
                        {(this.props.profileImage === undefined || this.props.profileImage === null) &&
                            <Avatar alt="Profile Pic" src={require('../../assets/img/noImage.png')} className={classes.avatar}>
                                <ImageIcon />
                            </Avatar>
                        }
                    </div>
                    <div className={classes.profile}>
                        <Link to={`${path}/${link}`} style={{ fontSize: '16px', textTransform: 'capitalize', textDecoration: 'none', fontWeight: 'bold', color: '#fff' }} onClick={this.handleDrawerToggle}>{userProfileData ? userProfileData.firstname ? `${userProfileData.role === 'doctor' ? 'Dr. ' : ''}${userProfileData.firstname} ${userProfileData.lastname}` : "No name" : "No name"}
                        </Link>
                        <Typography style={{ fontSize: '13px', textTransform: 'capitalize', textDecoration: 'none', color: '#fff', paddingTop: '5px' }}>{userProfileData ? userProfileData.speciality && userProfileData.speciality !== undefined ? userProfileData.speciality.title : `${userProfileData.age ? `${userProfileData.age} years,` : ''} ${userProfileData.gender}` : ''}</Typography>
                        <Typography style={{ fontSize: '13px', textTransform: 'capitalize', textDecoration: 'none', color: '#fff' }}>{userProfileData ? userProfileData.address ? userProfileData.address : 'No location' : ''}</Typography>
                    </div>
                    <div className={classes.profile} style={{ marginTop: '10px' }}>
                        <div className={`col-md-12`}>
                            <div style={{ display: 'inline-block', textAlign: 'center', width: '50%' }}>
                                <Typography style={{ fontSize: '18px', textTransform: 'capitalize', textDecoration: 'none', color: '#fff' }}>{localStorage.getItem('role') === 'doctor' ? doctorPrescriptionData.length : localStorage.getItem('role') === 'pharmacy' ? connectedDoctors.length : patientDoctorData.length}</Typography>
                                <Typography style={{ fontSize: '12px', fontWeight: '600', textTransform: 'capitalize', textDecoration: 'none', color: '#fff' }}>{localStorage.getItem('role') === 'doctor' ? 'Consultations' : localStorage.getItem('role') === 'pharmacy' ? 'Doctors' : 'Doctors'}</Typography>
                            </div>
                            <div style={{ display: 'inline-block', textAlign: 'center', width: '50%' }}>
                                <Typography style={{ fontSize: '18px', textTransform: 'capitalize', textDecoration: 'none', color: '#fff' }}>{localStorage.getItem('role') === 'doctor' ? doctorPatientData.length : localStorage.getItem('role') === 'pharmacy' ? this.props.pharmacyOrderData.length : orderData.length}</Typography>
                                <Typography style={{ fontSize: '12px', fontWeight: '600', textTransform: 'capitalize', textDecoration: 'none', color: '#fff' }}>{localStorage.getItem('role') === 'doctor' ? 'Patients' : localStorage.getItem('role') === 'pharmacy' ? 'Orders' : 'Orders'}</Typography>
                            </div>
                        </div>
                    </div>
                </div>
                <div className={classes.divide2}>
                    <List>
                        {listMenu.map((text, index) => (
                            <ListItem className={`ListItem ${this.state.activeTab !== null && text === this.state.activeTab ? 'ListItemActive' : ''}`} button key={text} onClick={(e) => this.handleHistory(e, text)}>
                                {/*  <ListItemText primary={this.state.activeTab === text ? 'yess' : 'noo'} /> */}
                                <ListItemText primary={text} />
                            </ListItem>
                        ))}
                        <ListItem  button onClick={(e) => this.handleLogout()}>
                            <ListItemText>
                                {(
                                    isLogged ? <span style={{ color: 'red' }}> <b>Logout</b>  </span> : ''
                                )}
                            </ListItemText>
                        </ListItem>
                    </List>
                </div>
            </div>
        );
        return (
            <div className={classes.root}>
                <CssBaseline />
                <nav className={classes.drawer}>
                    {/* The implementation can be swapped with js to avoid SEO duplication of links. */}
                    <Hidden smDown implementation="css">
                        <Drawer
                            container={this.props.container}
                            variant="temporary"
                            anchor={theme.direction === 'rtl' ? 'right' : 'left'}
                            open={mobileOpen}
                            onClose={this.handleDrawerToggle}
                            classes={{
                                paper: classes.drawerPaper,
                            }}>
                            {drawer}
                        </Drawer>
                    </Hidden>
                    <Hidden xsDown implementation="css">
                        <Drawer
                            classes={{
                                paper: classes.drawerPaper,
                            }}
                            variant="permanent"
                            open >
                            {drawer}
                        </Drawer>
                    </Hidden>
                </nav>
                <div className={classes.content}>
                    <div className={classes.toolbar} />
                    {children}
                </div>
            </div>
        );
    }
}

ResponsiveDrawer.propTypes = {
    classes: PropTypes.object.isRequired,
    container: PropTypes.object,
    theme: PropTypes.object.isRequired,
};

const mapStateToProps = ({ authUser, orderReducer, doctorReducer, prescriptionReducer, pharmacyReducer }) => {
    const { userProfileData, profileImage } = authUser
    const { pharmacyOrderData, orderData } = orderReducer
    const { patientDoctorData, connectedDoctors, doctorPatientData } = doctorReducer
    const { doctorPrescriptionData } = prescriptionReducer
    const { connectedPharmacies } = pharmacyReducer
    return { userProfileData, profileImage, pharmacyOrderData, patientDoctorData, orderData, connectedDoctors, doctorPrescriptionData, connectedPharmacies, doctorPatientData }
}

export default connect(mapStateToProps, { getuserProfileData, getPharmacyOrders, getOrders, getPatientDoctorsData, connectedDoctorsData, getDoctorPrescriptionData, connectedPharmaciesData, getDoctorPatientsData })(withRouter(withStyles(styles, { withTheme: true })(ResponsiveDrawer)))



