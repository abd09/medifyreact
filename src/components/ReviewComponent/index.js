import React, { Component } from 'react';
import useMediaQuery from '@material-ui/core/useMediaQuery';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import FormControl from '@material-ui/core/FormControl'
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import RatingComponent from '../RatingComponent';
import { addReview, getReviewDetail } from '../../actions';
import { connect } from 'react-redux';
import appConfig from '../../constant/config';

const styles = theme => ({
  form: {
    display: 'flex',
    flexDirection: 'column',
    margin: 'auto',
  },
  signup: {
    backgroundColor: appConfig.colors.primary,
    borderRadius: '7px',
    color: appConfig.colors.white,
    padding: '5px 10px',
    textTransform: 'none',
    /*  float: 'right', */
    display: 'flex',
    marginRight: '5px',
    "&:disabled": {
      color: 'white !important',
    },
    ['@media (max-width:320px)']: {
      marginTop: '5px'
    },
  }

});

class ReviewComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      open: false,
      setOpen: false,
      review: '',
      rating: 0
    }
    this.ratingRef = React.createRef();
  }

  handleClickOpen = (e) => {
    e.preventDefault();
    this.setState({ open: true });
  }

  handleClose = (e) => {
    e.preventDefault();
    console.log('children rating', this.ratingRef.current)
    this.setState({ open: false });
  }

  handleChange = (event, key) => {
    event.preventDefault();
    this.setState({ [key]: event.target.value });
  }

  handleSubmit = (e) => {
    e.preventDefault();
    let rating = this.ratingRef.current.state.rating;
    let tempState = { ...this.state }
    let data = {
      rating: rating,
      review: tempState.review,
      to: this.props.to,
      from: this.props.from
    }
    this.props.addReview({ data: data, history: this.props.history });
    this.setState({ open: false });
  }

  render() {
    const { open, setOpen, review } = this.state;
    const { classes, isReview } = this.props;
    console.log('isReview', isReview)
    return (
      <React.Fragment>
        <Button className={`${classes.signup} mt-1` } variant="contained" color="white" onClick={(e) => this.handleClickOpen(e)}>
          Rate
        </Button>
        <Dialog
          fullWidth={true}
          maxWidth={'sm'}
          open={open}
          onClose={(e) => this.handleClose(e)}
          aria-labelledby="responsive-dialog-title"
        >
          <DialogTitle id="responsive-dialog-title">{"Add Review"}</DialogTitle>
          <DialogContent>
            <form className={classes.form} noValidate>
              <RatingComponent ref={this.ratingRef} />
              <FormControl>
                <InputLabel htmlFor="review">Review</InputLabel>
                <Input type="textarea" id="review" value={review} aria-describedby="review" onChange={(e) => { this.handleChange(e, 'review') }} />
              </FormControl>
            </form>
          </DialogContent>
          <DialogActions>
            <Button className="successButton" onClick={(e) => this.handleSubmit(e)}>
              Submit
            </Button>
            <Button className="cancelButton" onClick={(e) => this.handleClose(e)}>
              Cancel
            </Button>
          </DialogActions>
        </Dialog>
      </React.Fragment>
    );
  }
}


export default connect(null, { addReview, getReviewDetail })(withStyles(styles)(ReviewComponent));