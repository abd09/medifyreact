import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';
import { getuserProfileData, getPatientPrescriptionData } from '../../actions';
import { connect } from 'react-redux';
import axios from '../../constant/axios';
import appConfig from '../../constant/config';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import moment from 'moment';
import ListItemText from '@material-ui/core/ListItemText';

const drawerWidth = 240;
const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    appBar: {
        marginLeft: drawerWidth,
        [theme.breakpoints.up('sm')]: {
            width: `calc(100% - ${drawerWidth}px)`,
        },
    },
    menuButton: {
        marginRight: 20,
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
    userName: {
        color: appConfig.colors.primary,
        marginBottom: 14,
        fontWeight: 'bolder',
        fontSize: '19px'
    },
    userDetails: {
        marginTop: '5px',
        color: appConfig.colors.primary,
        fontSize: '15px',
        marginBottom: 0,
        fontWeight: '400'
    },
    dialogDone: {
        backgroundColor: '#454545',
        color: "#3ddb20",
        '&:hover': {
            background: 'none',
            backgroundColor: '#454545',
            color: '#3ddb20'
        },
        borderColor: '#454545'
    },
    dialogTitle: {
        backgroundColor: '#5a5a5a',
        '& h2': {
            color: 'white',
            textAlign: 'center'
        },
        borderBottom: '1px solid #5a5a5a'
    },
    dialogContent: {
        backgroundColor: '#5a5a5a',
    },
    root2: {
        flexGrow: 1,
        backgroundColor: '#fff'
    },
    signup: {
        backgroundColor: appConfig.colors.primary,
        borderRadius: '5px',
        color: appConfig.colors.white,
        padding: '5px 10px',
        textTransform: 'none',
        textAlign: 'right',
        float: 'right'
    },
    tabsRoot: {
        color: appConfig.colors.primary,
        borderBottom: '1px solid #7a7a7a',
    },
    tabsIndicator: {
        backgroundColor: '#7a7a7a',
        height: '3px'
    },
    rightDiv: {
        color: "#CCCACD",
        marginBottom: 0,
        fontWeight: 'bold',
        fontSize: '16px',
        /*  textAlign: 'right',
         float: 'right' */
    },
    listitems: {
        paddingLeft: 40,
        paddingRight: 10,
        paddingTop: 5,
        paddingBottom: 5
    },
    listitems1: {
        paddingLeft: 40,
        paddingRight: 10,
        paddingTop: 14,
        paddingBottom: 5
    },
    tabRoot: {
        textTransform: 'initial',
        minWidth: '50%',
        fontWeight: theme.typography.fontWeightRegular,
        fontFamily: [
            '-apple-system',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ].join(','),
        fontSize: '15px',
    },
    typography: {
        padding: '15px 0px',
        color: '#CCCACD',
        ['@media (max-width:787px)']: {
            padding: '15px 15px',
        },
    },
    boldSize: {
        fontSize: '16px'
    },
    imageDiv: {
        textAlign: 'right',
        height: '70px',
        width: '70px',

        /*   position: 'fixed', */
        height: '70px',
        width: '70px',
    },
    avatar: {
        margin: theme.spacing.unit * 2,
        marginLeft: theme.spacing.unit * 4,
        width: 70,
        height: 70
    },
    profile: {
        marginTop: theme.spacing.unit * 4,
        lineHeight: 1
    },
    tableDiv: {
        padding: '0 50px',
    },
    tabsFlex: {
        borderBottom: '0px solid #7a7a7a',
    },
    tableThTd: {
        color: appConfig.colors.white,
    },
    infoDiv: {
        padding: '0',
        backgroundColor: "#fff"
    },
    disableClick: {
        pointerEvents: 'none'
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: appConfig.colors.white,
        '&:hover': {
            backgroundColor: appConfig.colors.white,
        },
        marginRight: theme.spacing(2),
        marginLeft: 0,
        width: '100%',
        borderRadius: '40px',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(3),
            width: 'auto',
        },
    },
    searchIcon: {
        width: theme.spacing(7),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        fontWeight: 'bold',
        opacity: 1
    },
    inputRoot: {
        color: '#c6c6c6',
    },
    patientName: {
        color: appConfig.colors.white,
        font: 'bolder'
    },
    inputInput: {
        color: '#c6c6c6',
        padding: theme.spacing(1, 1, 1, 7),
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: 200,
        },
    },
    Sidebardivide: {
        marginLeft: '12px',
        marginRight: '12px',
        borderBottom: '1px solid #eeeeee'
    },
    icon: {
        position: 'fixed',
        right: '10px',
        color: '#fff'
    },
    iconHover: {
        margin: theme.spacing(2),
        '&:hover': {
            color: red[800],
        },
    },
    avatar: {
        margin: 0,
        padding: 0
    },
    avatarlist: {
        margin: 0,
        padding: 0,
        width: 35,
        height: 35,
    },
    profile: {
        margin: 0,
        height: 100,
        lineHeight: 'normal'
    },
    backBtn: {
        backgroundColor: appConfig.colors.primary,
        borderRadius: '0px',
        color: '#fff',
        marginTop: '5px',
        padding: '5px 22px',
        textTransform: 'none',

    }
});

class MedicalHistory extends Component {
    constructor(props) {
        super(props)
        console.log('hey  i m here', this.props.location)
        this.state = {
            value: 0,
            openModal: false,
            isLoading: true,
            mobileOpen: false,
            anchorEl: null,
            fullWidth: true,
            maxWidth: 'md',
            status: false,
            patient: {},
            url: '',
            croppedImage: '',
            profileimage: '',
            profileimageMain: '',
        };
    }

    componentWillReceiveProps = (nextProps) => {
        console.log("this.props.userProfileData", nextProps.profileImage)
        if (this.props.userProfileData !== nextProps.userProfileData) {
            this.setState({ croppedImage: nextProps.profileImage })
        }
        if (this.props.profileImage !== nextProps.profileImage) {
            this.setState({ croppedImage: nextProps.profileImage })
        }
    }

    componentWillMount() {
        this.props.getuserProfileData({ history: this.props.history });
        axios.get(`/users/patientDetails/${this.props.id}`, {
            headers: {
                'Content-Type': 'application/json',
                'token': localStorage.getItem('token')
            }
        }).then(result => {
            console.log("patientDetails result###", result.data.detail);
            this.setState({
                patient: result.data.detail && result.data.detail ? result.data.detail : {},
                url: result.data.url
            })
        })
            .catch(error => {
                console.log("error....", error);
            });
        console.log('this.props.id', this.props.id);
  /*       this.props.getPatientPrescriptionData({ data: { patientId: this.props.id }, history: this.props.history }); */
    }

    openUrl = (e, url) => {
        e.preventDefault();
        window.open(url);
    }

    goBack = (e) => {
        console.log('e', e);
        e.preventDefault();
        console.log('this.props.history', this.props.history);
        this.props.history.goBack();
    }

    handleChange = (event, value) => {
        console.log('value', value)
        this.setState({ value });
    };

    handleDrawerToggle = () => {
        this.setState(state => ({ mobileOpen: !state.mobileOpen }));
    };

    handleModalClose = () => {
        this.setState({ openModal: false, errors: {} });
    }

    handleModal = (e) => {
        e.preventDefault();
        this.setState({ openModal: true })
    }

    handleOnClick = (e, id) => {
        e.preventDefault();
        console.log('id***', id);
        this.props.history.push(`/${localStorage.getItem('role')}/prescriptionDetails/${id}`)
    }

    render() {
        const { classes } = this.props;
        const { patient, croppedImage } = this.state;
        console.log('prescriptionData', this.props.prescriptionData)
        return (
            <div className={classes.tableDiv} style={{ padding: '0', marginTop: '-13px' }}>
                <div key={'List'}>
                    <List className={classes.root2} >
                        {
                            this.props.prescriptionData && this.props.prescriptionData.length > 0 ?
                                this.props.prescriptionData.map((data, index) => {
                                    console.log('dataaaa', data);
                                    return <div key={index}>
                                        <ListItem className={classes.listitems} onClick={(e) => this.handleOnClick(e, data._id)}>
                                            <ListItemText primary={<React.Fragment>
                                                <Typography variant="h5" style={{ color: '#42425E', fontWeight: 'bold', fontSize: '16px', padding: '5px 0 5px 0' }} className={classes.patientName}>{data && data.title ? data.title : '--'}</Typography>
                                            </React.Fragment>} secondary={<Typography style={{ color: '#42425E', fontWeight: 'bold', fontSize: '11px' }}>Date: {data && data.createdAt ? moment(data.createdAt).format("D MMMM YYYY") : ''}</Typography>} />
                                        </ListItem>
                                        <li className={classes.Sidebardivide}></li>
                                    </div>
                                }) : ''
                        }
                    </List>
                </div>
            </div>
        );
    }
}

MedicalHistory.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = ({ authUser, prescriptionReducer }) => {
    const { userProfileData, profileImage } = authUser
/*     const { patientPrescriptionData } = prescriptionReducer */
    return { userProfileData, /* patientPrescriptionData,  */profileImage }
}

export default connect(mapStateToProps, { getuserProfileData/* , getPatientPrescriptionData  */})(withStyles(styles)(MedicalHistory));