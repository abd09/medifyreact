import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';
import { red } from '@material-ui/core/colors';
import { getuserProfileData, cropImage, connectDoctor, getReviewDetail, getDoctorDetail } from '../../actions';
import { connect } from 'react-redux';
import appConfig from '../../constant/config'
import axios from '../../constant/axios';
import Button from '@material-ui/core/Button';
import ReviewComponent from '../../components/ReviewComponent';
import ShowCertificate from '../../components/ShowCertificate';
const drawerWidth = 240;
const styles = theme => ({
    root: {
        flexGrow: 1,
    },
    backBtn: {
        backgroundColor: appConfig.colors.primary,
        borderRadius: '0px',
        color: '#fff',
        marginTop: '5px',
        padding: '5px 22px',
        textTransform: 'none',
        marginRight: '8px',
        ['@media (max-width:707px)']: {
            marginRight: 'unset',
        },
        ['@media (max-width:599px)']: {
            marginRight: '8px',
        },
        ['@media (max-width:436px)']: {
            marginRight: 'unset',
        },
    },
    appBar: {
        marginLeft: drawerWidth,
        [theme.breakpoints.up('sm')]: {
            width: `calc(100% - ${drawerWidth}px)`,
        },
    },
    menuButton: {
        marginRight: 20,
        [theme.breakpoints.up('sm')]: {
            display: 'none',
        },
    },
    connectDiv: {
        backgroundColor: appConfig.colors.primary,
        borderRadius: '7px',
        color: appConfig.colors.white,
        padding: '6px 10px',
        textTransform: 'none',
        textAlign: 'left',
        /* float: 'left' */
        display: 'flex'
    },
    dialogDone: {
        backgroundColor: '#454545',
        color: "#3ddb20",
        '&:hover': {
            background: 'none',
            backgroundColor: '#454545',
            color: '#3ddb20'
        },
        borderColor: '#454545'
    },
    dialogTitle: {
        backgroundColor: '#5a5a5a',
        '& h2': {
            color: 'white',
            textAlign: 'center'
        },
        borderBottom: '1px solid #5a5a5a'
    },
    dialogContent: {
        backgroundColor: '#5a5a5a',
    },
    root2: {
        flexGrow: 1,
        backgroundColor: '#fff'
    },
    tabsRoot: {
        color: appConfig.colors.primary,
        borderBottom: '1px solid #7a7a7a',
    },
    tabsIndicator: {
        backgroundColor: '#7a7a7a',
        height: '3px'
    },
    rightDiv: {
        color: "#CCCACD",
        marginBottom: 0,
        fontWeight: 'bold',
        fontSize: '16px',
    },
    userName: {
        color: appConfig.colors.primary,
       /*  marginBottom: 14, */
        fontWeight: 'bolder',
        fontSize: '19px'
    },
    userDetails: {
        marginTop: '5px',
        color: appConfig.colors.primary,
        fontSize: '15px',
        marginBottom: 0,
        fontWeight: '400'
    },
    patientName: {
        color: appConfig.colors.secondary,
        font: 'bolder'
    },
    boldSize: {
        fontSize: '16px'
    },
    availabilityBox: {
        backgroundColor: appConfig.colors.primary,
        padding: '10px',
        borderRadius: '7px',
        margin: '12px 0'
    },
    listitems: {
        paddingLeft: 40,
        paddingRight: 10,
        paddingTop: 5,
        paddingBottom: 5
    },
    listitems1: {
        paddingLeft: 40,
        paddingRight: 10,
        paddingTop: 14,
        paddingBottom: 5
    },
    tabRoot: {
        textTransform: 'initial',
        minWidth: '50%',
        fontWeight: theme.typography.fontWeightRegular,
        fontFamily: [
            '-apple-system',
            'BlinkMacSystemFont',
            '"Segoe UI"',
            'Roboto',
            '"Helvetica Neue"',
            'Arial',
            'sans-serif',
            '"Apple Color Emoji"',
            '"Segoe UI Emoji"',
            '"Segoe UI Symbol"',
        ].join(','),
        fontSize: '15px',
    },
    typography: {
        padding: '15px 0px',
        color: '#CCCACD',
        ['@media (max-width:787px)']: {
            padding: '15px 15px',
        },
    },
    imageDiv: {
        textAlign: 'left',
        height: '70px',
        width: '70px',
    },
    avatar: {
        margin: theme.spacing.unit * 2,
        marginLeft: theme.spacing.unit * 4,
        width: 70,
        height: 70
    },
    tabsFlex: {
        borderBottom: '0px solid #7a7a7a',
    },
    tableThTd: {
        color: '#ffffff',
    },
    infoDiv: {
        /*   padding: 18,
          backgroundColor: "#fff" */
        padding: '0 0 18px 0',
        backgroundColor: "#fff"
    },
    disableClick: {
        pointerEvents: 'none'
    },
    search: {
        position: 'relative',
        borderRadius: theme.shape.borderRadius,
        backgroundColor: '#ffffff',
        '&:hover': {
            backgroundColor: '#ffffff',
        },
        marginRight: theme.spacing(2),
        marginLeft: 0,
        width: '100%',
        borderRadius: '40px',
        [theme.breakpoints.up('sm')]: {
            marginLeft: theme.spacing(3),
            width: 'auto',
        },
    },
    searchIcon: {
        width: theme.spacing(7),
        height: '100%',
        position: 'absolute',
        pointerEvents: 'none',
        display: 'flex',
        alignItems: 'center',
        justifyContent: 'center',
        fontWeight: 'bold',
        opacity: 1
    },
    inputRoot: {
        color: '#c6c6c6',
    },
    inputInput: {
        color: '#c6c6c6',
        padding: theme.spacing(1, 1, 1, 7),
        transition: theme.transitions.create('width'),
        width: '100%',
        [theme.breakpoints.up('md')]: {
            width: 200,
        },
    },
    icon: {
        position: 'fixed',
        right: '15px',
        color: '#fff'
    },
    iconHover: {
        margin: theme.spacing(2),
        '&:hover': {
            color: red[800],
        },
    },
    avatar: {
        margin: 0,
        padding: 0,
    },
    avatarlist: {
        margin: 0,
        padding: 0,
        width: 35,
        height: 35,
    },
    profile: {
        margin: '10px 0 0 0',
        lineHeight: 'normal'
    },
    container: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    textField: {
        marginLeft: theme.spacing(1),
        marginRight: theme.spacing(1),
        borderRadius: '8px',
        color: appConfig.colors.primary
    },
    dense: {
        marginTop: theme.spacing(2),
    },
    menu: {
        width: 200,
    },
    submit: {
        backgroundColor: appConfig.colors.primary,
        color: '#fff',
        marginTop: theme.spacing.unit * 2,
        padding: '8px 30px',
        textTransform: 'none',
        borderRadius: '0'
    },
    form: {
        width: '100%',
        paddingLeft: '20px',
        paddingRight: '35px',
        marginTop: theme.spacing.unit,
    },
});

class Personal extends Component {
    constructor(props) {
        super(props)
        console.log('hey  i m here', this.props.location)
        this.state = {
            uploaded_image_path: '',
            value: 0,
            tool: false,
            croppedImage: '',
            profileimage: '',
            imagePreview: '',
            profileimageMain: '',
            image: '',
            imageLoaded: false,
            alert: false,
            openModal: false,
            isLoading: true,
            mobileOpen: false,
            doctorDetails: '',
            connect: false,
            doctor: '',
            review: false,
            rates: '',
            speciality: '',
            experience: '',
            serviceProvided: '',
            waitingTime: '',
            value: 0
        };
    }

    componentWillReceiveProps = (nextProps) => {
        console.log("this.props.userProfileData", nextProps.profileImage)
        if (this.props.userProfileData !== nextProps.userProfileData) {
            this.setState({ croppedImage: nextProps.profileImage })
        }
        if (this.props.profileImage !== nextProps.profileImage) {
            this.setState({ croppedImage: nextProps.profileImage })
        }
    }

    componentWillMount() {
        this.props.getuserProfileData({ history: this.props.history });
        this.props.getReviewDetail({ data: this.props.id, history: this.props.history });

        this.props.getDoctorDetail({ data: { id: this.props.id }, history: this.props.history })

        axios.get(`/users/getConnectDetails/${this.props.id}`, {
            headers: {
                'Content-Type': 'application/json',
                'token': localStorage.getItem('token')
            }
        }).then(result => {
            console.log("ConnectDetails result^^^", result.data.detail);
            this.setState({ connect: result.data.detail && result.data.detail != null ? true : false })
        })
            .catch(error => {
                console.log("error....", error);
            });
    }

    openUrl = (e, url) => {
        e.preventDefault();
        window.open(url);
    }


    saveProfile = (e) => {
        e.preventDefault();
        let data = {
            profileimageMain: this.state.profileimageMain,
        }
        console.log("this.state.profileimageMain", this.state.profileimageMain)
        this.props.cropImage(data)
    }

    handleChange = (event, value) => {
        console.log('value', value)
        this.setState({ value });
    };

    handleDrawerToggle = () => {
        this.setState(state => ({ mobileOpen: !state.mobileOpen }));
    };

    handleModalClose = () => {
        this.setState({ openModal: false, errors: {} });
    }

    handleConnect = (e) => {
        e.preventDefault();
        let data = {
            user: this.props.userProfileData._id,
            doctor: this.props.id,
            connect: true
        }
        this.setState({ connect: true })
        this.props.connectDoctor({
            data,
            history: this.props.history
        })
    }

    handleDisconnect = (e) => {
        e.preventDefault();
        let data = {
            user: this.props.userProfileData._id,
            doctor: this.props.id,
            connect: false
        }
        this.setState({ connect: false })
        this.props.connectDoctor({
            data,
            history: this.props.history
        })
    }

    onConfirm(key, image) {
        let node = this[image]
        console.log('nodes=', node.crop())
        let img = node.crop()
        this.setState({ [key]: false, croppedImage: img, profileimageMain: img })
        console.log('image=', this.state)
    }

    onCancel(key) {
        console.log("key", key)
        this.setState({ [key]: false })
    }

    handleImageLoaded(state) {
        this.setState({
            [state + 'Loaded']: true
        })
    }

    openAlert(e, key) {
        this.setState({ images: e.target.files, image: '' })
        console.log('files=', e.target.files[0], e.target.files)
        var file = e.target.files[0];
        var reader = new FileReader();
        var url = reader.readAsDataURL(file);
        console.log('url=,', url)
        reader.onloadend = function (e) {
            this.setState({
                imagePreview: [reader.result]
            })
        }.bind(this);
        console.log('url=,', url)
        this.setState({ [key]: true });
        console.log('files=', e.target.files[0])

    }

    uploadFile(e) {
        e.preventDefault();
        this.setState({ tool: true })
    }

    openImageUploader() {
        this.setState({ tool: false })
        this.refs.imageUploader.click();
    }

    removeProfilePhoto = () => {
        this.setState({ profileimageMain: null, croppedImage: null, tool: false });
    }

    render() {
        const { classes, userProfileData, children, reviewDetail, doctorDetails } = this.props;

        const { tool, review, value } = this.state;
        console.log('doctorDetails', doctorDetails)
        return (
            <React.Fragment>
                <div>
                    <div className={classes.rightDiv} style={{ textAlign: "center" }}>
                        {
                            this.state.croppedImage &&
                            <Avatar alt='Profile' src={localStorage.getItem('role') == 'doctor' ? this.state.croppedImage ? this.state.croppedImage : require('../../assets/img/noImage.png') : doctorDetails.profileImage && doctorDetails.profileImage != undefined && doctorDetails.profileImage != '' ? appConfig.baseUrl + '/users/' + doctorDetails.profileImage + '.png' : require('../../assets/img/noImage.png')} className={classes.imageDiv}>
                            </Avatar>
                        }
                        {(this.state.croppedImage == undefined || this.state.croppedImage == "") &&
                            <Avatar alt='Profile' src={require('../../assets/img/noImage.png')} className={classes.imageDiv}>
                            </Avatar>
                        }
                    </div>
                    <Typography variant="body1" gutterBottom align="left" className={classes.userName}>
                        Dr. {doctorDetails && doctorDetails.firstname ? doctorDetails.firstname + ' ' + doctorDetails.lastname : '--'}
                    </Typography>
                    <div className={classes.rightDiv} style={{ textAlign: "center" }}>
                        {this.state.connect ? <Button
                            variant="contained"
                            className={classes.connectDiv} onClick={(e) => this.handleDisconnect(e)}
                        > Disconnect</Button> : <Button
                            variant="contained"
                            className={classes.connectDiv} onClick={(e) => this.handleConnect(e)} style={{ padding: '6px 17px' }}
                        > Connect</Button>}
                        {
                            this.state.connect ? (reviewDetail && Object.keys(reviewDetail).length === 0) ? <ReviewComponent className={`${classes.connectDiv} `} isReview={review} from={this.props.userProfileData._id} to={this.props.id} /> : '' : ''
                        }
                    </div>
                    <div className="app-wrapper">
                        <Typography variant="body1" gutterBottom align="left" className={classes.userDetails}><b className={classes.boldSize}>Phone: </b>{doctorDetails && doctorDetails.mobile && doctorDetails.mobile != null ? doctorDetails.mobile : '--'}
                        </Typography>
                        <Typography variant="body1" gutterBottom align="left" className={classes.userDetails}><b className={classes.boldSize}>Email: </b>{doctorDetails && doctorDetails.email && doctorDetails.email != null ? doctorDetails.email : 'N/A'}
                        </Typography>
                        <Typography variant="body1" gutterBottom align="left" className={classes.userDetails}> <b className={classes.boldSize}>Gender: </b> {doctorDetails && doctorDetails != null && doctorDetails.gender && doctorDetails.gender != null ? doctorDetails.gender.charAt(0).toUpperCase() + doctorDetails.gender.slice(1) : '--'}
                        </Typography>
                        <Typography variant="body1" gutterBottom align="left" className={classes.userDetails}><b className={classes.boldSize}>License No.: </b>{doctorDetails && doctorDetails.licenseNo && doctorDetails.licenseNo != null ? doctorDetails.licenseNo : '--'}
                        </Typography>
                        <Typography variant="body1" gutterBottom align="left" className={classes.userDetails}> <b className={classes.boldSize}>Age: </b>{doctorDetails && doctorDetails != null && doctorDetails.age && doctorDetails.age != null ? `${doctorDetails.age} years` : '--'}
                        </Typography>
                    </div>


                </div>

            </React.Fragment>);
    }
}

Personal.propTypes = {
    classes: PropTypes.object.isRequired,
};

const mapStateToProps = ({ authUser, ReviewReducer, doctorReducer }) => {
    const { userProfileData, profileImage } = authUser
    const { reviewDetail } = ReviewReducer;
    const { doctorDetails } = doctorReducer;
    return { userProfileData, profileImage, reviewDetail, doctorDetails }
}

export default connect(mapStateToProps, { getuserProfileData, cropImage, connectDoctor, getReviewDetail, getDoctorDetail })(withStyles(styles)(Personal));