import React, { Component } from 'react';
import StarRatingComponent from 'react-star-rating-component';
import InputLabel from '@material-ui/core/InputLabel';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  formControl: {
    marginTop: theme.spacing(2),
    minWidth: 120,
  },
  formControlLabel: {
    marginTop: theme.spacing(1),
  },
});

class RatingComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {
      rating: 0
    };
  }

  onStarClick(value) {
    this.setState({ rating: value });
  }

  render() {
    const { rating } = this.state;
    return (
      <div style={{fontSize:'20px'}}>
        <InputLabel htmlFor="review">Rating</InputLabel>
        <StarRatingComponent
          name="rating"
          starCount={5}
          value={rating}
          onStarClick={this.onStarClick.bind(this)}
        />
      </div>
    );
  }
}

export default (withStyles(styles)(RatingComponent));