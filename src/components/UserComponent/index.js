import React, { Component } from 'react';
import { Route, withRouter } from 'react-router-dom';
import UserLayout from '../RctUserLayout';
import Prescriptions from '../../routes/prescriptions';
import Doctors from '../../routes/doctors';
import Pharmacies from '../../routes/patientPharmacies';
import Orders from '../../routes/orders';
import OrderDetail from '../../routes/orderDetails';
import AddPrescription from '../../routes/addPrescription';
import AddOrder from '../../routes/addOrder';
import PrescriptionDetail from '../../routes/prescriptionDetails';
import EditPrescription from '../../routes/editPrescription';
import PharmacyDetail from '../../routes/pharmacyDetails';
import DoctorDetail from '../../routes/docDetails';
import PatientDetail from '../../routes/patientUserDetails';
import EditPatientDetail from '../../routes/editPatientDetails';
import ChatListComponent from '../ChatListComponent';
import Vitals from '../../routes/vitals';

class UserComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }
  render() {
    const { match } = this.props;
    console.log('match=', match)
    return (
      <UserLayout>
        <Route path={`${match.url}/`} exact component={Prescriptions} />
        <Route path={`${match.url}/prescriptions`} component={Prescriptions} />
        <Route path={`${match.url}/doctors`} component={Doctors} />
        <Route path={`${match.url}/orders`} component={Orders} />
        <Route path={`${match.url}/doctorDetails/:id`} component={DoctorDetail} />
        <Route path={`${match.url}/profile/:id`} component={PatientDetail} />
        <Route path={`${match.url}/editPatientDetails`} component={EditPatientDetail} />
        <Route path={`${match.url}/orderDetails/:id`} component={OrderDetail} />
        <Route path={`${match.url}/pharmacies`} component={Pharmacies} />
        <Route path={`${match.url}/addPrescription`} component={AddPrescription} />
        <Route path={`${match.url}/addOrder`} component={AddOrder} />
        <Route path={`${match.url}/prescriptionDetails/:id`} component={PrescriptionDetail} />
        <Route path={`${match.url}/pharmacyDetails/:id`} component={PharmacyDetail} />
        <Route path={`${match.url}/editPrescription/:id`} component={EditPrescription} />
        <Route path={`${match.url}/vitals`} component={Vitals} />


        <Route path={`${match.url}/messages`} component={ChatListComponent} />
      </UserLayout>
    );
  }
}

export default withRouter(UserComponent);