import React, { Component } from 'react'
import { Route, withRouter } from 'react-router-dom'
import UserLayout from '../RctUserLayout';
import Doctors from '../../routes/pharmacyDoctors';
import PharmacyOrders from '../../routes/pharmacyOrders';
import PharmacyUserDetails from '../../routes/pharmacyUserDetails';
import EditPharmacyDetails from '../../routes/editPharmacyDetails';
import OrderDetail from '../../routes/orderDetails';
import AddOrder from '../../routes/addOrder';
import DoctorDetail from '../../routes/pharmacyDoctorDetails';
import Inventory from '../../routes/inventory';
import ChatListComponent from '../ChatListComponent';
import ReviewListComponent from '../ReviewListComponent';
import PatientDetail from '../../routes/patientDetails';
import Schemes from '../../routes/schemes';

class UserComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {}
    }
    render() {
        const { match } = this.props;
        console.log('match=', match)
        return (
            <UserLayout>
                <Route path={`${match.url}/`} exact component={PharmacyOrders} />
                <Route path={`${match.url}/inventory`} component={Inventory} />
                <Route path={`${match.url}/doctors`} component={Doctors} />
                <Route path={`${match.url}/orders`} component={PharmacyOrders} />
                <Route path={`${match.url}/profile/:id`} component={PharmacyUserDetails} />
                <Route path={`${match.url}/editPharmacyDetails`} component={EditPharmacyDetails} />
                <Route path={`${match.url}/patientDetails/:id`} component={PatientDetail} />
                <Route path={`${match.url}/orderDetails/:id`} component={OrderDetail} />
                <Route path={`${match.url}/doctorDetails/:id`} component={DoctorDetail} />
                <Route path={`${match.url}/reviews`} component={ReviewListComponent} />
                <Route path={`${match.url}/addOrder`} component={AddOrder} />
                <Route path={`${match.url}/messages`} component={ChatListComponent} />
                <Route path={`${match.url}/schemes`} component={Schemes} />
            </UserLayout>
        );
    }
}

export default withRouter(UserComponent);