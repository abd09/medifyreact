import React, { Component } from 'react'
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import SearchBar from 'material-ui-search-bar'
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import AppBar from '@material-ui/core/AppBar';
import SideBar from '../../components/userSideBar'
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import { getReview } from '../../actions';
import Button from '@material-ui/core/Button';
import { connect } from 'react-redux';
import ShowRatingComponent from '../ShowRatingComponent';
import axios from '../../constant/axios';
import { NotificationManager } from 'react-notifications';
import { Col, Row } from 'reactstrap';


const drawerWidth = 240;
const styles = theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: '#f5f5f5 !important',
  },
  root2: {
    flexGrow: 1,
    backgroundColor: '#fff'
  },
  appBar: {
    marginLeft: drawerWidth,
    [theme.breakpoints.up('sm')]: {
      width: `calc(100% - ${drawerWidth}px)`,
    },
  },
  menuButton: {
    marginRight: 20,
    [theme.breakpoints.up('sm')]: {
      display: 'none',
    },
  },
  tabsRoot: {
    borderBottom: '1px solid #454545',
  },
  tabsIndicator: {
    backgroundColor: '#58de45',
    height: '4px'
  },
  listitems: {
    paddingLeft: 40,
    paddingRight: 20,
  },
  tabRoot: {
    textTransform: 'initial',
    minWidth: '50%',
    fontWeight: theme.typography.fontWeightRegular,
    fontFamily: [
      '-apple-system',
      'BlinkMacSystemFont',
      '"Segoe UI"',
      'Roboto',
      '"Helvetica Neue"',
      'Arial',
      'sans-serif',
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(','),
  },
  tabSelected: {},
  typography: {
    padding: '15px 0px',
    color: '#CCCACD',
    ['@media (max-width:787px)']: {
      padding: '15px 15px',
    },
  },
  avatar: {
    margin: theme.spacing.unit * 2,
    marginLeft: theme.spacing.unit * 4,
    width: 70,
    height: 70
  },
  profile: {
    marginTop: theme.spacing.unit * 4,
    lineHeight: 1
  },
  tableDiv: {
    padding: '0px',
  },
  tabsFlex: {
    borderBottom: '1px solid #454545',
  },
  search: {
    position: 'relative',
    borderRadius: theme.shape.borderRadius,
    backgroundColor: '#ffffff',
    '&:hover': {
      backgroundColor: '#ffffff',
    },
    marginRight: theme.spacing(2),
    marginLeft: 0,
    width: '100%',
    borderRadius: '40px',
    [theme.breakpoints.up('sm')]: {
      marginLeft: theme.spacing(3),
      width: 'auto',
    },
  },
  searchIcon: {
    width: theme.spacing(7),
    height: '100%',
    position: 'absolute',
    pointerEvents: 'none',
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'center',
    fontWeight: 'bold',
    opacity: 1
  },
  inputRoot: {
    color: '#c6c6c6',
  },
  patName: {
    color: '#FFFFFF',
    font: 'bolder'
  },
  inputInput: {
    color: '#c6c6c6',
    padding: theme.spacing(1, 1, 1, 7),
    transition: theme.transitions.create('width'),
    width: '100%',
    [theme.breakpoints.up('md')]: {
      width: 200,
    },
  },
  listdivide: {
    marginLeft: '12px',
    marginRight: '12px',
    borderBottom: '6px solid #eeeeee'
  },
  button: {
    color: '#ffffff',
    backgroundColor: '#FF3739',
    padding: 0,
    fontSize: 10
  }
});

class ReviewListComponent extends Component {
  constructor(props) {
    super(props)
    console.log('hey  i m here', this.props.location)
    this.state = {
      mobileOpen: false,
      value: '',
      isLoading: true,
      patients: [],
      searchText: ''
    };
  }

  componentWillMount() {
    this.props.getReview({ history: this.props.history });
  }

  handleSearchText = (e, key) => {
    console.log('e searchText key', e);
    this.setState({ [key]: e })
    if (e.length > 0) {
      let data = {
        searchText: e
      }
      /* this.props.searchDoctorPatients({
        data: data, history: this.props.history
      }); */
    } else {
      /* this.props.getDoctorPatientsData({
        history: this.props.history
      }); */
    }
  }

  searchResults = (e, searchText) => {
    console.log('resultssss');
    if (searchText.length > 0) {
      let data = {
        searchText: searchText
      }
      /*  this.props.searchDoctorPatients({
         data: data, history: this.props.history
       }); */

    } else {
      /* this.props.getDoctorPatientsData({
        history: this.props.history
      }); */
    }
  }

  handleDrawerToggle = () => {
    this.setState(state => ({ mobileOpen: !state.mobileOpen }));
  };

  handleReport = (e, id) => {
    e.preventDefault();
    axios.get(`/review/report/${id}`, {
      headers: {
        'Content-Type': 'application/json',
        'token': localStorage.getItem('token')
      }
    }).then(result => {
      if (result.data.error === false) {
        NotificationManager.success(result.data.title);
      } else {
        NotificationManager.error(result.data.title)
      }
    }).catch(error => {
      NotificationManager.error('Something went wrong, please try again.')
    });
  }

  render() {
    const { classes, userProfileData, children, reviewList } = this.props;
    const { searchText } = this.state;
    console.log('reviewList', reviewList)

    return (
      <React.Fragment>
        <AppBar position="fixed" className={classes.appBar}>
          <Toolbar>
            <IconButton
              color="inherit"
              aria-label="Open drawer"
              onClick={this.handleDrawerToggle}
              className={classes.menuButton}
            >
              <MenuIcon />
            </IconButton>
            <Typography variant="h6" color="inherit" style={{ fontWeight: 'bold', color: '#fff' }} noWrap>
              {"Reviews"}
            </Typography>
          </Toolbar>
        </AppBar>
        <div className='page-content' style={{ fontWeight: 'bold', backgroundColor: '#fff', marginTop: '-56px' }}>
          <SideBar mobileOpen={this.state.mobileOpen} handleDrawerToggle={this.handleDrawerToggle} userProfileData={userProfileData} >
            {children}
          </SideBar>
        </div>
        <div className={classes.root}>
          {/* <div className={classes.tableDiv} >
            <Typography className={classes.typography}>
              {<div className={classes.search}>
                {<SearchBar value={searchText} onRequestSearch={(e) => this.searchResults(e, searchText)} onChange={(e) => this.handleSearchText(e, 'searchText')} style={{ borderRadius: '40px', height: '40px', fontWeight: 'bold', boxShadow: 'none', marginBottom: '8px' }} />}
              </div>}
            </Typography>
          </div> */}
          <div key={'List'} className={classes.tableDiv} style={{ overflowX: 'hidden' }} >
            <List className={classes.root2} >
              {
                reviewList && reviewList.length > 0 ?
                  reviewList.map((data, index) => {
                    console.log('dataaaa', data);
                    return <div>
                      <ListItem>
                        <div style={{width : '100%'}}>
                          <Row style={{ margin: 0 }}>
                            <Col xs="8" md="8" sm="8">
                             <ListItemText primary={<Typography style={{ color: '#42425E', fontWeight: 'bold', fontSize: '15px', textAlign: 'left' }}>{data.from.firstname} {data.from.lastname}</Typography>} />
                            </Col>
                            <Col xs="4" md="4" sm="4" style={{textAlign: 'right'}}>
                              <ListItemText primary={<React.Fragment>
                                <row>
                                  <Button className={classes.button} disabled={(data && data.report != undefined) ? data.report : false} onClick={(e) => this.handleReport(e, data._id)}>
                                    Report
                              </Button>
                                  <Typography style={{ fontSize: 18 }}>{data && data.rating != '' ? <ShowRatingComponent rating={data.rating} /> : ''}</Typography>
                                </row>
                              </React.Fragment>} />
                              
                            </Col>
                          </Row>
                          <Row style={{ padding: '0 15px 5px 15px' }}>
                            <Col xs="12" md="12" sm="12">
                            <ListItemText primary={<Typography style={{ color: '#42425E', fontWeight: 'bold', fontSize: '12px', textAlign: 'justify' }} className={classes.patName}>{data && data.review && data.review != '' ? data.review : 'N/A'}</Typography>}/>
                            </Col>
                          </Row>
                        </div>
                      </ListItem>
                      <li className={classes.listdivide}></li>
                    </div>
                  }) : <div><Typography style={{
                    backgroundColor: '#fff', color: 'gray', fontWeight: 'bold', fontSize: '25px',
                  }}>No Reviews</Typography></div>
              }
            </List>
          </div>
        </div>
      </React.Fragment >);
  }
}

ReviewListComponent.propTypes = {
  classes: PropTypes.object.isRequired,
};

const mapStateToProps = ({ authUser, ReviewReducer }) => {
  const { userProfileData } = authUser
  const { reviewList } = ReviewReducer
  return { userProfileData, reviewList }
}

export default connect(mapStateToProps, { getReview })(withStyles(styles)(ReviewListComponent));