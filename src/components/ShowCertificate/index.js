import React, { Component } from 'react';
import { Col, Row } from 'reactstrap';
import appConfig from '../../constant/config';
import { withStyles } from '@material-ui/core/styles';
import { Label } from 'reactstrap';

const styles = theme => ({
  image: {
    height: 250,
    width: 250,
    padding: 10
  }
});

class ShowCertificate extends Component {
  constructor(props) {
    super(props);
    this.state = {}
  }

  openImage = (e) => {
    e.preventDefault();
    window.open('');
  }

  render() {
    let { certificate, classes } = this.props;
    return (
      <React.Fragment>
        <Row>
          {
            certificate && certificate.length > 0 ? certificate.map((eachImage, key) => {
              return <Col md={4} sm={12} xs={12}>
                <img className={classes.image} src={appConfig.baseUrl + '/certificates/' + eachImage} onClick={(e) => this.openImage(e)} />
              </Col>
            }) : ''
          }
        </Row>
      </React.Fragment>
    );
  }
}

export default (withStyles(styles)(ShowCertificate));