import React, { Component } from 'react';
import StarRatingComponent from 'react-star-rating-component';
import InputLabel from '@material-ui/core/InputLabel';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  formControl: {
    marginTop: theme.spacing(2),
    minWidth: 120,
  },
  formControlLabel: {
    marginTop: theme.spacing(1),
  },
});

class ShowRatingComponent extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const { rating } = this.props;
    return (
      <React.Fragment >
        <StarRatingComponent
          name="rating"
          starCount={5}
          editing={false}
          value={rating}
        />
      </React.Fragment>
    );
  }
}

export default (withStyles(styles)(ShowRatingComponent));