import React, { Component } from 'react'
import PropTypes from 'prop-types';
import SideBar from '../userSideBar'
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Toolbar from '@material-ui/core/Toolbar';
import AppBar from '@material-ui/core/AppBar';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import { connect } from 'react-redux';
import { withRouter, Link } from 'react-router-dom';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import { Badge } from 'reactstrap';
import { getActiveTab, getuserProfileData } from '../../actions'
import Icon from '@material-ui/core/Icon';
import { red } from '@material-ui/core/colors';

const drawerWidth = 240;
const styles = theme => ({
  root: {
    display: 'flex',
  },
  drawer: {
    [theme.breakpoints.up('sm')]: {
      width: drawerWidth,
      flexShrink: 0,
    },
    borderRight: 'none',
  },
  appBar: {
    marginLeft: drawerWidth,

    [theme.breakpoints.up('sm')]: {
      width: `calc(100% - ${drawerWidth}px)`,
    },
  },
  menuButton: {
    marginRight: 20,
    [theme.breakpoints.up('sm')]: {
      display: 'none',
    },
    borderRight: 'none',
  },
  toolbar: theme.mixins.toolbar,
  drawerPaper: {
    width: drawerWidth,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
  },
  avatar: {
    margin: theme.spacing.unit * 2,
    width: 70,
    height: 70
  },
  dialogDone: {
    backgroundColor: '#454545',
    color: "#3ddb20",
    '&:hover': {
      background: 'none',
      backgroundColor: '#454545',
      color: '#3ddb20'
    },
    borderColor: '#454545'
  },
  dialogTitle: {
    backgroundColor: '#5a5a5a',
    '& h2': {
      color: 'white',
      textAlign: 'center'
    },
    borderBottom: '1px solid #5a5a5a'
  },
  dialogContent: {
    backgroundColor: '#5a5a5a',
  },
  icon: {
    position: 'fixed',
    right: '10px',
    color: '#fff'
  },
  iconHover: {
    margin: theme.spacing(2),
    '&:hover': {
      color: red[800],
    },
  },
});

class UserLayout extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mobileOpen: false,
      fullWidth: true,
      maxWidth: 'md',
      openModal: false,
      anchorEl: null,
      heading: ''
    }
  }

  componentWillMount() {
    this.setState({ heading: this.props.location.pathname.slice(6) })
  }

  componentDidMount() {
    this.props.getuserProfileData({ history: this.props.history });
  }

  handleDrawerToggle = () => {
    this.setState(state => ({ mobileOpen: !state.mobileOpen }));
  };

  handleModalClose = () => {
    this.setState({ openModal: false, errors: {} });
  }

  handleClick = (e) => {
    this.props.history.push(`/user/${this.state.heading}`)
  }

  showInfo = (event) => {
    event.preventDefault();
    this.setState({ anchorEl: event.currentTarget, openModal: true });
  }

  render() {
    const { children, classes, userProfileData } = this.props
    console.log('this.propsasdsadas', this.props);
    let length = this.props.location.pathname.match(/chat/g);
    let heading = '';
    console.log('heading................length', length)
    if (length !== null) {
      heading = 'Chat'
    } else if (this.props.location.pathname.match(/messages/g)) {
      heading = 'Messages'
    } else {
      heading = this.props.location.pathname.slice(6)
    }
    console.log('heading................', heading)

    const { openModal } = this.state;
    return (
      <React.Fragment >
        <AppBar position="fixed" className={classes.appBar}>
          <Toolbar style={{ borderRight: 'none', }}>
            <IconButton
              color="inherit"
              aria-label="Open drawer"
              onClick={this.handleDrawerToggle}
              className={classes.menuButton}
            >
              <MenuIcon />
            </IconButton>
            <Typography variant="h6" color="inherit" style={{ fontWeight: 'bold', color: '#fff' }} noWrap>
              {heading.charAt(0).toUpperCase() + heading.slice(1)}
            </Typography>
            {
              this.props.location.pathname.match(/messages/g) ?
                '' :
                <Icon className={classes.icon} color="disabled" onClick={(e) => { this.handleClick(e) }} fontSize="large">
                  add
            </Icon>
            }

          </Toolbar>
        </AppBar>
        <div className='page-content' >
          <SideBar mobileOpen={this.state.mobileOpen} handleDrawerToggle={this.handleDrawerToggle} userProfileData={userProfileData}>
            {children}
          </SideBar>
        </div>
        <Dialog
          fullWidth={this.state.fullWidth}
          maxWidth={this.state.maxWidth}
          open={openModal}
          onClose={this.handleModalClose}
          aria-labelledby="max-width-dialog-title"
        >
          <DialogTitle id="max-width-dialog-title" className={classes.dialogTitle}>INFO</DialogTitle>
          <DialogContent className={classes.dialogContent}>
            <DialogContentText style={{
              textAlign: 'center',
              color: '#fff'
            }}>
              <div style={{ float: 'left', textAlign: 'center' }}>
                <Badge color="danger" style={{ display: 'inline-block', borderRadius: 10, padding: 8, marginTop: 10 }}>
                </Badge> :
              </div>
            </DialogContentText>
          </DialogContent>
        </Dialog>
      </React.Fragment>
    );
  }
}
UserLayout.propTypes = {
  classes: PropTypes.object.isRequired,
  container: PropTypes.object,
  theme: PropTypes.object.isRequired,
};

const mapStateToProps = ({ authUser }) => {
  const { userProfileData } = authUser
  return { userProfileData }
}

export default connect(mapStateToProps, { getuserProfileData })(withRouter(withStyles(styles, { withTheme: true })(UserLayout)));