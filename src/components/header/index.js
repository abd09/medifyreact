import React, { Component } from 'react'
import { withRouter } from 'react-router-dom';
import { FormGroup, Input, FormText } from 'reactstrap'
import { connect } from 'react-redux'
class Header extends Component {
  constructor(props) {
    super(props)
    this.state = {
      search: props.match.params.key
    }
  }
  handleChanges = (e, key) => {
    this.setState({ [key]: e.target.value })
  }
  render() {
    return (
      <div className='header' style={{ backgroundColor: "grey" }}>
        <div className='row'>

        </div>
      </div>
    );
  }
}
const mapStateToProps = ({ authUser }) => {
  const { loggedIn, userDetails } = authUser

  return { loggedIn, userDetails }
}
export default withRouter(connect(mapStateToProps, null)(Header));